<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Forget Password</title>
</head>
<body>
@include('emails.include.header')

<div style="padding: 30px 30px; border: 1px solid #73b843; font-size: 14px">
    คุณ{{ $firstname.' '.$lastname }} ,<br><br>
    ยินดีต้องรับสู่เว็บไซต์ YAKYIM คุณได้ทำการขอเปลี่ยนรหัสใหม่<br><br>
    ตั้งค่ารหัสผ่านใหม่, <a href="{{ url()->to('reset-password?token='.$gen_token) }}">คลิก</a><br><br>
    เพื่อความปลอดภัยในการใช้ ท่านควรรักษารหัสผ่านของท่านเป็นความลับ
</div>

@include('emails.include.footer')
</body>
</html>