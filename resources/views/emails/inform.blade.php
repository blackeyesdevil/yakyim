<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>แจ้งการชำระเงิน</title>
</head>
<body>
Order Number. {{ $orders_no }}<br>
วันที่แจ้งชำระเงิน : {{ $inform->created_at }}<br>
วันที่โอนเงิน : {{ $inform->payment_date }}<br>
ธนาคาร : {{ $inform->bank }}<br>
จำนวนเงิน : {{ number_format($inform->total_price,2) }}<br><br>

@if($inform->img_name != '')
    <img src="{{ URL::asset('uploads/inform/'.$inform->img_name) }}">
@endif
</body>
</html>
