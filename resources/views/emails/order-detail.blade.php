<?php
$payment_method = config()->get('constants.payment_method_th');
$arr_shipping_method = config()->get('constants.shipping_method');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Order Detail</title>
</head>
<body>
    @include('emails.include.header')

    <div style="padding: 30px 30px; border: 1px solid #73b843; font-size: 14px">
        <b>Order Details</b><br>
        <b>วันที่ / Date:</b> {{ $orders->orders_date }}<br>
        <b>เลขที่สั่งซื้อ / Order Number:</b> {{ $orders->orders_no }}<br>
        <b>วิธีการจัดส่ง / Shipping Method:</b> {{ $arr_shipping_method[$lang][$orders->shipping_method_id] }} <br>
        <b>วิธีการชำระเงิน / Payment Method:</b> {{ $payment_method[$orders->payment_method_id] }}  <br>
        <b>สถานะการสั่งซื้อ / Order Status:</b> {{ $orders->Status['status_name_th'] }}<br>
        @if($orders->status_id == '6')
            <b>เลขที่พัสดุ / Tracking Number:</b> {{ $orders->tracking_no }}<br>
        @endif
        @if($orders->payment_date != '0000-00-00 00:00:00')
            <b>วันที่ชำระเงิน / Payment Date:</b> {{ $orders->payment_date }}
        @endif
        <br><br>
        <table width="100%" >
            <tr style="vertical-align: top">
                <td width="50%">
                    <b>Shipping Information :</b><br>
                    <hr style="border: 1px solid #cccccc;">
                    {!! $orders->shipping_address !!}
                </td>
                <td width="50%">
                    <b>Billing Information :</b><br>
                    <hr style="border: 1px solid #cccccc;">
                    {!! $orders->billing_address !!}
                </td>
            </tr>
        </table>
        <br>
        <table width="100%" border="1" cellspacing="0" cellpadding="5" style="border-collapse: collapse; border: 1px solid #cccccc;">
            <tr style="background-color: #dddddd">
                <th align="center">#</th>
                <th></th>
                <th align="left">Item</th>
                <th align="center">Quantity</th>
                <th align="right">Unit Cost</th>
                <th align="right">Total(THB)</th>
            </tr>
            <?php $i = 1; ?>
            @foreach($orders_detail as $item)
                <?php
                    $qty = $item->qty;
                    $unit_price = $item->unit_price;
                    $p_total_price = $qty * $unit_price;

                    $dir = 'uploads/product/100/';
                    $img_name = $item->img_name;
                    $img_path = url()->asset($dir.$img_name);
                    if($img_name == null || !file_exists($dir.$img_name)){
                        $img_path = 'http://fakeimg.pl/100x150/?text=Yakyim';
                    }
                ?>
                <tr>
                    <td align="center">{{ $i }}</td>
                    <td align="center"><img src="{{ $img_path }}" style="max-width: 100%; width: 50px"></td>
                    <td>
                        {{ $item->product_name_th }}<br>
                        {{ "Model : ".$item->model }}
                    </td>
                    <td align="center">{{ $qty }}</td>
                    <td align="right">{{ number_format($unit_price ,2) }}</td>
                    <td align="right">{{ number_format($p_total_price ,2) }}</td>
                </tr>
                <?php $i++; ?>
            @endforeach
            <?php
            $total_price = $orders->total_price;
            $shipping_price = $orders->shipping_price;
            $discount_price = $orders->discount_price;
            $use_point = $orders->use_point;
            $grand_price = $total_price + $shipping_price-$discount_price-$use_point;
            ?>
            <tr align="right" style="font-weight: bold">
                {{--<td  colspan="5">มูลค่าสินค้ารวมภาษีมูลค่าเพิ่ม / Sub - Total amount</td>--}}
                <td  colspan="5">มูลค่าสินค้า / Sub - Total amount</td>
                <td>{{ number_format($total_price ,2) }}</td>
            </tr>
            <tr align="right" style="font-weight: bold">
                <td  colspan="5">ส่วนลด / Discount @if(!empty($log_code)) [{{ $log_code->discount_code }}] @endif</td>
                <td style="color: green">- {{ number_format($discount_price ,2) }}</td>
            </tr>
            <tr align="right" style="font-weight: bold">
                <td  colspan="5">ค่าขนส่ง / Shipping </td>
                <td style="color: red">+ {{ number_format($shipping_price ,2) }}</td>
            </tr>
            <tr align="right" style="font-weight: bold">
                <td  colspan="5">ใช้คะแนนสะสม / Use Point </td>
                <td style="color: green">- {{ number_format($use_point ,2) }}</td>
            </tr>
            <tr align="right" style="font-weight: bold">
                {{--<td  colspan="5">สินค้ารวมภาษี / Total</td>--}}
                <td  colspan="5">ราคาสินค้า / Total</td>
                <td>{{ number_format($grand_price ,2) }}</td>
            </tr>
        </table>
        <br>
        @if($orders->shipping_method_id == '2')
            <div style="color: red; text-align: center">** ราคาที่ระบุดังกล่าว ยังไม่ได้รวมค่าจัดส่งของ Lalamove ซึ่งต้องตกลงระยะทางกับผู้ดูแล Yamyim อีกครั้ง **</div>
        @endif
        <br>
        <br>
        @if($orders->payment_method_id == '1')
            <b>หากชำระเงินโดยการโอนเงิน กรุณาโอนเงินเข้าบัญชี</b><br>
            <b>ธนาคารกสิกรไทย</b><br>
            ชื่อ บริษัท ยักษ์ยิ้ม จำกัด<br>
            เลขที่ 806-2-09769-9<br>
            <b>ธนาคารกรุงไทย</b><br>
            ชื่อ บริษัท ยักษ์ยิ้ม จำกัด<br>
            เลขที่ 077-0-23011-3<br><br>

            <b>ยืนยันการชำระเงินที่</b><br>
            <a href="{{url()->to('account/order-inform/'.$orders->orders_no)}}">{{ url()->to('account/order-inform/'.$orders->orders_no) }}</a>
        @endif
    </div>
    @include('emails.include.footer')
</body>
</html>