<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Stock</title>
</head>
<body>
@include('emails.include.header')

<div style="padding: 30px 30px; border: 1px solid #73b843; font-size: 14px; text-align: center;">
    <h3>มีการสั่งซื้อสินค้าผ่านทางเว็บไซต์ กรุณาตรวจสอบรายการสั่งซื้อค่ะ</h3>
    <p><a href="{{ url()->to('_admin/orders_detail/create?orders_id='.$orders_id) }}">ตรวจสอบรายการสั่งซื้อ</a></p>
</div>

@include('emails.include.footer')
</body>
</html>