<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>Confirm order</title>
</head>
<body style="font-size: 14px">
@include('emails.include.header')
@include('emails.include.order-detail')

<?php
$lang = session()->get('locale');
$bank_name = config()->get('main.bank_name');
$branch_name = config()->get('main.branch_name');
$acc_name = config()->get('main.acc_name');
$acc_no = config()->get('main.acc_no');

?>
@if($orders->payment_method_id == '2')
<b>หากชำระเงินโดยการโอนเงิน กรุณาโอนเงินเข้าบัญชี</b><br>
{{ $bank_name[$lang] }}<br>
{{ $branch_name[$lang] }}<br>
{{ $acc_name[$lang] }}<br>
{{ $acc_no[$lang] }}
@endif
@include('emails.include.footer')
</body>
</html>
