<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Stock</title>
</head>
<body>
@include('emails.include.header')

<div style="padding: 30px 30px; border: 1px solid #73b843; font-size: 14px; text-align: center;">
    <h3>คุณได้รับคะแนนสะสมเพิ่มจากการสั่งซื้อสินค้าของเพื่อนจำนวน {{ $member_point }} คะแนนค่ะ</h3>
</div>

@include('emails.include.footer')
</body>
</html>