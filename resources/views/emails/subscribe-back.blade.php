<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>Subscribe</title>
</head>
<body>
@include('emails.include.header')
<div style="padding: 30px 30px; border: 1px solid #73b843; font-size: 14px; text-align: center;">
    <b>{{ $subject }}</b><br>
    {!! $message2 !!}
</div>
@include('emails.include.footer')
</body>
</html>
