<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>Email Forget Password</title>
</head>
<body>
คุณ{{ $firstname.' '.$lastname }},
<br><br>
Please click the link to reset your password.
<a href="{{ URL::to('reset-password?token='.$gen_token) }}">Reset Password</a>
<br><br>
</body>
</html>
