<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>Email Register</title>
    <style>
        a{
            display: inline-block;
            padding: 10px 30px;
            background-color: #ffcc00;
            color: #000000;
            text-decoration: none;
        }
    </style>
</head>
<body>

<img src="{{ URL::asset('images/register-mail.jpg') }}">
<br><br>
สวัสดี คุณ{{ $firstname.' '.$lastname }} <br><br>


กรุณายืนยันตัวตนของท่านเพื่อเข้าสู่เว็บไซต์<br><br>
<a href="{{ URL::to('register-login/verify?token='.$gen_token) }}">ยืนยันตัวตน</a>

@include('emails.include.footer')
</body>
</html>
