<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Stock</title>
</head>
<body>
@include('emails.include.header')

<div style="padding: 30px 30px; border: 1px solid #73b843; font-size: 14px">
    หมายเลขคำสั่งซื้อเลขที่ {{ $orders_no }} ชำระเงินด้วย PayPal [Not Completed] กรุณาตรวจสอบยอดการชำระเงินในระบบของ PayPal เพื่อตัดยอดเงินเข้าระบบค่ะ
</div>

@include('emails.include.footer')
</body>
</html>