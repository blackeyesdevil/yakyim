<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Contact Us</title>
</head>
<body>
@include('emails.include.header')

<div style="padding: 30px 30px; border: 1px solid #73b843; font-size: 14px">
    Webmaster Yakyim,<br><br>
    คุณ{{ $name }} ติดต่อเข้ามาผ่านทางเว็บไซต์<br>
    เบอร์โทรฯ {{ $mobile }}<br><br>
    ข้อความ<br>
    {{ $msg }}
</div>

@include('emails.include.footer')
</body>
</html>