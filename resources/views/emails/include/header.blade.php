<div style="background-color: #73b843; padding: 15px 15px; color: #ffffff; font-weight: bold">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td width="20%" style="padding-right: 10px">
                <img src="{{ url()->asset('images/logo-white.png') }}" width="100">
            </td>
            <td>
                เลขที่ 111/21 หมู่บ้านธัญญาภิรมย์ ตำบลรังสิต อำเภอธัญบุรี จังหวัดปทุมธานี 12110<br>
                อีเมล : sale@yakyim.com<br>
                โทรศัพท์ : 02-191-3955, 084-657-1001
            </td>
        </tr>
    </table>

</div> 