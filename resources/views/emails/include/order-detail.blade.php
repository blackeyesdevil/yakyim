<br>
<?php $a_paysbuy_method = config()->get('main.paysbuy_method'); ?>
<b>Order Details</b><br>
<b>วันที่ / Date:</b> {{ $orders->orders_date }}<br>
<b>เลขที่สั่งซื้อ / Order Number:</b> {{ $orders->orders_no }}<br>
<b>วิธีการชำระเงิน / Payment Method:</b> @if($orders->payment_method_id == '2') {{ $orders->Payment->name_th.' ' }} @else {{ '-' }} @endif @if(!empty($orders->paysbuy_method)) {{ $a_paysbuy_method[$orders->paysbuy_method] }} @endif<br>
<b>สถานะการสั่งซื้อ / Order Status:</b> {{ $orders->Status->status_name_th }}<br>
@if($orders->payment_date != '0000-00-00 00:00:00')
    <b>วันที่ชำระเงิน / Payment Date:</b> {{ $orders->payment_date }}
@endif
<br><br>
<table width="100%" >
    <tr style="vertical-align: top">
        <td width="50%">
            <b>Shipping Information :</b><br>
            <hr style="border: 1px solid #cccccc;">
            {!! $orders->shipping_address !!}
        </td>
        <td width="50%">
            <b>Billing Information :</b><br>
            <hr style="border: 1px solid #cccccc;">
            {!! $orders->billing_address !!}
        </td>
    </tr>
</table>
<br>
<table width="100%" border="1" cellspacing="0" cellpadding="5" style="border-collapse: collapse; border: 1px solid #cccccc;">
    <tr style="background-color: #dddddd">
        <th>#</th>
        <th align="left">Item</th>
        <th align="center">Quantity</th>
        <th align="right">Unit Cost</th>
        <th align="right">Total(THB)</th>
    </tr>
    <?php $i = 1; ?>
    @foreach($orders_detail as $item)
        <?php
            $qty = $item->qty;
            $unit_price = $item->unit_price;
            $p_total_price = $qty * $unit_price;
        ?>
        <tr>
            <td align="center">{{ $i }}</td>
            <td>{{ $item->product_name }}</td>
            <td align="center">{{ $qty }}</td>
            <td align="right">{{ number_format($unit_price ,2) }}</td>
            <td align="right">{{ number_format($p_total_price ,2) }}</td>
        </tr>
        <?php $i++; ?>
    @endforeach
    <?php
    $total_price = $orders->total_price;
    $discount_price = $orders->discount_price;
    $shipping_price = $orders->shipping_price;
    $grand_price = $total_price - $discount_price + $shipping_price;
    ?>
    <tr align="right" style="font-weight: bold">
        <td  colspan="4">มูลค่าสินค้ารวมภาษีมูลค่าเพิ่ม / Sub - Total amount</td>
        <td>{{ number_format($total_price ,2) }}</td>
    </tr>
    <tr align="right" style="font-weight: bold">
        <td  colspan="4">ส่วนลด / Discount </td>
        <td>{{ number_format($discount_price ,2) }}</td>
    </tr>
    <tr align="right" style="font-weight: bold">
        <td  colspan="4">ค่าขนส่ง / Shipping </td>
        <td>{{ number_format($shipping_price ,2) }}</td>
    </tr>
    <tr align="right" style="font-weight: bold">
        <td  colspan="4">สินค้ารวมภาษี/Total</td>
        <td>{{ number_format($grand_price ,2) }}</td>
    </tr>
</table>
<br>