<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>Member Loft</title>
</head>
<body>
@include('emails.include.header')

สวัสดี, {{ $user_name }}<br>
คุณไปเป็นสมาชิก Loft อย่างสมบูรณ์แล้ว<br>
ทางร้านจะจัดส่งบัตรสมาชิก Loft ไปให้คุณตามที่อยู่ที่บันทึกไว้ในระบบค่ะ

@include('emails.include.footer')

</body>
</html>
