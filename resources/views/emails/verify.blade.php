<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Verify</title>
</head>
<body>
@include('emails.include.header')

<div style="padding: 30px 30px; border: 1px solid #73b843; font-size: 14px">
    คุณ{{ $firstname.' '.$lastname }} ,<br><br>
    ยินดีต้อนรับสู่เว็บไซต์ Yakyim ขอบคุณสำหรับการสมัครสมาชิก<br><br>
    กรุณายืนยันการสมัครสมาชิก, <a href="{{ url()->to('register/verify?token='.$gen_token) }}">คลิก</a><br><br>
    ขอให้คุณเพลิดเพลินกับการช๊อปปิ้งกับผลิตภัณฑ์มากมายของเราค่ะ
</div>

@include('emails.include.footer')
</body>
</html>