@extends('frontend.layouts/main')

@section('title','Old Member')

@section('more-stylesheet')
  <link rel="stylesheet" href="{{ URL::asset('css/front/home.css') }}">
@endsection

@section('content')
  <div id="home-member">
    <div class="container">
      <div class="content col-xs-12">
        <div class="headline">Old Member</div>
        <div class="form ">
          <form action="{{ URL::to('old-member/check') }}" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            @if(session()->has('successMsg'))
              <div class="row">
                <div class="col-sm-5">
                  <div class="alert alert-success text-center">{!! session()->get('successMsg') !!}</div>
                </div>
              </div>
            @endif
            @if(session()->has('errorMsg'))
              <div class="row">
                <div class="col-sm-5">
                  <div class="alert alert-danger text-center">{!! session()->get('errorMsg') !!}</div>
                </div>
              </div>
            @endif
            <div class="row">
              <div class="col-sm-5">
                <input type="text" name="mobile" value="" placeholder="Mobile" class="input input-block" />
                @if($errors->first('mobile'))
                  <div class="text-danger">{{ $errors->first('mobile') }}</div>
                @endif
              </div>
            </div>
            <div class="row">
              <div class="col-sm-5">
                <input type="submit" name="submit" value="Submit" class="btn btn-block" />
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>



@endsection
