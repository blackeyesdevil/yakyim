@extends('frontend.layouts/main')

@section('title','Home')

@section('more-stylesheet')

@endsection

@section('content')

    <div id="content" style="padding-top:0px;">
      <div id="map">
        <iframe src="//www.google.com/maps/embed/v1/place?q=ยักษ์ยิ้ม+เสื้อผ้าผู้ชายอ้วน
      &zoom=17
      &key={{ env("API_KEY") }}" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen>
        </iframe>
      </div>
      <div class="container">
        @if(session()->has('successMsg'))
          <div class="alert alert-success text-center">{{ session()->get('successMsg') }}</div>
        @endif
        <h1 style="padding-left: 30px;">{{ trans('contactus.contact-form') }}</h1>
        <hr style="max-width:95%">

        <div class="row">
          <div class="col-md-7" style="padding-left: 70px;">
            <form method="POST" action="{{ url()->to('contactus/check') }}">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>{{ trans('contactus.form-name') }}</label>
                    <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                    @if(!empty($errors->first('name')))
                      <div class="text-danger">{{ $errors->first('name') }}</div>
                    @endif
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>{{ trans('contactus.form-email') }}</label>
                    <input type="text" class="form-control" name="email" value="{{ old('email') }}">
                    @if(!empty($errors->first('email')))
                      <div class="text-danger">{{ $errors->first('email') }}</div>
                    @endif
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label >{{ trans('contactus.form-mobile') }}</label>
                    <input type="text" class="form-control" name="mobile" value="{{ old('mobile') }}">
                    @if(!empty($errors->first('mobile')))
                      <div class="text-danger">{{ $errors->first('mobile') }}</div>
                    @endif
                  </div>
                </div>

                <div class="col-md-12">
                  <div class="form-group">
                    <label >{{ trans('contactus.form-msg') }}</label>
                    <textarea class="form-control" rows="5" name="msg">{{ old('msg') }}</textarea>
                    @if(!empty($errors->first('msg')))
                      <div class="text-danger">{{ $errors->first('msg') }}</div>
                    @endif
                  </div>
                </div>

                <div class="col-md-12">
                  <div class="g-recaptcha" data-sitekey="{{ env('RE_CAP_SITE') }}"></div>
                  @if(!empty($errors->first('g-recaptcha-response')))
                    <div class="text-danger">{{ $errors->first('g-recaptcha-response') }}</div>
                  @endif
                </div>

              </div>

              <p style="margin-top:20px;"><button type="submit" class="btn btn-default">{{ trans('contactus.form-submit') }}</button></p>

            </form>
          </div>
          <div class="col-md-5" id="contact-right">
            <img src="{{ URL::asset('images/logo.jpg') }}" alt="" class="img-responsive center">

            <table>
              <tr>
                <td style="vertical-align: top;width:30%;"><b>{{ trans('contactus.address') }} : </b></td>
                <td>{!! trans('contactus.company-address') !!}</td>
              </tr>
              <tr>
                <td><b>{{ trans('contactus.email') }} :</b></td>
                <td>sale@yakyim.com</td>
              </tr>
              <tr>
                <td><b>{{ trans('contactus.phones') }} :</b></td>
                <!-- <td>02-191-3955, 084-657-1001</td> -->
                <td>096-7249789</td>
              </tr>
              <tr>
                <td><b>{{ trans('contactus.line') }} :</b></td>
                <td>@yakyim</td>
              </tr>
            </table>



          </div>
        </div>

      </div>


    </div>

@endsection

@section('more-script')
  <script src='https://www.google.com/recaptcha/api.js'></script>
@endsection
