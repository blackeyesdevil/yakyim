@extends('layout-frontend')
    @section('content')
        <div id="wrapper">
            <div class="container">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">ID.</th>
                            <th>Product Name</th>
                            <th class="text-right">Price</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if(count($products) > 0)
                        @foreach($products as $field)
                        <tr>
                            <td class="text-center">{{ $field->product_id }}</td>
                            <td><a href="{{ URL::to('api/shop/product/'.$field->product_id) }}">{{ $field->product_name }}</a></td>
                            <?php $price = $field->price; ?>
                            @if($price == null)
                                <?php $price = $field->retail_price; ?>
                            @endif
                            <td class="text-right">{{ number_format($price,2) }}</td>
                        </tr>
                        @endforeach
                    @else
                        <tr>
                            <td class="text-center" colspan="3">No Result</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    @endsection