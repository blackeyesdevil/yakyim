@extends('frontend.layouts/main')

@section('more-stylesheet')
{!! Html::style('css/front/register-login.css') !!}
@endsection

@section('content')
  <div id="content">
      <div class="container">
        <h1 class="text-center">{{ trans('forget-reset.forget') }}</h1>
        <p class="text-center">{{ trans('forget-reset.detail') }}</p>
        <div style="overflow: hidden;">
          @if(session()->has('errorMsg'))
            <div class="alert alert-danger text-center">{{ session()->get('errorMsg') }}</div>
          @endif
          @if(session()->has('successMsg'))
            <div class="alert alert-success text-center">{{ session()->get('successMsg') }}</div>
          @endif
          <form method="POST" action="{{ url()->to('forget-password/check') }}" id="form" class="form-inline col-md-6 col-md-offset-3 text-center">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group">
              <label class="sr-only">{{ trans('forget-reset.email') }}</label>
              <p class="form-control-static"><b>{{ trans('forget-reset.email') }}</b></p>
            </div>
            <div class="form-group">
              <label for="inputPassword2" class="sr-only">{{ trans('forget-reset.email') }}</label>
              <input type="text" class="form-control" name="email">
              @if(!empty($errors->first('email')))
                <div class="text-danger">{{ $errors->first('email') }}</div>
              @endif
            </div>
            <button type="submit" class="btn btn-default">{{ trans('forget-reset.send') }}</button>
          </form>
        </div>

        <div class="text-center" style="font-size:10pt;margin-top:30px;">
          <a href="{{ url()->to('login') }}">{{ trans('forget-reset.back-to-login') }}</a>
        </div>

      </div>
    </div>
@endsection
@section('more-script')
@endsection
