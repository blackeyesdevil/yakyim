@extends('frontend.layouts/main')

@section('title','Blog')

@section('more-stylesheet')
<link rel="stylesheet" href="{{ URL::asset('css/front/blog-list.css') }}">
@endsection

@section('content')
<div id="blog" class="container">
    <div id="breadcrumb">
        <a href="{{ URL::route('home') }}">{{ trans('global.home') }}</a>
        <span class="arrow">></span>
        <span class="active">{{ trans('global.blog') }}</span>
    </div>

    <div class="row">
        <div id="blog-list" class="col-sm-9">
            @foreach ($blogs as $blog)
            <div class="item row">
              <div class="text-center col-sm-4">
                <a href="{{ URL::to('blog/' . $blog->blog_id) }}" class="blog-img">
                    <div class="img" style="background-image: url('http://loft.bkksol.com/uploads/blog_thumb/{{ $blog->thumb_img_name }}');'"></div>

                    <div class="hover">
                        <i class="fi-eye"></i>
                    </div>
                </a>
              </div>

              <div class="blog-info col-sm-8">
                  <div class="blog-title{{ trans('global.th-font') }}">{{ $blog['title_' . session()->get('locale')] }}</div>
                  <div class="blog-detail">{{ trans('global.post-on') }} {{ date('j M, Y', strtotime($blog->post_datetime)) }}</div>
                  <div class="blog-short-desc">
                      {!! $blog['short_detail_' . session()->get('locale')] !!}
                  </div>

                  <a href="{{ URL::to('blog/' . $blog->blog_id) }}" class="btn"><i class="fi-arrow-right"></i> {{ trans('global.read-more') }}</a>
              </div>
            </div>
            @endforeach
        </div>

        <div class="col-sm-3">
            <div id="sidebar">
                <div class="promo-banner-1 hidden-xs">
                    <img src="images/home-member-banner.png" />
                </div>

                <div class="promo-banner-2 row hidden-xs">
                    <div class="col-xs-6">
                        <img src="images/home-member-promo-01.jpg" />
                    </div>

                    <div class="col-xs-6">
                        <img src="images/home-member-promo-02.jpg" />
                    </div>
                </div>

                <!-- <div id="category-list">
                    <div class="headline">CATEGORIES</div>
                    <ul class="list">
                        <li><a href="#"><i class="fi-sound"></i> Furniture</a></li>
                        <li><a href="#"><i class="fi-sound"></i> Decor</a></li>
                        <li><a href="#"><i class="fi-sound"></i> Lighting</a></li>
                        <li><a href="#"><i class="fi-sound"></i> Bed & Bath</a></li>
                    </ul>
                </div> -->

                <div id="tags">
                    <div class="headline">{{ trans('product.tags') }}</div>
                    <div class="list">
                        @foreach ($tags as $tag)
                        <a href="{{ URL::to('search?q=' . $tag->tag['tag_name_' . session()->get('locale')]) }}" class="item">{{ $tag->tag['tag_name_' . session()->get('locale')] }}</a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
