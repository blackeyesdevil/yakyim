<?php
$lang = session()->get('locale');
?>
@extends('frontend.layouts/main')

@section('title','Blog')

@section('more-stylesheet')
<link rel="stylesheet" href="{{ URL::asset('css/front/blog-detail.css') }}">
@endsection

@section('content')
<div id="blog-detail" class="container">
  <div id="breadcrumb">
    <a href="{{ URL::route('home') }}">{{ trans('global.home') }}</a>
    <span class="arrow">></span>
    <a href="{{ URL::route('blog-list') }}">{{ trans('global.blog') }}</a>
    <span class="arrow">></span>
    <span class="active">{{ $blog['title_' . session()->get('locale')] }}</span>
  </div>

  <div id="content">
    <div class="blog-title{{ trans('global.th-font') }}">{{ $blog['title_' . session()->get('locale')] }}</div>
    <div class="blog-detail">{{ trans('global.post-on') }} {{ date('j M, Y', strtotime($blog->post_datetime) )}}</div>
    <div class="blog-img">
      <img src="{{ URL::asset('uploads/blog_img_name/' . $blog->img_name) }}" />
    </div>
    <div class="blog-content">
      {!! $blog['msg_' . session()->get('locale')] !!}
    </div>

    @if (count($tags) > 0)
    <div class="blog-tags">
      <span class="bold">{{ trans('product.tags') }}:</span> &nbsp;&nbsp;
      @foreach($tags as $i => $tag)
      <a href="{{ URL::to('search?q=' . $tag->tag['tag_name_' . $lang]) }}">{{ $tag->tag['tag_name_'.$lang] }}</a>
      @if ($i != count($tags)-1)
      <span class="slash">/</span>
      @endif
      @endforeach
    </div>
    @endif

    <div class="blog-more-pages{{ trans('global.th-font') }}">
      @if (!empty($previous_blog))
      <a href="{{ URL::to('blog/' . $previous_blog[0]->blog_id) }}" class="prev">
        <div class="option"><i class="fi-arrow-left"></i> {{ trans('global.prev') }}</div>
        <div class="blog-title">{{ $previous_blog[0]->{'title_' . session()->get('locale')} }}</div>
      </a>
      @else
      <div class="prev"></div>
      @endif

      @if (!empty($next_blog))
      <a href="{{ URL::to('blog/' . $next_blog[0]->blog_id) }}" class="next">
        <div class="option">{{ trans('global.next') }} <i class="fi-arrow-right"></i></div>
        <div class="blog-title">{{ $next_blog[0]->{'title_' . session()->get('locale')} }}</div>
      </a>
      @else
      <div class="next"></div>
      @endif
    </div>
  </div>

  @if (count($products) != 0)
  <div id="related-products">
    <div class="container">
      <div class="headline{{ trans('global.th-font') }}">{{ trans('global.related-products') }}</div>
      <div class="sub-headline{{ trans('global.th-font') }}">{{ trans('global.related-products-slogan') }}</div>

      <div class="product-list">
        <div class="row">
          @foreach ($products as $product)
          <?php
          if ($product->photo != '' && $product->photo != null)
              $img_url = URL::asset('uploads/product_option/' . $product->photo);
          else
            $img_url = 'http://dummyimage.com/200x200/f5f5f5/ccc.png';
          ?>
          <div class="item col-sm-3">
            <div class="product-img">
              <img src="{{ $img_url }}" />
            </div>

            <div class="product-info">
              <div class="product-name">{{ $product['product_name_' . session()->get('locale')] }}</div>
              <div class="product-price">THB {{ number_format($product->retail_price, 2) }}</div>
            </div>
          </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
  @endif
</div>
@endsection
