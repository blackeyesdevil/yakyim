@extends('frontend.layouts/home')
@section('more-stylesheet')

<link rel="stylesheet" href="{{ URL::asset('tools/bxslider/jquery.bxslider.css') }}">
<link rel="stylesheet" href="{{ URL::asset('css/front/home.css') }}">
@endsection

@section('content')
    <?php
        if(!empty($banner_top)){
            $banner_detail = $banner_top['detail_'.$lang];
            $banner_url = $banner_top->link;
            $dir = 'uploads/banner_top/';

            $mo = 'mobile_banner_top/500/'; $xs = '770/'; $sm = '995/'; $md = '1600/';
            $img_name = $banner_top->img_name;
            $img_mobile = $banner_top->img_mobile;

            $img_dir_mo = $dir.$mo.$img_mobile;
            $img_dir_xs = $dir.$xs.$img_name;
            $img_dir_sm = $dir.$sm.$img_name;
            $img_dir_md = $dir.$md.$img_name;

            $img_path_mo = url()->asset($img_dir_mo);
            $img_path_xs = url()->asset($img_dir_xs);
            $img_path_sm = url()->asset($img_dir_sm);
            $img_path_md = url()->asset($img_dir_md);

            if($img_name == '' || !file_exists($img_dir_md)){
                $img_path_mo = url()->asset('images/yakyim/770x374.jpg');
                $img_path_xs = url()->asset('images/yakyim/770x374.jpg');
                $img_path_sm = url()->asset('images/yakyim/995x484.jpg');
                $img_path_md = url()->asset('images/yakyim/1600x778.jpg');
            }

            if($banner_url == '') $banner_url = '#';
        }
    ?>

    	<div class="headertop" @if(!empty($banner_top)) data-value="true" data-mo="{{ $img_path_mo }}" data-xs="{{ $img_path_xs }}" data-sm="{{ $img_path_sm }}" data-md="{{ $img_path_md }}" @endif style="padding-top: 70px;">
        <img id="headertop" class="img-responsive"src="" style="max-width:100%;">
        @if(!empty($banner_top) && $banner_detail != '')
            <div class="header-content">
                <div class="header-content-inner">
                    {!! $banner_detail !!}<br>
                    <a href="{{ url()->to($banner_url) }}" class="btn btn-default btn-xl" target="_blank">shop now</a>
                </div>
            </div>
        @endif
      </div>
    	<!-- <header @if(!empty($banner_top)) data-value="true" data-mo="{{ $img_path_mo }}" data-xs="{{ $img_path_xs }}" data-sm="{{ $img_path_sm }}" data-md="{{ $img_path_md }}" @endif>
            @if(!empty($banner_top) && $banner_detail != '')
                <div class="header-content">
                    <div class="header-content-inner">
                        {!! $banner_detail !!}
                        {{--<a href="#about" class="btn btn-default btn-xl">Lookbooks</a>--}}
                        <a href="{{ url()->to($banner_url) }}" class="btn btn-default btn-xl" target="_blank">shop now</a>
                    </div>
                </div>
            @endif
        </header> -->


    <section class="no-padding" id="category">
        <div class="container-fluid">
        <div class="col-sm-12 form-group">
            <div class="addthis_inline_share_toolbox_v3yt text-center"></div>
        </div>
            @if(count($categories) > 0)
                @foreach($categories as $category)
                    <?php
                    $category_id = $category->category_id;
                    $category_name = $category['category_name_'.$lang];
                    $category_name_en = str_slug($category['category_name_en'],'-');
                    $title_name = $category['title_home_'.$lang];

                    $dir = 'uploads/category/650/';
                    $img_name = $category->img_name;
                    $img_dir = $dir.$img_name;
                    $img_path = url()->asset($img_dir);

                    if($img_name == '' || !file_exists($img_dir)){
                        $img_path = url()->asset('images/yakyim/650x460.jpg');
                    }
                    ?>
                    <div class="col-lg-6 col-sm-12">
                        <a href="{{ url()->to('product/'.$category_name_en.'/'.$category_id) }}" class="portfolio-box" title="{{ $category_name }}">
                            <img src="{{ $img_path }}" class="img-responsive" alt="{{ $category_name }}">
                            <div class="portfolio-box-caption">
                                <div class="portfolio-box-caption-content">
                                    <button class="btn btn-default btn-xl">SHOP NOW</button>
                                </div>
                            </div>
                        </a>
                        <div class="box-caption text-center">
                            <p>{{ $title_name }}</p>
                            <h2>{{ $category_name }}</h2>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </section>

    <div id="promotion">
        @if(!empty($banner_promo))
            <?php
                $banner_title = $banner_promo['title_'.$lang];
                $banner_url = $banner_promo->link;

                $dir = 'uploads/banner_promotion/';
                $xs = '755/'; $sm = '980/'; $md = '1600/';
                $img_name = $banner_promo->img_name;

                $img_dir_xs = $dir.$xs.$img_name;
                $img_dir_sm = $dir.$sm.$img_name;
                $img_dir_md = $dir.$md.$img_name;

                $img_path_xs = url()->asset($img_dir_xs);
                $img_path_sm = url()->asset($img_dir_sm);
                $img_path_md = url()->asset($img_dir_md);

                if($img_name == '' || !file_exists($img_dir_md)){
                    $img_path_xs = url()->asset('images/yakyim/755x110.jpg');
                    $img_path_sm = url()->asset('images/yakyim/980x142.jpg');
                    $img_path_md = url()->asset('images/yakyim/1600x232.jpg');
                }
                if($banner_url == '') $banner_url = '#';
            ?>
            <a href="{{ $banner_url }}" title="{{ $banner_title }}" target="_blank">
                <img src="{{ $img_path_xs }}" alt="{{ $banner_title }}" width="100%" class="visible-xs">
                <img src="{{ $img_path_sm }}" alt="{{ $banner_title }}" width="100%" class="visible-sm">
                <img src="{{ $img_path_md }}" alt="{{ $banner_title }}" width="100%" class="visible-md visible-lg">
            </a>
        @endif
    </div>

    <section id="new">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading">NEW ARRIVAL </h2>
                    <p class="sub">Life style of cubby guy</p>
                </div>
            </div>
        </div>
    </section>
    <section class="no-padding" id="new-pics">
        <div class="container-fluid">
            <div class="row no-gutter popup-gallery">

                @if(count($new_products) > 0)
                    @foreach($new_products as $item)
                        <?php
                        $product_id = $item->product_id;
                        $product_name = $item['product_name_'.$lang];
                        $url_name = str_slug($item['product_name_en']);

                        $dir = 'uploads/banner_product/635/';
                        $img_name = $item->img_new_product;
                        $img_dir = $dir.$img_name;
                        $img_path = url()->asset($img_dir);

                        if($img_name == '' || !file_exists($img_dir)){
                            $img_path = url()->asset('images/yakyim/635x635.jpg');
                        }
                        ?>
                        <div class="col-lg-4 col-sm-12">
                        <a href="{{ url()->to('product/detail/'.$product_id.'/'.$url_name) }}" class="portfolio-box" title="{{ $product_name }}">
                            <img src="{{ $img_path }}" class="img-responsive" alt="{{ $product_name }}">
                            <div class="portfolio-box-caption">
                                <div class="portfolio-box-caption-content">
                                    {{--<div class="project-category text-faded">--}}
                                        {{--Category--}}
                                    {{--</div>--}}
                                    <div class="project-name">{{$product_name}}</div>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endforeach
                @endif
            </div>
        </div>
    </section>

@endsection

@section('more-script')
  <script src="{{ URL::asset('tools/bxslider/jquery.bxslider.js') }}"></script>
  <script src="{{ URL::asset('js/front/home.js') }}"></script>
  <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57fb10513368b28d"></script>

@endsection
