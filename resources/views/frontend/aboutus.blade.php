@extends('frontend.layouts/main')

@section('title','Home')

@section('more-stylesheet')

@endsection

@section('content')

    <div id="content">

      <div class="container">
        <h1 style="padding-left: 30px;">COMPANY PROFILE</h1>
        <hr style="max-width:95%">
        <img src="{{ URL::asset('images/logo.jpg') }}" alt="" class="img-responsive center">
        <p>
            @if($lang == 'th')
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ยักษ์ยิ้มก่อตั้งเมื่อปี 2013 โดย พรพรรณ (เก่ง) มีความตั้งใจที่จะทำเสื้อผ้าผู้ชายไซส์ใหญ่ เพื่อเสริมสร้างบุคลิกและความมั่นใจให้ "หนุ่มบิ๊กไซส์" โดยออกแบบเสื้อผ้าให้มีอารมณ์สนุกสนาน ร่าเริง ในสไตล์ลำลองภายใต้ key word  3 C "Casual, Comfort and Cool" ซึ่งมีคุณ ป๊อบ ปองกูล นักร้อง ศิลปินชื่อดังในประเทศไทยให้คำแนะนำการออกแบบแพทเทิร์นมาให้เหมาะสมกับ หนุ่มบิ๊กไซส์ อีกทั้งยังทำให้ หนุ่มบิ๊กไซส์ ดูเพรียวขึ้น
            @else
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ยักษ์ยิ้มก่อตั้งเมื่อปี 2013 โดย พรพรรณ (เก่ง) มีความตั้งใจที่จะทำเสื้อผ้าผู้ชายไซส์ใหญ่ เพื่อเสริมสร้างบุคลิกและความมั่นใจให้ "หนุ่มบิ๊กไซส์" โดยออกแบบเสื้อผ้าให้มีอารมณ์สนุกสนาน ร่าเริง ในสไตล์ลำลองภายใต้ key word  3 C "Casual, Comfort and Cool" ซึ่งมีคุณ ป๊อบ ปองกูล นักร้อง ศิลปินชื่อดังในประเทศไทยให้คำแนะนำการออกแบบแพทเทิร์นมาให้เหมาะสมกับ หนุ่มบิ๊กไซส์ อีกทั้งยังทำให้ หนุ่มบิ๊กไซส์ ดูเพรียวขึ้น
                <!-- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;YAKYIM CLOTHING BRAND FOR CHUBBY AND CHIPPER GUY PATH TO “YAKYIM” MENSWEAR BRAND “YAKYIM” THE BIG SIZE CLOTHING BRAND FOR MEN HAS BEEN ESTABLISHED SINCE YEAR 2012. -->
            @endif
        </p>

      </div>


    </div>

@endsection

@section('more-script')

@endsection
