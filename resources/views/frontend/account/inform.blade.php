<?php
$a_bank = config()->get('constants.bank');
?>
@extends('frontend.layouts/main')
@section('more-stylesheet')
  <link rel="stylesheet" href="{{ URL::asset('css/front/account-global.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('css/front/account-order.css') }}">
@endsection

@section('content')
<div id="account" class="container">
  <div id="content" class="row">
    <div class="col-sm-9">
      <div id="breadcrumb">
        <!-- <a href="{{ URL::route('home') }}">{{ trans('global.home') }}</a>
        <span class="arrow">></span>
        <a href="{{ URL::to('account') }}">{{ trans('global.my-account') }}</a>
        <span class="arrow">></span> -->
        <a href="{{ URL::to('account/order-history') }}">{{ trans('account.order-history') }}</a>
        <span class="arrow">></span>
        <span class="active">{{ trans('account.transfer-inform') }}</span>
      </div>

      <div class="headline{{ trans('global.th-font') }}">{{ trans('account.transfer-inform') }}</div>

      <form action="{{ URL::to('account/order-inform/check') }}" method="post" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="orders_no" value="{{ $order->orders_no }}">

        @if(session()->has('errorMsg'))
          <div class="alert alert-danger text-center">{!! session()->get('errorMsg') !!}</div>
        @endif

        <div class="description">{!! '<strong>' . trans('account.order-no') . '</strong> ' . $order->orders_no !!}</div>

        <?php $field = 'payment_date'; ?>
        <div class="form-group row">
          <div class="form-label col-sm-3">{{ trans('account.payment-date') }}</div>

          <div class="value col-sm-5">
            <input type="date" name="{{ $field }}" placeholder="{{ trans('account.payment-date') }}" class="input input-block">
            @if($errors->first($field))
              <div class="text-danger">{{ $errors->first($field) }}</div>
            @endif
          </div>
        </div>
        <?php $field = 'bank'; ?>
        <div class="form-group row">
          <div class="form-label col-sm-3">{{ trans('account.bank') }}</div>

          <div class="value col-sm-5">
            <select name="{{ $field }}">
              @foreach($a_bank as $bank)
                <option value="{{ $bank }}">{{ $bank }}</option>
              @endforeach
            </select>
            @if($errors->first($field))
              <div class="text-danger">{{ $errors->first($field) }}</div>
            @endif
          </div>
        </div>
        <?php $field = 'total_price'; ?>
        <div class="form-group row">
          <div class="form-label col-sm-3">{{ trans('account.total-price') }}</div>

          <div class="value col-sm-5">
            <input type="text" name="{{ $field }}" placeholder="{{ trans('account.total-price') }}" class="input input-block">
            @if($errors->first($field))
              <div class="text-danger">{{ $errors->first($field) }}</div>
            @endif
          </div>
        </div>

        <?php $field = 'img_name'; ?>
        <div class="form-group row">
          <div class="form-label col-sm-3">{{ trans('account.proof-of-payment') }}</div>

          <div class="value form-label col-sm-5">
            <input type="file" name="{{ $field }}">
            @if($errors->first($field))
              <div class="text-danger">{{ $errors->first($field) }}</div>
            @endif
          </div>
        </div>

        <div class="option">
          <button type="submit" name="submit" class="btn btn-default black">{{ trans('account.transfer-inform') }}</button>
        </div>
      </form>
    </div>

    <div class="col-sm-3">
      @include('frontend.account.sidebar')
    </div>
  </div>
</div>
@endsection
