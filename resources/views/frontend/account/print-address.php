<?php
$payment_method = config()->get('constants.payment_method_'.$lang);
?>
@extends('frontend.layouts/main')

@section('title','Order Return Product')

@section('more-stylesheet')
  <link rel="stylesheet" href="{{ URL::asset('css/front/account-global.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('css/front/account-order-return.css') }}">
@endsection

@section('content')
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
    <div class="container">
        <!-- BEGIN PAGE CONTENT INNER -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN SAMPLE TABLE PORTLET-->

                <div class="portlet light">
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-xs-12 invoice-block text-right">
                                <a class="btn btn-md blue hidden-print margin-bottom-5" onclick="javascript:window.print();">
                                    Print <i class="fa fa-print"></i>
                                </a>
                            </div>
                        </div>
                        <div class="invoice">
                            <div class="row invoice-logo">
                                <div class="invoice-logo-space">
                                    <div class="col-xs-2">
                                        <img src="{{ URL::asset('/images/logo.jpg') }}" class="img-responsive" alt=""/>
                                    </div>


                                </div>
                            </div>

                          </div>
                            <div class="row">
                              <div class="col-md-6">
                                วันที่ วว/ดด/ปป<br>
                                เลขที่สั่งของ {{ $orders->orders_no}}<br>
                                ที่อยู่ : {!!$orders->billing_address !!}

                              </div>
                              <div class="col-md-6">
                                <br>
                                <br>
                                To.<br>
                                บริษัท ยักษ์ ยิ้ม กรุ๊ป จำกัด <br>เลขที่ 111/21 หมู่บ้านธัญญาภิรมย์ <br>ตำบลรังสิต อำเภอธัญบุรี จังหวัดปทุมธานี 12110<br><br>
                                Yakyim Group Ltd <br>111/21 Thanyaphirom Village <br> Rangsit Thnayaburi Pathumthani<br>
                                Tel. 02-191-3955, 084-657-1001<br>
                            </div>
                          </div>
                          </div>
            </div>
        </div>
    </div>
</div>
</div>

@endsection
@section('more-script')
  <!-- <script src="{{ URL::asset('tools/bxslider/jquery.bxslider.js') }}"></script> -->
  <script src="{{ URL::asset('js/jquery-ui/jquery-ui.min.js') }}"></script>
  <script src="{{ URL::asset('js/front/return-product.js') }}"></script>
@endsection
