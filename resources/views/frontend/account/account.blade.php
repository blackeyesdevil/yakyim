<!-- resources/views/frontend/account/account.blade.php-->
<?php
$lang = session()->get('locale');
?>
@extends('frontend.layouts/main')
@section('more-stylesheet')
    <link rel="stylesheet" href="{{ URL::asset('css/front/account-global.css') }}">
@endsection

@section('content')
<div id="account" class="container">
  <div id="content" class="row">
  <div class="col-sm-9">
      <h2>{{ trans('account.edit-profile') }}</h2>

      <form action="{{ url()->to('account/profile/update') }}" method="post" >
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
          @if(session()->has('successMsg'))
              <div class="alert alert-success text-center">
                  {{ session()->get('successMsg') }}
              </div>
          @endif
        <div class="form-group row">
            <div class="form-label col-sm-3">{{ trans('account.firstname').' - '.trans('account.lastname') }}</div>
            <div class="value col-xs-6 col-sm-4">
                <input type="text" name="firstname" value="{{ $data->firstname }}" class="input input-block form-control">
                @if(!empty($errors->first('firstname')))
                    <div class="text-danger">{{ $errors->first('firstname') }}</div>
                @endif

            </div>
            <div class="value col-xs-6 col-sm-4">
                <input type="text" name="lastname" value="{{ $data->lastname }}" class="form-control input input-block">
                @if(!empty($errors->first('lastname')))
                    <div class="text-danger">{{ $errors->first('lastname') }}</div>
                @endif
            </div>
        </div>
        <div class="row">
          <div class="col-sm-6">
             <div class="form-group row">
              <div class="form-label col-sm-6">{{ trans('account.birth_date') }}</div>

              <div class="value col-sm-6">
                 <label class="form-label">{{ $data->birth_date }}</label>
              </div>
            </div>
          </div>
        </div>
        <div class="form-group row">
          <div class="form-label col-sm-3">{{ trans('account.mobile') }}</div>

          <div class="value col-sm-4">
            <input type="text" name="mobile" value="{{ $data->mobile }}" class="input input-block form-control" maxlength="10">
          </div>
        </div>
        <div class="form-group row">
          <div class="form-label col-sm-3">{{ trans('account.line') }}</div>

          <div class="value col-sm-4">
              <input type="text" name="line" value="{{ $data->line }}" class="input input-block form-control">
          </div>
        </div>
        <div class="form-group row">
          <div class="form-label col-sm-3">{{ trans('account.facebook') }}</div>

          <div class="value col-sm-8">
              <div class="input-group">
                  <div class="input-group-addon">www.facebook.com/</div>
                  <input type="text" name="facebook" value="{{ $data->facebook }}" class="input input-block form-control">
              </div>
          </div>
        </div>

        <div class="address-div">
          <div class="sub-headline th">{{ trans('account.current-address') }}</div>


          <div class="row">
            <div class="col-xs-6">
                <div class="form-group row">
                <div class="col-sm-6">{{ trans('account.address') }}</div>
                <div class="value col-sm-6">
                    <textarea name="address" class="form-control">{{ $data->address }}</textarea>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group row">
            <div class="form-label col-sm-3">{{ trans('account.province') }}</div>

                        <div class="value col-sm-4">
              <label class="dropdown input-block">
                <select name="province_id" class=" form-control input-block filter-province" data-section="account"></select>
              </label>
                          </div>
          </div>

          <div class="form-group row">
            <div class="form-label col-sm-3">{{ trans('account.amphur') }}</div>

                        <div class="value col-sm-4">
              <label class="dropdown input-block">
                <select name="amphur_id" class="form-control input-block filter-amphur" data-section="account"></select>
              </label>
                          </div>
          </div>

          <div class="form-group row">
            <div class="form-label col-sm-3">{{ trans('account.district') }}</div>

                        <div class="value col-sm-4">
              <label class="dropdown input-block">
                <select name="district_id" class="form-control input-block filter-district" data-section="account"></select>
              </label>
                          </div>
          </div>

                    <div class="form-group row">
            <div class="form-label col-sm-3">{{ trans('account.zipcode') }}</div>

            <div class="value col-sm-4">
              <input type="text" name="zipcode" value="" class="form-control input input-block filter-zipcode" data-section="account">
                          </div>
          </div>
        </div>

        <div class="option">
          <button type="submit" name="submit" class="btn btn-default black">{{ trans('account.update-btn') }}</button>
        </div>
      </form>
    </div>
    <div class="col-sm-3">
      @include('frontend.account.sidebar')
    </div>
  </div>
</div>
@endsection
@section('more-script')
    {{ Html::script('js/filter-address.js') }}
    <script>
        $(document).ready(function() {
            filter_address('province','province',null,'{{ $data->province_id or '' }}',"account"); // province list
            filter_address('province','amphur','{{ $data->province_id or '' }}','{{ $data->amphur_id or '' }}',"account");
            filter_address('amphur','district','{{ $data->amphur_id or '' }}','{{ $data->district_id or '' }}',"account");
            filter_address('district','zipcode','{{ $data->district_id or '' }}','{{ $data->zipcode or '' }}',"account");

            // validatetion
            $( "[name=facebook]" ).keypress(function(e) {
              pattern = "[A-Za-z0-9.]+";
              var found = e.key.match(pattern);
              if(!found)
                return false
            });

            $( "[name=line]" ).keypress(function(e) {
              pattern = "[A-Za-z0-9._-]+";
              var found = e.key.match(pattern);
              if(!found)
                return false
            });
        });
    </script>

@endsection
