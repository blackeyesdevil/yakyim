<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Return Product</title>
</head>
<body>
@include('emails.include.header')

<div style="padding: 30px 30px; border: 1px solid #73b843; font-size: 14px">
    <b>Order Details</b><br>
    <b>วันที่ / Date:</b> {{ $orders->orders_date }}<br>
    <b>เลขที่สั่งซื้อ / Order Number:</b> {{ $orders->orders_no }}<br>

    @if($orders->payment_date != '0000-00-00 00:00:00')
        <b>วันที่ชำระเงิน / Payment Date:</b> {{ $orders->payment_date }}
    @endif
    <br><br>
    <table width="100%" >
        <tr style="vertical-align: top">
            <td width="50%">
                <b>Shipping Information :</b><br>
                <hr style="border: 1px solid #cccccc;">
                {!! $orders->shipping_address !!}
            </td>
            <td width="50%">
                <b>Billing Information :</b><br>
                <hr style="border: 1px solid #cccccc;">
                {!! $orders->billing_address !!}
            </td>
        </tr>
    </table>
    <br>
    <table width="100%" border="1" cellspacing="0" cellpadding="5" style="border-collapse: collapse; border: 1px solid #cccccc;">
        <tr style="background-color: #dddddd">
            <th>#</th>
            <th align="left">Item</th>
            <th align="center">Quantity</th>
            <th align="center">Reason</th>
            <!-- <th align="right">Total(THB)</th> -->
        </tr>
        <?php $i = 1;
          ?>
            @foreach($itemlist as $value)
                <?php
                $qty = $value['number'];
                $reason = $value['reason'];
                $data = $value['data'];
                $data = json_decode($data);
                $product_name_th = $data->product_name_th;
                ?>
            @endforeach
            <tr>
                  <td align="center">{{ $i }}</td>
                  <td >{{ $product_name_th }}</td>
                  <td align="center">{{ $qty }}</td>
                  <td align="center">{{ $reason }}</td>
            </tr>

            <?php $i++; ?>
    </table>
    <br>

</div>

@include('emails.include.footer')
</body>
</html>
