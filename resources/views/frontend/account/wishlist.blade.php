@extends('frontend.layouts/main')

@section('more-stylesheet')
  <link rel="stylesheet" href="{{ URL::asset('css/front/account-global.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('css/front/cart.css') }}">

@endsection
@section('content')
<div id="account" class="container">

  <div id="cart">
    <div id="content" class="row">
      <div class="col-sm-9">
        <h2>{{ trans('account.wish-list') }}</h2>

        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
            <tr>
              <th width="55%" colspan="2">{{ trans('account.order-product') }}</th>
              <th width="15%" class="text-right">{{ trans('product.price') }}</th>
              <th width="20%" class="text-center">{{ trans('product.view-detail') }}</th>
              <th width="10%" class="text-center">{{ trans('product.remove') }}</th>
            </tr>
            </thead>

            <tbody>
            @if(count($wishlists) > 0)
              @foreach($wishlists as $item)
                <?php
                $dir = 'uploads/product/340/';
                $img_name = $item->img_name;
                $img_dir = $dir.$img_name;

                $img_path = url()->asset($img_dir);

                if($img_name == '' || !file_exists($img_dir)){
                  $img_path = url()->asset('images/yakyim/150x200.jpg');
                }

                $thb_price = $item->retail_price;
                $dollar_price = $obj_dollar->cal_dollar($thb_price,$currency);
                ?>
                <tr>
                    <td class="product-img text-center"><img src="{{ $img_path }}" width="150px"/></td>
                    <td><h3>{{ $item['product_name_'.$lang] }}</h3></td>
                    <td class="text-right">
                      <b>
                        @if($lang == 'th')
                          THB {{ number_format($thb_price,2) }}
                        @else
                          $ {{ number_format($dollar_price,2) }}
                        @endif
                      </b>

                    </td>
                    <td class="text-center">
                      <a href="{{ url()->to('product/detail/'.$item->product_id.'/'.str_slug($item['product_name_en'])) }}" data-id="1" class="add-to-cart-btn">
                        <button type="button " class="btn btn-default ">{{ trans('product.view-detail') }}</button>
                      </a>
                    </td>
                    <td class="text-center">
                      <a href="{{ url()->to('account/wishlist/delete/'.$item->wishlist_id) }}" data-id="" class="del-btn"><img src="{{ URL::asset('images/btn-remove.png') }}" class="remove" alt=""></a>
                    </td>
                  </tr>
              @endforeach
              @endif

            </tbody>
          </table>
        </div>
      </div>

      <div class="col-sm-3">
        @include('frontend.account.sidebar')
      </div>
    </div>
  </div>
</div>

@endsection
@section('more-script')

@endsection

