<?php
$orders_no = $order->orders_no;
$status_id = $order->status_id;
?>

@extends('frontend.layouts/main')
@section('more-stylesheet')
  <link rel="stylesheet" href="{{ URL::asset('css/front/account-global.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('css/front/account-order-tracking.css') }}">
@endsection

@section('content')
<div id="account" class="container">

  <div id="content" class="row">
    <div class="col-sm-9">
      <h2>{{ trans('account.order-tracking') }}</h2>

      <div id="order-tracking">
        <div class="order-detail text-center{{ trans('global.th-font') }}">
          <div class="order_no"><strong>{{ trans('account.order-no').' '.$orders_no }}</strong></div>
          <div class="order-date"> {{ trans('account.order-tracking-date').' '.$order->orders_date }}</div>
          <div class="order-status">{{ $order->Status['status_name_'.$lang] }}</div>
          <a href="{{ url()->to('account/order-history/detail/'.$orders_no) }}" class="view-detail btn btn-default black ">{{ trans('account.view-detail') }}</a>
        </div>

        <div class="order-icons">
         
          <div class="item @if($status_id != '6' && $status_id != '5') active @endif">
            <div class="icon">
              <i class="glyphicon glyphicon-refresh"></i>
            </div>
            <div class="text{{ trans('global.th-font') }}">{{ trans('account.inprocess') }}</div>
          </div>

          <div class="line @if($status_id == '5') active @endif"></div>

          <div class="item @if($status_id == '5') active @endif">
            <div class="icon">
              <i class="glyphicon glyphicon-envelope"></i>
            </div>

            <div class="text{{ trans('global.th-font') }}">{{ trans('account.shipping') }}</div>
          </div>

          <div class="line @if($status_id == '6') active @endif"></div>

          <div class="item @if($status_id == '6') active @endif">
            <div class="icon">
              <i class="glyphicon glyphicon-ok"></i>
            </div>

            <div class="text{{ trans('global.th-font') }}">{{ trans('account.shipped') }}</div>
          </div>
        </div>

        <div class="table-responsive">
          <table class="table table-bordered">
            <tr>
              <th>{{ trans('account.date-added') }}</th>
              <th>{{ trans('account.status') }}</th>
              <th>{{ trans('account.note') }}</th>
            </tr>
            @if(count($data) > 0)
              @foreach($data as $item)
                <tr>
                  <td>{{ $item->created_at }}</td>
                  <td>{{ $item->Status['status_name_'.$lang] }}</td>
                  <td>
                    {!! $item->Status['detail_'.$lang] !!}
                    @if($item->status_id == '5' ) {{ ', Tracking Number '. $order->tracking_no }} @endif
                  </td>
                </tr>
              @endforeach
            @else
              <tr>
                <td colspan="3" class="text-center">{{ trans('account.no-tracking') }}</td>
              </tr>
            @endif
          </table>
        </div>
      </div>
    </div>

    <div class="col-sm-3">
        @include('frontend.account.sidebar')

    </div>
  </div>
</div>
@endsection
