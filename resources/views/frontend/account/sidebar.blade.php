<div id="sidebar">
        <h2>{{ trans('account.account-detail') }}</h2>

        <div class="block">
          <div class="account-info">
            <div class="detail">
              <div class="account-name"><strong>@if(session()->get('s_user_type') == '1') {!! "<i class='glyphicon glyphicon-star text-success'></i> VIP " !!} @endif {{ 'No. '.session()->get('s_user_no') }}</strong></div>
              <div class="account-name"><strong>{{ session()->get('s_user_name') }}</strong></div>
              <div class="account-email">{{ session()->get('s_user_email') }}</div>
              <div class="account-point">Point(s) {{ number_format($count_point,0) }}</div>
            </div>
          </div>
          <a @if(request()->path() == 'account/profile') class="menu-active" @endif href="{{ url()->to('account/profile') }}">{{ trans('account.edit-profile') }}</a>
          <a @if(request()->path() == 'account/change-password') class="menu-active" @endif href="{{ url()->to('account/change-password') }}">{{ trans('account.change-password') }}</a>
          <a @if(request()->path() == 'account/address-book') class="menu-active" @endif href="{{ url()->to('account/address-book') }}">{{ trans('account.address-books') }}</a>
          <a @if(request()->path() == 'account/order-history') class="menu-active" @endif href="{{ url()->to('account/order-history') }}">{{ trans('account.order-history') }}</a>
          <a @if(request()->path() == 'account/point-history') class="menu-active" @endif href="{{ url()->to('account/point-history') }}">{{ trans('account.point-history') }}</a>
          <a @if(request()->path() == 'account/order-return') class="menu-active" @endif href="{{ url()->to('account/order-return') }}">{{ trans('account.order-return') }}</a>
          <a @if(request()->path() == 'account/wish-list') class="menu-active" @endif href="{{ url()->to('account/wishlist') }}">{{ trans('account.wish-list') }}</a>
          <a href="{{ url()->to('logout') }}">{{ trans('account.logout') }}</a>
        </div>
      </div>
