<?php
$a_paysbuy = config()->get('main.paysbuy_method');
$payment_method = config()->get('constants.payment_method_'.$lang);
?>
@extends('frontend.layouts/main')
@section('more-stylesheet')
  <link rel="stylesheet" href="{{ URL::asset('css/front/account-global.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('css/front/account-order.css') }}">
@endsection

@section('content')
<div id="account" class="container">
  <div id="content" class="row">
    <div class="col-sm-9">
      <h2>{{ trans('account.order-return') }}</h2>


      <div id="order" class="table-responsive">
        <table class="table table-bordered">
          <thead>
            <tr>
                <th class="text-center">{{ trans('account.order-no') }}</th>
                <th class="text-center">{{ trans('account.return-date') }}</th>
                <th class="text-center">{{ trans('account.status-refund') }}</th>
                <th class="text-center">{{ trans('account.view-detail') }}</th>
                <!-- <th nowrap class="text-right">{{ trans('account.total-price') }}</th> -->

            </tr>
          </thead>

          <tbody>

            @if(!empty($data_returns))

                @foreach($data_returns as $key => $item)

                    <tr>
                        <td class="text-center">{{ $item->orders_no }}</td>
                        <td class="order-date text-center">{{$objFn->format_date_en($item->created_at,4)}}</td>
                        <td class="text-center">
                          <div class="status">

                            @if( $item->status == 1)
                                  {{ trans('account.status_return')}}
                            @elseif( $item->status == 2)
                              {{ trans('account.received-product')}}
                            @elseif( $item->status == 3)
                              {{ trans('account.transfer-completed')}}
                            @endif

                          </div>
                        </td>

                        @if($item->orders_no != '')
                          <td class="text-center">
                              <a href="{{ url()->to('account/order-return/returndetail/'.$item->orders_no) }}"><button class="btn btn-default black">{{ trans('account.view-detail') }}</button></a>
                          </td>
                        @else
                        <td class="text-center">
                            <a href="{{ url()->to('account/order-return/returndetailorderno/') }}"><button class="btn btn-default black">{{ trans('account.view-detail') }}</button></a>
                        </td>
                        @endif
                          <!-- @if($item->status_return_product == 1) -->
                            <!-- <a href="{{ url()->to('account/order-return/long/'.$item->orders_no) }}"><button class="btn btn-default black">{{ trans('account.view-detail') }}</button></a> -->
                          <!-- @elseif($item->status == 2) -->
                          <!-- <a href="{{ url()->to('account/order-return/returndetail/'.$item->orders_no) }}"><button class="btn btn-default black">{{ trans('account.view-detail') }}</button></a> -->

                            <!-- <a href="{{ url()->to('account/order-return/detail/'.$item->orders_no) }}"><button class="btn btn-default black">{{ trans('account.view-detail') }}</button></a><br><br>
                            <a href="{{ url()->to('account/order-return/detail/'.$item->orders_no) }}"><button class="btn btn-default black">เลขพัสดุ</button></a> -->
                          <!-- @endif -->
                        <!-- </td> -->
                    </tr>

                @endforeach
            @else
                <tr>
                    <!-- <td colspan="8" class="text-center">{{ trans('account.no-order') }}</td> -->
                    <td colspan="8" class="text-center">{{ trans('account.no-return-items') }}</td>
                </tr>
            @endif
          </tbody>
        </table>
      </div>

    </div>

    <div class="col-sm-3">
      @include('frontend.account.sidebar')
    </div>
  </div>
</div>
@endsection
3
