<?php
$payment_method = config()->get('constants.payment_method_'.$lang);
?>
@extends('frontend.layouts/main')

@section('title','Order Return Product confirm')

@section('more-stylesheet')
  <link rel="stylesheet" href="{{ URL::asset('css/front/account-global.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('css/front/account-order-return.css') }}">
@endsection

@section('content')
<div id="account" class="container">
  <div id="content" class="row">
    <div class="col-sm-9">
        <div id="breadcrumb">
          <!-- <a href="{{ URL::route('home') }}">{{ trans('global.home') }}</a>
          <span class="arrow">></span>
          <a href="{{ URL::to('account') }}">{{ trans('global.my-account') }}</a>
          <span class="arrow">></span> -->
          <a href="{{ URL::to('account/order-return') }}">{{ trans('account.order-return') }}</a>
          <span class="arrow">></span>

          <span class="arrow">></span>
          <span class="active">ส่งเรืองเรียบร้อย</span>

        </div>
        <h1>ส่งเรืองคืนสินค้าเรียบร้อย</h1>


        <?php
          // $arrayJson = json_encode($arrayJson);
        ?>
          เมื่อบริษัทได้รับสินค้าแล้วจะดำเนินการคืนเงินภายใน 15 วัน นับจากวันที่ได้รับสินค้าจากลูกค้า<br><br><br>



          <a href="{{ url()->to('account/order-return') }}"><button class="btn btn-default black">กลับหน้าแรก</button></a>


    </div>


    <div class="col-sm-3">
      @include('frontend.account.sidebar')
    </div>
  </div>
</div>
@endsection
@section('more-script')
  <!-- <script src="{{ URL::asset('tools/bxslider/jquery.bxslider.js') }}"></script> -->
  <script src="{{ URL::asset('js/jquery-ui/jquery-ui.min.js') }}"></script>
  <script src="{{ URL::asset('js/front/return-product.js') }}"></script>
@endsection
