<?php
$payment_method = config()->get('constants.payment_method_'.$lang);
?>
@extends('frontend.layouts/main')

@section('title','Order Return Product')

@section('more-stylesheet')
  <link rel="stylesheet" href="{{ URL::asset('css/front/account-global.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('css/front/account-order-return.css') }}">
@endsection

@section('content')
@if($emyty_detail == '')
<div id="account" class="container">
  <div id="content" class="row">
    <div class="col-sm-9">
      <div id="breadcrumb">
        <!-- <a href="{{ URL::route('home') }}">{{ trans('global.home') }}</a>
        <span class="arrow">></span>
        <a href="{{ URL::to('account') }}">{{ trans('global.my-account') }}</a>
        <span class="arrow">></span> -->

        <a href="{{ URL::to('account/order-return') }}">{{ trans('account.order-return') }}</a>
        <span class="arrow">></span>

        <span class="active">{{ trans('account.view-order-detail') }} #{{ $order->orders_no }}</span>
      </div>
      <h2>{{ trans('account.list-product')}}</h2>

      <div id="summary" class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr>
                  <th class="text-center">#</th>
                  <th class="text-center">รหัสสินค้า</th>
                  <th class="text-center">รูปภาพ</th>
                  <th class="text-center">{{ trans('account.order-product') }}</th>
                  <th class="text-center">ขนาด</th>
                  <th class="text-center">ราคา</th>
                  <!-- <th class="text-center">{{ trans('account.order-product')}}</th> -->
                  <th class="text-center">{{ trans('account.order-quantity')}}</th>
                  <th class="text-center">{{ trans('account.order-reason')}}</th>
              </tr>
            </thead>
            <tbody>
              <?php $i = 1;?>
            
              @foreach($return_detail as $key => $return)
                @foreach($order_detail as $key => $order)

              <tr>
                <?php $order_return_id = $return['order_return_id'] ?>
                <?php

                    $qty = $order->qty;
                    $model = $order->model;
                    $product_gallery = $order->product_gallery;
                    $product_option = $order->product_option;
                    $unit_price = $order->unit_price;
                    $p_total_price_th = $qty * $unit_price;

                    $dollar_unit_price = $order->dollar_unit_price;
                    $p_total_price_en = $qty * $dollar_unit_price;
                ?>
                    @if($return['product_id'] == $order['product_id'])
                    <td class="text-center">
                      {{$i}}
                    </td>
                    <td class="text-center">
                      {{$model}}
                    </td>
                    <td class="text-center">
                      <?php
                        if ($product_gallery->photo !='' && file_exists('uploads/product/'.$product_gallery->photo))
                            $img_url = URL::asset('uploads/product/' . $product_gallery->photo);
                        else
                            $img_url = URL::asset('images/logo.jpg');
                       ?>
                       <img src="{{$img_url}}" class="text-center" style="height:100px;">


                    </td>

                    <td class="text-center">
                      {{$order['product_name_'.$lang]}}
                    </td>
                    <td class="text-center">
                      {{ $product_option['option_name_' . session()->get('locale')] }}
                    </td>
                    @if( $lang == 'th')
                      <!-- <td class="text-center">{{ $p_total_price_th }}</td> -->
                      <td class="text-center">{{ $return['qty'] * $unit_price }}</td>
                    @else
                      <td class="text-center">{{ $return['qty'] * $dollar_unit_price }}</td>
                    @endif
                    <td class="text-center">
                      {{$return['qty']}}
                    </td>
                    <td>
                      {{$return['reason']}}
                    </td>
                      <?php $i++;?>
                    @endif

              </tr>
              @endforeach
            @endforeach
            </tbody>
          </table>
          {{ trans('account.address-yakyim')}} :
          <a href="{{ url()->to('account/order-return/print/'.$order_return_id) }}" target="_blank"><button class="btn btn-default">Print</button></a>
      </div>

      <form name="myForm"action="{{ URL::to('account/order-return-refund-product-detail')}}" method="post" onsubmit="return validateForm()" enctype="multipart/form-data">
        <h4>{{ trans('account.upload-tracking')}}</h4>
        <input type="file" id="img_path" name="img_path" class="custom-file-input">
        <span class="custom-file-control"></span>
          @if($errors->first('img_path'))
            <div class="text-danger">กรุณาใส่อัพโหลดรูป</div>
          @endif
        <h4>{{ trans('account.account-number')}}</h4>
        <div class="col-md-3">
          <select name="bank" class="form-control select2-container form-control select2me" data-placeholder="Select...">
              <option value="">ธนาคาร ...</option>
                  <option value="ธนาคารกรุงเทพ">ธนาคารกรุงเทพ</option>
                  <option value="ธนาคารกสิกรไทย">ธนาคารกสิกรไทย</option>
                  <option value="ธนาคารกรุงไทย">ธนาคารกรุงไทย</option>
                  <option value="ธนาคารกรุงศรีอยุธยา">ธนาคารกรุงศรีอยุธยา</option>
                  <option value="ธนาคารธนชาต">ธนาคารธนชาต</option>
                  <option value="ธนาคารทหารไทย">ธนาคารทหารไทย</option>
                  <option value="ธนาคารออมสิน">ธนาคารออมสิน</option>
                  <option value="ธนาคารไทยพาณิชย์">ธนาคารไทยพาณิชย์</option>
                  <option value="ธนาคารซีไอเอ็มบีไทย">ธนาคารซีไอเอ็มบีไทย</option>
                  <option value="ธนาคารเกียรตินาคิน">ธนาคารเกียรตินาคิน</option>
                  <option value="ธนาคารทิสโก้">ธนาคารทิสโก้</option>
                  <option value="ธนาคารยูโอบี">ธนาคารยูโอบี</option>
                  <option value="ธนาคารสแตนดาร์ดชาร์เตอร์ด">ธนาคารสแตนดาร์ดชาร์เตอร์ด (ไทย)</option>
                  <option value="ธนาคารอาคารสงเคราะห์">ธนาคารอาคารสงเคราะห์</option>
          </select>
        </div>
        <div class="col-md-9">
        </div>
        <!-- <div class="col-md-3"> -->
        <br><br><input type="text" id="account_number" name="account_number" class="" maxlength="10" style="margin-left:15px;padding-left:16px;"><br>
          <!-- @if($errors->first('account_number'))
            <div class="text-danger">กรุณาใส่เลขที่บัญชี</div>
          @endif -->
        @foreach($order_detail as $key => $order)
          <input type="hidden" name="order_id" value="{{$order['orders_id']}}">
        @endforeach
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <br><a><button class="btn btn-default black">ตกลง</button></a>
        <a href="{{URL::to('account/order-return')}}" class="btn btn-default black">กลับ</a>

      </form>
      <!-- <br> -->
    <!-- </div> -->

      <br>หมายเหตุ<br>
          พบปัญหาติดต่อ Support@Yakyim.com
    </div>



  </div>
</div>
@else
<div id="account" class="container">
  <div id="content" class="row">
    <div class="col-sm-9">
      <div id="breadcrumb">
<h2>{{ trans('account.list-product')}}</h2>
<div id="summary" class="table-responsive">
    <table class="table table-bordered">
      <thead>
        <tr>
            <th class="text-center">{{$emyty_detail}}</th>
            <!-- <th class="text-center">{{ trans('account.order-product')}}</th>
            <th class="text-center">{{ trans('account.order-quantity')}}</th>
            <th class="text-center">{{ trans('account.order-reason')}}</th> -->
        </tr>
      </thead>
      <tbody>

      </tbody>

    </table>
  </div>
  </div>
  </div>
  <div class="col-sm-3">
    @include('frontend.account.sidebar')
  </div>
  </div>
  </div>

@endif
@endsection
@section('more-script')
  <!-- <script src="{{ URL::asset('tools/bxslider/jquery.bxslider.js') }}"></script> -->\
  <script>
  function validateForm() {
      var x = document.forms["myForm"]["account_number"].value;
      var p = document.forms["myForm"]["img_path"].value;
      if (x == null || x == "") {
          alert("กรุณาใส่หมายเลขบัญชีของท่านค่ะ");
          return false;
      }
      if (p == null || p == "") {
          alert("กรุณาอัพโหลดใบเสร็จเลขที่พัสดุ");
          return false;
      }

  }
  </script>
  <script src="{{ URL::asset('js/jquery-ui/jquery-ui.min.js') }}"></script>
  <script src="{{ URL::asset('js/front/return-product.js') }}"></script>
@endsection
