<?php
$a_paysbuy = config()->get('main.paysbuy_method');
$payment_method = config()->get('constants.payment_method_'.$lang);
?>
@extends('frontend.layouts/main')
@section('more-stylesheet')
  <link rel="stylesheet" href="{{ URL::asset('css/front/account-global.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('css/front/account-order.css') }}">
@endsection

@section('content')
<div id="account" class="container">
  <div id="content" class="row">
    <div class="col-sm-9">
      <h2>{{ trans('account.order-history') }}</h2>
      @if(session()->has('successMsg'))
          <div class="alert alert-success text-center">{!! session()->get('successMsg') !!}</div>
      @endif
      <div id="order" class="table-responsive">
        <table class="table table-bordered">
          <thead>
            <tr>
                <th class="text-center">{{ trans('account.order-no') }}</th>
                <th class="text-center">{{ trans('account.order-date') }}</th>
                <th>{{ trans('account.order-status') }}</th>
                <th nowrap class="text-right">{{ trans('account.total-price') }}</th>
                <th></th>
            </tr>
          </thead>

          <tbody>
            @if(count($data)>0)
                @foreach($data as $key => $item)

                    <tr>
                        <td class="text-center">{{ $item->orders_no }}</td>
                        <td class="order-date text-center">{{ $objFn->format_date_en($item->orders_date,4) }}</td>
                        <td>
                          <div class="status">
                              {{ $payment_method[$item->payment_method_id] }} /
                              <span class="@if($item->status_id == '7' || $item->status_id == '8' || $item->status_id == '9') red @endif">{{ $item->Status['status_name_'.$lang] }}</span>
                          </div>
                        </td>
                        <?php
                            $grand_total = $item->total_price + $item->shipping_price - $item->discount_price - $item->use_point;
                            $dollar_grand_total = $item->dollar_total_price + $item->dollar_shipping_price - $item->dollar_discount_price - $item->dollar_use_point;

                        ?>
                        <td class="text-right">
                            @if($lang == 'th')
                                {{ 'THB '. number_format($grand_total, 2) }}
                            @else
                                {{ '$ '. number_format($dollar_grand_total, 2) }}
                            @endif
                        </td>
                        <td class="text-center">
                            @if($item->payment_method_id == '1' && $item->payment_inform_id == null && $item->status_id == '2')
                                <a href="{{ url()->to('account/order-inform/'.$item->orders_no) }}"><button class="btn btn-default">{{ trans('account.transfer-inform') }}</button></a><br>
                            @endif
                            @if($item->payment_method_id == '2' && $item->status_id == '2')
                                <a href="{{ url()->to('account/order-payment/' . $item->orders_no) }}"><button class="btn btn-default">{{ trans('account.pay-now') }}</button></a><br>
                            @endif
                            @if($item->status_id == 6 && $item->status_return_product == 1)
                                <a href="{{ url()->to('account/order-return/detail/'.$item->orders_no) }}"><button class="btn btn-default">{{ trans('account.order-refund')}}</button></a><br>
                            @endif
                            <a href="{{ url()->to('account/order-tracking/'.$item->orders_no) }}"><button class="btn btn-default">{{ trans('account.view-order-status') }}</button></a><br>
                            <a href="{{ url()->to('account/order-history/detail/'.$item->orders_no) }}"><button class="btn btn-default black">{{ trans('account.view-detail') }}</button></a>

                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="8" class="text-center">{{ trans('account.no-order') }}</td>
                </tr>
            @endif
          </tbody>
        </table>
      </div>
        @if(count($data) > 0)
            {!! $data->render() !!}
        @endif
    </div>

    <div class="col-sm-3">
      @include('frontend.account.sidebar')
    </div>
  </div>
</div>
@endsection
