<?php
$payment_method = config()->get('constants.payment_method_'.$lang);
?>
@extends('frontend.layouts/main')

@section('title','Order History')

@section('more-stylesheet')
  <link rel="stylesheet" href="{{ URL::asset('css/front/account-global.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('css/front/account-order-detail.css') }}">
@endsection

@section('content')
<div id="account" class="container">
  <div id="content" class="row">
    <div class="col-sm-9">
      <div id="breadcrumb">
        <a href="{{ URL::to('account/order-history') }}">{{ trans('account.order-history') }}</a>
        <span class="arrow">></span>

        <span class="active">{{ trans('account.view-order-detail') }} #{{ $order->orders_no }}</span>
      </div>

      <div class="headline{{ trans('global.th-font') }}">{{ trans('account.view-order-detail') }} #{{ $order->orders_no }}</div>
      <div id="order-detail" class="row">
        <div class="table-responsive col-sm-6">
          <table class="table table-bordered">
            <tr>
              <th>{{ trans('account.order-detail') }}</th>
            </tr>
            <tr>
              <td>
                <div class="row">
                  <div class="topic col-xs-5">{{ trans('account.order-date') }}</div>
                  <div class="value col-xs-7">{{ $objFn->format_date_en($order->orders_date,4) }}</div>
                </div>

                <div class="row">
                  <div class="topic col-xs-5">{{ trans('account.order-no') }}</div>
                  <div class="value col-xs-7">{{ $order->orders_no }}</div>
                </div>

                <div class="row">
                  <div class="topic col-xs-5">{{ trans('account.payment-method') }}</div>
                  <div class="value col-xs-7">{{ $payment_method[$order->payment_method_id] }}</div>
                </div>

                <div class="row">
                  <div class="topic col-xs-5">{{ trans('account.status') }}</div>
                  <div class="value col-xs-7">{{ $order->Status['status_name_' . $lang] }}</div>
                </div>

                  @if($order->status_id == '3')
                <div class="row">
                  <div class="topic col-xs-5">{{ trans('account.payment-date') }}</div>
                  <div class="value col-xs-7">{{ $objFn->format_date_en($order->payment_date,4) }}</div>
                </div>
                @endif
              </td>
            </tr>
          </table>
        </div>
      </div>

      <div id="address" class="row">
        <div class="col-sm-6">
          <div class="block">
            <div class="sub-headline">{{ trans('account.shipping-address') }}</div>

            <div class="item">
              {!! $order->shipping_address !!}
            </div>
          </div>
        </div>

        <div class="col-sm-6">
          <div class="block">
            <div class="sub-headline">{{ trans('account.billing-address') }}</div>

            <div class="item">
              {!! $order->billing_address !!}
            </div>
          </div>
        </div>
      </div>

      <div id="summary" class="table-responsive">
        <table class="table table-bordered">
            <tr>
                <th class="text-center">#</th>
                <th class="text-center"></th>
                <th>{{ trans('account.order-product') }}</th>
                <th class="text-center">{{ trans('account.order-quantity') }}</th>
                <th class="text-right">{{ trans('account.order-unit-cost') }}</th>
                <th class="text-right">{{ trans('account.order-product-sub-total') }}</th>
            </tr>

            @foreach($order_detail as $key => $item)
                <tr>
                    <?php
                        $qty = $item->qty;
                        $unit_price = $item->unit_price;
                        $p_total_price = $qty * $unit_price;

                        $dollar_unit_price = $item->dollar_unit_price;
                        $p_dollar_total_price = $qty * $dollar_unit_price;

                        $dir = 'uploads/product/100/';
                        $img_name = $item->img_name;
                        $img_path = url()->asset($dir.$img_name);
                        if($img_name == null || !file_exists($dir.$img_name)){
                            $img_path = url()->asset('images/yakyim/100x150.jpg');
                        }
                    ?>
                    <td class="text-center">{{ $key+1 }}</td>
                    <td class="text-center"><img src="{{ $img_path }}" style="max-width: 100%"></td>
                    <td>
                        {{ $item['product_name_'.$lang] }}<br>
                        {{ "Model : ".$item['model'] }}
                    </td>
                    <td class="text-center">{{ $qty }}</td>
                    <td class="text-right">
                        @if( $lang == 'th' )
                            {{ number_format($unit_price ,2) }}
                        @else
                            {{ number_format($dollar_unit_price ,2) }}
                        @endif
                    </td>
                    <td class="text-right">
                        @if( $lang == 'th' )
                            {{ number_format($p_total_price ,2) }}
                        @else
                            {{ number_format($p_dollar_total_price ,2) }}
                        @endif
                    </td>
                </tr>
            @endforeach
            <?php
                $total_price = $order->total_price;
                $discount_price = $order->discount_price;
                $shipping_price = $order->shipping_price;
                $use_point = $order->use_point;
                $grand_price = $total_price - $discount_price + $shipping_price - $use_point;

                if($lang == 'en'){
                    $total_price = $order->dollar_total_price;
                    $discount_price = $order->dollar_discount_price;
                    $shipping_price = $order->dollar_shipping_price;
                    $use_point = $order->dollar_use_point;
                    $grand_price = $total_price - $discount_price + $shipping_price - $use_point;
                }
            ?>
            <tr>
                <th colspan="5" class="text-right">{{ trans('account.order-sub-total') }}</th>
                <th class="text-right">
                    {{ number_format($total_price ,2) }}
                </th>
            </tr>

            @if ($discount_price > 0)
            <tr>
                <th colspan="5" class="text-right text-success">{{ trans('account.order-discount').' [ '.$log_code->discount_code.' ]' }}</th>
                <th class="text-right text-success">
                    {{ '- ' .number_format($discount_price ,2) }}
                </th>
            </tr>
            @endif

            @if ($shipping_price > 0)
            <tr>
                <th colspan="5" class="text-right text-danger">{{ trans('account.order-shipping-cost') }}</th>
                <th class="text-right text-danger">
                    {{ '+ ' .number_format($shipping_price ,2) }}
                </th>
            </tr>
            @endif
            @if ($use_point > 0)
                <tr>
                    <th colspan="5" class="text-right text-success">{{ trans('account.order-use-point') }}</th>
                    <th class="text-right text-success">
                        {{ '- ' .number_format($use_point ,2) }}
                    </th>
                </tr>
            @endif
            <tr>
                <th colspan="5" class="text-right">{{ trans('account.order-total') }}</th>
                <th class="text-right">
                    {{ number_format($grand_price ,2) }}
                </th>
            </tr>
        </table>
      </div>
    </div>

    <div class="col-sm-3">
      @include('frontend.account.sidebar')
    </div>
  </div>
</div>
@endsection
