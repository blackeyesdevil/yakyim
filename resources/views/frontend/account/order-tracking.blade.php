<?php
$lang = session()->get('locale');
$a_paysbuy = config()->get('main.paysbuy_method');
?>
@extends('frontend.layouts/main')

@section('title','Order Tracking')

@section('more-stylesheet')
  <link rel="stylesheet" href="{{ URL::asset('css/front/account-global.css') }}">
@endsection

@section('content')
<div id="account" class="container">

  <div id="content" class="row">
    <div class="col-sm-9">
      <h2>{{ trans('account.order-tracking') }}</h2>

      @if(session()->has('successMsg'))
          <div class="alert alert-success text-center">{!! session()->get('successMsg') !!}</div>
      @endif

      <form action="{{ URL::to('account/order-tracking/check') }}" method="get">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        @if(session()->has('successMsg'))
          <div class="alert alert-success text-center">{!! session()->get('successMsg') !!}</div>
        @endif

        @if(session()->has('errorMsg'))
          <div class="alert alert-danger text-center">{!! session()->get('errorMsg') !!}</div>
        @endif

        <?php $field = 'email'; ?>
        <div class="form-group row">
          <div class="form-label col-sm-3">{{ trans('account.' . $field) }}</div>

          <div class="value col-sm-5">
            <input type="text" name="{{ $field }}" placeholder="{{ trans('account.' . $field) }}" class="form-control input input-block">
            @if($errors->first($field))
              <div class="text-danger">{{ $errors->first($field) }}</div>
            @endif
          </div>
        </div>

        <?php $field = 'order_no'; ?>
        <div class="form-group row">
          <div class="form-label col-sm-3">{{ trans('account.order-no') }}</div>

          <div class="value col-sm-5">
            <input type="text" name="{{ $field }}" placeholder="{{ trans('account.order-no') }}" class="form-control input input-block">
            @if($errors->first($field))
              <div class="text-danger">{{ $errors->first($field) }}</div>
            @endif
          </div>
        </div>

        <div class="option">
          <button type="submit" class="btn btn-default">{{ trans('account.check') }}</button>
        </div>
      </form>
    </div>

    <div class="col-sm-3">
      @if (!empty(session()->get('s_user_id')))
        @include('frontend.account.sidebar')
      @endif
    </div>
  </div>
</div>
@endsection
