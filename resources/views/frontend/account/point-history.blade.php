@extends('frontend.layouts/main')
@section('more-stylesheet')
  <link rel="stylesheet" href="{{ URL::asset('css/front/account-global.css') }}">
  <style>
  .font-green{ color: #73b843; }
  .font-red{ color: #990000; }
  </style>
@endsection

@section('content')
<div id="account" class="container">
  <div id="content" class="row">
    <div class="col-sm-9">
      <h2>{{ trans('account.point-history') }}</h2>

      <h4>{{ trans('account.current-point') }}: &nbsp;&nbsp; {{ $current_point . ' ' . trans('account.point') }}</h4>

      <div id="point" class="table-responsive">
        <table class="table table-bordered">
          <thead>
            <tr>
                <th class="text-center">#</th>
                <th class="text-center">{{ trans('account.point-date') }}</th>
                <th>{{ trans('account.point-detail') }}</th>
                <th class="text-center">{{ trans('account.point-amount') }}</th>
                <th class="text-center">{{ trans('account.point-total') }}</th>
            </tr>
          </thead>

          <tbody>
            @if(count($data)>0)
                @foreach($data as $key => $item)
                    <tr>
                        <td class="text-center">{{ $key+1 }}</td>
                        <td class="text-center">{{ $objFn->format_date_en($item->created_at, 4) }}</td>
                        <td>{{ $item->detail }}</td>
                        <td class="text-center {{ ($item->point >= 0)?'font-green':'font-red' }}">{{ ($item->point >= 0)?'+':'-' }} {{ abs($item->point) }}</td>
                        <td class="text-center">{{ $item->total }}</td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="8" class="text-center">{{ trans('account.no-point-transaction') }}</td>
                </tr>
            @endif
          </tbody>
        </table>
      </div>
        @if(count($data) > 0)
            {!! $data->render() !!}
        @endif
    </div>

    <div class="col-sm-3">
      @include('frontend.account.sidebar')
    </div>
  </div>
</div>
@endsection
