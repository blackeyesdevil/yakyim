@extends('frontend.layouts/main')

@section('more-stylesheet')
<link rel="stylesheet" href="{{ URL::asset('css/front/account-global.css') }}">
<link rel="stylesheet" href="{{ URL::asset('css/front/account-address-book.css') }}">
@endsection

@section('content')

    <div id="content">
  
      <div id="account" class="container">
        <div id="content" class="row">
          <div class="col-sm-9">
            <h2>{{ trans('account.address-book') }}</h2>

            <div id="address-book" class="row">
              <div class="col-sm-6">
                <div class="block">
                  <div class="sub-headline th">{{ trans('account.billing-address') }}</div>
                    @if(empty($billing))
                        <a href="{{ url()->to('account/address-book/add/billing') }}" class="add-btn"><button class="btn btn-default btn-lookbook black"><i class="fi-plus"></i> {{ trans('account.add-address') }}</button></a>
                        <div class="item text-center">{{ trans('account.no-billing') }}</div>
                    @else
                    <div class="item">
                        <div class="name">{{ $billing->firstname.' '.$billing->lastname }}</div>
                        <div class="address">
                            {{ $billing->address }}<br>
                            {{ $billing->District['district_name_'.$lang].' '.$billing->Amphur['amphur_name_'.$lang] }}<br>
                            {{ $billing->Province['province_name_'.$lang] }}<br>
                            {{ $billing->zipcode }}
                        </div>
                        <div class="tax_id"><strong>TAX ID: </strong>{{ $billing->tax_id }}</div>
                        <div class="tel"><strong>TEL. </strong>{{ $billing->mobile }}</div>

                    <div class="option">
                      <a href="{{ url()->to('account/address-book/update/billing/'.$billing->address_book_id) }}"><button class="btn btn-default">{{ trans('account.edit') }}</button></a>
                    </div>
                  </div>
                @endif
                </div>
              </div>

              <div class="col-sm-6">
                <div class="block">
                  <div class="sub-headline th">{{ trans('account.shipping-address') }}</div>
                    <a href="{{ url()->to('account/address-book/add/shipping') }}" class="add-btn"><button class="btn btn-default btn-lookbook black"><i class="fi-plus"></i> {{ trans('account.add-address') }}</button></a>
                    <div class="list">
                        @if (count($shipping) > 0)
                            @foreach($shipping as $item)
                                <div class="item">
                                    <div class="name">{{ $item->firstname.' '.$item->lastname }}</div>
                                    <div class="address">
                                        {{ $item->address }}<br>
                                        {{ $item->District['district_name_'.$lang].' '.$item->Amphur['amphur_name_'.$lang] }}<br>
                                        {{ $item->Province['province_name_'.$lang] }}<br>
                                        {{ $item->zipcode }}
                                    </div>
                                    <div class="tel"><strong>TEL. </strong>{{ $item->mobile }}</div>
                                    <div class="option">
                                        <a href="{{ url()->to('account/address-book/update/shipping/'.$item->address_book_id) }}"><button class="btn btn-default">{{ trans('account.edit') }}</button></a>
                                        <a href="{{ url()->to('account/address-book/delete/'.$item->address_book_id) }}"><button class="btn btn-default btn-lookbook black">{{ trans('account.remove') }}</button></a>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="item text-center">{{ trans('account.no-shipping') }}</div>
                        @endif
                    </div>

                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-3">
            @include('frontend.account.sidebar')
          </div>
        </div>
      </div>
    </div>
@endsection
@section('more-script')
@endsection
