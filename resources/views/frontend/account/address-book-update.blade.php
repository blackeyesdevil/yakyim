@extends('frontend.layouts/main')
@section('more-stylesheet')
  <link rel="stylesheet" href="{{ URL::asset('css/front/account-global.css') }}">
@endsection

@section('content')
<div id="account" class="container">
  <div id="content" class="row">
    <div class="col-sm-9">
      <div class="headline{{ trans('global.th-font') }}">{{ trans('account.' . $mode . '-' . $billing_shipping) }}</div>

      <form action="{{ URL::to('account/address-book/check') }}" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="billing_shipping" value="{{ $billing_shipping }}">
        <input type="hidden" name="mode" value="{{ $mode }}">
        <input type="hidden" name="address_book_id" value="{{ $data->address_book_id or '' }}">

        @if(session()->has('successMsg'))
          <div class="alert alert-success text-center">{!! session()->get('successMsg') !!}</div>
        @endif

        @if(session()->has('errorMsg'))
          <div class="alert alert-danger text-center">{!! session()->get('errorMsg') !!}</div>
        @endif

        <div class="form-group row">
          <div class="form-label col-sm-3">{{ trans('account.firstname').'-'.trans('account.lastname') }}</div>

          <?php $field = 'firstname'; ?>
          <div class="value col-xs-6 col-sm-4">
            <input type="text" name="{{ $field }}" value="{{ $data[$field] or '' }}" placeholder="{{ trans('account.' . $field) }}" class="input input-block">
            @if($errors->first($field))
              <div class="text-danger">{{ $errors->first($field) }}</div>
            @endif
          </div>

          <?php $field = 'lastname'; ?>
          <div class="value col-xs-6 col-sm-4">
            <input type="text" name="{{ $field }}" value="{{ $data[$field] or '' }}" placeholder="{{ trans('account.' . $field) }}" class="input input-block">
            @if($errors->first($field))
              <div class="text-danger">{{ $errors->first($field) }}</div>
            @endif
          </div>
        </div>

        <?php $field = 'address'; ?>
        <div class="form-group row">
          <div class="form-label col-sm-3">{{ trans('account.address') }}</div>

          <div class="value col-sm-4">
            <input type="text" name="{{ $field }}" value="{{ $data[$field] or '' }}" placeholder="{{ trans('account.address') }}" class="input input-block">
            @if($errors->first($field))
              <div class="text-danger">{{ $errors->first($field) }}</div>
            @endif
          </div>
        </div>

        <div class="form-group row">
          <div class="form-label col-sm-3">{{ trans('account.province') }}</div>

          <?php $field = 'province_id'; ?>
          <div class="value col-sm-4">
            <label class="dropdown input-block">
              <select name="{{ $field }}" class="input-block filter-province" data-section="address"></select>
            </label>
            @if($errors->first($field))
              <div class="text-danger">{{ $errors->first($field) }}</div>
            @endif
          </div>
        </div>

        <div class="form-group row">
          <div class="form-label col-sm-3">{{ trans('account.amphur') }}</div>

          <?php $field = 'amphur_id'; ?>
          <div class="value col-sm-4">
            <label class="dropdown input-block">
              <select name="{{ $field }}" class="input-block filter-amphur" data-section="address"></select>
            </label>
            @if($errors->first($field))
              <div class="text-danger">{{ $errors->first($field) }}</div>
            @endif
          </div>
        </div>

        <div class="form-group row">
          <div class="form-label col-sm-3">{{ trans('account.district') }}</div>

          <?php $field = 'district_id'; ?>
          <div class="value col-sm-4">
            <label class="dropdown input-block">
              <select name="{{ $field }}" class="input-block filter-district" data-section="address"></select>
            </label>
            @if($errors->first($field))
              <div class="text-danger">{{ $errors->first($field) }}</div>
            @endif
          </div>
        </div>

        <?php $field = 'zipcode'; ?>
        <div class="form-group row">
          <div class="form-label col-sm-3">{{ trans('account.' . $field) }}</div>

          <div class="value col-sm-4">
            <input type="text" id="zipcode" name="{{ $field }}" value="{{ $data[$field] or '' }}" placeholder="{{ trans('account.' . $field) }}" class="input input-block filter-zipcode" data-section="address">
            @if($errors->first($field))
              <div class="text-danger">{{ $errors->first($field) }}</div>
            @endif
          </div>
        </div>

        <?php $field = 'mobile'; ?>
        <div class="form-group row">
          <div class="form-label col-sm-3">{{ trans('account.' . $field) }}</div>

          <div class="value col-sm-4">
            <input type="text" name="{{ $field }}" value="{{ $data[$field] or '' }}" placeholder="{{ trans('account.' . $field) }}" class="input input-block" maxlength="10">
            @if($errors->first($field))
              <div class="text-danger">{{ $errors->first($field) }}</div>
            @endif
          </div>
        </div>

        @if($billing_shipping == "billing")
        <input type="hidden" name="set_default" value="1">

        <?php $field = 'tax_id'; ?>
        <div class="form-group row">
          <div class="form-label col-sm-3">{{ trans('account.' . $field) }}</div>

          <div class="value col-sm-4">
            <input type="text" name="{{ $field }}" value="{{ $data[$field] or '' }}" placeholder="{{ trans('account.' . $field) }}" class="input input-block" maxlength="13">
            @if($errors->first($field))
              <div class="text-danger">{{ $errors->first($field) }}</div>
            @endif
          </div>
        </div>
        @endif

        <div class="option">
          <a href="{{ URL::asset('account/address-book') }}"><button type="button" class="-default">{{ trans('checkout.go-back') }}</button></a>
          <button type="submit" name="submit" class="-alt">{{ trans('account.' . $mode . '-address') }}</button>
        </div>
      </form>
    </div>

    <div class="col-sm-3">
      @include('frontend.account.sidebar')
    </div>
  </div>
</div>
@endsection
@section('more-script')
  {{ Html::script('js/filter-address.js') }}
  <script>
    $(document).ready(function() {
      filter_address('province','province',null,'{{ $data->province_id or '' }}','address');
      filter_address('province','amphur','{{ $data->province_id or '' }}','{{ $data->amphur_id or '' }}','address');
      filter_address('amphur','district','{{ $data->amphur_id or '' }}','{{ $data->district_id or '' }}','address');
      filter_address('district','zipcode','{{ $data->district_id or '' }}','{{ $data->zipcode or '' }}','address');
    });
  </script>
@endsection
