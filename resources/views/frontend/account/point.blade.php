@extends('frontend.layouts/main')

@section('title','Point Transaction')

@section('more-stylesheet')
  <link rel="stylesheet" href="{{ URL::asset('css/front/home.css') }}">
@endsection

@section('content')
  <div id="home-member">
    <div class="container">
      <div class="content col-xs-12">
        <div class="headline">Point Transaction</div>
        <div>
          @if(count($points) > 0)
            <?php $total_point = 0; ?>
            @foreach($points as $point)
              {!! $point->detail.' '.$point->point.' '.$point->total.'<br>' !!}
            @endforeach
          @else
            {{ 'No Result' }}
          @endif
        </div>

      </div>
    </div>
  </div>



@endsection
