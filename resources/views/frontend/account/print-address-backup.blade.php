<?php
$payment_method = config()->get('constants.payment_method_'.$lang);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Order Detail</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
    @include('emails.include.header')
    <div style="padding: 30px 30px; border: 1px solid #73b843; font-size: 14px">
                <div class="row">
                    <div class="col-xs-6">
                      <?php
                        $date = date('d-m-Y');
                      ?>
                      วันที่ {{$date}}<br>
                      เลขที่สั่งของ {{ $orders->orders_no}}<br><br>
                      ชื่อและที่อยู่ผู้ส่ง <?php echo '<br>';?>{!!$orders->billing_address !!}

                    </div>
                    <div class="col-xs-6">

                      TO.<br>
                      บริษัท ยักษ์ ยิ้ม กรุ๊ป จำกัด <br>เลขที่ 111/21 หมู่บ้านธัญญาภิรมย์ <br>ตำบลรังสิต อำเภอธัญบุรี จังหวัดปทุมธานี 12110<br><br>
                      Yakyim Group Ltd <br>111/21 Thanyaphirom Village <br> Rangsit Thnayaburi Pathumthani<br>
                      Tel. 02-191-3955, 084-657-1001<br>
                  </div>

                </div>
                <br><br>
                <p>* หากสินค้าชำรุด เสียหาย หรือไม่ถูกต้อง ทางเราจะคืนเงินค่าจัดส่งให้ท่าน</p><br>

                    <div class="col-xs-12 invoice-block text-left">
                        <a class="btn btn-md blue hidden-print margin-bottom-5" onclick="javascript:window.print();">
                            Print <i class="fa fa-print"></i>
                        </a>
                    </div>
    </div>


    @include('emails.include.footer')
</body>
</html>
