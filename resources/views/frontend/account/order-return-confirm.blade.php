<?php
$payment_method = config()->get('constants.payment_method_'.$lang);
?>
@extends('frontend.layouts/main')

@section('title','Order Return Product confirm')

@section('more-stylesheet')
  <link rel="stylesheet" href="{{ URL::asset('css/front/account-global.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('css/front/account-order-return.css') }}">
@endsection

@section('content')
<div id="account" class="container">
  <div id="content" class="row">
    <div class="col-sm-9">
        <div id="breadcrumb">
          <!-- <a href="{{ URL::route('home') }}">{{ trans('global.home') }}</a>
          <span class="arrow">></span>
          <a href="{{ URL::to('account') }}">{{ trans('global.my-account') }}</a>
          <span class="arrow">></span> -->
          <a href="{{ URL::to('account/order-return') }}">{{ trans('account.order-return') }}</a>
          <span class="arrow">></span>
          <span class="active">{{ trans('account.view-order-detail') }} #{{ $order_no }}</span>
          <span class="arrow">></span>
          <span class="active">ยืนยันการคืนสินค้า</span>

        </div>
        <h1>ยืนยันการคืนสินค้า</h1>

        <form action="{{ URL::to('account/order-return-success') }}" method="post">
        <div id="order" class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr>
                  <th class="text-center">#</th>
                  <th class="text-center">รายการสินค้าที่จะคืน</th>
                  <th class="text-center">จำนวน</th>
                  <th class="text-center">เหตุผล</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                @foreach($arrayJson as $key => $value)
                <td class="text-center">
                  {{$key+1}}
                </td>
                <td class="text-center">
                  {{$value['product']}}
                </td>
                <td class="text-center">
                  {{$value['number']}}
                </td>
                <td>
                  {{$value['reason']}}
                </td>
                @endforeach
              </tr>
            </tbody>
          </table>
        </div>

        หมายเหตุ<br>
          เมื่อบริษัทได้รับสินค้าแล้วจะดำเนินการคืนเงินภายใน 15 วัน นับจากวันที่ได้รับสินค้าจากลูกค้า<br><br><br>

          <!-- <span>เลือกธนาคารที่รับเงินคืน</span><br>
          <select>
          <option value="กสิกรไทย">กสิกรไทย</option>
          <option value="ไทยพาณิชย์">ไทยพาณิชย์</option>
          <option value="กรุงไทย">กรุงไทย</option>
          <option value="กรุงศรี">กรุงศรี</option>
          <option value="กรุงเทพ">กรุงเทพ</option>
          </select>
          <br><br>เลขที่บัญชี
          <br><input id="account_number" name="account_number" type="type" maxlength="10"></input>
          @if($errors->first('account_number'))
            <div class="text-danger">กรุณาเลือกสินค้าที่ต้องการคืนค่ะ</div>
          @endif -->
          <input type="hidden" name="order" value="{{ $order }}">
          <input type="hidden" name="arrayJson" value="{{ json_encode($arrayJson) }}">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <button type="submit" name="submit" class="btn btn-default black">ตกลง</button>
          <!-- <a href="{{ url()->to('account/order-return') }}"><button class="btn btn-default black">ยกเลิก</button></a> -->
          <input type="button" onclick="history.go(-1);" value="ยกเลิก" class="btn btn-default black">
        </form>

    </div>


    <div class="col-sm-3">
      @include('frontend.account.sidebar')
    </div>
  </div>
</div>
@endsection
@section('more-script')
  <!-- <script src="{{ URL::asset('tools/bxslider/jquery.bxslider.js') }}"></script> -->
  <script src="{{ URL::asset('js/jquery-ui/jquery-ui.min.js') }}"></script>
  <script src="{{ URL::asset('js/front/return-product.js') }}"></script>
  <script src="{{ URL::asset('js/jquery.min.js') }}"></script>
@endsection
