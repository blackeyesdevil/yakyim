<?php
$payment_method = config()->get('constants.payment_method_'.$lang);
?>
@extends('frontend.layouts/main')

@section('title','Order Return Product confirm')

@section('more-stylesheet')
  <link rel="stylesheet" href="{{ URL::asset('css/front/account-global.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('css/front/account-order-return.css') }}">
@endsection

@section('content')
<div id="account" class="container">
  <div id="content" class="row">
    <div></div>


    <div class="col-sm-3">
      @include('frontend.account.sidebar')
    </div>
  </div>
</div>
@endsection
@section('more-script')
  <!-- <script src="{{ URL::asset('tools/bxslider/jquery.bxslider.js') }}"></script> -->
  <script src="{{ URL::asset('js/jquery-ui/jquery-ui.min.js') }}"></script>
  <script src="{{ URL::asset('js/front/return-product.js') }}"></script>
@endsection
