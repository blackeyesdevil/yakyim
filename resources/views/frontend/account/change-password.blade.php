@extends('frontend.layouts/main')
@section('more-stylesheet')
  <link rel="stylesheet" href="{{ URL::asset('css/front/account-global.css') }}">
@endsection

@section('content')
<div id="account" class="container">
  <div id="content" class="row">
    <div class="col-sm-9">
      <h2>{{ trans('account.change-password') }}</h2>

      <form action="{{ URL::to('account/change-password/check') }}" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        @if(session()->has('successMsg'))
          <div class="alert alert-success text-center">{!! session()->get('successMsg') !!}</div>
        @endif

        @if(session()->has('errorMsg'))
          <div class="alert alert-danger text-center">{!! session()->get('errorMsg') !!}</div>
        @endif
        <?php $field = 'old_password'; ?>
        <div class="form-group row">
          <div class="form-label col-sm-3">{{ trans('account.old_password') }}</div>

          <div class="value col-sm-5">
            <input class="form-control" type="password" name="{{ $field }}" placeholder="" class="input input-block">
            @if($errors->first($field))
              <div class="text-danger">{{ $errors->first($field) }}</div>
            @endif
          </div>
        </div>

        <?php $field = 'password'; ?>
        <div class="form-group row">
          <div class="form-label col-sm-3">{{ trans('account.new_password') }}</div>

          <div class="value col-sm-5">
            <input class="form-control" type="password" name="{{ $field }}" placeholder="" class="input input-block">
            @if($errors->first($field))
              <div class="text-danger">{{ $errors->first($field) }}</div>
            @endif
          </div>
        </div>

        <?php $field = 'c_password'; ?>
        <div class="form-group row">
          <div class="form-label col-sm-3">{{ trans('account.c_password') }}</div>

          <div class="value col-sm-5">
            <input class="form-control" type="password" name="{{ $field }}" placeholder="" class="input input-block">
            @if($errors->first($field))
              <div class="text-danger">{{ $errors->first($field) }}</div>
            @endif
          </div>
        </div>

        <div class="option">
          <button type="submit" name="submit" class="btn btn-default black">{{ trans('account.change-password') }}</button>
        </div>
      </form>
    </div>

    <div class="col-sm-3">
      @include('frontend.account.sidebar')
    </div>
  </div>
</div>
@endsection
