<?php
$payment_method = config()->get('constants.payment_method_'.$lang);
?>
@extends('frontend.layouts/main')

@section('title','Order Return Product')

@section('more-stylesheet')
  <link rel="stylesheet" href="{{ URL::asset('css/front/account-global.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('css/front/account-order-return.css') }}">
@endsection

@section('content')
<div id="account" class="container">
  <div id="content" class="row">
    <div class="col-sm-9">
      <div id="breadcrumb">
        <!-- <a href="{{ URL::route('home') }}">{{ trans('global.home') }}</a>
        <span class="arrow">></span>
        <a href="{{ URL::to('account') }}">{{ trans('global.my-account') }}</a>
        <span class="arrow">></span> -->
        <a href="{{ URL::to('account/order-return') }}">{{ trans('account.order-return') }}</a>
        <span class="arrow">></span>

        <span class="active">{{ trans('account.view-order-detail') }} #{{ $order->orders_no }}</span>
      </div>

      <div class="headline{{ trans('global.th-font') }}">{{ trans('account.view-order-detail') }} #{{ $order->orders_no }}</div>
      <div id="summary" class="table-responsive">

        <form action="{{ URL::to('account/order-return-post') }}" method="post" enctype="multipart/form-data">
          <input type="hidden" name="order_shipping" value="{{$order->shipping_address}}"></input>
          <input type="hidden" name="order_no" value="{{$order->orders_no}}"></input>
          <input type="hidden" name="order" value="{{$order}}"></input>
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <h3>{{ trans('account.order-refund')}}</h3>
        <table class="table table-bordered">
            <tr>
                <th class="text-center">#</th>
                <th class="text-center">รหัสสินค้า</th>
                <th class="text-center">รูปภาพ</th>
                <th>{{ trans('account.order-product') }}</th>
                <th class="text-center">ขนาด</th>
                <th class="text-center">ราคา</th>
                <th class="text-center">{{ trans('account.order-quantity') }}</th>
                <th class="text-center">{{ trans('account.order-reason') }}</th>
                <!-- <th cslass="text-right">{{ trans('account.order-product-sub-total') }}</th> -->
            </tr>

            @foreach($order_detail as $key => $item)

                <tr>
                    <?php
                        $qty = $item->qty;
                        $model = $item->model;
                        $product_gallery = $item->product_gallery;
                        $product_option = $item->product_option;
                        $unit_price = $item->unit_price;
                        $p_total_price_th = $qty * $unit_price;

                        $dollar_unit_price = $item->dollar_unit_price;
                        $p_total_price_en = $qty * $dollar_unit_price;
                    ?>
                    <td class="text-center">{{ $key+1 }}</td>
                    <td class="text-center">{{ $model }}</td>
                    <td class="text-center">
                      <?php
                        if ($product_gallery->photo !='' && file_exists('uploads/product/'.$product_gallery->photo))
                            $img_url = URL::asset('uploads/product/' . $product_gallery->photo);
                        else
                            $img_url = URL::asset('images/logo.jpg');
                       ?>
                       <img src="{{$img_url}}" class="text-center" style="height:100px;">

                    </td>
                    <!-- <td class="text-center"><input type="checkbox" name="checkbox"></input></td> -->

                    @if( $lang == 'th' )
                    <!-- <td><input type="checkbox" name="check[]" value="รายการสินค้า{{ $item['product_name_'.$lang] }} จำนวน {{$qty}} ชิ้น ราคา {{ $unit_price }} บาท ต่อชิ้น"></input>  {{ $item['product_name_'.$lang] }} -->
                      <td>

                        <input  class="check_box" id="check_box[]" type="checkbox" name="check_box[]" value="{{$key}}" data-a="รายการสินค้า{{ $item['product_name_'.$lang] }}"></input>  {{ $item['product_name_'.$lang] }}
                        @if($errors->first('checkbox'))
                          <div class="text-danger">กรุณาเลือกสินค้าที่ต้องการคืนค่ะ</div>
                        @endif
                        </td>
                    @else
                      <input  class="check_box" id="check_box[]" type="checkbox" name="check_box[]" value="{{$key}}" data-a="รายการสินค้า{{ $item['product_name_'.$lang] }}"></input>  {{ $item['product_name_'.$lang] }}
                        @if($errors->first('checkbox'))
                          <div class="text-danger">กรุณาเลือกสินค้าที่ต้องการคืนค่ะ</div>
                        @endif
                        </td>
                    @endif

                    <td class="text-center">{{ $product_option['option_name_' . session()->get('locale')] }}</td>
                    @if( $lang == 'th')
                      <td class="text-center">{{ $p_total_price_th }}</td>
                    @else
                      <td class="text-center">{{ $p_total_price_en }}</td>
                    @endif

                    <td class="text-center">
                      <input class="qty" id="qty[]"type="number" name="qty[]" value="{{ $qty }}" min="1" max="{{$qty}}" ></input>
                    </td>
                    <td class="text-center">
                        <input type="text" class="reason" id="reason[]" name="reason[]" ></input>
                        <input type="hidden" class="" id="item[]" name="item[]" value="{{$item}}" ></input>
                    </td>

                </tr>
            @endforeach
            <?php
                $total_price = $order->total_price;
                $discount_price = $order->discount_price;
                $shipping_price = $order->shipping_price;
                $grand_price = $total_price - $discount_price + $shipping_price;

                $dollar_total_price = $order->dollar_total_price;
                $dollar_discount_price = $order->dollar_discount_price;
                $dollar_shipping_price = $order->dollar_shipping_price;
                $dollar_grand_price = $dollar_total_price - $dollar_discount_price + $dollar_shipping_price;
            ?>

        </table>

        <button type="submit" name="submit" class="btnsubmit btn btn-default black" disabled>{{ trans('account.order-return') }}</button>
        <input type="button" onclick="history.go(-1);" value="{{ trans('account.cancel')}}" class="btn btn-default black"><br><br><br>
        หมายเหตุ<br>
          1.กรุณาตรวจสอบความถูกต้อง ของสินค้าที่จะคืน <br>
          2.เมื่อบริษัทได้รับสินค้าแล้วจะดำเนินการคืนเงินภายใน 15 วัน นับจากวันที่ได้รับสินค้า<br><br><br>

        <!-- <a href="{{ url()->to('account/order-return/postReturn_Product') }}"><button class="btn btn-default black">aaaa</button></a> -->
          </form>

      </div>

    </div>


    <div class="col-sm-3">
      @include('frontend.account.sidebar')
    </div>
  </div>
</div>
@endsection
@section('more-script')
  <!-- <script src="{{ URL::asset('tools/bxslider/jquery.bxslider.js') }}"></script> -->
  <script src="{{ URL::asset('js/jquery-ui/jquery-ui.min.js') }}"></script>
  <script src="{{ URL::asset('js/front/return-product.js') }}"></script>
@endsection
