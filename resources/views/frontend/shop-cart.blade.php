@extends('layout-frontend')
    @section('content')
        <div id="wrapper">
            <div class="container">
                <h3>Shopping Cart</h3>
                <hr>
                <table class="table table-bordered">
                    <tr>
                        <th class="col-md-1 text-center">#</th>
                        <th class="col-md-5">Product Name</th>
                        <th class="col-md-1 text-center">Qty.</th>
                        <th class="col-md-2 text-right">Price</th>
                        <th class="col-md-2 text-right">Total Price</th>
                        <th class="col-md-1 text-center"></th>
                    </tr>
                    @if(count($carts) > 0)
                        <?php
                            $i=1;
                            $total_price2 = 0;
                        ?>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        @foreach($carts as $field)
                            <?php
                                $a_option_group = $field->option;
                                $cart_id = $field->cart_id;
                                $qty = $field->qty;
                                $price = $field->price;
                                if($price == null) $price = $field->retail_price;

                                $total_price1 = $price*$qty;
                                $total_price2 += $total_price1;
                            ?>
                            <tr>
                                <td class="text-center">{{ $i }}</td>
                                <td>
                                    {{ $field->product_name }}
                                    @if(count($a_option_group) > 0)
                                        <?php $txt_option = ''; ?>
                                        @foreach($a_option_group as $option)
                                            <?php $txt_option .= ', '.$option->group_name.':'.$option->option_name; ?>
                                        @endforeach
                                        <br>
                                        <small>{{ substr($txt_option,1) }}</small>
                                    @endif
                                </td>
                                <td class="text-center">{{ $qty }}</td>
                                <td class="text-right">{{ number_format($price,2) }}</td>
                                <td class="text-right">{{ number_format($total_price1,2) }}</td>
                                <th class="text-center">
                                    <a href="#" class="manage-cart" data-id="{{ $cart_id }}" data-type="plus">+</a>
                                    <a href="#" class="manage-cart" data-id="{{ $cart_id }}" data-type="minus">-</a>
                                    <a href="#" class="manage-cart" data-id="{{ $cart_id }}" data-type="delete">x</a>
                                </th>
                            </tr>
                            <?php $i++; ?>
                        @endforeach
                        <tr>
                            <td colspan="3"></td>
                            <td class="text-right"><b>Sub Total</b></td>
                            <td class="text-right"><b>{{ number_format($total_price2,2)}}</b></td>
                            <td></td>
                        </tr>
                    @else
                        <tr>
                            <td colspan="10" class="text-center">No Result</td>
                        </tr>
                    @endif
                </table>
                <hr>
                <div class="col-md-6">
                    <a class="btn btn-default" href="{{ URL::to('api/shop/product') }}">Back</a>
                </div>
                <div class="col-md-6 text-right">
                    <a class="btn btn-info" href="#">Next</a>
                </div>
            </div>
        </div>
    @endsection

    @section('more-script')
        <script>
            $(document).ready(function() {
                // Delete Cart
                $('.manage-cart').on('click',function(e){
                    e.preventDefault();
                    var token = $('input[name=_token]').val();
                    var type = $(this).attr('data-type');
                    var cart_id = $(this).attr('data-id');
                    var url = '';
                    if(type == 'plus'){ url = 'plus'; }
                    else if(type == 'minus'){ url = 'minus' }
                    else if(type == 'delete'){ url = 'delete'; }

                    $.ajax({
                        url: "cart/"+url,
                        method : "POST",
                        dataType: 'json',
                        data: { "_token":token,"cart_id": cart_id },
                        success: function (data) {
                            location.reload();
                            console.log(data);
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                });

            });
        </script>
    @endsection