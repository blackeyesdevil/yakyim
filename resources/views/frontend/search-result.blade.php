@extends('frontend.layouts/main')

@section('title', trans('global.search-result') . ' "' . $keyword . '"')

@section('more-stylesheet')
    <link rel="stylesheet" href="{{ URL::asset('css/front/search-result.css') }}">
@endsection

@section('content')
<div id="search-result" class="container">
    <div id="breadcrumb">
        <a href="{{ URL::route('home') }}">{{ trans('global.home') }}</a>
        <span class="arrow">></span>

        <span class="active">{{ trans('global.search-result') }} "{{ $keyword }}"</span>
    </div>

    <div id="content">
      <div class="headline{{ trans('global.th-font') }}">{{ trans('global.search-result') }} "{{ $keyword }}"</div>

      @if (count($products) > 0)
      <div class="block">
        <div class="sub-headline{{ trans('global.th-font') }}">{{ trans('global.product')}}</div>

        <div id="product-list" class="result">
          <div class="row">
            @foreach ($products as $i => $product)
            <?php
              if (!empty($product->photo))
                $img_url = URL::asset('uploads/product_option/250/' . $product->photo);
              else
                $img_url = URL::asset('images/no-pic.jpg');

              $p_retail = $product->retail_price;
              $p_special = $product->special_price;
              $p_price = $p_retail;
              if(!empty($p_special)) $p_price = $p_special;

              $product_name_en = str_replace($keyword, '<span class="highlight">' . $keyword . '</span>', strtolower($product->product_name_en));
              $product_name_th = str_replace($keyword, '<span class="highlight">' . $keyword . '</span>', strtolower($product->product_name_th));
            ?>
            <a href="{{ URL::route('product.product-detail', $product->product_id) }}" class="item col-sm-3">
              <div class="product-img">
                <img src="{{ $img_url }}" />
              </div>

              <div class="product-info">
                <div class="product-name">{!! $product_name_en !!}</div>
                <div class="product-name-th">{!! $product_name_th !!}</div>
                <div class="product-price">THB {{ number_format($p_price, 2) }}</div>
              </div>
            </a>
            @if (($i+1)%4 == 0)
          </div>
          <div class="row">
            @endif
            @endforeach
          </div>
        </div>

        <div class="pagination">
          {{ $products->appends(array_except(Request::query(), 'page'))->links() }}
        </div>
      </div>
      @endif

      @if (count($blogs) > 0)
      <div class="block">
        <div class="sub-headline{{ trans('global.th-font') }}">{{ trans('global.blog')}}</div>

        <div id="blog-list" class="result">
          @foreach($blogs as $i => $blog)
          <?php
          $title_en = str_replace($keyword, '<span class="highlight">' . $keyword . '</span>', strtolower($blog->title_en));
          $title_th = str_replace($keyword, '<span class="highlight">' . $keyword . '</span>', strtolower($blog->title_th));
          ?>
          <div class="item row">
            <div class="text-center col-sm-4">
              <a href="{{ URL::to('blog/' . $blog->blog_id) }}" class="blog-img">
                  <div class="img" style="background-image: url('{{ URL::asset('uploads/blog_thumb/' . $blog->thumb_img_name) }}');'"></div>

                  <div class="hover">
                      <i class="fi-eye"></i>
                  </div>
              </a>
            </div>

            <div class="blog-info col-sm-8">
                <div class="blog-title">{!! $title_en !!}</div>
                <div class="blog-sub-title th">{!! $title_th !!}</div>
                <div class="blog-detail">{{ trans('global.post-on') }} {{ date('j M, Y', strtotime($blog->post_datetime)) }}</div>
                <div class="blog-short-desc">
                    {!! $blog['short_detail_' . session()->get('locale')] !!}
                </div>

                <a href="{{ URL::to('blog/' . $blog->blog_id) }}" class="btn"><i class="fi-arrow-right"></i> {{ trans('global.read-more') }}</a>
            </div>
          </div>
          @endforeach
        </div>
      </div>
      @endif
    </div>
</div>
@endsection
