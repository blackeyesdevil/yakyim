@extends('frontend.layouts/main')

@section('title','Home')

@section('more-stylesheet')
<link rel="stylesheet" href="{{ URL::asset('css/front/howto.css') }}">
@endsection

@section('content')
    <div id="banner">
      <div id="inner-banner">
        <div id="pic-banner">
          <img src="{{ URL::asset('images/how-to-m05.png') }}" alt="" class="">
        </div>
        <div id="text-banner" class="text-center">
          <p class="how-to">HOW TO</p>
          <h2>เปลี่ยน/คืนสินค้า</h2>
          <p class="caption">คุณสามารถใช้ส่วนลดในการสั่งซื้อสินค้าได้ โดยโค้ดส่วนลดอาจได้จากกิจกรรมต่างๆของยักษ์ยิ้ม</p>
        </div>

      </div>

    </div>
    <div id="content">
      <div class="container">
        <div class="col-md-3">
          @include('frontend.howto.side-how-to')
        </div>
        <div class="col-md-9" id="inner">
        <h2>{{trans('how-to.change-return')}}</h2>
        <br>
        <br>
        <h3>{{trans('how-to.change-product')}}</h3>
          <div class="row">
            <div class="col-md-5">
              <p class="heading-step ">1. {{trans('how-to.change-return-general')}}</p>
              <img src="{{ URL::asset('images/how-to/hdc1.png') }}" alt="" class="center img-responsive">
            </div>
            <div class="col-md-7">
              <ul>
              <br>
              @if(session()->get('locale') == 'th')
                <li>ลูกค้าทั่วไป ไม่สามารถเปลี่ยนสินค้าได้</li>
              @else
                <li>General customer &#39;CAN NOT&#39; change or return product.</li>
              @endif
              </ul>
            </div>
          </div>

          <div class="row">
            <div class="col-md-5">
              <p class="heading-step ">2. {{trans('how-to.change-return-members')}}</p>
              <img src="{{ URL::asset('images/how-to/hdc2.png') }}" alt="" class="center img-responsive">
            </div>
            <div class="col-md-7">
              <ul>
                @if(session()->get('locale') == 'th')
                  <li>สามารถเปลี่ยนขนาดสินค้าได้เท่านั้น ยกเว้นในแบบเดียวกันขนาดสินค้าที่ต้องการไม่มีให้เปลี่ยน</li>
                  <li>กางเกงในและบ๊อกเซอร์ไม่สามารถเปลี่ยนได้ กรุณาตรวจสอบขนาดสินค้าก่อนชำระเงิน ถ้าไม่มั่นใจสามารถสอบถามได้ที่ Line : @yakyim</li>
                  <li>เปลี่ยนได้ภายใน 7 วัน หลังที่ลูกค้าได้รับสินค้า (ตรวจสอบจากวันรับสินค้าจากระบบ Track&Trace ของไปรณีย์ หรือระบบบตรวจสอบการจัดส่งสินค้า</li>
                  <li>ลูกค้าเป็นผู้รับผิดชอบค่าจัดส่งทั้งหมด</li>
                  <li>ค่าจัดส่งเป็นแบบ EMS เท่านั้น ราคาเหมา 80 บาท (ลูกค้าชำระโอนเงินเข้าธนาคาร บ/ช บจก ยักษ์ยิ้มกรุ๊ป)</li>
                  <li>ต้องไม่ผ่านการซักหรือนำป้ายกระดาษออกจากตัวสินค้า</li>
                  <li>ค่าส่วนต่างที่เกิดขึ้นจากการเปลี่ยนทางร้านจะทำการคำนวณ และแจ้งยอดกลับไปให้ </li>
                  <ul><li>กรณีที่ค่าสินค้าใหม่มากกว่าราคาสินค้าเดิม หลังจากทราบยอดสินค้าแล้ว สามารถชำระเงินได้โดยการโอนเงิน เข้า บ/ช บจก ยักษ์ยิ้ม กรุ๊ป</li></ul>
                  <ul><li>กรณีที่ค่าสินค้าใหม่ต่ำกว่าราคาสินค้าเดิม ทางร้านจะไม่มีการคืนเงินเป็นเงินสด แต่จะเก็บเป็นคะแนนสะสมให้ตามจำนวนเงินที่เกิน</li></ul>
                  <li>สินค้าใหม่จะถูกจัดส่ง เมื่อทางร้านได้สินค้าจากลูกค้า และตรวจสอบสินค้าแล้วว่าอยู่ในสภาพเดิม และทำการชำระเงินค่าส่วนต่างเรียบร้อยแล้ว</li>
                  <li>กรณีที่เปลี่ยนสินค้า คะแนนสะสมจะถูกคำนวณใหม่ตามยอดสินค้าที่สั่งซื้อจริง</li>
                @else
                  <li>Yakyim members &#39;ONLY CAN&#39; change a product providing by same pattern in different size. (Except same pattern do not have in stock you can change a new pattern)</li>
                  <li>Underwear and Boxer &#39;CAN NOT&#39; change. Please make sure about size before ordering. You can get more information at Line: @yakyim.</li>
                  <li>Product can be change under 7 days when you received a packaged. (Yakyim will check timetable from Track&amp;Trace of Thailand Post or on website)</li>
                  <li>Yakyim members must be pay all of return shipping cost only in EMS method for 80 bath. (You must pay return shipping cost via bank account Yakyim Group Co., Ltd.)</li>
                  <li>Product &#39;DO NOT&#39; wash and cut a label.</li>
                  <li>For the settlement of return product. Yakyim will be recalculation then notify total cost to Yakyim members.</li>
                  <li>In case, when a new product price is higher than return product. Yakyim members must be pay via bank account Yakyim Group Co., Ltd.</li>
                  <li>In case, when a new product price is lower than return product. Yakyim &#39;DO NOT&#39; return into cash but will collect into reward points that calculate from excess.</li>
                  <li>New product will be shipping when Yakyim receive and check a return product from customers. *Yakyim members must be already paid the settlement.*</li>
                  <li>In case, when Yakyim members change a product reward points will be recalculation followed by a new order cost.</li>
                @endif
              </ul>
            </div>
          </div>
          <div class="row">
            <div class="col-md-5">
              <p class="heading-step ">3. {{trans('how-to.change-return-vip')}}</p>
              <img src="{{ URL::asset('images/how-to/hdc3.png') }}" alt="" class="center img-responsive">
            </div>
            <div class="col-md-7">
              <ul>
                @if(session()->get('locale') == 'th')
                  <li>สามารถเปลี่ยนขนาดและแบบสินค้าได้ </li>
                  <li>สามารถคืนสินค้าได้ เมื่อไม่มีสินค้าที่ต้องการ และสินค้าอยู่ในสภาพเดิม (ไม่มีการซัก ไม่มีการแกะป้ายสินค้าและป้ายราคาออก)</li>
                  <li>ไม่มีการคืนเงินค่าสินค้าเป็นเงินสด แต่จะเก็บไว้ในรูปแบบคะแนนสะสมตามราคาสินค้า (ไม่รวมค่าจัดส่ง)</li>
                @else
                  <li>Yakyim VIP &#39;CAN&#39; change size and pattern.</li>
                  <li>Yakyim VIP &#39;CAN&#39; return a product but product must be into same packaged (DO NOT WASH AND CUT A LABLE)</li>
                  <li>Yakyim &#39;DO NOT&#39; return into cash but will collect into reward points followed by price of product.</li>
                @endif
              </ul>
            </div>
          </div>
          @if(session()->get('locale') == 'th')
            <h3>คืนสินค้า</h3>
            <div class="row">
            <div class="col-md-5">
              <p class="heading-step ">1.คลิกเลือกที่ปุ่ม <b>"คืนสินค้า"</b> </p>
              <img src="{{ URL::asset('images/how-to/hck1.png') }}" alt="" class="center img-responsive">
            </div>
            <div class="col-md-7">
              <ul>
                <li>คลิกเลือกที่ปุ่มเมนู <b>"คืนสินค้า"</b></li>
              </ul>
            </div>
          </div>
          @endif
        </div>

      </div>


    </div>

@endsection

@section('more-script')

  <script src="{{ URL::asset('tools/bxslider/jquery.bxslider.js') }}"></script>
  <script type="text/javascript">
    $('.bxSlider').bxSlider({
      'auto': true,
      'controls': true
    });
  </script>
  <script>
  $(document).ready(function(){
    $('.dropdown-submenu a.test').on("click ", function(e){
      $(this).next('ul').toggle();
      e.stopPropagation();
      e.preventDefault();
    });
    // $('.dropdown-submenu a').on(" mouseenter", function(e){
    //   $(this).next('ul').hide();
    //   e.stopPropagation();
    //   e.preventDefault();
    // });
  });
  </script>
@endsection
