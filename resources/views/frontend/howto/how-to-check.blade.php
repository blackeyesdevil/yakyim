@extends('frontend.layouts/main')

@section('title','Home')

@section('more-stylesheet')
<link rel="stylesheet" href="{{ URL::asset('css/front/howto.css') }}">
@endsection

@section('content')
    <div id="banner">
      <div id="inner-banner">
        <div id="pic-banner">
          <img src="{{ URL::asset('images/m04.png') }}" alt="" class="">
        </div>
        <div id="text-banner" class="text-center">
          <p class="how-to">HOW TO</p>
          <h2>ตรวจสอบสถานะ</h2>
          <p class="caption">ถ้าคุณอยากรู้ว่าสินค้าที่สั่งไปอยู่ไหนแล้ว
สามารถทำได้ง่ายดังต่อไปนี้</p>
        </div>

      </div>

    </div>
    <div id="content">
      <div class="container">
        <div class="col-md-3">
          @include('frontend.howto.side-how-to')
        </div>
        <div class="col-md-9" id="inner">
        <h2>{{trans('how-to.tracking')}}</h2>

          <div class="row">
            <div class="col-md-5">
              <p class="heading-step "> {{trans('how-to.tracking-post')}}</p>
              <img src="{{ URL::asset('images/how-to/hc1.png') }}" alt="" class="center img-responsive">
            </div>
            <div class="col-md-7">

              <ul>
                @if(session()->get('locale') == 'th')
                  <li>เข้าไปที่ <a href="http://track.thailandpost.co.th/tracking/default.aspx" target="_blank">http://track.thailandpost.co.th/tracking/default.aspx</a></li>
                @else
                  <li>Go to http://track.thailandpost.co.th/tracking/default.aspx</li>
                  <li>Fill your &#39;TRACKING NUMBER&#39; then please slide to track.</li>
                  <li>Get your track result.</li>
                @endif
              </ul>
            </div>
          </div>
          @if(session()->get('locale') == 'th')
          <div class="row">
            <div class="col-md-5">
              <p class="heading-step ">2. กรอกหมายเลข Tracking Number</p>
              <img src="{{ URL::asset('images/how-to/hc2.png') }}" alt="" class="center img-responsive">
            </div>
            <div class="col-md-7">
              <ul>

                <li>นำหมายเลขแทร๊คมาใส่ที่ช่องค้นหา แล้วสไลด์เพื่อ
 เริ่มการค้นหา</li>
              <li>จากนั้นระบบจะดำเนินการค้นหา แล้วแจ้งผลการ
 ดำเนินการ เป็นขั้นตอนว่าตอนนี้สินค้าของท่าน
 อยู่ตรงไหนแล้ว</li>

              </ul>
            </div>
          </div>
          @endif
          <h2>{{trans('how-to.tracking-status-web')}}</h2>

          <div class="row">
            <div class="col-md-5">
              <p class="heading-step ">1. {!!trans('how-to.tracking-status-web-1')!!}</p>
              <img src="{{ URL::asset('images/how-to/hc2_1.png') }}" alt="" class="center img-responsive">
            </div>
            <div class="col-md-7">
              <ul>
                @if(session()->get('locale') == 'th')
                  <li>คุณสามารถตรวจสอบสถานะของสินค้าผ่านเว็บได้โดย คลิกปุ่มเมนู <b>"ติดตามการสั่งซื้อ"</b> จาก Footer ด้านล่างสุดของเว็บ</li>
                @endif
              </ul>
            </div>
          </div>

           <div class="row">
            <div class="col-md-5">
              <p class="heading-step ">2. {!!trans('how-to.tracking-status-web-2')!!}</p>
              <img src="{{ URL::asset('images/how-to/hc2_2.png') }}" alt="" class="center img-responsive">
            </div>
            <div class="col-md-7">
              <ul>
                @if(session()->get('locale') == 'th')
                  <li> 1. เลือกคลิกที่เมนู <b>"ประวัติการสั่งซื้อ"</b></li>
                  <li> 2. ให้คลิกเลือกที่ปุ่ม<b>"ดูสถานะการสั่งซื้อ"</b> จากบล็อกประวัติการสั่งซื้อ</li>
                @endif
              </ul>
            </div>
          </div>

          <div class="row">
            <div class="col-md-5">
              <p class="heading-step ">3. {!!trans('how-to.tracking-status-web-3')!!}</p>
              <img src="{{ URL::asset('images/how-to/hc2_3.png') }}" alt="" class="center img-responsive">
            </div>
            <div class="col-md-7">
              <ul>

              @if(session()->get('locale') == 'th')
                <h5><b>จะมีสถานะการสั่งซื้อเเสดงบนหน้าจอ</b></h5>
                <li>สถานะ<b>(ดำเนินการ)</b>เป็นสถานะตรวจสอบการสั่งซื้อ ว่ามีการสั่งซื้อถูกต้องหรือไม่?</li>
              @else
                <li>In progress : Waiting for order checking and stock confirmation.</li>
                <li>Shipping : Your packaged will be shipping.</li>
                <li>Shipped   : Your packaged was already shipping.</li>
              @endif

            </ul>
            </div>
          </div>

        </div>

      </div>


    </div>

@endsection

@section('more-script')

  <script src="{{ URL::asset('tools/bxslider/jquery.bxslider.js') }}"></script>
  <script type="text/javascript">
    $('.bxSlider').bxSlider({
      'auto': true,
      'controls': true
    });
  </script>
  <script>
  $(document).ready(function(){
    $('.dropdown-submenu a.test').on("click ", function(e){
      $(this).next('ul').toggle();
      e.stopPropagation();
      e.preventDefault();
    });
    // $('.dropdown-submenu a').on(" mouseenter", function(e){
    //   $(this).next('ul').hide();
    //   e.stopPropagation();
    //   e.preventDefault();
    // });
  });
  </script>
@endsection
