<?php
$config_part = config()->get('constants.part');
$arr_part = $config_part[$lang];
?>
@extends('frontend.layouts/main')

@section('title','Home')

@section('more-stylesheet')
<link rel="stylesheet" href="{{ URL::asset('css/front/howto.css') }}">
@endsection

@section('content')
    <div id="banner">
        <div id="inner-banner">
            <div id="pic-banner">
                <img src="{{ URL::asset('images/how-to-m04.png') }}" alt="" class="">
            </div>
            <div id="text-banner" class="text-center">
                <p class="how-to">HOW TO</p>
                <h2>{{ trans('global.sizing-chart') }}</h2>
                <p class="caption">
                    สำหรับลูกค้าที่ไม่ทราบขนาดที่ควรซื้อ สามารถตรวจสอบได้จากตารางขนาดสินค้าของเรา
                </p>
            </div>
        </div>
    </div>

    <div id="content">
        <div class="container">
            <div class="col-md-3">
              @include('frontend.howto.side-how-to')
            </div>
            <div class="col-md-9" id="inner">

                @foreach($charts as $key => $chart)
                <div class="chart-item">
                    <div class="headline">{{ ($key+1) . '. ' . $chart['chart_name_' . session()->get('locale')] }}</div>
                    <div class="content">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th width="200">{{ trans('global.size-chart-size') }}</th>
                                        @foreach ($chart->sizes as $size)
                                        <th class="text-center">{{ $size->size_name }}</th>
                                        @endforeach
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php
                                    $arr_chart='';
                                    $array_part = [];
                                    // echo $chart->values;
                                    foreach($chart->values as $key => $item){
                                      $part = $item->part;
                                      $size_name = $item->size_name;
                                      $value = $item->value;
                                      $arr_chart[$part][$size_name] = $value;
                                      if(!in_array($part, $array_part, true)){
                                          array_push($array_part, $part);
                                      }


                                    }
                                    // var_dump(print_r($arr_chart, TRUE));
                                    // echo '--------------------';
                                    // echo json_encode($array_part);

                                      ?>

                                      {{--@for($i = 1;$i <= 9;$i++)--}}



                                            @foreach($array_part as $a)
                                            <tr>
                                            <td>{!!Html::decode($arr_part[$a]) !!}</td>
                                               @foreach ($chart->sizes as $size)
                                                  {{--@if($a == $i)--}}

                                                   <td class="text-center">

                                                         {{ (array_key_exists($size->size_name, $arr_chart[$a]))?$arr_chart[$a][$size->size_name]:'-' }}

                                                     {{--{{(array_key_exists($size->size_name, $arr_chart[$i]))?$arr_chart[$i][$size->size_name]:'-'}}--}}
                                                     {{--{{(array_key_exists($size->size_name, $arr_chart['1']))?$arr_chart['1'][$size->size_name]:'-'}}--}}
                                                     {{-- {{(array_key_exists($size->size_name, $arr_chart))?$arr_chart[$i][$size->size_name]:'-'}} --}}
                                                   </td>
                                                   {{--@endif--}}
                                                 @endforeach
                                                 </tr>
                                               @endforeach


                                      {{--@endfor--}}


                                    {{--@foreach ($arr_chart as $part => $arr_size)
                                    <tr>
                                        <td>{{ $arr_part[$part] }}</td>
                                        @foreach ($chart->sizes as $size)
                                        <td class="text-center">
                                            {{ (array_key_exists($size->size_name, $arr_size))?$arr_size[$size->size_name]:'-' }}
                                        </td>
                                        @endforeach
                                    </tr>
                                    @endforeach --}}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                @endforeach

            </div>
        </div>
    </div>

@endsection
