@extends('frontend.layouts/main')

@section('title','Home')

@section('more-stylesheet')
<link rel="stylesheet" href="{{ URL::asset('css/front/howto.css') }}">
@endsection

@section('content')
    <div id="banner">
      <div id="inner-banner">
        <div id="pic-banner">
          <img src="{{ URL::asset('images/how-to-m08.png') }}" alt="" class="">
        </div>
        <div id="text-banner" class="text-center">
          <p class="how-to">HOW TO</p>
          <h2>เเก้ไขที่อยู่</h2>
          <p class="caption">หากคุณต้องการเปลี่ยนเเปลงที่อยู่การจัดส่งสินค้า คุณสามารถทำได้ง่ายๆตามขั้นตอนต่อไปนี้</p>
        </div>

      </div>

    </div>
    <div id="content">
      <div class="container">
        <div class="col-md-3">
          @include('frontend.howto.side-how-to')
        </div>
        <div class="col-md-9" id="inner">
      <h2>{{trans('how-to.change-address')}}</h2>
          <div class="row">
            <div class="col-md-5">
              <p class="heading-step ">1. {{trans('how-to.change-address-menu')}}</p>
              <img src="{{ URL::asset('images/how-to/hca2.png') }}" alt="" class="center img-responsive">
            </div>
            <div class="col-md-7">

              <ul>
                @if(session()->get('locale') == 'th')
                  <li>เมื่อ Login เข้ามาเเล้วระบบจะเเสดงหน้า รายละเอียดสมาชิก</li>
                  <li>ให้เลือกที่ปุ่มเมนู <b>"ข้อมูลที่อยู่"</b></li>
                @endif



              </ul>
            </div>
          </div>

            <div class="row">
            <div class="col-md-5">
              <p class="heading-step ">2.{{trans('how-to.change-address-edit')}}</b></p>
              <img src="{{ URL::asset('images/how-to/hca3.png') }}" alt="" class="center img-responsive">
            </div>
            <div class="col-md-7">

              <ul>
                @if(session()->get('locale') == 'th')
                  <li>หลังจากคลิกเมนู "ข้อมูลที่อยู่"</li>
                  <li>ให้เลือกปุ่ม "เเก้ไข" ในบล็อกของ ที่อยู่สำหรับจัดส่งสินค้า</li>
                @endif
              </ul>
            </div>
          </div>


          <div class="row">
            <div class="col-md-5">
              <p class="heading-step ">3. {{trans('how-to.change-address-save')}}</b></p>
              <img src="{{ URL::asset('images/how-to/hca4.png') }}" alt="" class="center img-responsive">
            </div>
            <div class="col-md-7">

              <ul>
                @if(session()->get('locale') == 'th')
                  <li>เเก้ไข้ข้อมูลที่อยู่การจัดส่ง</li>
                  <li>เมื่อกรอกรายละเอียดครบถ้วน กดปุ่ม "เเก้ไขที่อยู่" ระบบจะทำการบันทึกข้อมูลใหม่เข้าสู่ระบบ</li>
                @endif
              </ul>
            </div>
          </div>



        </div>

      </div>


    </div>

@endsection

@section('more-script')

  <script src="{{ URL::asset('tools/bxslider/jquery.bxslider.js') }}"></script>
  <script type="text/javascript">
    $('.bxSlider').bxSlider({
      'auto': true,
      'controls': true
    });
  </script>
  <script>
  $(document).ready(function(){
    $('.dropdown-submenu a.test').on("click ", function(e){
      $(this).next('ul').toggle();
      e.stopPropagation();
      e.preventDefault();
    });
    // $('.dropdown-submenu a').on(" mouseenter", function(e){
    //   $(this).next('ul').hide();
    //   e.stopPropagation();
    //   e.preventDefault();
    // });
  });
  </script>
@endsection
