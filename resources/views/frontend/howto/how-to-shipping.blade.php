@extends('frontend.layouts/main')

@section('title','Home')

@section('more-stylesheet')
<link rel="stylesheet" href="{{ URL::asset('css/front/howto.css') }}">
@endsection

@section('content')
    <div id="banner">
      <div id="inner-banner">
        <div id="pic-banner">
          <img src="{{ URL::asset('images/how-to-m07.png') }}" alt="" class="">
        </div>
        <div id="text-banner" class="text-center">
          <p class="how-to">HOW TO</p>
          <h2>การขนส่ง</h2>
          <p class="caption">ช่องทางการขนส่งสินค้าที่เหมาะ สำหรับคุณ ในการจัดส่งสินค้า</p>
        </div>

      </div>

    </div>
    <div id="content">
      <div class="container">
        <div class="col-md-3">
          @include('frontend.howto.side-how-to')
        </div>
        <div class="col-md-9" id="inner">
        <h2>{{trans('how-to.shipping')}}</h2>

          <div class="row">
            <div class="col-md-5">
              <p class="heading-step ">1. {{trans('how-to.shipping-ems')}}</p>
              <img src="{{ URL::asset('images/how-to/hsp1.png') }}" alt="" class="center img-responsive">
            </div>
            <div class="col-md-7">

              <ul>
                @if(session()->get('locale') == 'th')
                  <li>จัดส่งสินค้าแบบ EMS โดยไปรษณีย์ไทยทั่วประเทศ ค่าจัดส่งเหมาะรวม 80 บาท</li>
                @else
                  <li>Shipping in EMS method by Thailand Post. (Shipping cost 80 bath)</li>
                @endif
              </ul>
            </div>
          </div>

          <div class="row">
            <div class="col-md-5">
              <p class="heading-step ">2. {{trans('how-to.shipping-lalamove')}}</p>
              <img src="{{ URL::asset('images/how-to/hsp2.png') }}" alt="" class="center img-responsive">
            </div>
            <div class="col-md-7">
              <ul>
                @if(session()->get('locale') == 'th')
                  <li>จัดส่งสินค้าด่วน ด้วยมอร์เตอร์ไซส์ โดยบริษัท Lalamove เฉพาะกรุงเทพฯและปริมณฑล ค่าจัดส่งคำนวณตามระยะทาง สามารถตรวจสอบค่าจัดส่งได้ตาม link https://www.lalamove.com/th-thai/business-download</li>
                @else
                  <li>Shipping via motorcycle by Lalamove only in Bangkok and perimeter. (Shipping cost calculate by distance)</li>
                  <li>You can track on this website https://www.lalamove.com/th-thai/business-download</li>
                @endif
              </ul>
            </div>
          </div>
          <div class="row">
            <div class="col-md-5">
              <p class="heading-step ">3. {{trans('how-to.shipping-own')}}</p>
              <img src="{{ URL::asset('images/how-to/hsp3.png') }}" alt="" class="center img-responsive">
            </div>
            <div class="col-md-7">
              <ul>
                @if(session()->get('locale') == 'th')
                  <li>มารับสินค้าด้วยตัวเองที่ออฟฟิศ ม.ธัญญาภิรมย์ รังสิต คลอง 5 เปิดทำการจันทร์-ศุกร์ เวลา 9:00-17:00</li>
                @else
                  <li>Pick by your own at office. Office Hours: 9 AM - 5 PM Office Address: 111/21 Thanyaphirom Village, Rangsit, Thnayaburi, Pathumthani</li>
                @endif

              </ul>
            </div>
          </div>


        </div>

      </div>


    </div>

@endsection

@section('more-script')

  <script src="{{ URL::asset('tools/bxslider/jquery.bxslider.js') }}"></script>
  <script type="text/javascript">
    $('.bxSlider').bxSlider({
      'auto': true,
      'controls': true
    });
  </script>
  <script>
  $(document).ready(function(){
    $('.dropdown-submenu a.test').on("click ", function(e){
      $(this).next('ul').toggle();
      e.stopPropagation();
      e.preventDefault();
    });
    // $('.dropdown-submenu a').on(" mouseenter", function(e){
    //   $(this).next('ul').hide();
    //   e.stopPropagation();
    //   e.preventDefault();
    // });
  });
  </script>
@endsection
