<div id="how-side">
            <ul id="menu-how-to">
              <li class="heading-how-to">HOW TO</li>
              <li @if(request()->path() == 'how-to-register') class="menu-active" @endif><a href="{{ url()->to('how-to-register') }}">{{ trans('global.member-register') }}</a>
              <li @if(request()->path() == 'how-to-pay') class="menu-active" @endif><a href="{{ url()->to('how-to-pay') }}">{{ trans('global.payment') }}</a>
              <li @if(request()->path() == 'how-to-check') class="menu-active" @endif><a href="{{ url()->to('how-to-check') }}">{{ trans('global.status-check') }}</a>
              <li @if(request()->path() == 'how-to-sizing-chart') class="menu-active" @endif><a href="{{ url()->to('how-to-sizing-chart') }}">{{ trans('global.sizing-chart') }}</a>
               <li @if(request()->path() == 'how-to-change-product') class="menu-active" @endif><a href="{{ url()->to('how-to-change-product') }}">{{ trans('global.change-product') }}</a>
              <li @if(request()->path() == 'how-to-discount') class="menu-active" @endif><a href="{{ url()->to('how-to-discount') }}">{{ trans('global.discount') }}</a>
              <li @if(request()->path() == 'how-to-shipping') class="menu-active" @endif><a href="{{ url()->to('how-to-shipping') }}">{{ trans('global.shipping') }}</a>
              <li @if(request()->path() == 'how-to-change-address') class="menu-active" @endif><a href="{{ url()->to('how-to-change-address') }}">{{ trans('global.change-address') }}</a>
            </ul>
          </div>
