@extends('frontend.layouts/main')

@section('title','Home')

@section('more-stylesheet')
<link rel="stylesheet" href="{{ URL::asset('css/front/howto.css') }}">
@endsection

@section('content')
    <div id="banner">
      <div id="inner-banner">
        <div id="pic-banner">
          <img src="{{ URL::asset('images/how-to-order.png') }}" alt="" class="">
        </div>
        <div id="text-banner" class="text-center">
          <p class="how-to">HOW TO</p>
          <h2>วิธีสั่งซื้อสินค้าผ่านหน้าเว็บไซต์</h2>
          <p class="caption">ซื้อสินค้าผ่านหน้าเว็บไซต์ ทั้งสะดวกทั้งง่าย
แถมยังได้สะสมแต้ม เพื่อรับส่วนลดอีกด้วย</p>
        </div>
        
      </div>
        
    </div>
    <div id="content">
      <div class="container">

        <div class="col-md-3">
          @include('frontend.howto.side-how-to')
        </div>
        <div class="col-md-9" id="inner">
          <div class="row">
            <div class="col-md-5">
              <p class="heading-step">1. เลือกรูปแบบและสินค้าที่ต้องการ</p>
              <img src="{{ URL::asset('images/how-to/ho1.png') }}" alt="" class="center img-responsive">
            </div>
            <div class="col-md-7">
            
              <ul>
                <li>เมื่อเข้ามาในหมวดที่ต้องการแล้ว คุณสามารถดูได้ว่า
 ในหมวดนั้นมีสินค้ารูปแบบใดให้เลือกบ้าง</li>
                <li>ระบบจะแสดงรายละเอียดของสินค้า เช่น คำอธิบาย,
  ราคา, ขนาด</li>
                <li>หากคุณต้องการดูขนาดของสินค้า ให้กดที่ปุ่ม
 SIZE CHART</li>

              </ul>
            </div>
          </div>
          <div class="row">
            <div class="col-md-5">
              <p class="heading-step ">2. สั่งซื้อสินค้าที่ต้องการ</p>
              <img src="{{ URL::asset('images/how-to/ho2.png') }}" alt="" class="center img-responsive">
            </div>
            <div class="col-md-7">
            
              <ul>
                <li>หลังจากที่เลือกขนาดของสินค้าแล้วให้กดปุ่ม
 หยิบใส่ตะกร้า เพื่อยืนยัน</li>
                <li>ถ้าคุณเลือกขนาดแล้ว มีคำขึ้นว่า ไม่พบสินค้า
 แสดงว่าสินค้า SIZE นั้นหมด</li>
                <li>หลังจากที่คุณหยิบสินค้าลงตะกร้า ระบบจะให้เลือก
 ว่าจะซื้อต่อหรือไปขั้นตอนชำระเงิน</li>

              </ul>
            </div>
          </div>
          <div class="row">
            <div class="col-md-5">
              <p class="heading-step ">3. ยืนยันการสั่งซื้อ/เข้าสู่ระบบ</p>
              <img src="{{ URL::asset('images/how-to/ho3.png') }}" alt="" class="center img-responsive">
            </div>
            <div class="col-md-7">
            
              <ul>
                <li>กรุณา Log in เพื่อเข้าสู่ระบบก่อน แล้วจึงสามารถ
  ดำเนินตามขั้นตอนต่อไปได้</li>
                <li>เมื่อกดปุ่มยืนยันการสั่งซื้อ ระบบจะแจ้งรายละเอียด
  สินค้าที่ท่านสั่งซื้อทั้งหมด</li>
                <li>ตรวจสอบที่อยู่ในการจัดส่งสินค้า แล้วดำเนินตาม
  ขั้นตอนต่อไป</li>

              </ul>
            </div>
          </div>
          <div class="row">
            <div class="col-md-5">
              <p class="heading-step ">4. เลือกวิธีการจัดส่งสินค้า</p>
              <img src="{{ URL::asset('images/how-to/ho4.png') }}" alt="" class="center img-responsive">
            </div>
            <div class="col-md-7">
            
              <ul>
                <li>คุณสามารถเลือกวิธีส่งสินค้าได้ 2 แบบ<br>
 1. EMS <br>
 2. มารับเองที่ร้าน</li>
                <li>
•กดปุ่ม ยอมรับเงื่อนไข เพื่อยอมรับกฏระเบียบ ของการ
 บริการ</li>

              </ul>
            </div>
          </div>
          <div class="row">
            <div class="col-md-5">
              <p class="heading-step ">5. เลือกวิธีการชำระเงิน</p>
              <img src="{{ URL::asset('images/how-to/ho5.png') }}" alt="" class="center img-responsive">
            </div>
            <div class="col-md-7">
            
              <ul>
                <li>รอสักครู่ระบบจะแสดงช่องทางการชำระเงินให้คุณเลือก
 มีทั้งหมด 3 ช่องทาง <br>
 1. โอนเงินผ่านธนาคาร <br>
 2. ชำระผ่าน Paypal <br>
 3. ชำระด้วยบัตรเครดิตผ่าน T2P CHECK OUT</li>
                <li>เลือกวิธีการชำระเงินที่ต้องการ แล้วดำเนินตามขั้นตอน</li>
                
              </ul>
            </div>
          </div>
          <hr style="max-width:100%">
          <div class="row">
            <div class="col-md-5">
              
              <img src="{{ URL::asset('images/m01.png') }}" alt="" class="center img-responsive">
            </div>
            <div class="col-md-7">
             <h2> สิทธิพิเศษสำหรับสมาชิก </h2>
1.  ทุก 100 บาท ได้คะแนนสะสม 5 คะแนน <br>
2. คะแนนสะสม เอาไปแลกรับส่วนลด ในการซื้อสินค้าครั้งถัดไป <br>
3. ได้รับส่วนลด 10% ในวันเกิดของคุณ <br>
            </div>
          </div>
        </div>

      </div>
      

    </div>
 
@endsection

@section('more-script')

  <script src="{{ URL::asset('tools/bxslider/jquery.bxslider.js') }}"></script>
  <script type="text/javascript">
    $('.bxSlider').bxSlider({
      'auto': true,
      'controls': true
    });
  </script>
  <script>
  $(document).ready(function(){
    $('.dropdown-submenu a.test').on("click ", function(e){
      $(this).next('ul').toggle();
      e.stopPropagation();
      e.preventDefault();
    });
    // $('.dropdown-submenu a').on(" mouseenter", function(e){
    //   $(this).next('ul').hide();
    //   e.stopPropagation();
    //   e.preventDefault();
    // });
  });
  </script>
@endsection
