@extends('frontend.layouts/main')

@section('title','Home')

@section('more-stylesheet')
<link rel="stylesheet" href="{{ URL::asset('css/front/howto.css') }}">
@endsection

@section('content')
    <div id="banner">
      <div id="inner-banner">
        <div id="pic-banner">
          <img src="{{ URL::asset('images/m03.png') }}" alt="" class="">
        </div>
        <div id="text-banner" class="text-center">
          <p class="how-to">HOW TO</p>
          <h2>การชำระเงิน</h2>
          <p class="caption"> ช่องทางง่ายๆ สะดวกในการชำระเงิน
และยืนยันการชำระเงิน</p>
        </div>

      </div>

    </div>
    <div id="content">
      <div class="container">
        <div class="col-md-3">
          @include('frontend.howto.side-how-to')
        </div>
        <div class="col-md-9" id="inner">
          <h2>{{trans('how-to.pay')}}</h2>
          <div class="row">
            <div class="col-md-5">
              @if(session()->get('locale') == 'th')
                <p class="heading-step ">1. ชำระเงินเข้าบัญชี <br> &nbsp;&nbsp; &nbsp;  บจก.ยักษ์ยิ้ม กรุ๊ป จำกัด</p>
              @else
                <p class="heading-step ">1. Bank Transfer</p>
              @endif
              <img src="{{ URL::asset('images/how-to/hp1.png') }}" alt="" class="center img-responsive">
            </div>
            <div class="col-md-7">

              <ul>
                @if(session()->get('locale') == 'th')
                  <li>
                      หากชำระเงินโดยการโอนเงิน กรุณาโอนเงินเข้าบัญชี<br>
                      <b>ธนาคารกสิกรไทย</b><br>
                      ชื่อ บริษัท ยักษ์ยิ้ม จำกัด<br>
                      เลขที่ 806-2-09769-9<br><br>
                      <b>ธนาคารกรุงไทย</b><br>
                      ชื่อ บริษัท ยักษ์ยิ้ม จำกัด<br>
                      เลขที่ 077-0-23011-3 (พร้อมเพย์)

                  </li>
                @else
                  <li>Select 'BANK TRANFER'</li>
                  <li>Click 'CONTINUE' for the next step.</li>
                @endif
              </ul>
            </div>
          </div>
          <div class="row">
            <div class="col-md-5">
              <p class="heading-step ">2. {{trans('how-to.paypal')}}</p>
              <img src="{{ URL::asset('images/how-to/hp2.png') }}" alt="" class="center img-responsive">
              <img src="{{ URL::asset('images/how-to/c_paypal.jpg') }}" alt="" class="center img-responsive">
              <p>Then Full fill all box.</p>
              <img src="{{ URL::asset('images/how-to/c_paypal2.jpg') }}" alt="" class="center img-responsive">
            </div>
            <div class="col-md-7">

              <ul>
                @if(session()->get('locale') == 'th')
                <li>เมื่อต้องการชำระผ่าน Paypal ให้กดที่ Pay with your
  card or PayPal account</li>
                <li>log in เข้า PayPal และชำระเงิน</li>
                @else
                  <li>Select 'PayPal'</li>
                  <li>Click 'CONTINUE' for the next step.</li>
                  <li>Make your transactions via PayPal</li>
                  <li>If you have no paypal account you can click "Pay with my debit or credit card"</li>
                @endif
              </ul>
            </div>
          </div>


          <h2>{{trans('how-to.payment-confirm')}}</h2>

          <div class="row">
            <div class="col-md-5">
              <p class="heading-step ">1.{{trans('how-to.paypal-confirm-web')}} </p>
              <h5>&nbsp;&nbsp;&nbsp;&nbsp;1.1 เลือกที่เมนู <b>"ประวัติการสั่งซื้อ"</b></h5>
              <img src="{{ URL::asset('images/how-to/hp2_1.png') }}" alt="" class="center img-responsive">
              <h5>&nbsp;&nbsp;&nbsp;&nbsp;1.2  กรอกรายละเอียดข้อมูลการชำระเงิน</h5>
              <img src="{{ URL::asset('images/how-to/hp2_2.png') }}" alt="" class="center img-responsive">
            </div>
            <div class="col-md-7">

              <ul>
                @if(session()->get('locale') == 'th')
                  <li>คลิกเลือกเมนู <b>"ประวัติการสั่งซื้อ"</b></li>
                  <li>คลิกเลือก <b>"เเจ้งการชำระเงิน"</b> จากบล็อกประวัติการสั่งซื้อ</li>
                @else
                  <li>Select 'Order Menu'</li>
                  <li>Click 'PAYMENT CONFIRMATION'</li>

                @endif
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                @if(session()->get('locale') == 'th')
                  <li>กรอกรายละเอียดข้อมูลการชำระเงินให้ครบถ้วน พร้อมแนบใบโอนเงินเป็นหลักฐาน</li>
                @else
                  <li>Please fill your transactions data then attach your payment evidence.</li>
                @endif
              </ul>
            </div>
          </div>


          <div class="row">
            <div class="col-md-5">
              <!-- <p class="heading-step ">2. ยืนยันผ่าน <a href="http://m.me/yakyimthai" target="_blank">Facebook : yakyim</a></p> -->
              <p class="heading-step ">2. {!! trans('how-to.paypal-confirm-face') !!}</p>
              <img src="{{ URL::asset('images/how-to/hp5.png') }}" alt="" class="center img-responsive">
            </div>
            <div class="col-md-7">

              <ul>
                @if(session()->get('locale') == 'th')
                <li>เลือก ส่งข้อความ บนหน้าเพจ <a href="http://m.me/yakyimthai" target="_blank">Yakyim</a></li>
  <li>แจ้งข้อมูลการติดต่อ</li>
  <li>แนบหลักฐานการโอนเงิน</li>
                @else
                <li>Click &#39;m.me/yakyimthai&#39;</li>
                <li>Inform your contact by providing your name, surname,
                  address and mobile number with your payment evidence.</li>
                @endif
              </ul>
            </div>
          </div>
          <div class="row">
            <div class="col-md-5">
              <p class="heading-step ">3. {{trans('how-to.paypal-confirm-line')}} </p>
                {{--<img src="{{ URL::asset('images/qr-code.jpg') }}" alt="" class="center img-responsive" >--}}
                <a href="https://line.me/R/ti/p/%40yakyim" target="_blank"><img src='https://chart.googleapis.com/chart?cht=qr&chl=https%3A%2F%2Fline.me%2FR%2Fti%2Fp%2F%2540yakyim&chs=180x180&choe=UTF-8&chld=L|2' rel='nofollow' alt='qr code' class="center img-responsive"></a>
                <a href='http://www.qrcode-generator.de' border='0' style='cursor:default'  rel='nofollow'></a>
            </div>
            <div class="col-md-7">

              <ul>
                @if(session()->get('locale') == 'th')
                <li>ADD LINE ID : @yakyim </li>
  <li>แจ้งข้อมูลการติดต่อ</li>
  <li>แนบหลักฐานการโอนเงิน</li>
                @else
                <li>Add Line ID: @yakyim</li>
                <li>Inform your contact by providing your name, surname,
                  address and mobile number with your payment evidence.</li>
                @endif
              </ul>
            </div>
          </div>

          </div>


        </div>

      </div>


    </div>

@endsection

@section('more-script')

  <script src="{{ URL::asset('tools/bxslider/jquery.bxslider.js') }}"></script>
  <script type="text/javascript">
    $('.bxSlider').bxSlider({
      'auto': true,
      'controls': true
    });
  </script>
  <script>
  $(document).ready(function(){
    $('.dropdown-submenu a.test').on("click ", function(e){
      $(this).next('ul').toggle();
      e.stopPropagation();
      e.preventDefault();
    });
    // $('.dropdown-submenu a').on(" mouseenter", function(e){
    //   $(this).next('ul').hide();
    //   e.stopPropagation();
    //   e.preventDefault();
    // });
  });
  </script>
@endsection
