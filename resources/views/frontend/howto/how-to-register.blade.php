@extends('frontend.layouts/main')

@section('title','Home')

@section('more-stylesheet')
<link rel="stylesheet" href="{{ URL::asset('css/front/howto.css') }}">
@endsection

@section('content')
    <div id="banner">
      <div id="inner-banner">
        <div id="pic-banner">
          <img src="{{ URL::asset('images/m02.png') }}" alt="" class="">
        </div>
        <div id="text-banner" class="text-center">
          <p class="how-to">HOW TO</p>
          <h2>วิธีสมัครสมาชิก</h2>
          <p class="caption">สำหรับลูกค้าที่ซื้อสินค้าเราขอแนะนำให้สมัครสมาชิก
เพื่อสะสมแต้มเอาไว้แลกส่วนลดสินค้า</p>
        </div>

      </div>

    </div>
    <div id="content">
      <div class="container">
        <div class="col-md-3">
          @include('frontend.howto.side-how-to')
        </div>
        <div class="col-md-9" id="inner">
          <div class="row">
            <div class="col-md-5">
              <p class="heading-step ">1. {{ trans('how-to.new-account') }}</p>
              <img src="{{ URL::asset('images/how-to/hr1.png') }}" alt="" class="center img-responsive">
            </div>
            <div class="col-md-7">

              <ul>
                @if(session()->get('locale') == 'th')
                  <li>เข้าระบบในหน้าเว็บ www.yakyim.com แล้วมองหาปุ่ม
    sign in อยู่มุมขวาบน</li>
                  <li>ระบบจะแสดงรายละเอียดของสินค้า เช่น คำอธิบาย,
    ราคา, ขนาด</li>
                  <li>กดปุ่ม CREATE AN ACCOUNT </li>
                @else
                <li>Go to www.yakyim.com</li>
                <li>CLick 'SIGN IN'</li>
                <li>Our website will show description of products such price and size.</li>
                <li>Click 'CREATE ACCOUNT'</li>
                @endif
              </ul>
            </div>
          </div>
          <div class="row">
            <div class="col-md-5">
              <p class="heading-step ">2. {{trans('how-to.new-account-two')}}</p>
              <img src="{{ URL::asset('images/how-to/hr2.png') }}" alt="" class="center img-responsive">
            </div>
            <div class="col-md-7">

              <ul>
                @if(session()->get('locale') == 'th')
                <li>ในหน้า “ข้อมูลส่วนตัว” ให้ใส่ข้อมูลให้ครบ
  ทุกช่องที่มี*</li>
                @else
                <li>Please fill personal data by providing your name, surname, email address and password. (* Required Field)</li>
                @endif
              </ul>
            </div>
          </div>
          <div class="row">
            <div class="col-md-5">
              <p class="heading-step ">3. {{trans('how-to.new-account-three')}}</p>
              <img src="{{ URL::asset('images/how-to/hr3.png') }}" alt="" class="center img-responsive">
            </div>
            <div class="col-md-7">

              <ul>
                @if(session()->get('locale') == 'th')
                <li>เมื่อใส่ข้อมูลครบแล้วให้กดปุ่ม สมัครสมาชิก
  เพื่อยืนยัน</li>
                @else
                <li>Click 'CREATE ACCOUNT' for complete your registration.</li>
                @endif
              </ul>
            </div>
          </div>

          </div>


        </div>

      </div>


    </div>

@endsection

@section('more-script')

  <script src="{{ URL::asset('tools/bxslider/jquery.bxslider.js') }}"></script>
  <script type="text/javascript">
    $('.bxSlider').bxSlider({
      'auto': true,
      'controls': true
    });
  </script>
  <script>
  $(document).ready(function(){
    $('.dropdown-submenu a.test').on("click ", function(e){
      $(this).next('ul').toggle();
      e.stopPropagation();
      e.preventDefault();
    });
    // $('.dropdown-submenu a').on(" mouseenter", function(e){
    //   $(this).next('ul').hide();
    //   e.stopPropagation();
    //   e.preventDefault();
    // });
  });
  </script>
@endsection
