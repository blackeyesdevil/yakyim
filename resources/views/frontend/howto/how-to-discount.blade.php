@extends('frontend.layouts/main')

@section('title','Home')

@section('more-stylesheet')
<link rel="stylesheet" href="{{ URL::asset('css/front/howto.css') }}">
@endsection

@section('content')
    <div id="banner">
      <div id="inner-banner">
        <div id="pic-banner">
          <img src="{{ URL::asset('images/how-to-m06.png') }}" alt="" class="">
        </div>
        <div id="text-banner" class="text-center">
          <p class="how-to">HOW TO</p>
          <h2>การใช่ส่วนลด</h2>
          <p class="caption">คุณสามารถใช้ส่วนลดในการสั่งซื้อสินค้าได้ โดยโค้ดส่วนลดอาจได้จากกิจกรรมต่างๆของยักษ์ยิ้ม</p>
        </div>

      </div>

    </div>
    <div id="content">
      <div class="container">
        <div class="col-md-3">
          @include('frontend.howto.side-how-to')
        </div>
        <div class="col-md-9" id="inner">
        <h2>{{trans('how-to.discount')}}</h2>
          <div class="row">
            <div class="col-md-5">
              <p class="heading-step ">1. {{trans('how-to.discount-1')}}</p>
              <img src="{{ URL::asset('images/how-to/hdc1.png') }}" alt="" class="center img-responsive">
            </div>
            <div class="col-md-7">
              <ul>
              <br>
              @if(session()->get('locale') == 'th')
                <li>หลังการเลือกรูปแบบการชำระเงิน คุณสามารถเลือกให้คูปองส่วนลดที่ได้จากกิจกรรมพิเศษของยักษ์ยิ้ม</li>
              @endif
              </ul>
            </div>
          </div>

          <div class="row">
            <div class="col-md-5">
              <p class="heading-step ">2. {{trans('how-to.discount-2')}}</p>
              <img src="{{ URL::asset('images/how-to/hdc2.png') }}" alt="" class="center img-responsive">
            </div>
            <div class="col-md-7">
              <ul>
                @if(session()->get('locale') == 'th')
                  <li>ระบบจะทำการตรวจสอบ รหัสส่วนลดเป็นรหัสที่ถูกต้องหรือไม่? โดยคลิกที่ปุ่ม<b>"ตรวจสอบ"</b></li>
                @endif
              </ul>
            </div>
          </div>
          <div class="row">
            <div class="col-md-5">
              <p class="heading-step ">3. {{trans('how-to.discount-3')}}</p>
              <img src="{{ URL::asset('images/how-to/hdc3.png') }}" alt="" class="center img-responsive">
            </div>
            <div class="col-md-7">
              <ul>
                @if(session()->get('locale') == 'th')
                  <li>ระบบจะทำการคำนวนส่วนลดของสินค้าใน Order นั้น ให้โดยอัตโนมัติ</li>
                @endif

              </ul>
            </div>
          </div>


        </div>

      </div>


    </div>

@endsection

@section('more-script')

  <script src="{{ URL::asset('tools/bxslider/jquery.bxslider.js') }}"></script>
  <script type="text/javascript">
    $('.bxSlider').bxSlider({
      'auto': true,
      'controls': true
    });
  </script>
  <script>
  $(document).ready(function(){
    $('.dropdown-submenu a.test').on("click ", function(e){
      $(this).next('ul').toggle();
      e.stopPropagation();
      e.preventDefault();
    });
    // $('.dropdown-submenu a').on(" mouseenter", function(e){
    //   $(this).next('ul').hide();
    //   e.stopPropagation();
    //   e.preventDefault();
    // });
  });
  </script>
@endsection
