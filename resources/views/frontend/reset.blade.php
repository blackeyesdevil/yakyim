@extends('frontend.layouts/main')
@section('more-stylesheet')
@endsection

@section('content')
<div id="content">
  <div class="container" >
    <h1 class="text-center">{{ trans('forget-reset.reset') }}</h1>
    <p class="text-center">{{ trans('forget-reset.reset-detail') }}</p>
    <div class="col-md-4 col-md-offset-4">
      @if(session()->has('errorMsg'))
        <div class="alert alert-danger text-center">{{ session()->get('errorMsg') }}</div>
      @endif
      @if(session()->has('successMsg'))
        <div class="alert alert-success text-center">{{ session()->get('successMsg') }}</div>
      @endif

      <form action="{{ URL::to('reset-password/check') }}" method="post" class="form-horizontal">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="gen_token" value="{{ Input::get('token') }}">
        <div class="row">
          <?php $field = 'password'; ?>
          <div class="form-group">
            <label for="{{ $field }}">{{ trans('account.new_password') }} *</label>
            <input type="password" class="form-control" name="{{ $field }}">
            @if(!empty($errors->first($field)))
              <div class="text-danger">{{ $errors->first($field) }}</div>
            @endif
          </div>
          <?php $field = 'c_password' ;?>
          <div class="form-group">
            <label for="{{ $field }}">{{ trans('account.' . $field) }} *</label>
            <input type="password" class="form-control" name="{{ $field }}" aria-describedby="helpBlock">
            @if(!empty($errors->first($field)))
              <div class="text-danger">{{ $errors->first($field) }}</div>
            @endif
          </div>

        </div>
        <div class="text-center">
          <button class="btn btn-default btn-lookbook">{{ trans('forget-reset.send') }}</button>
        </div>
      </form>
    </div>
  </div>
</div>

@endsection
