<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {!! SEOMeta::generate() !!}
    {!! OpenGraph::generate() !!}
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="lang" content="{{ $lang }}">


    <!-- Custom Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ URL::asset('fonts/foundation-icons/foundation-icons.css') }}">

    <!-- Sass compile result-->
    <link href="{{URL::asset('css/front/app.css')}}" rel="stylesheet">

    @yield('more-stylesheet')
            <!-- END Sass -->

    {{-- Fonts --}}
    <link href="{{URL::asset('css/fonts/fontface.css')}}" rel="stylesheet">
    {{-- END Fonts --}}
    <link href="{{URL::asset('css/glyphicons-master/css/bootstrap.icon-large.min.css')}}" rel="stylesheet">
</head>

<body>
@include('frontend.layouts.header')
     @yield('content')
@include('frontend.layouts.footer')
</body>

<!-- Import script from browserify -->
<script src="{{URL::asset('js/jquery.min.js')}}"></script>
<script src="{{URL::asset('js/front/main.js')}}"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<!-- END -->

@yield('more-script')

</html>
