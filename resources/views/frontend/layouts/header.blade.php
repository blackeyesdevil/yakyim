<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-32003856-1', 'auto');
    ga('send', 'pageview');

</script>
<div id="nav">
    <div class="flex container-fluid">
        <div class="nav-brand">
            <a href="{{ URL::route('home') }}" class="nav-logo"><img src="{{ URL::asset('images/logo.jpg') }}" alt=""></a>
            <a href="{{ url()->to('lang/th') }}"  class="nav-lang @if($lang == 'th') active @endif" >TH</a> | <a href="{{ url()->to('lang/en') }}"  class="nav-lang @if($lang == 'en') active @endif">EN</a>
        </div>
        <div class="nav-menu">
            <a href="{{ URL::to('account/wishlist') }}" class="icon-wishlist hidden-md hidden-lg"><span class="wishlist-qty @if($wishlist_qty > 0) alert-qty @endif" >{{ $wishlist_qty }}</span><img src="{{ url()->asset('images/icon-wishlist-black.png') }}"></a>
            <a href="{{ URL::to('cart') }}" class="icon-bag hidden-md hidden-lg"><span class="header-qty @if($cart_qty > 0) alert-qty @endif" >{{ $cart_qty }}</span><img src="{{ url()->asset('images/shopping-bag-black.png') }}"> </a>
            <a href="#" class="toggle-menu hidden-md hidden-lg text-right">
                <img src="{{ url()->asset('images/icon-toggle.png') }}">
            </a>
            <ul>
                <li>
                    <a href="{{ url()->to('product/top/1') }}">{{ trans('header.top') }}</a>
                </li>
                <li>
                    <a href="{{ url()->to('product/bottom/3') }}">{{ trans('header.bottom') }}</a>
                </li>
                <li>
                    <a href="{{ url()->to('product/promotion/2') }}">{{ trans('header.promotion') }}</a>
                </li>
                <li>
                    <a href="{{ url()->to('product/accessory/4') }}">{{ trans('header.accessory') }}</a>
                </li>
                <li>
                    <a href="{{ url()->to('product/selected-brand/67') }}">{{ trans('header.other') }}</a>
                </li>
                <li>
                    <a href="http://www.chubbystyle.com/" target="_blank">LOOKBOOK</a>
                </li>
                <li>
                    @if(!session()->has('s_user_id'))
                        <a class="btn-signin" href="{{ url()->to('login') }}">SIGN IN</a>
                    @else
                        <a class="btn-signin" href="{{ url()->to('account/profile') }}">MY ACCOUNT</a>
                    @endif
                </li>
                <li class="hidden-sm hidden-xs">
                    <a href="{{ URL::to('account/wishlist') }}" class="icon-wishlist"><span class="wishlist-qty @if($wishlist_qty > 0) alert-qty @endif" >{{ $wishlist_qty }}</span><img src="{{ url()->asset('images/icon-wishlist-black.png') }}"> <span class="hidden-md hidden-lg">Wishlist</span></a>
                </li>
                <li class="hidden-sm hidden-xs">
                    <a href="{{ URL::to('cart') }}" class="icon-bag"><span class="header-qty @if($cart_qty > 0) alert-qty @endif" >{{ $cart_qty }}</span><img src="{{ url()->asset('images/shopping-bag-black.png') }}"> <span class="hidden-md hidden-lg">Shopping Bag</span></a>
                </li>
            </ul>
        </div>
    </div>
</div>