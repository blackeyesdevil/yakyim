 <footer>
    <div id="full-footer" class="container">
        <div class="row">
            <div class="col-md-3 text-center" style="padding-top:30px;">
                <img src="{{ URL::asset('images/logo-white.png') }}" alt="" width="150px" class="center img-responsive">
                <p style="margin-top:10px;">
                    <a href="https://www.facebook.com/yakyimthai" target="_blank"><img src="{{ URL::asset('images/icon_facebook.png') }}" alt="" width="30px"></a>
                    <a href="http://m.me/yakyimthai" target="_blank"><img src="{{ URL::asset('images/icon_mail.png') }}" alt="" width="30px"></a>
                    <a href="{{ url()->to('contactus') }}" title="02-191-3955"><img src="{{ URL::asset('images/icon_tel.png') }}" alt="" width="30px"></a>
                    <a href="http://line.me/ti/p/@yakyim" target="_blank"><img src="{{ URL::asset('images/icon_line.png') }}" alt="" width="30px"></a>
                </p>
                
                <div id='ajaxDivReg'style="display:inline-block;"><script src="https://www.trustmarkthai.com/callbackData/initialize.js?t=79a93bdca9-33-5-9697acf6611ab796f3e0eb0103356efb629" id="dbd-init"></script><div id="Certificate-banners"></div></div>
               
            </div>

            <div class="col-md-3 ">
                    <ul class="list-unstyled">
                        <li class="topic">{{ trans('global.about-us') }}</li>
                        <li><a href="{{ URL::to('aboutus') }}">{{ trans('global.company-profile') }}</a></li>
                        <li><a href="{{ URL::to('contactus') }}">{{ trans('global.contact-us') }}</a></li>
                    </ul>

                    <br>

                    <ul class="list-unstyled">
                        <li class="topic">SERVICES</li>
                        <li><a href="{{ URL::to('account/order-tracking') }}">{{ trans('global.tracking-order') }}</a></li>
                    </ul>
            </div>

            <div class=" col-md-3">
                    <ul class="list-unstyled{{ trans('global.th-font') }}">
                        <li class="topic">HOW TO</li>
                        <li><a href="{{ URL::to('how-to-register') }}">{{ trans('global.member-register') }}</a></li>
                        <li><a href="{{ URL::to('how-to-pay') }}">{{ trans('global.payment') }}</a></li>
                        <li><a href="{{ URL::to('how-to-check') }}">{{ trans('global.status-check') }}</a></li>
                        <li><a href="{{ URL::to('how-to-sizing-chart') }}">{{ trans('global.sizing-chart') }}</a></li>
                        <li><a href="{{ URL::to('how-to-change-product') }}">{{ trans('global.change-product') }}</a></li>
                        <li><a href="{{ URL::to('how-to-discount') }}">{{ trans('global.discount') }}</a></li>
                        <li><a href="{{ URL::to('how-to-shipping') }}">{{ trans('global.shipping') }}</a></li>
                        <li><a href="{{ URL::to('how-to-change-address') }}">{{ trans('global.change-address') }}</a></li>
                    </ul>
            </div>

            <div class="col-md-3" >
                <p class="text-left{{ trans('global.th-font') }}">{{ trans('global.newsletter') }}</p>
                <p class="text-center">
                    <form method="POST" action="#" id="newsletter-form" class="form-inline">
                        <input type="text" name="email-subscribe" placeholder="E-mail@example.com" class="form-control" value="">
                        <button type="submit" class="btn btn-subscribe">Subscribe</button>
                        <div class="alert-subscribe hide{{ trans('global.th-font') }}"></div>
                    </form>
                </p>
                <br>

                <p class="text-left{{ trans('global.th-font') }}">{{ trans('global.delivery-services') }}</p>
                <p class="text-left"><img src="{{ URL::asset('images/icon-lalamove.png') }}" alt="" width="70px"><img src="{{ URL::asset('images/thailandpost.png') }}" alt="" width="70px"></p>
                <br>

                <p class="text-left{{ trans('global.th-font') }}">{{ trans('global.payment') }}</p>
                

                <p class="text-left"><img src="{{ URL::asset('images/wpcopylogo_image_1.png') }}" alt="" width="150px"></p>
            </div>
        </div>

        <div class="bottom">
          <p class="text-center">COPYRIGHT  &copy; 2016 YAKYIM, ALL RIGHT RESERVED</p>
          <p class="text-center">E-COMMERCE REGISTRATION NUMBER: 0135558001720</p>
        </div>
    </div>
</footer>
