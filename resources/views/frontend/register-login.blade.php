@extends('frontend.layouts/main')

@section('title','Register')

@section('more-stylesheet')

@endsection

@section('content')
  <div id="content">
      <div class="container">
        <h1 class="text-center">CREATE AN ACCOUNT</h1>
        <div class="col-md-6 col-md-offset-3">
          <form>
            <div class="row">
              <div class="form-group col-md-6">
                <label for="username">USER NAME*</label>
                <input type="text" class="form-control" id="username" placeholder="">
              </div>
              </div>
              <div class="row">
                <div class="form-group col-md-6">
                  <label for="fname">FIRST NAME*</label>
                  <input type="text" class="form-control" id="fname" placeholder="">
                </div>
                <div class="form-group col-md-6">
                  <label for="lname">LAST NAME*</label>
                  <input type="text" class="form-control" id="lname" placeholder="">
                </div>
              </div>
              <div class="row">
                <div class="form-group col-md-6">
                  <label for="email">EMAIL*</label>
                  <input type="email" class="form-control" id="email" placeholder="">
                 
                </div>
                 <div class="form-group col-md-6">
                  <label for="pwd">PASSWORD*</label>
                  <input type="password" class="form-control" id="pwd" placeholder=""  aria-describedby="helpBlock"> 
                  <span id="helpBlock" class="help-block">(minimum 6 characters)</span>
                </div>
              </div>
              <div class="form-horizontal">
                <div class="form-group">
                  <label for="bd" class="col-sm-3 control-label" style="text-align:left;">DATE OF BIRTH</label>
                  <div class="col-sm-9">
                    <input type="date" class="form-control" id="bd" placeholder="">
                  </div>
                </div>
              </div>
            <div class="checkbox">
              <label>
                <input type="checkbox"> Sign up for our newsletter!
              </label>
            </div>
            <div class="checkbox">
              <label>
                <input type="checkbox"> Recieve opecial offers from our partners!
              </label>
            </div>
            <div class="text-center">
              <button class="btn btn-default btn-lookbook">CREATE ACCOUNT</button>
            </div>
          </form>
        </div>
       


      </div>
      

    </div>
@endsection
@section('more-script')
@endsection
