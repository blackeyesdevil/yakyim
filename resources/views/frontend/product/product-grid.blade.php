@extends('frontend.layouts/main')

@section('title','Products')

@section('more-stylesheet')
<link rel="stylesheet" href="{{ URL::asset('css/front/product-global.css') }}">
<link rel="stylesheet" href="{{ URL::asset('css/front/product-grid.css') }}">
@endsection

@section('content')
<div id="banner">
  <div class="container">

  </div>
</div>

<div id="product-grid" class="container">
  <div id="breadcrumb">
    @include('frontend.product.breadcrumb')
  </div>

  <div id="content" class="row">
    <div class="col-md-3">
      @include('frontend.product.sidebar')
    </div>
    <div class="col-md-9">
      <div id="main-content">
        <div class="option">

          <div class="view-option">
            <a class="btn active" href="#"><i class="fi-thumbnails"></i> {{ trans('product.grid') }}</a><a class="btn" href="{{ $link }}"><i class="fi-list"></i> {{ trans('product.list') }}</a>
          </div>

          <div class="sorting-option">
            <label class="dropdown">
              <span class="topic">{{ trans('product.sort-by') }}:</span>
              <select id="sorting">
                <option value="1"{{ ($order_by=='product_id')?' selected="selected"':'' }}>{{ trans('product.sort-by-newest') }}</option>
                <option value="2"{{ ($order_by==('product_name_'.session()->get('locale')))?' selected="selected"':'' }}>{{ trans('product.sort-by-name') }}</option>
                <option value="3"{{ ($order_by=='retail_price')?' selected="selected"':'' }}>{{ trans('product.sort-by-price') }}</option>
              </select>
            </label>
          </div>
        </div>

        <div class="list">
          @if (count($products) == 0)
          <div class="msg">{{ trans('product.no-products') }}</div>
          @endif
          <div class="row">
            @foreach ($products as $i => $product)
            <?php
              if (!empty($product->photo))
                $img_url = URL::asset('uploads/product_option/250/' . $product->photo);
              else
                $img_url = URL::asset('images/no-pic.jpg');

              $p_retail = $product->retail_price;
              $p_special = $product->special_price;
              $p_price = $p_retail;
              if(!empty($p_special)) $p_price = $p_special;
            ?>
            <!-- <a href="MainFunction::getDetailLink($current_category->category_id, $current_sub_category->category_id, $current_sub_sub_category->category_id, $product->product_id)" class="item col-sm-3"> -->
            <a href="{{ URL::route('product.product-detail', $product->product_id) }}" class="item col-sm-3">
              <div class="product-img">
                <img src="{{ $img_url }}" />
              </div>

              <div class="product-info">
                <div class="product-name">{{ $product['product_name_' . session()->get('locale')] }}</div>
                <div class="product-price">THB {{ number_format($p_price, 2) }}</div>
              </div>
            </a>
            @if (($i+1)%4 == 0)
          </div>
          <div class="row">
            @endif
            @endforeach
          </div>
        </div>

        <div id="pagination-bar">
          {{ $products->render() }}
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('more-script')
<script type="text/javascript">
  var link = '{{ $link2 }}';
  var locale = '{{ session()->get("locale") }}';
</script>
<script type="text/javascript" src="{{ URL::asset('js/front/product-list.js')}}"></script>
@endsection
