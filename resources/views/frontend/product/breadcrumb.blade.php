<a href="{{ URL::route('home') }}">{{ trans('global.home') }}</a>
<span class="arrow">></span>
<a href="{{ URL::route('product.category-list') }}">{{ trans('product.product-category') }}</a>
<span class="arrow">></span>
@if ($current_sub_sub_category->category_id != '0')
<a href="{{ URL::route('product.cat-product-list', array($current_category->category_id, 'grid')) }}">{{ $current_category->category_name_en }}</a>
<span class="arrow">></span>
<a href="{{ URL::route('product.subcat-product-list', array($current_category->category_id, $current_sub_category->category_id, 'grid')) }}">{{ $current_sub_category->category_name_en}}</a>
<span class="arrow">></span>
<span class="active">{{ $current_sub_sub_category['category_name_' . session()->get('locale')] }}</span>
@elseif ($current_sub_category->category_id != '0')
<a href="{{ URL::route('product.cat-product-list', array($current_category->category_id, 'grid')) }}">{{ $current_category['category_name_' . session()->get('locale')] }}</a>
<span class="arrow">></span>
<span class="active">{{ $current_sub_category['category_name_' . session()->get('locale')] }}</span>
@else
<span class="active">{{ $current_category['category_name_' . session()->get('locale')] }}</span>
@endif
