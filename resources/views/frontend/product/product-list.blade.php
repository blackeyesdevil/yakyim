<?php
$page = Input::get('page');
?>
@extends('frontend.layouts/main')

@section('title','Home')

@section('more-stylesheet')

<link rel="stylesheet" href="{{ URL::asset('tools/bxslider/jquery.bxslider.css') }}">
<link rel="stylesheet" href="{{ URL::asset('css/front/list.css') }}">
@endsection

@section('content')
    @if($category->parent_category_id == 0 && ($page == null || $page == '1'))

        <?php
        $dir_bg = 'uploads/category_bg/';
        $img_bg_name = $category->img_bg_name;
        $sm = '995/'; $md = '1600/';
        $img_bg_dir_sm = $dir_bg.$sm.$img_bg_name;
        $img_bg_dir_md = $dir_bg.$md.$img_bg_name;

        $img_bg_path_sm = url()->asset($img_bg_dir_sm);
        $img_bg_path_md = url()->asset($img_bg_dir_md);

        if($img_bg_name == '' || !file_exists($img_bg_dir_md)){
            $img_bg_path_sm = 'https://fakeimg.pl/995x300/?text=Yakyim';
            $img_bg_path_md = 'https://fakeimg.pl/1600x460/?text=Yakyim';
        }

        $dir_right = 'uploads/category_right/460/';
        $img_right_name = $category->img_right_name;
        $img_right_dir = $dir_right.$img_right_name;
        $img_right_path = url()->asset($img_right_dir);
        ?>
        <div id="banner" class="hidden-xs" data-value="true" data-sm="{{ $img_bg_path_sm }}" data-md="{{ $img_bg_path_md }}">
          <div id="inner-banner">
            <div id="text-banner">
              {!! $category['title_'.$lang] !!}
            </div>
            <div id="pic-banner">
                @if($img_right_name != '' || file_exists($img_right_dir))
                    <img src="{{ $img_right_path }}" alt="" class="img-responsive">
                @endif
            </div>
          </div>
        </div>
    @endif
    <div id="content">
      <div class="container">
        <h1 style="padding-left: 30px;">{{ $category['category_name_'.$lang] }}</h1>
        <hr style="max-width:95%">
        <div style="padding-left: 30px;">
            @if(count($search_category) > 0)
              <div class="dropdown">
                <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">{{ trans('product.shirts-dropdown-category') }}
                <span class="caret"></span></button>
                <ul class="dropdown-menu">
                    @foreach($search_category as $search_cat)
                        <?php
                            $sub_cat_id = $search_cat->category_id;
                            $sub_cat_name = $search_cat['category_name_'.$lang];
                            $sub_cat_name_en = str_slug($search_cat['category_name_en'],'-');
                            $a_sub_sub_cate = $search_cat->subcate;
                        ?>
                      <li @if(count($a_sub_sub_cate) > 0) class="dropdown-submenu" @endif>
                          <a class="test" tabindex="-1" href="{{ url()->to('product/'.$sub_cat_name_en.'/'.$sub_cat_id) }}">{{ $sub_cat_name }} @if(count($a_sub_sub_cate) > 0) <span class="caret"></span> @endif</a>

                          @if(count($a_sub_sub_cate) > 0)
                              <ul class="dropdown-menu">
                                  @foreach($a_sub_sub_cate as $sub_sub_cat)
                                  <?php
                                  $sub_sub_cat_id = $sub_sub_cat->category_id;
                                  $sub_sub_cat_name = $sub_sub_cat['category_name_'.$lang];
                                  $sub_sub_cat_name_en = str_slug($sub_sub_cat['category_name_en'],'-');
                                  ?>
                                      <li><a href="{{ url()->to('product/'.$sub_sub_cat_name_en.'/'.$sub_sub_cat_id) }}">{{ $sub_sub_cat_name }}</a></li>
                                  @endforeach
                              </ul>
                          @endif
                      </li>
                    @endforeach
                </ul>
              </div>
            @endif
            @if(count($search_size) > 0)
              <div class="dropdown">
                <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">{{ trans('product.shirts-dropdown-size') }}
                <span class="caret"></span></button>
                <ul class="dropdown-menu">
                    @if($category_id <> 4)

                    @foreach($sizes as $value)

                    @foreach($search_size as $size)
                            @if($value->option_name == $size->option_name)
                      <li><a tabindex="-1" href="{{ url()->to('product/'.str_slug($category->category_name_en).'/'.$category->category_id.'/size/'.$size->option_name) }}">{{ $size->option_name }}</a></li>
                            @endif
                    @endforeach

                    @endforeach

                        @else

                        @foreach($search_size as $size)
                                <li><a tabindex="-1" href="{{ url()->to('product/'.str_slug($category->category_name_en).'/'.$category->category_id.'/size/'.$size->option_name) }}">{{ $size->option_name }}</a></li>
                        @endforeach


                    @endif
                </ul>
              </div>
            @endif
        </div>
        <section class="no-padding" id="product">
            @if(count($products) > 0)
              <div class="container-fluid">
                  @foreach($products as $key => $item)
                      <?php

                      $product_id = $item->product_id;
                      $product_name = $item['product_name_'.$lang];
                      $url_name = str_slug($item['product_name_en']);
                      $url_to = url()->to('product/detail/'.$product_id.'/'.$url_name);

                      $retail_price = $item->retail_price;
                      $dollar_retail_price = $obj_dollar->cal_dollar($retail_price,$currency);

                      $price = $retail_price;
                      $dollar_price = $dollar_retail_price;

                      $special_price = $item->special_price;
                      $dollar_special_price = $obj_dollar->cal_dollar($special_price,$currency);

                      if($special_price > 0){
                          $price = $special_price;
                          $dollar_price = $dollar_special_price;
                      }


                      $dir = 'uploads/product/';
                      $img_name = $item->img_name;
                      $img_name2 = $item->img_name2;
                      $img_dir = $dir.$img_name;
                      $img_dir2 = $dir.$img_name2;
                      $xs_sm = '420/'; $md_lg = '570/';

                      $img_xs_sm_path = url()->asset($dir.$xs_sm.$img_name);
                      $img_md_lg_path = url()->asset($dir.$md_lg.$img_name);

                      $img_xs_sm_path2 = url()->asset($dir.$xs_sm.$img_name2);
                      $img_md_lg_path2 = url()->asset($dir.$md_lg.$img_name2);

                      if($img_name == '' || !file_exists($img_dir)){
                          $img_xs_sm_path = 'https://fakeimg.pl/315x420/?text=Yakyim';
                          $img_md_lg_path = 'https://fakeimg.pl/427x570/?text=Yakyim';
                      }
                      if($img_name2 == '' || !file_exists($img_dir2)){
                          $img_xs_sm_path2 = $img_xs_sm_path;
                          $img_md_lg_path2 = $img_md_lg_path;
                      }
                      ?>
                      <div class="col-lg-4 col-sm-6">
                          <a href="{{ $url_to }}" class="product-box" title="{{ $product_name }}">
                              @if($item->sum_qty == '0')
                                  <div class="out-of-stock">{{ trans('product.sold-out') }}</div>
                              @endif
                              <img src="{{ $img_xs_sm_path }}" data-alt-src="{{ $img_xs_sm_path2 }}" class="img-small img-responsive center img-product hidden-md hidden-lg" alt="{{ $product_name }}">
                              <img src="{{ $img_md_lg_path }}" data-alt-src="{{ $img_md_lg_path2 }}" class="img-big img-responsive center img-product hidden-xs hidden-sm" alt="{{ $product_name }}">

                              {{--<button class="add-to-wishlist"><span class="glyphicon glyphicon-heart-empty" aria-hidden="true"></span></button>--}}
                          </a>
                          <div class="box-caption text-center">
                              <a href="{{ $url_to }}">
                                  <p class="title">{{ $product_name }}</p>
                              </a>
                              <p class="price">
                                  @if($lang == 'th')
                                      {{ '฿ '.number_format($price,2) }}
                                        @if($special_price > 0)
                                            <span class="retail_price">{{ '฿ '.number_format($retail_price,2) }}</span>
                                        @endif
                                  @else
                                      {{ '$ '.number_format($dollar_price,2) }}
                                      @if($special_price > 0)
                                          <span class="retail_price">{{ '$ '.number_format($dollar_retail_price,2) }}</span>
                                      @endif
                                  @endif
                              </p>
                          </div>
                      </div>
                      @if(($key+1) % 2 == 0)
                        <div class="visible-sm-block visible-md-block clearfix"></div>
                      @endif

                      @if(($key+1) % 3 == 0)
                          <div class="visible-lg-block clearfix"></div>
                      @endif

                  @endforeach
              </div>
              <div class="right">
                {!! $products->links() !!}
              </div>
            @else
                <div class="text-center">No Result.</div>
            @endif
      </section>

      </div>


    </div>

@endsection

@section('more-script')

  <script src="{{ URL::asset('tools/bxslider/jquery.bxslider.js') }}"></script>
  <script src="{{ URL::asset('js/front/product-list.js') }}"></script>
  <script type="text/javascript">
    $('.bxSlider').bxSlider({
      'auto': true,
      'controls': true
    });
  </script>
  <script>
  $(document).ready(function(){
    $('.dropdown-submenu a.test').on("click ", function(e){
      $(this).next('ul').toggle();
      e.stopPropagation();
      e.preventDefault();
    });
    // $('.dropdown-submenu a').on(" mouseenter", function(e){
    //   $(this).next('ul').hide();
    //   e.stopPropagation();
    //   e.preventDefault();
    // });
  });
  </script>
@endsection
