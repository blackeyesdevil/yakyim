@extends('frontend.layouts/main')

@section('title','Products')

@section('more-stylesheet')
<link rel="stylesheet" href="{{ URL::asset('tools/bxslider/jquery.bxslider.css') }}">
<link rel="stylesheet" href="{{ URL::asset('css/front/category-list.css') }}">
@endsection

@section('content')
<div id="category-list">
  <!-- <div class="highlight">
    <div class="headline{{ trans('global.th-font') }}">{{ trans('product.our-products') }}</div>
    <div class="sub-headline{{ trans('global.th-font') }}">{{ trans('product.our-products-slogan') }}</div>
  </div> -->

  <div id="banner" class="bxSlider">
    @foreach ($banners as $banner)
      <?php
        $img_name = $banner->img_name;
        $img_path = URL::asset('uploads/banner_product/'.$img_name);
        if($img_name == '' || file_exists($img_path))
          $img_path = url()->asset('images/yakyim/1167x352.jpg');
      ?>
      @if (!empty($banner->link))
      <a href="{{ $banner->link }}">
      @endif
        <img src="{{ $img_path }}" class="img-responsive" />
      @if (!empty($banner->link))
      </a>
      @endif
    @endforeach
  </div>

  <div class="list container">
    @foreach ($categories as $cat)
    <div class="col-sm-6 col-md-4">
      <a href="{{ URL::route('product.cat-product-list', array($cat->category_id, 'grid')) }}" class="item">
        <img src="https://loft.bkksol.com/uploads/category/{{ $cat->img_name }}" />
        <div class="category-name{{ trans('global.th-font') }}">
          <i class="{{ $cat->icon_name }}"></i>
          {{ $cat['category_name_' . session()->get('locale')] }}
        </div>
      </a>
    </div>
    @endforeach
  </div>
</div>
@endsection

@section('more-script')
  <script src="{{ URL::asset('tools/bxslider/jquery.bxslider.js') }}"></script>
  <script type="text/javascript">
    $('.bxSlider').bxSlider({
      'auto': true,
      'controls': true
    });
  </script>
@endsection
