<div id="sidebar">
  <div class="category-list box">
    <div class="headline">{{ trans('product.product-categories') }} <i class="glyphicon glyphicon-chevron-down visible-xs"></i></div>

    <div class="list">
      @foreach ($categories as $cat)
      <div class="item{{ ($current_category->category_id == $cat->category_id)?' active':'' }}">
        <a href="{{ URL::route('product.cat-product-list', array($cat->category_id, 'grid')) }}" class="text">
          {{ $cat['category_name_' . session()->get('locale')] }}
        </a>

        @if ($current_category->category_id == $cat->category_id)
        <div class="sub-category-list">
          @foreach ($cat->sub_categories as $sub_cat)

          <div class="item{{ ($current_sub_category->category_id == $sub_cat->category_id)?' active':'' }}">
            <i class="fi-play"></i>

            <a href="{{ URL::route('product.subcat-product-list', array($cat->category_id, $sub_cat->category_id, 'grid')) }}" class="text">
              {{ $sub_cat['category_name_' . session()->get('locale')] }}
            </a>

            @if ($current_sub_category->category_id == $sub_cat->category_id)
            <div class="sub-sub-category-list">
              @foreach ($sub_cat->sub_categories as $sub_sub_cat)
              <div class="item{{ ($current_sub_sub_category->category_id == $sub_sub_cat->category_id)?' active':'' }}">
                <i class="fi-play"></i>

                <a href="{{ URL::route('product.subsubcat-product-list', array($cat->category_id, $sub_cat->category_id, $sub_sub_cat->category_id, 'grid')) }}" class="text">
                  {{ $sub_sub_cat['category_name_' . session()->get('locale')] }}
                </a>
              </div>
              @endforeach
            </div>
            @endif
          </div>
          @endforeach
        </div>
        @endif
      </div>
      @endforeach
    </div>
  </div>

  <div id="best-seller" class="box">
    <div class="headline">{{ trans('product.best-seller') }}</div>

    <div class="product-list list">
      @foreach ($best_sellers as $prod)
      <?php
      if (!empty($prod->photo))
        $img_url = URL::asset('uploads/product_option/' . $prod->photo);
      else
        $img_url = URL::asset('images/no-pic.jpg');
      ?>
      <a href="{{ URL::route('product.product-detail', $prod->product_id) }}" class="item">
        <div class="product-img">
          <img src="{{ $img_url }}" />
        </div>

        <div class="product-info">
          <div class="product-name">{{ $prod['product_name_' . session()->get('locale')] }}</div>
          <div class="product-price">THB {{ number_format($prod->retail_price, 2) }}</div>
        </div>
      </a>
      @endforeach
    </div>
  </div>
</div>
