<?php
$config_part = config()->get('constants.part');
$arr_part = $config_part[$lang];
$size_ar = array('63','64','65','66','67','68','69','70','71','72','L','XL','2XL','3XL','4XL','5XL','6XL','7XL','8XL','9XL','10XL');
?>
@extends('frontend.layouts/main')
@section('more-stylesheet')
    <link rel="stylesheet" href="{{ URL::asset('css/front/product.css') }}">
    <link href="{{ URL::asset('tools/desoslide/css/jquery.desoslide.css')}}" rel="stylesheet">
    <link href="{{ URL::asset('tools/fancybox/jquery.fancybox.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
@endsection

@section('content')

    <div id="content">
        <div class="container">
            <h1 style="padding-left: 30px;">{{ $main_category_name }}</h1>
            <ol class="breadcrumb" style="margin-left: 17px;">
                <li><a href="{{ url()->to('/') }}" title="{{ trans('product.home') }}">{{ trans('product.home') }}</a></li>
                {!! $category_name_list !!}
            </ol>
            <hr style="max-width:95%">
            <div class="product-info">
                <div class="col-sm-12 col-md-7" id="pic-product">
                    <div class="row">
                        {{--ถ้ามี Option--}}
                        @if (count($gallery) > 0)
                            <div id="slideshow" class="col-md-10"></div>
                            <div id="slideshow_thumbs" class="col-md-2">
                                <ul class="list-inline">
                                    @foreach ($gallery as $key => $gel)
                                        <?php
                                        $dir = 'uploads/product/';
                                        $img_name = $gel->photo;
                                        $dir_img = $dir.'570/'.$img_name;
                                        $m_dir_img = $dir.'340/'.$img_name;
                                        $s_dir_img = $dir.'100/'.$img_name;

                                        $img_path = url()->asset($dir_img);
                                        $m_img_path = url()->asset($m_dir_img);
                                        $s_img_path = url()->asset($s_dir_img);

                                        if($img_name == '' || !file_exists($dir_img))
                                            $img_path = url()->asset('images/yakyim/570x640.jpg');

                                        if($img_name == '' || !file_exists($m_dir_img))
                                            $m_img_path = url()->asset('images/yakyim/340x384.jpg');

                                        if($img_name == '' || !file_exists($s_dir_img))
                                            $s_img_path = url()->asset('images/yakyim/100x113.jpg');
                                        ?>
                                        <li>
                                            <a href="{{ $img_path }}" data-desoslide-index="{{$key}}">
                                                <img src="{{ $s_img_path }}" class="img-responsive" alt="">
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="col-md-5" id="detail">
                    <?php
                    $retail_price = $product->retail_price;
                    $dollar_retail_price = $obj_dollar->cal_dollar($retail_price,$currency);

                    $price = $retail_price;
                    $dollar_price = $dollar_retail_price;

                    $special_price = $product->special_price;
                    $dollar_special_price = $obj_dollar->cal_dollar($special_price,$currency);

                    if($special_price > 0){
                        $price = $special_price;
                        $dollar_price = $dollar_special_price;
                    }


                    ?>
                    <p class="name">{{ $product['product_name_'.$lang] }}</p>
                    @if(count($product_option) > 0)
                        <p class="model">
                            @foreach($product_option as $key => $size)
                                <span class="hide" data-id="{{ $size->product_option_id }}">{{ 'Model : '.$size->model }}</span>
                            @endforeach
                        </p>
                    @endif

                    <p class="price">
                        @if($lang == 'th')
                            {{ '฿ '.number_format($price,2) }}
                            @if($special_price > 0)
                                <span class="retail_price">{{ '฿ '.number_format($retail_price,2) }}</span>
                            @endif
                        @else
                            {{ '$ '.number_format($dollar_price,2) }}
                            @if($special_price > 0)
                                <span class="retail_price">{{ '$ '.number_format($dollar_retail_price,2) }}</span>
                            @endif
                        @endif
                    </p>
                    @if($product->sum_qty == '0')
                        <p class="out-of-stock">{{ trans('product.sold-out') }}</p>
                    @endif
                    {{--ถ้ามี Option--}}
                    @if(count($product_option) > 0)
                        <nav class="text-center">
                            <ul class="pagination">
                                @foreach($size_ar as $value)

                                    @foreach($product_option as $key => $size)
                                        @if($value == $size['option_name_'.$lang])
                                            <li class="size @if($size->qty == 0) disabled @endif"><a data-id="{{ $size->product_option_id }}">{{ $size['option_name_'.$lang] }}</a></li>
                                        @endif
                                    @endforeach

                                @endforeach
                            </ul>
                        </nav>
                        <p><button class="btn btn-add-to-cart cart" >ADD TO BAG</button></p>
                    @endif

                    <p class="options"><button class="btn btn-add-to-wishlist wishlist" data-id="{{ $product->product_id }}"><span class="glyphicon glyphicon-heart" aria-hidden="true"></span> WISHLIST</button> <a href="@if($product->look_book != ''){{ $product->look_book }} @else # @endif" class="btn btn-default btn-lookbook" target="_blank">LOOKBOOKS</a></p>

                    <br>
                    <!-- Go to www.addthis.com/dashboard to customize your tools -->
                    <div class="addthis_inline_share_toolbox_v3yt text-center"></div>
                    <br>

                    <div class="alert text-center" style="display: none;"></div>
                </div>
            </div>

            <div class="content">
                @if($product->size_chart_id <> 0)
                @if(count($chart) > 0)
                    <?php
                    $i = 1;
                    foreach($chart as $item){
                        $part = $item->part;
                        $size_name = $item->size_name;
                        $value = $item->value;

                        $arr_chart[$part][$size_name] = $value;
                    }
                    ?>
                    <h4>{{ trans('product.size-chart') }}</h4>
                    <hr style="max-width:100%; margin-top: 4px;">
                    <div class="table-responsive">
                        <table class="table table-bordered" >
                            @foreach($arr_chart as $part => $arr_size)
                                @if($i==1)
                                    <tr>
                                        <th>ขนาด (นิ้ว)</th>
                                        @foreach($sizes as $size)
                                            <th class="text-center">{{ $size->size_name }}</th>
                                        @endforeach
                                    </tr>
                                @endif
                                <tr>
                                    <td class="col-md-2">{{ $arr_part[$part] }} </td>
                                    @foreach($size_ar as $value)
                                    @foreach($sizes as $size)
                                            @if($value == $size->size_name)
                                        <td class="text-center">
                                            {{ (array_key_exists($size->size_name, $arr_size))?$arr_size[$size->size_name]:'-' }}
                                        </td>
                                            @endif
                                    @endforeach
                                    @endforeach
                                </tr>
                                <?php $i++; ?>
                            @endforeach

                        </table>
                    </div>
                    <br>


                <h4>{{ trans('product.description') }}</h4>
                <hr style="max-width: 100%; margin-top: 4px;">
                <p>
                    {!! $product['detail_'.$lang] !!}
                </p>
                <br>
                    @endif
                @endif

                <h4>{{ trans('product.complete-your-outfit') }}</h4>
                <hr style="max-width: 100%; margin-top: 4px;">
                <div id="product-related" class="row">
                    @if(count($related_product) > 0)
                        @foreach($related_product as $item)
                            <?php
                            $product_id = $item->product_id;
                            $product_name = $item['product_name_'.$lang];
                            $url_name = str_slug($item['product_name_en']);
                            $url_to = url()->to('product/detail/'.$product_id.'/'.$url_name);
                            $retail_price = $item->retail_price;
                            $dollar_retail_price = $obj_dollar->cal_dollar($retail_price,$currency);

                            $price = $retail_price;
                            $dollar_price = $dollar_retail_price;

                            $special_price = $product->special_price;
                            $dollar_special_price = $obj_dollar->cal_dollar($special_price,$currency);

                            if($special_price > 0){
                                $price = $special_price;
                                $dollar_price = $dollar_special_price;
                            }

                            $dir = 'uploads/product/340/';
                            $img_name = $item->img_name;
                            $img_dir = $dir.$img_name;
                            $img_path = url()->asset($img_dir);
                            if($img_name == '' || !file_exists($img_dir)){
                                $img_path = url()->asset('images/yakyim/225x340.jpg');
                            }
                            ?>
                            <div class="col-xs-2 col-sm-3" >
                                <a href="{{ $url_to }}" class="product-box" title="{{ $product_name }}">
                                    @if($item->sum_qty == '0')
                                        <div class="out-of-stock">{{ trans('product.sold-out') }}</div>
                                    @endif
                                    <img src="{{ $img_path }}" class="img-responsive center img-product" alt="{{ $product_name }}">
                                </a>
                                <div class="box-caption text-center">
                                    <p class="title">{{ $product_name }}</p>
                                    <p class="price">
                                        @if($lang == 'th')
                                            {{ '฿ '.number_format($price,2) }}
                                            @if($special_price > 0)
                                                <span class="retail_price">{{ '฿ '.number_format($retail_price,2) }}</span>
                                            @endif
                                        @else
                                            {{ '$ '.number_format($dollar_price,2) }}
                                            @if($special_price > 0)
                                                <span class="retail_price">{{ '$ '.number_format($dollar_retail_price,2) }}</span>
                                            @endif
                                        @endif
                                    </p>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection

@section('more-script')
    <script src="{{ URL::asset('js/front/product-detail.js')}}"></script>
    <script src="{{ URL::asset('js/front/cart-add.js')}}"></script>

    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57fb10513368b28d"></script>

    <!-- Desoslide -->
    <script src="{{ URL::asset('tools/desoslide/js/jquery.desoslide.min.js')}}"></script>
    <script src="{{ URL::asset('tools/fancybox/jquery.fancybox.pack.js')}}"></script>

    <script>
        $(window).load(function() {
            $('#slideshow').desoSlide({
                thumbs: $('#slideshow_thumbs ul').find('li > a'),
                overlay: 'none',
                thumbEvent: 'click',
                effect: {
                    provider: 'animate',
                    name: 'fade',
                    interval: 300,
                },
                events: {

                }
            });
            $(document).ready(function() {
                $(".desoslide-wrapper").fancybox({
                    autoSize	: true,
                    fitToView	: true,
                    padding:0,
                    afterClose : function() {
                        $(".desoslide-wrapper").removeAttr("style");
                        return;
                    }
                });
            });
        });
    </script>

@endsection
