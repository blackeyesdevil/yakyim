@extends('frontend.layouts/main')

@section('title','Privacy Policy')

@section('more-stylesheet')
    <link rel="stylesheet" href="{{ URL::asset('css/front/condition.css') }}">
@endsection

@section('content')
    <div id="condition" class="container">
        <div id="breadcrumb">
            <a href="{{ URL::route('home') }}">{{ trans('global.home') }}</a>
            <span class="arrow">></span>

            <span class="active">{{ trans('global.delivery') }}</span>
        </div>

        <div id="heading" class="th">
            <div>Delivery</div>
            <div>การจัดส่งสินค้า</div>
        </div>

        <div id="content" class="th">
            <div class="text-content col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                <div class="table-heading"><img src="{{ URL::asset('images/icon-delivery-cost.png') }}" /> ค่าขนส่ง</div>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="text-center">เงื่อนไขการจัดส่ง</th>
                                <th class="text-center">ค่าขนส่ง</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="col-xs-6">ยอดสั่งซื้อตั้งแต่ 499 บาทขึ้นไป</td>
                                <td class="col-xs-6">ส่งฟรีทั่วประเทศ</td>
                            </tr>
                            <tr>
                                <td class="col-xs-6">ยอดสั่งซื้อที่ต่ำกว่า 499 บาท</td>
                                <td class="col-xs-6">ค่าจัดส่ง 50 บาท</td>
                            </tr>
                            <tr>
                                <td colspan="2">หมายเหตุ : สำหรับสินค้าบางรายการที่มีขนาดใหญ่ หรือน้ำหนักมาก อาจมีค่าจัดส่งเพิ่มเติมเป็นพิเศษตามที่ระบุเอาไว้ในรายละเอียดของสินค้านั้นๆ</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="table-heading"><img src="{{ URL::asset('images/icon-delivery-time.png') }}" /> วิธีการและระยะเวลาในการจัดส่ง</div>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th class="text-center">พื่นที่การจัดส่ง</th>
                            <th class="text-center">ระยะเวลาการจัดส่ง</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="col-xs-6">พื้นที่จัดส่ง กรุงเทพฯ, นนทบุรี, ปทุมธานี</td>
                            <td class="col-xs-6">
                                สินค้าจะถูกจัดส่งภายใน 1-2 วันทำการ หลังจากการชำระเงินแล้ว<br><br>
                                หมายเหตุ : กรณีทำรายการสั่งซื้อตั้งแต่ 3 โมงเย็นวันศุกร์ถึงวันอาทิตย์ สินค้าจะถูกจัดส่งภายในวันพุธ
                            </td>
                        </tr>
                        <tr>
                            <td class="col-xs-6">นอกพื้นที่การจัดส่งจังหวัดข้างต้น</td>
                            <td class="col-xs-6">สินค้าจะถูกจัดส่งภายใน 1-2 วันทำการ หลังจากการชำระเงินแล้ว</td>
                        </tr>
                        <tr>
                            <td colspan="2">หมายเหตุ : สินค้าบางรายการอาจใช้ระยะเวลาในการรอผลิตสินค้า และอาจส่งผลให้มีการจัดส่งเกินระยะเวลาที่กำหนดไว้</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <div class="table-responsive">
                  <table class="table">
                    <thead>
                      <tr>
                        <th class="padding-left">การตรวจสอบสถานะการจัดส่งสินค้า (Order Tracking)</th>
                      </tr>
                    </thead>

                    <tbody>
                      <tr>
                        <td>
                          ลูกค้าสามารถตรวจสอบสถานะการจัดส่งสินค้าของคุณได้ 3 ช่องทางดังนี้ <br>
                          1. คลิกที่ ข้อมูลการสั่งซื้อ/รายการสั่งซื้อของฉัน<br>
                          2. ติดต่อเจ้าหน้าที่ทาง Loft Online Call Center : 071-398-1870<br>
                          3. ส่งอีเมลมาที่ eshop@loftbangkok.com
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>

                <div class="table-responsive">
                  <table class="table">
                    <thead>
                      <tr>
                        <th class="padding-left">การยกเลิกคำสั่งซื้อ (Cancel Order)</th>
                      </tr>
                    </thead>

                    <tbody>
                      <tr>
                        <td>
                          ลูกค้าสามารถยกเลิกคำสั่งซื้อก่อนการชำระเงินได้ 2 ช่องทางดังนี้ <br>
                          1. ติดต่อเจ้าหน้าที่ทาง Loft Online Call Center : 071-398-1870<br>
                          2. ส่งอีเมลมาที่ eshop@loftbangkok.com
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
            </div>
        </div>


    </div>
@endsection
