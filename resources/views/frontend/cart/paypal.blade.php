@extends('frontend.layouts/main')

@section('title','Paypal')

@section('more-stylesheet')

@endsection

@section('content')
  <div id="content">
    <div class="container">
      <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
        <input type="hidden" name="cmd" value="_xclick">
        {{--<input type="hidden" name="business" value="charuwan@bangkoksolutions.com">--}}
        <input type="hidden" name="business" value="{{ env('PAYPAL_BUSINESS') }}">

        <input type="hidden" name="item_name" value="Shopping Online YakYim.">
        <input type="hidden" name="item_number" value="{{ $orders_no }}">
        <input type="hidden" name="amount" value="{{ $grand_total }}">
        <input type="hidden" name="no_shipping" value="1">
        <input type="hidden" name="currency_code" value="THB">

        <input type="hidden" name="return" value="{{ url()->to('paypal/status') }}">
        <input type="hidden" name="cancel_return" value="{{ url()->to('paypal/cancel?token='.$token) }}">
        <div style="display: none;">
          <input type="image" name="submit"
                 src="https://www.j2store.org/images/plugins_logo/paypal-express-checkout-logo.png" width="140"
                 alt="PayPal - The safer, easier way to pay online">
        </div>
      </form>
    </div>
  </div>
@endsection
@section('more-script')
  <script>
    $(function(){
      $('form [name=submit]').click();
    });
  </script>
@endsection
