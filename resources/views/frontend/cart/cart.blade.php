<?php
$total_price = 0;
?>
@extends('frontend.layouts/main')

@section('more-stylesheet')
<link rel="stylesheet" href="{{URL::asset('css/front/cart.css')}}">

@endsection

@section('content')

    <div id="content">

      <div class="container">
        <h1>{{ trans('product.shopping-cart') }}</h1>
        <hr style="max-width:100%">
        <div class="table-responsive">
          <table class="table">
            <thead>
              <tr>
                <th class="col-xs-1"></th>
                <th class="col-xs-3 text-left">{{ trans('product.product-name') }} </th>
                <th class="col-xs-2 text-right">{{ trans('product.price') }}</th>
                <th class="col-xs-1">{{ trans('product.quantity') }}</th>
                <th class="col-xs-2 text-right">{{ trans('product.sub-total') }}</th>
                <th class="col-xs-1 text-center">{{ trans('product.remove') }}</th>
              </tr>
            </thead>
            <tbody>
            @if(count($carts) > 0)
                @foreach($carts as $item)
                    <?php
                    $qty = $item->cart_qty;

                    $product_name = $item['product_name_'.$lang].' '.$item['option_name_'.$lang];
                    $product_name_en = $item['product_name_en'];

                    $retail_price = $item->retail_price;
                    $price = $retail_price;
                    $special_price = $item->special_price;

                    if($lang == 'th'){
                        if($special_price > 0)
                            $price = $special_price;
                    }else{
                        $retail_price = $obj_dollar->cal_dollar($retail_price,$currency);
                        $price = $retail_price;
                        $special_price = $obj_dollar->cal_dollar($special_price,$currency);

                        if($special_price > 0)
                            $price = $special_price;
                    }

                    $sub_price = $price * $qty;
                    $total_price += $sub_price;

                    $url = url()->to('product/detail/'.$item->product_id.'/'.str_slug($product_name_en,'-'));
                    $dir = 'uploads/product/100/';
                    $img_name = $item->img_name;
                    $img_path = url()->asset($dir.$img_name);
                    if($img_name == null || !file_exists($dir.$img_name)){
                        $img_path = url()->asset('images/yakyim/100x150.jpg');
                    }
                    ?>
                        <tr>
                            <td>
                                <a href="{{ $url }}" title="{{ $product_name }}">
                                    <img src="{{ $img_path }}" alt="" class="img-responsive center" width="100px"></a>
                            </td>
                            <td>
                                <a href="{{ $url }}" title="{{ $product_name }}">
                                    {{ $product_name }}<br>
                                    {{ 'Model '.$item->model }}
                                </a>
                                <div class="text-alert text-danger"></div>
                            </td>
                            <td class="eprice text-right">{{ trans('product.currency-sign') }} {{ number_format($price,2) }}</td>
                            <td class="text-center">
                                <input type="number" class="" name="qty" value="{{ $qty }}">
                                <input type="hidden" name="cart_id" value="{{ $item->cart_id }}">
                                <input type="hidden" name="old_qty" value="{{ $qty }}">
                            </td>
                            <td class="subtotal text-right">{{ trans('product.currency-sign') }} {{ number_format($sub_price,2) }}</td>
                            <td class="text-center">
                                <a href="#">
                                    <img src="{{ URL::asset('images/btn-remove.png') }}" class="remove" alt="">
                                </a>
                            </td>
                        </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="6" class="text-center">{{ trans('product.no-item-in-cart') }}</td>
                </tr>
            @endif
            </tbody>
              @if(count($carts) > 0)
                  <tfoot>
                    <tr>
                        <td colspan="3"></td>
                        <td class="text-right"><b>{{ trans('product.total-price') }}</b></td>
                        <td class="text-right totalprice" style="font-weight: bold">{{ trans('product.currency-sign') }} {{ number_format($total_price,2) }}</td>
                        <td></td>
                    </tr>
                  </tfoot>
              @endif
        </table>

        </div>
          <div class="row" id="last-section">
              <div class="col-xs-6">
                  <a href="{{ URL::to('/') }}"><button class="btn btn-default ctn">&laquo; {{ trans('product.continue-shopping') }}</button></a>
              </div>
              <div class="col-xs-6 text-right checkout">
                  <a href="{{ URL::to('checkout') }}" class="@if(count($carts) == 0) hide @endif"><button class="btn btn-default chk" style="background: black;color: white;">{{ trans('product.proceed-to-checkout') }} &raquo;</button></a>
              </div>
          </div>

      </div>


    </div>

@endsection

@section('more-script')
<script src="{{URL::asset('js/jquery.numble.min.js')}}"></script>
<script src="{{URL::asset('js/front/cart-updated.js')}}"></script>
@endsection
