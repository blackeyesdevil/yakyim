<?php
$total_price = 0;
$user_type = session()->get('s_user_type');
?>
@extends('frontend.layouts/main')
@section('more-stylesheet')
  <link rel="stylesheet" href="{{ URL::asset('css/front/checkout.css') }}">
@endsection
@section('content')
  <div id="checkout" class="container">
    <div id="content" class="row">
      <div class="col-sm-8 col-sm-offset-2">
        <h2>{{ trans('global.checkout') }}</h2>
        <form id="confirm-form" class="form-horizontal" action="{{ url()->to('confirm') }}" method="POST">
          <input type="hidden" name="_token" value="{{ csrf_token() }}" >
          <input type="hidden" name="cart" value="{{ count($carts) }}" >
          <!-- Select Shipping Address -->
          <div id="shipping-information" class="block active">
            <div class="header">
              <span class="step-no">1</span> <span>{{ trans('checkout.shipping_information') }}</span>
            </div>
            <div class="content">
              @if(count($shipping) > 0)
                <div class="choice active">
                  <label>
                    <input type="radio" name="shipping_type" value="old" checked> {{ trans('checkout.shipping_old') }}
                  </label>
                  <div class="sub-content row">
                    <label class="dropdown" id="old">
                      <select name="old_address" class="form-control">
                        @foreach($shipping as $item)
                          <option value="{{ $item->address_book_id }}">{{ $item->firstname.' '.$item->lastname.' - '.$item->address.' '.$item->District['district_name_'.$lang].' '.$item->Amphur['amphur_name_'.$lang].' '.$item->Province['province_name_'.$lang].' '.$item->zipcode }}</option>
                        @endforeach
                      </select>
                    </label>
                  </div>
                </div>
              @endif
              <div class="choice @if(count($shipping) <= 0) active @endif">
                <label>
                  <input type="radio" name="shipping_type" value="new" @if(count($shipping) <= 0) checked @endif > {{ trans('checkout.shipping_new') }}
                </label>
                <div class="sub-content row">
                  <div class="alert alert-danger text-center hide" role="alert">{{ trans('message.checkout-input-field') }}</div>
                  <?php $field = 'shipping_firstname'; ?>
                  <div class="form-group">
                    <label for="{{ $field }}" class="col-sm-3 control-label">{{ trans('checkout.' . $field) }}</label>
                    <div class="col-sm-9 col-md-5">
                      <input type="text" id="{{ $field }}" name="{{ $field }}" value="" class="form-control">
                    </div>
                  </div>
                  <?php $field = 'shipping_lastname'; ?>
                  <div class="form-group">
                    <label for="{{ $field }}" class="col-sm-3 control-label">{{ trans('checkout.' . $field) }}</label>
                    <div class="col-sm-9 col-md-5">
                      <input type="text" id="{{ $field }}" name="{{ $field }}" value="" class="form-control">
                    </div>
                  </div>
                  <?php $field = 'shipping_address'; ?>
                  <div class="form-group">
                    <label for="{{ $field }}" class="col-sm-3 control-label">{{ trans('checkout.' . $field) }}</label>
                    <div class="col-sm-9 col-md-5">
                      <textarea id="{{ $field }}" name="{{ $field }}" rows="5" class="form-control"></textarea>
                    </div>
                  </div>
                  <?php $field = 'shipping_province'; ?>
                  <div class="form-group">
                    <label for="{{ $field }}" class="col-sm-3 control-label">{{ trans('checkout.' . $field) }}</label>
                    <div class="col-sm-9 col-md-5">
                      <label class="dropdown">
                        <select id="{{ $field }}" name="{{ $field }}" class="filter-province form-control" data-section="shipping"></select>
                      </label>
                    </div>
                  </div>
                  <?php $field = 'shipping_amphur'; ?>
                  <div class="form-group">
                    <label for="{{ $field }}" class="col-sm-3 control-label">{{ trans('checkout.' . $field) }}</label>
                    <div class="col-sm-9 col-md-5">
                      <label class="dropdown">
                        <select id="{{ $field }}" name="{{ $field }}" class="filter-amphur form-control" data-section="shipping"></select>
                      </label>
                    </div>
                  </div>
                  <?php $field = 'shipping_district'; ?>
                  <div class="form-group">
                    <label for="{{ $field }}" class="col-sm-3 control-label">{{ trans('checkout.' . $field) }}</label>
                    <div class="col-sm-9 col-md-5">
                      <label class="dropdown">
                        <select id="{{ $field }}" name="{{ $field }}" class="filter-district form-control" data-section="shipping"></select>
                      </label>
                    </div>
                  </div>
                  <?php $field = 'shipping_zipcode'; ?>
                  <div class="form-group">
                    <label for="{{ $field }}" class="col-sm-3 control-label">{{ trans('checkout.' . $field) }}</label>
                    <div class="col-sm-9 col-md-5">
                      <input type="text" id="{{ $field }}" name="{{ $field }}" class="filter-zipcode form-control" data-section="shipping">
                    </div>
                  </div>
                  <?php $field = 'shipping_tel'; ?>
                  <div class="form-group">
                    <label for="{{ $field }}" class="col-sm-3 control-label">{{ trans('checkout.' . $field) }}</label>
                    <div class="col-sm-9 col-md-5">
                      <input type="text" id="{{ $field }}" name="{{ $field }}" value="{{ $user->mobile or ''}}" maxlength="10" size="10" class="form-control">
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group navigate">
                <button type="button" id="shipping-continue-btn" class="btn btn-default black continue-btn" data-current-step="shipping-information" data-next-step="billing-information">{{ trans('checkout.continue') }}</button>
              </div>
            </div>
          </div>

          <!-- Select Billing Address -->
          <div id="billing-information" class="block">
            <div class="header">
              <span class="step-no">2</span> <span>{{ trans('checkout.billing_information') }}</span>
            </div>
            <div class="content single-content">
              <div class="row">
                <div class="choice">
                  <label>
                    <input type="radio" name="billing_type" value="same" checked> {{ trans('checkout.billing_same') }}
                  </label>
                </div>
                <div class="choice">
                  <label>
                    <input type="radio" name="billing_type" value="old"> @if(empty($billing)) {{ trans('checkout.billing_new') }} @else {{ trans('checkout.billing_edit') }} @endif
                  </label>
                  <div class="sub-content">
                    <div class="alert alert-danger text-center hide" role="alert">{{ trans('message.checkout-input-field') }}</div>
                    <?php $field = 'billing_firstname'; ?>
                    <div class="form-group">
                      <label for="{{ $field }}" class="col-sm-3 control-label">{{ trans('checkout.' . $field) }}</label>
                      <div class="col-sm-9 col-md-5">
                        <input type="text" id="{{ $field }}" name="{{ $field }}" value="{{ $billing->firstname or '' }}" class="form-control">
                      </div>
                    </div>
                    <?php $field = 'billing_lastname'; ?>
                    <div class="form-group">
                      <label for="{{ $field }}" class="col-sm-3 control-label">{{ trans('checkout.' . $field) }}</label>
                      <div class="col-sm-9 col-md-5">
                        <input type="text" id="{{ $field }}" name="{{ $field }}" value="{{ $billing->lastname or '' }}" class="form-control">
                      </div>
                    </div>
                    <?php $field = 'billing_address'; ?>
                    <div class="form-group">
                      <label for="{{ $field }}" class="col-sm-3 control-label">{{ trans('checkout.' . $field) }}</label>
                      <div class="col-sm-9 col-md-5">
                        <textarea id="{{ $field }}" name="{{ $field }}" rows="5" class="form-control">{{ $billing->address or '' }}</textarea>
                      </div>
                    </div>
                    <?php $field = 'billing_province'; ?>
                    <div class="form-group">
                      <label for="{{ $field }}" class="col-sm-3 control-label">{{ trans('checkout.' . $field) }}</label>
                      <div class="col-sm-9 col-md-5">
                        <label class="dropdown">
                          <select id="{{ $field }}" name="{{ $field }}" class="filter-province form-control" data-section="billing"></select>
                        </label>
                      </div>
                    </div>
                    <?php $field = 'billing_amphur'; ?>
                    <div class="form-group">
                      <label for="{{ $field }}" class="col-sm-3 control-label">{{ trans('checkout.' . $field) }}</label>
                      <div class="col-sm-9 col-md-5">
                        <label class="dropdown">
                          <select id="{{ $field }}" name="{{ $field }}" class="filter-amphur form-control" data-section="billing"></select>
                        </label>
                      </div>
                    </div>
                    <?php $field = 'billing_district'; ?>
                    <div class="form-group">
                      <label for="{{ $field }}" class="col-sm-3 control-label">{{ trans('checkout.' . $field) }}</label>
                      <div class="col-sm-9 col-md-5">
                        <label class="dropdown">
                          <select id="{{ $field }}" name="{{ $field }}" class="filter-district form-control" data-section="billing"></select>
                        </label>
                      </div>
                    </div>
                    <?php $field = 'billing_zipcode'; ?>
                    <div class="form-group">
                      <label for="{{ $field }}" class="col-sm-3 control-label">{{ trans('checkout.' . $field) }}</label>
                      <div class="col-sm-9 col-md-5">
                        <input type="text" id="{{ $field }}" name="{{ $field }}" value="{{ $billing->zipcode or '' }}" class="filter-zipcode" data-section="billing" class="form-control">
                      </div>
                    </div>
                    <?php $field = 'billing_tel'; ?>
                    <div class="form-group">
                      <label for="{{ $field }}" class="col-sm-3 control-label">{{ trans('checkout.' . $field) }}</label>
                      <div class="col-sm-9 col-md-5">
                        <input type="text" id="{{ $field }}" name="{{ $field }}" value="{{ $billing->mobile or '' }}" maxlength="10" size="10" class="form-control">
                      </div>
                    </div>
                    <?php $field = 'billing_tax'; ?>
                    <div class="form-group">
                      <label for="{{ $field }}" class="col-sm-3 control-label">{{ trans('checkout.' . $field) }}</label>
                      <div class="col-sm-9 col-md-5">
                        <input type="text" id="{{ $field }}" name="{{ $field }}" value="{{ $billing->tax_id or '' }}" maxlength="13" size="13" class="form-control">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group navigate">
                <button type="button" class="btn btn-default prev-btn" data-current-step="billing-information" data-prev-step="shipping-information">{{ trans('checkout.go-back') }}</button>
                <button type="button" id="billing-continue-btn" class="btn btn-default black continue-btn" data-current-step="billing-information" data-next-step="payment-method">{{ trans('checkout.continue') }}</button>
              </div>
            </div>
          </div>

          <!-- Select Payment Method -->
          <div id="payment-method" class="block">
            <div class="header">
              <span class="step-no">3</span> <span>{{ trans('checkout.payment-method') }}</span>
            </div>
            <div class="content">
              <div id="shipping-list">
                <div class="choice">
                  <label><input type="radio" name="shipping_method" value="1" checked> {{ trans('checkout.shipping-1') }} (จัดส่ง EMS)<br>
                    <img class="icon" src="{{ URL::asset('images/thailandpost02.png') }}" alt="">
                  </label>
                </div>
                <div class="choice">
                  <label>
                    <input type="radio" name="shipping_method" value="2"> {{ trans('checkout.shipping-2') }} ({{ trans('checkout.shipping-express') }})
                    <span class="text-danger hide">{{ trans('message.checkout-select-lalamove') }} </span><br>
                    <img class="icon" src="{{ URL::asset('images/icon-lalamove.png') }}" alt="">

                  </label>
                </div>
              </div>
              <hr style="max-width: 100%">
              <div id="payment-list">
                <div class="col-md-12"><div class="alert alert-danger text-center hide" role="alert">{{ trans('message.checkout-select-payment') }}</div></div>

                <div class="choice">
                  <label>
                    <input type="radio" name="payment_type" value="1">
                    {{ trans('checkout.payment-1') }} <br>
                    <img class="icon" src="{{ URL::asset('images/kbank.png') }}" alt=""> KBANK 806-2-09769-9<br>
                    <img class="icon" src="{{ URL::asset('images/ktb.png') }}" alt=""> KTB 488-0-56792-2
                  </label>
                </div>
                <div class="choice">
                  <label>
                    <input type="radio" name="payment_type" value="2">
                    {{ trans('checkout.payment-2') }} <br>
                    <img class="icon" src="{{ URL::asset('images/paypal.png') }}" alt="">
                  </label>
                </div>
              </div>
              <div class="form-group navigate">
                <button type="button" class="btn btn-default prev-btn" data-current-step="payment-method" data-prev-step="billing-information">{{ trans('checkout.go-back') }}</button>
                <button id="payment-continue-btn" type="button" class="btn btn-default black continue-btn" data-current-step="payment-method" data-next-step="order-confirmation">{{ trans('checkout.continue') }}</button>
              </div>
            </div>
          </div>

        </form>
          <!-- Confirm Order -->
          <div id="order-confirmation" class="block">
            <div class="header">
              <span class="step-no">4</span> <span>{{ trans('checkout.order-confirmation') }}</span>
            </div>
            <div class="content single-content ">
              <div class="table-responsive">
                <table class="table-bordered">
                  <thead>
                    <tr>
                      <th width="5%" class="text-center">#</th>
                      <th width="55%" colspan="2">{{ trans('product.product-name') }}</th>
                      <th width="15%" class="text-right">{{ trans('product.price') }}</th>
                      <th width="10%" class="text-right">{{ trans('product.quantity') }}</th>
                      <th width="15%" class="text-right">{{ trans('product.sub-total') }}</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($carts as $key => $item)
                      <?php
                      $qty = $item->cart_qty;

                      $product_name = $item['product_name_'.$lang].' '.$item['option_name_'.$lang];
                      $product_name_en = $item['product_name_en'];

                      $retail_price = $item->retail_price;
                      $price = $retail_price;
                      $special_price = $item->special_price;

                      if($lang == 'th'){
                        if($special_price > 0)
                          $price = $special_price;
                      }else{
                        $retail_price = $obj_dollar->cal_dollar($retail_price,$currency);
                        $price = $retail_price;
                        $special_price = $obj_dollar->cal_dollar($special_price,$currency);

                        if($special_price > 0)
                          $price = $special_price;
                      }

                      $sub_price = $price * $qty;
                      $total_price += $sub_price;

                      $url = url()->to('product/detail/'.$item->product_id.'/'.str_slug($product_name_en,'-'));
                      $dir = 'uploads/product/100/';
                      $img_name = $item->img_name;
                      $img_path = url()->asset($dir.$img_name);
                      if($img_name == null || !file_exists($dir.$img_name)){
                        $img_path = url()->asset('images/yakyim/100x150.jpg');
                      }
                      ?>
                      <tr>
                        <td>{{ $key+1 }}</td>
                        <td><img src="{{ $img_path }}" /></td>
                        <td>{{ $product_name }}</td>
                        <td class="text-right">{{ trans('product.currency-sign') }} {{ number_format($price,2) }}</td>
                        <td class="text-right">{{ $qty }}</td>
                        <td class="text-right">{{ trans('product.currency-sign') }} {{ number_format($sub_price,2) }}</td>
                      </tr>
                    @endforeach
                    <?php
                    if($lang == 'en'){
                      $shipping_price = $obj_dollar->cal_dollar($shipping_price,$currency);
                    }
                    $grand_price = $shipping_price + $total_price;
                    ?>
                    <tr class="text-right" style="font-weight: bold">
                      <td colspan="5">{{ trans('checkout.total-price') }}</td>
                      <td>
                        <div class="total-price">{{ trans('product.currency-sign') }} <span id="total-price">{{ number_format($total_price,2) }}</span></div>
                      </td>
                    </tr>
                    <tr class="discount-price text-right text-success hide" style="font-weight: bold">
                      <td colspan="5">{{ trans('checkout.discount-price') }}</td>
                      <td>
                        - {{ trans('product.currency-sign') }} <span id="discount-price">0</span>
                      </td>
                    </tr>
                    <tr class="shipping-price text-right text-danger @if($shipping_price == 0) hide @endif" style="font-weight: bold">
                      <td colspan="5">{{ trans('checkout.shipping-price') }}</td>
                      <td>
                        + {{ trans('product.currency-sign') }} <span id="shipping-price">{{ number_format($shipping_price,2) }}</span>
                      </td>
                    </tr>
                    <tr class="point text-right text-success" style="font-weight: bold; display: none;">
                      <td colspan="5">{{ trans('checkout.use-point') }}</td>
                      <td>
                        - {{ trans('product.currency-sign') }} <span id="point"></span>
                      </td>
                    </tr>
                    <tr class="text-right" style="font-weight: bold">

                      <td colspan="5">{{ trans('checkout.grand-total') }}</td>
                      <td>
                        {{ trans('product.currency-sign') }} <span class="grand-total"> {{ number_format($grand_price,2) }}</span>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>

              <div class="discount-div">
                <strong>{{ trans('checkout.have-a-discount-code') }}</strong><br>
                {{--<div class="col-md-12">--}}
                  {{--ตรงกับวันเกิด และ ยังไม่ได้ใช้ส่วนลดวันเกิดในปีนั้นเลย--}}
                  @if(substr($user->birth_date,'5') == date('m-d') && $chk_used_code < 1)
                    <div class="radio birth_day hide">
                      <label><input type="radio" class="select-discount" name="select_discount" value="1"> {{ trans('checkout.discount-birthday') }}</label>
                    </div>
                    <span class="text-danger birth-error"></span>
                  @endif
                  <div class="radio">
                    <label><input type="radio" class="select-discount" name="select_discount" value="2"> {{ trans('checkout.discount-code') }}</label>
                  </div>
                  <div class="radio">
                    <label><input type="radio" class="select-discount" name="select_discount" value="3"> {{ trans('checkout.discount-user-no') }}</label>
                  </div>
                {{--</div>--}}


                {{--<div class="clearfix"></div>--}}
                <form id="discount-form" action="{{ URL::to('discount') }}" method="post" style="display: none">
                  <input type="text" id="discount_code" name="discount_code">
                  <button type="submit" name="submit" class="btn btn-default black">{{ trans('checkout.apply-code') }}</button>
                  <div class="error-msg text-danger"></div>
                </form>

                <div class="current-code">
                  {{ trans('checkout.current-code') }}: <span class="code text-success"></span> <a href="#" data-url="{{ URL::to('discount/remove-discount') }}" class="btn-danger"><i class="fi-x"></i> {{ trans('checkout.remove') }}</a>
                </div>
              </div>

              <?php
              $con_price = 1000;
              if($lang == 'en'){
                $con_price = $obj_dollar->cal_dollar($con_price,$currency);
              }
              ?>
              @if(!empty($user_point) && $user_point->total > 0 && $total_price >= $con_price)
                <div id="use-point">
                  <strong>{{ trans('checkout.user-point') }} {{ $user_point->total }} {{ trans('checkout.points') }}</strong><br>
                  <div class="checkbox">
                    <label><input type="checkbox" class="select-point" name="select_point" value="1"> {{ trans('checkout.use-point-text') }}</label>
                  </div>
                </div>
              @endif
              <div class="option form-group">
                <a href="#">
                  <button type="button" class="btn btn-default prev-btn" data-current-step="order-confirmation" data-prev-step="payment-method">{{ trans('checkout.go-back') }}</button>
                  @if(count($carts) > 0)
                  <button type="button" id="finish-checkout" data-url="{{ URL::to('confirm') }}" class="btn btn-default black">{{ trans('checkout.finish-checkout') }}</button>
                    @endif
                </a>
              </div>


            </div>
          </div>

      </div>
    </div>
  </div>
@endsection

@section('more-script')
  {{ Html::script('js/filter-address.js') }}
  <script type="text/javascript">
    $(document).ready(function(){
      filter_address('province','province',null,'{{ $billing->province_id or '' }}','billing');
      filter_address('province','amphur','{{ $billing->province_id or '' }}','{{ $billing->amphur_id or '' }}','billing');
      filter_address('amphur','district','{{ $billing->amphur_id or '' }}','{{ $billing->district_id or '' }}','billing');
      filter_address('district','zipcode','{{ $billing->district_id or '' }}','{{ $billing->zipcode or '' }}','billing');
      filter_address('province','province',null,'','shipping');
      filter_address('province','amphur','','','shipping');
      filter_address('amphur','district','','','shipping');
      filter_address('district','zipcode','','','shipping');
    });
  </script>
  <script>
    var user_type = '{{ $user_type }}';
  </script>
  <script src="{{ URL::asset('js/front/checkout.js') }}"></script>
@endsection
