@extends('frontend.layouts/main')
@section('title','Shopping Cart')
@section('more-stylesheet')
@endsection
@section('content')
    <form name="myform" id="myform" method="post" action="https://www.paysbuy.com/paynow.aspx">
    <input type="Hidden" Name="psb" value="psb"/>
    <input Type="Hidden" Name="biz" value="{{ $account }}"/>
    <input Type="Hidden" Name="inv" value="{{ $invoice }}"/>
    <input Type="Hidden" Name="itm" value="{{ $description }}"/>
    <input Type="Hidden" Name="amt" value="{{ $price }}"/>
    <input Type="Hidden" Name="postURL" value="{{ $postURL }}"/>
    {{--<input type="image" src="https://www.paysbuy.com/imgs/S_click2buy.gif" border="0" name="submit" alt="Make it easier,PaySbuy - it's fast,free and secure!"/>--}}
</form >
@endsection
@section('more-script')
    <script>
        $(document).ready(function(){
            $('#myform').submit();
        });
    </script>
@endsection
