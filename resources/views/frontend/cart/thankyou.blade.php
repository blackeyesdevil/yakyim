@extends('frontend.layouts/main')
@section('more-stylesheet')
  <link rel="stylesheet" href="{{URL::asset('css/front/profile.css')}}">
@endsection
@section('title','Thank You')
@section('content')
    <section id="content">
      <div class="container more-white">
        <div id="thankyou" class="col-sm-6 col-sm-offset-3 text-center">
          <h4 class="text-center">ขอบคุณที่อุดหนุนสินค้าของยักษ์ยิ้ม</h4>
          <h5 class="text-center" style="color:red;"><b>ถ้าหากไม่พบ Email ใน Inbox กรุณาตรวจสอบใน Junk Mail หรือ Spam</b></h5>
          {{--<img src="{{ url()->asset('images/icon-thankyou.png') }}" style="width: 100%; max-width: 200px;">--}}
          <div class="text">
            รหัสการสั่งซื้อของคุณคือ <strong>'{{ session()->get('s_order_no') }}'</strong>.<br>
            คุณจะได้รับอีเมลเกี่ยวกับรายละเอียดการสั่งซื้อนี้<br><br>

            <a href="{{ url()->to('/') }}">
              <button type="button" class="btn btn-default">กลับไปเลือกสินค้า</button>
            </a>
          </div>
        </div>
        </div>
    </section>
@endsection
@section('more-script')


@endsection
