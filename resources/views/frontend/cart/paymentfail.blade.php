@extends('frontend.layouts/main')
@section('more-stylesheet')
  <link rel="stylesheet" href="{{URL::asset('css/front/profile.css')}}">
@endsection
@section('title','Thank You')
@section('content')
    <section id="content">
      <div class="container more-white">
        <div id="thankyou" class="col-sm-6 col-sm-offset-3 text-center">
          <h1>การชำระเงินล้มเหลว</h1>
          <div class="text">
            รหัสการสั่งซื้อของคุณคือ <strong>'{{ session()->get('s_order_no') }}'</strong>.<br>
            การชำระเงินของคุณล้มเหลว<br><br>

            <a href="{{ url()->to('/') }}">
              <button type="button" class="btn btn-default">กลับไปหน้าหลัก</button>
            </a>
          </div>
        </div>

    </section>


@endsection
@section('more-script')


@endsection
