<?php
$lang = session()->get('locale');
?>
@extends('frontend.layouts/main')

@section('title','Promotion')

@section('more-stylesheet')
    <link rel="stylesheet" href="{{ URL::asset('css/front/promotion.css') }}">
@endsection

@section('content')
    <div id="promotion" class="container">
        <div id="breadcrumb">
            <a href="{{ URL::route('home') }}">{{ trans('global.home') }}</a>
            <span class="arrow">></span>
            <span>{{ trans('global.promotion') }}</span>
            <span class="arrow">></span>
            <span class="active">{{ $data['page_name_'.$lang] }}</span>
        </div>

        <div id="promotion-detail" class="{{ trans('global.th-font') }}">
            <div class="img">
                <img src="{{ URL::asset('uploads/'.$data->img_path) }}">
            </div>
            <div class="detail">
                {!! $data['description_'.$lang] !!}
            </div>
        </div>
    </div>
@endsection
@section('more-script')

@endsection