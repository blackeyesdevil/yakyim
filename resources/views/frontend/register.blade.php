@extends('frontend.layouts/main')

@section('title','Register')

@section('more-stylesheet')
{!! Html::style('css/front/register-login.css') !!}
@endsection

@section('content')
  <div id="content">
    <div class="container">
      <h1 class="text-center">{{ trans('register-login.create') }}</h1>
      <div class="col-md-6 col-md-offset-3">
        @if(session()->has('successMsg'))
          <div class="alert alert-success text-center">{!! session()->get('successMsg') !!}</div>
        @endif
        @if(session()->has('errorMsg'))
          <div class="alert alert-danger text-center">{!! session()->get('errorMsg') !!}</div>
        @endif
      </div>

      <div class="col-md-6 col-md-offset-3">
        <form method="POST" action="{{ url()->to('register/check') }}" id="form">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="row">
            <?php $field = 'firstname' ;?>
            <div class="form-group col-md-6">
              <label for="{{ $field }}">{{ trans('register-login.firstname') }} *</label>
              <input type="text" class="form-control" name="{{ $field }}" value="{{old('firstname')}}">
              @if(!empty($errors->first($field)))
                <div class="text-danger">{{ $errors->first($field) }}</div>
              @endif
            </div>
            <?php $field = 'lastname' ;?>
            <div class="form-group col-md-6">
              <label for="{{ $field }}">{{ trans('register-login.lastname') }} *</label>
              <input type="text" class="form-control" name="{{ $field }}" value="{{ old('lastname') }}">
              @if(!empty($errors->first($field)))
                <div class="text-danger">{{ $errors->first($field) }}</div>
              @endif
            </div>
          </div>
          <div class="row">
            <?php $field = 'email' ;?>
            <div class="form-group col-md-6">
              <label for="{{ $field }}">{{ trans('register-login.email') }} *</label>
              <input type="text" class="form-control" name="{{ $field }}" value="{{ old('email') }}">
              @if(!empty($errors->first($field)))
                <div class="text-danger">{{ $errors->first($field) }}</div>
              @endif
            </div>
            <?php $field = 'password' ;?>
            <div class="form-group col-md-6">
              <label for="{{ $field }}">{{ trans('register-login.password') }} *</label>
              <input type="password" class="form-control" name="{{ $field }}" placeholder="{{ trans('register-login.password_min') }}"  aria-describedby="helpBlock">
              @if(!empty($errors->first($field)))
                <div class="text-danger">{{ $errors->first($field) }}</div>
              @endif
            </div>
          </div>
          <div class="form-horizontal" style="margin-top: 14px;">
            <?php $field = 'mobile' ;?>
            <div class="form-group">
              <label for="{{ $field }}" class="col-sm-4 control-label" style="text-align:left;">{{ trans('register-login.mobile') }} *</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" name="{{ $field }}" maxlength="10" value="{{ old('mobile') }}">
                @if(!empty($errors->first($field)))
                  <div class="text-danger">{{ $errors->first($field) }}</div>
                @endif
              </div>
            </div>
          </div>
          <div class="form-horizontal" style="margin-top: 14px;">
            <?php $field = 'birth_date' ;?>
            <div class="form-group">
              <label for="{{ $field }}" class="col-sm-4 control-label" style="text-align:left;">{{ trans('register-login.birth_date') }} *</label>
              <div class="col-sm-8">
                <input type="date" class="form-control" name="{{ $field }}" value="{{ old('birth_date')}}">
                @if(!empty($errors->first($field)))
                  <div class="text-danger">{{ $errors->first($field) }}</div>
                @endif
              </div>
            </div>
          </div>
          <div class="checkbox" style="margin-top: 40px;">
            <?php $field = 'receive_newsletter' ;?>
            <label>
              <input type="checkbox" name="{{ $field }}" value="1" checked> {{ trans('register-login.subscribe') }}
            </label>
          </div>
          <div class="checkbox">
            <?php $field = 'agree' ;?>
            <label>
              <input type="checkbox" name="{{ $field }}" value="1"> {{ trans('register-login.condition') }}  @if(session()->get('locale') == 'th')<a href="http://www.yakyim.com/how-to-register">เงื่อนไขของทางร้าน</a> @else <a href="http://www.yakyim.com/how-to-register">Terms & Conditions</a>  @endif
            </label>
            @if(!empty($errors->first($field)))
              <div class="text-danger">{{ $errors->first($field) }}</div>
            @endif
          </div>
          <div class="text-center footer">
            <button class="btn btn-default btn-lookbook">{{ trans('register-login.btn_create') }}</button>
          </div>
        </form>
      </div>
    </div>


  </div>
@endsection
@section('more-script')
@endsection
