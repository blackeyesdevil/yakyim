@extends('frontend.layouts/main')

@section('title','Old Member Update')

@section('more-stylesheet')
  <link rel="stylesheet" href="{{ URL::asset('css/front/home.css') }}">
@endsection

@section('content')
  <div id="home-member">
    <div class="container">
      <div class="content col-xs-12">
        <div class="headline">Old Member Update</div>
        <div class="form ">
          <form action="{{ URL::to('old-member/update') }}" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            @if(session()->has('successMsg'))
              <div class="row">
                <div class="col-sm-5">
                  <div class="alert alert-success text-center">{!! session()->get('successMsg') !!}</div>
                </div>
              </div>
            @endif
            @if(session()->has('errorMsg'))
              <div class="row">
                <div class="col-sm-5">
                  <div class="alert alert-danger text-center">{!! session()->get('errorMsg') !!}</div>
                </div>
              </div>
            @endif
            <div class="row">
              <div class="col-sm-5">
                <input type="text" name="firstname_th" value="{{ $user->firstname_th }}" placeholder="Firstname" class="input input-block" />
                @if($errors->first('firstname_th'))
                  <div class="text-danger">{{ $errors->first('firstname_th') }}</div>
                @endif
              </div>
            </div>
            <div class="row">
              <div class="col-sm-5">
                <input type="text" name="lastname_th" value="{{ $user->lastname_th }}" placeholder="Lastname" class="input input-block" />
                @if($errors->first('lastname_th'))
                  <div class="text-danger">{{ $errors->first('lastname_th') }}</div>
                @endif
              </div>
            </div>
            <div class="row">
              <div class="col-sm-5">
                <input type="text" name="id_card" value="{{ $user->id_card }}" placeholder="Id card or Passport Number" class="input input-block" />
                @if($errors->first('id_card'))
                  <div class="text-danger">{{ $errors->first('id_card') }}</div>
                @endif
              </div>
            </div>
            <div class="row">
              <div class="col-sm-5">
                <input type="text" name="mobile" value="{{ $user->mobile }}" placeholder="Mobile" class="input input-block" />
                @if($errors->first('mobile'))
                  <div class="text-danger">{{ $errors->first('mobile') }}</div>
                @endif
              </div>
            </div>
            <div class="row">
              <div class="col-sm-5">
                <input type="text" name="email" value="{{ $user->email }}" placeholder="Email Address" class="input input-block" />
                @if($errors->first('email'))
                  <div class="text-danger">{{ $errors->first('email') }}</div>
                @endif
              </div>
            </div>
            <div class="row">
              <div class="col-sm-5">
                <input type="password" name="password" placeholder="Password" class="input input-block" />
                @if($errors->first('password'))
                  <div class="text-danger">{{ $errors->first('password') }}</div>
                @endif
              </div>
            </div>
            <div class="row">
              <div class="col-sm-5">
                <input type="submit" name="submit" value="Update" class="btn btn-block" />
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>



@endsection
