@extends('frontend.layouts/main')

@section('title','Register')

@section('more-stylesheet')
{!! Html::style('css/front/register-login.css') !!}
@endsection

@section('content')
  <div id="content">
    <div class="container">
      <h1 class="text-center">{{ trans('register-login.login') }}</h1>
      <div class="col-md-6 col-md-offset-3">
        @if(session()->has('successMsg'))
          <div class="alert alert-success text-center">{!! session()->get('successMsg') !!}</div>
        @endif
        @if(session()->has('errorMsg'))
          <div class="alert alert-danger text-center">
            {!!  session()->get('errorMsg') !!}
            @if(session()->get('encrypted'))
              <a href="{{ url()->to('resend-email/'.session()->get('encrypted')) }}">{{ trans('message.resend-email') }}</a>
            @endif
          </div>
        @endif
      </div>
      <div class="col-md-6 col-md-offset-3">
        <form method="POST" action="{{ url()->to('login/check') }}" id="form">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="row">
            <?php $field = 'email' ;?>
            <div class="form-group col-md-6">
              <label for="{{ $field }}">{{ trans('register-login.email') }} *</label>
              <input type="text" class="form-control" name="{{ $field }}">
              @if(!empty($errors->first($field)))
                <div class="text-danger">{{ $errors->first($field) }}</div>
              @endif
            </div>
            <?php $field = 'password' ;?>
            <div class="form-group col-md-6">
              <label for="{{ $field }}">{{ trans('register-login.password') }} *</label>
              <input type="password" class="form-control" name="{{ $field }}" aria-describedby="helpBlock">
              @if(!empty($errors->first($field)))
                <div class="text-danger">{{ $errors->first($field) }}</div>
              @endif
            </div>
            <div class="form-group col-md-6">
              <a href="{{ URL::to('register') }}">{{ trans('register-login.create') }}</a>
            </div>
            <div class="form-group col-md-6">
              <a href="{{ URL::to('forget-password') }}">{{ trans('register-login.forget') }}</a>
            </div>
          </div>
          <div class="text-center footer">
            <button class="btn btn-default btn-lookbook">{{ trans('register-login.btn_signin') }}</button>
          </div>
        </form>
      </div>
    </div>


  </div>
@endsection
@section('more-script')
@endsection
