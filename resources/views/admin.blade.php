<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    <meta charset="UTF-8">
    <title>Dashboard</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('assets/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('assets/global/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <!-- <link href="{{ URL::asset('assets/global/plugins/uniform/css/uniform.default.css') }}" rel="stylesheet" type="text/css"> -->
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/global/plugins/select2/select2.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }} "/>

    <!-- END PAGE LEVEL SCRIPTS -->

    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="{{ URL::asset('assets/global/plugins/jquery-minicolors/jquery.minicolors.css')}}" rel="stylesheet" type="text/css"/>
    <!-- END PAGE LEVEL STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <!-- <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/global/plugins/clockface/css/clockface.css')}}"/> -->
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}"/>
    <!-- <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/global/plugins/bootstrap-colorpicker/css/colorpicker.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css')}}"/> -->
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')}}"/>
    <!-- END PAGE LEVEL STYLES -->
    <!-- BEGIN THEME STYLES -->
    <link href="{{ URL::asset('assets/global/css/components-rounded.css') }}" id="style_components" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('assets/global/css/plugins.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('assets/admin/layout3/css/layout.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('assets/admin/layout3/css/themes/default.css') }}" rel="stylesheet" type="text/css" id="style_color">
    <link href="{{ URL::asset('assets/admin/layout3/css/custom.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('assets/global/css/mainStyle.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('css/change-status.css') }}" rel="stylesheet" type="text/css">
    <!-- END THEME STYLES -->


    <link rel="stylesheet" type="text/css" media="screen" href="{{URL::asset('js/jquery-ui/jquery-ui.css')}}">
    <link rel="stylesheet" type="text/css" media="screen" href="{{URL::asset('js/jquery-ui/jquery-ui.theme.css')}}">
    <link rel="stylesheet" type="text/css" media="screen" href="{{URL::asset('tools/elfinder/css/elfinder.min.css')}}">

    @yield('css')

    <script src="{{ URL::asset('assets/global/plugins/jquery.min.js') }}" type="text/javascript"></script>

    <link rel="stylesheet" type="text/css" media="screen" href="{{URL::asset('css/momy-elfinder.css')}}">
    <link rel="shortcut icon" href="{{ URL::asset('favicon.ico') }}"/>

</head>
<body class="page-header-menu-fixed">

<!-- BEGIN HEADER -->
@include('backend.include.header')
        <!-- END HEADER -->

<!-- BEGIN CONTENT -->
@yield('content')
        <!-- BEGIN CONTENT -->

<!-- BEGIN FOOTER -->
@include('backend.include.footer')
        <!-- END FOOTER -->


<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="{{ URL::asset('assets/global/plugins/respond.min.js') }}"></script>
<script src="{{ URL::asset('assets/global/plugins/excanvas.min.js') }}"></script>
<![endif]-->

{{--17-11-58--}}
<script src="{{ URL::asset('assets/admin/scripts/page.js') }}"></script>
<script src="{{ URL::asset('js/jquery-ui/jquery-ui.min.js') }}"></script>




{{--<script src="{{ URL::asset('assets/global/plugins/jquery.min.js') }}" type="text/javascript"></script>--}}
<script src="{{ URL::asset('assets/global/plugins/jquery-migrate.min.js') }}" type="text/javascript"></script>

<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="{{ URL::asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/global/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/global/plugins/jquery.blockui.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/global/plugins/jquery.cokie.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/global/plugins/uniform/jquery.uniform.min.js') }}" type="text/javascript"></script>

<!-- <script src="{{URL::asset('assets/global/plugins/jquery-nestable/jquery.nestable.js')}}"></script> -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{ URL::asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/global/plugins/select2/select2.min.js') }}"></script>
<script src="{{ URL::asset('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ URL::asset('assets/global/plugins/ckeditor/ckeditor.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<!-- END PAGE LEVEL PLUGINS -->


{{--<script src="{{ URL::asset('assets/admin/pages/scripts/components-form-tools.js') }}"></script>--}}
<script src="{{ URL::asset('assets/global/scripts/mainScript.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/global/scripts/postcode.js') }}" type="text/javascript"></script>
<!-- END CORE PLUGINS -->


<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{ URL::asset('assets/global/plugins/bootstrap-selectsplitter/bootstrap-selectsplitter.min.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/global/plugins/jquery-minicolors/jquery.minicolors.min.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/global/plugins/autosize/autosize.min.js')}}" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{ URL::asset('assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}"></script>
<!-- <script type="text/javascript" src="{{ URL::asset('assets/global/plugins/clockface/js/clockface.js') }}"></script> -->
<!-- <script type="text/javascript" src="{{ URL::asset('assets/global/plugins/bootstrap-daterangepicker/moment.min.js') }}"></script> -->
<!-- <script type="text/javascript" src="{{ URL::asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script> -->
<!-- <script type="text/javascript" src="{{ URL::asset('assets/global/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js') }}"></script> -->
<script type="text/javascript" src="{{ URL::asset('assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- END CORE PLUGINS -->
<script src="{{ URL::asset('assets/global/scripts/metronic.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/admin/layout3/scripts/layout.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/admin/layout3/scripts/demo.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/admin/pages/scripts/components-pickers.js') }}"></script>
<script src="{{ URL::asset('assets/admin/pages/scripts/components-form-tools2.js') }}"></script>


{{--elfinder--}}
<script type="text/javascript" src="{{ URL::asset('tools/elfinder/js/elfinder.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('tools/elfinder/js/i18n/elfinder.ru.js') }}"></script>
{{--elfinder--}}
@yield('js')

<script>
    jQuery(document).ready(function() {
        Metronic.init(); // init metronic core components
        Layout.init(); // init current layout
        Demo.init(); // init demo features
        FormValidation.init();
        ComponentsPickers.init();
//            ComponentsDropdowns.init();
        ComponentsFormTools2.init();

    });
</script>
<!-- END JAVASCRIPTS -->

</body>
</html>
