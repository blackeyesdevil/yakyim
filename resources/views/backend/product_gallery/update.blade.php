<?php
use App\Http\Controllers\Backend\ProductGalleryController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new ProductGalleryController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}
$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);
?>



@extends('admin')
@section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1><a href="{{URL::to('_admin/product')}}">Product</a> / <a href="{{ URL::to('_admin/product_gallery?product_id='.$product_id) }}">Product Gallery</a> / {{ $txt_manage.' '.$titlePage }}</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD -->

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body form">
                                <form action="{{URL::to($url_to)}}" enctype="multipart/form-data" method="POST" id="form" class="form-horizontal">
                                    <input name="_method" type="hidden" value="{{$method}}">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">




                                    <div class="form-body">
                                        @if ($txt_manage == 'Update')
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Current Photo</label>
                                            <div class="col-md-4">
                                                <img src="{{ URL::asset('uploads/product/100/' . $photo) }}">
                                            </div>
                                        </div>
                                        @endif

                                        {{--<div class="form-group">--}}
                                            {{--<label class="control-label col-md-3">Photo</label>--}}
                                            {{--<div class="col-md-4">--}}
                                                {{--<input type='file' name="photo[]" id="photo" class="form-control" value="" multiple>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}

                                            {{--@if(!empty($gallery))--}}

                                                {{--<div class="form-group">--}}

                                                    {{--@foreach($gallery as $value)--}}
                                                        {{--<div style="width:250px;float:left;height: 250px;border-color: #333333;border:dashed 1px;margin:0 16px 5px;" id="head_photo_{{ $value->product_gallery_id }}">--}}
                                                            {{--<div style="height: 250px;">--}}
                                                                {{--@if($value->photo != '')--}}
                                                                    {{--<img src="{{ URL::asset('uploads/product_option/250/'.$value->photo) }}" id="img_photo_{{ $value->product_gallery_id }}">--}}
                                                                {{--@endif--}}
                                                            {{--</div>--}}

                                                            {{--<div style="position: relative;left:85px;bottom: 40px;">--}}
                                                                {{--<span class="label label-danger" onclick="remove_img({{ $value->product_gallery_id }})" style="cursor: pointer;"><i class="glyphicon glyphicon-minus"></i> Remove</span>--}}

                                                            {{--</div>--}}


                                                        {{--</div>--}}

                                                        {{--<input type="hidden" name="photo[{{ $value->product_gallery_id }}]" id="photo_{{ $value->product_gallery_id }}" value="{{ $value->photo }}">--}}
                                                    {{--@endforeach--}}

                                                {{--</div>--}}

                                            {{--@endif--}}

                                            {{--<div class="form-group">--}}
                                                {{--<label class="col-md-3 control-label"> </label>--}}
                                                {{--<div class="col-md-4">--}}
                                                    {{--<span style="color:#F00">* หมายเหตุ</span><br>--}}
                                                    {{--<span style="">--}}
                                                        {{--- ไฟล์ต้องเป็นมีขนาดกว้าง 660x880 pixel. <br>--}}
                                                        {{--- นามสกุลของรูปภาพต้อง .png, .jpg เท่านั้น<br>--}}
                                                    {{--</span>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}

                                        <div class="form-group">
                                            <label class="control-label col-md-3">New Photo</label>
                                            <div class="col-md-4">
                                                <input type="file" name="photo" class="form-control" value="{{ $photo }}">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label"> </label>
                                            <div class="col-md-4">
                                                <span style="color:#F00">* หมายเหตุ</span><br>
                                                                    <span style="">
                                                                    - ไฟล์ต้องเป็นมีขนาดกว้าง 660x880 pixel. <br>
                                                                    - นามสกุลของรูปภาพต้อง .png, .jpg เท่านั้น<br>
                                                                    </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <input type="hidden" name="product_id" value="{{Input::get('product_id')}}">
                                                <input type="hidden" name="strParam" value="{{$strParam}}">
                                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                                <button type="reset" class="btn default">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
    <script src="{{URL::asset('js/elfinder-upload.js')}}"></script>


@endsection
@section('js')

    <script>

        function remove_img(id) {
            var r = confirm('Are you sure you want to delete');
            if(r == true){
                $("#img_photo_"+id).removeAttr('src');
                $("#photo_"+id).val('');
                $("#head_photo_"+id).hide();
            }else{
                return false;
            }
        }

    </script>
    <script src="{{URL::asset('assets/admin/scripts/product_option.js')}}"></script>
@endsection
