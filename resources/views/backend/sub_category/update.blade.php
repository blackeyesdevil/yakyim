<?php
use App\Http\Controllers\Backend\SubCategoryController;
use App\Library\MainFunction;
use App\Library\CategoryFunction;
use Illuminate\Support\Facades\Input;

$objCon = new SubCategoryController();
$mainFn = new MainFunction();
$CategoryFn = new CategoryFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}
$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);
?>



@extends('admin')
@section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1><a href="{{URL::to('_admin/sub_category')}}">Sub Category</a> / {{ $txt_manage.' '.$titlePage }}</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD -->

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body form">
                                <form action="{{URL::to($url_to)}}" enctype="multipart/form-data" method="POST" id="form-category" class="form-horizontal">
                                    <input name="_method" type="hidden" value="{{$method}}">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">



                                    <div class="form-body">

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Category Name</label>
                                            <div class="col-md-4">
                                                <input type='text' name="category_name_th" id="category_name_th" class="form-control" value="{{ $category_name_th }}" >
                                            </div>
                                            <div class="col-md-1">
                                                ( TH )
                                            </div>
                                        </div>

                                        @if (!empty($errors->first('category_name_th')))
                                        <div class="form-group">
                                            <label class="control-label col-md-3"></label>
                                            <div class="col-md-4" style="color:#F00">
                                                {{ $errors->first('category_name_th') }}
                                            </div>
                                        </div>
                                        @endif

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Category Name</label>
                                            <div class="col-md-4">
                                                <input type='text' name="category_name_en" id="category_name_en" class="form-control" value="{{ $category_name_en }}" >
                                            </div>
                                            <div class="col-md-1">
                                                ( EN )
                                            </div>
                                        </div>

                                        @if (!empty($errors->first('category_name_en')))
                                        <div class="form-group">
                                            <label class="control-label col-md-3"></label>
                                            <div class="col-md-4" style="color:#F00">
                                                {{ $errors->first('category_name_en') }}
                                            </div>
                                        </div>
                                        @endif

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Parent Category</label>
                                            <div class="col-md-4">
                                                <select name="parent_category_id" class="form-control select2-container form-control select2me" >
                                                    @foreach($categories as $category)
                                                        <option value="{{ $category->category_id }}" @if($category_id == $category->category_id) selected @endif >{{ $category->category_name_th }}</option>
                                                        <?php
                                                            if($method=='POST'){
                                                                $cat_id = "";
                                                            }else{
                                                                $cat_id = $id;
                                                            }
                                                        ?>
                                                        <!-- CategoryFn->search_select_category($cat_id, $category_id, '')  -->
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <input type="hidden" name="strParam" value="{{$strParam}}">
                                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                                <button type="reset" class="btn default">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
    <script src="{{URL::asset('js/elfinder-upload.js')}}"></script>


@endsection
@section('js')
    <script src="{{URL::asset('assets/admin/scripts/category.js')}}"></script>
@endsection
