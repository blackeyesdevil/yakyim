<?php
use App\Http\Controllers\Backend\OrdersController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new OrdersController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}
$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);
?>
@extends('admin')

@section('css')
  <style>
    .headline{ font-size: 20px; padding-bottom: 10px; margin-bottom: 10px; border-bottom: 1px solid #DDD; }
    .headline i{ font-size: 20px !important; margin-right: 10px; }

    .complete{ background-color: #E0EDD3 !important; }
    .incomplete{ background-color: #F5CBCB !important; }
  </style>
@endsection

@section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1><a href="{{URL::to('_admin/orders')}}">Order</a>{{ ' - Update Stock' }}</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD -->

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div id="delivery-control" class="portlet light">
                    <div class="portlet-body form">
                        <div class="headline"><i class="fa fa-barcode"></i> Delivery Control</div>

                        <form action="{{ URL::to($path.'/'.$orders_id.'/updateStock?1'.$strParam) }}" method="post" class="form-horizontal">
                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Barcode</label>
                                    <div class="col-md-3">
                                        <input type="text" name="barcode" class="form-control">
                                    </div>
                                    <div class="col-md-3">
                                        <button type="submit" class="btn blue">Scan</button>
                                    </div>
                                </div>

                                <?php $error_msg = Session::get('error_msg'); ?>
                                @if (!empty($error_msg))
                                <div class="form-group font-red" style="margin-bottom: 0;">
                                    <label class="control-label col-md-2"></label>
                                    <div class="col-md-4">
                                        {{ $error_msg }}
                                    </div>
                                </div>
                                @endif
                            </div>
                        </form>
                    </div>
                </div>

                <div id="product-list" class="portlet light">
                    <div class="portlet-body">
                        <div class="headline">Product List</div>

                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th class="text-center font-blue">#</th>
                                    <th class="text-center font-blue">Model</th>
                                    <th class="text-center font-blue">Name [TH]</th>
                                    <th class="text-center font-blue">Name [EN]</th>
                                    <th class="text-center font-blue">Quantity</th>
                                    <th class="col-md-2 text-center font-blue">Scanned</th>
                                    <th class="col-md-1 text-center font-blue">Status</th>
                                </tr>
                                </thead>
                                <tbody>


                                <?php $id=''; ?>
                                @if(count($products) > 0)
                                    @foreach($products as $key => $field)
                                    <?php $isCompleted = ($field->qty == $field->scanned); ?>
                                        <tr class="{{ ($isCompleted)?'complete':'incomplete' }}">
                                            <td class="text-center">{{ $key+1 }}</td>

                                            <td class="text-center">{{ $field->model }}</td>
                                            <td class="text-center">{{ $field->product_name_th }}</td>
                                            <td class="text-center">{{ $field->product_name_en }}</td>
                                            <td class="text-center">{{ $field->qty }}</td>
                                            <td class="text-center">{{ $field->scanned }}</td>
                                            <td class="text-center">{{ ($isCompleted)?'Complete':'Incomplete' }}</td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr><td colspan='7' class='text-center'>No Result.</td></tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
    <script src="{{URL::asset('js/elfinder-upload.js')}}"></script>

@endsection
