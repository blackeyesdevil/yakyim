<?php
use App\Http\Controllers\Backend\UserController;
use App\Library\MainFunction;
$mainFn = new MainFunction(); // New Object Main Function

$filename  ="excel_report.xls";
header("Content-Type: application/vnd.ms-excel; charset=utf-8");
header("Content-Disposition: attachment; filename=report_product.xls");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private",false);
$total = '';
$no = '';
?>
<html >
<head>
    <meta charset="UTF-8">
</head>


<body>
<TABLE BORDER="1" width="100%">
    <?php $sumSale=0; $sumCost=0; $sumDis=0; $sumPro=0; ?>
    @foreach($data as $value)
        <?php
             $data_cost = DB::table('stock_log')->select('cost')
                     ->where('product_option_id', $value->product_option_id)->orderBy('stock_log_id','asc')->where('qty','<','0')->whereNull('deleted_at');
            $Count_cost = $data_cost->count();
            $data_cost = $data_cost->first();

            $sumSale += $value->qty * $value->unit_price ;
                if($Count_cost > 0){
                    $sumCost +=  $value->qty * $data_cost->cost;
//                    echo $value->product_option_id." : ".$data_cost->cost."<br>";
                }
            $sumDis += $value->discount_price ;
            $sumPro = $sumSale - $sumCost - $sumDis;


        ?>
    @endforeach
    <tr>
        <th align="right">ยอดขาย</th>
        <th align="left">{{ number_format($sumSale) }}</th>
    </tr>
    <tr>
        <th align="right">ต้นทุนขาย</th>
        <th align="left">{{ number_format($sumCost) }}</th>
    </tr>
    <tr>
        <th align="right">ส่วนลด</th>
        <th align="left">{{ number_format($sumDis) }}</th>
    </tr>
    <tr>
        <th align="right">กำไร</th>
        <th align="left">{{ number_format($sumPro) }}</th>
    </tr>
    <tr>
        <th><div align="left">รหัสสินค้า</div></th>
        <th><div align="left">ชื่อ</div></th>
        <th><div align="left">จำนวนที่ขาย</div></th>
        <th><div align="left">ราคาขาย</div></th>
        <th><div align="left">ยอดขาย</div></th>
        <th><div align="left">ส่วนลด</div></th>
    </tr>
    @if($countData > 0)
        @foreach($data as $field)
            @if($field->status_id >= 3 && $field->status_id <= 7 )
            <tr>
                <td>{{$field->model}}</td>
                <td>{{$field->product_name_th}}</td>
                <td>{{ number_format($field->qty) }}</td>
                <td>{{ number_format($field->unit_price) }}</td>
                <td>{{ number_format($field->qty*$field->unit_price) }}</td>
                <td>{{ number_format($field->discount_price) }}</td>
            </tr>
            @endif
        @endforeach
    @else
        <tr><td colspan='6' class='text-center'>No Result.</td></tr>
    @endif
</TABLE>
</body>
</html>
