<?php
use App\Http\Controllers\Backend\OrdersController;
use App\Library\MainFunction;

$objCon = new OrdersController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;
$path = $objCon->path;

$fieldList = $objCon->fieldList;
?>
<script type="text/javascript">
     window.print();
</script>
<style>
    body {
        height: 842px;
        width: 990px;
        margin-left: auto;
        margin-right: auto;

    }
    .label{
        width: 450px;
        height: 150px;
        padding: 10px;
        margin: 4px;
        font-size:18px;
        float: left;

        overflow: hidden;

        outline: 1px dotted;
    }
    .page-break  {
        clear: left;
        display:block;
        page-break-after:always;
    }

    .text-left{
        text-align: left;
    }
    .text-right{
        text-align: right;
    }
</style>


@foreach($data as $value)
<div class="label">
    <strong>ที่อยู่สำหรับจัดส่งสินค้า</strong><br><br>
    {!! $value->shipping_address !!}
</div>
{{--<div class="label">--}}
    {{--<strong>ที่อยู่สำหรับใบกำกับภาษี</strong>--}}
    {{--{!! $value->billing_address !!}--}}
{{--</div>--}}
@endforeach

<div class="page-break"></div>

