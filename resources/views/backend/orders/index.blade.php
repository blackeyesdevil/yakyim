<?php
use App\Http\Controllers\Backend\OrdersController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new OrdersController(); // New Object Controller
$mainFn = new MainFunction(); // New Object Main Function

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;
$path = $objCon->path;
$tb_name = $objCon->tbName;

$paysbuy = Config()->get('main.paysbuy_method');
$color_status = Config()->get('constants.color_status');

$status = App\Model\Status::whereNull('deleted_at')->get();

$orderBy = Input::get('orderBy');
$sortBy = Input::get('sortBy');
$search = Input::get('search');

$a_otherParam = Input::except(['orderBy','sortBy']);
$strParam = $mainFn->parameter($a_otherParam);

?>

@extends('admin')
@section('content')
    <div class="page-container">
        <div class="page-head">
            <div class="container">
                <div class="page-title">
                    <h1>{{ $titlePage }}</h1>
                </div>
            </div>
        </div>

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN SAMPLE TABLE PORTLET-->

                        <div class="portlet light">

                            <form action{{URL::to($path)}} class="form-horizontal" method="GET" >
                                <div class="form-group">
                                    <label class="control-label col-md-1">Search</label>
                                    <div class="col-md-3">
                                        <input class="form-control" type="text" name="search" value="{{ $search }}" >
                                        <span class="help-block">Search by Firstname , Lastname , Orders No.</span>
                                    </div>

                                    <div class="col-md-3">
                                        <select name="status" class="form-control select2-container form-control select2me">
                                            <option  value="">Status..</option>
                                            @foreach($status as $value)
                                                    <option value="{{ $value->status_id }}" @if($value->status_id==input::get('status')) selected @endif>{{ $value->status_name_th }}</option>
                                                @endforeach
                                        </select>
                                        <span class="help-block">Search by Status Order</span>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <select name="payment_by" class="form-control select2-container form-control select2me" >
                                                <option  value="">Payment..</option>
                                                <option value="0">รอการอนุมัติจาก Admin</option>
                                                <option value="1">โอนเงิน</option>
                                                <option value="2">Paypal</option>
                                                <!-- <option value="Cash on delivery" >Cash on delivery</option>
                                                <option value="ATM"  >ATM</option>
                                                <option value="Internet Banking" >Internet Banking</option>
                                                <option value="Counter Service" >Counter Service</option> -->
                                            </select>
                                            <span class="help-block">Search by Payment by</span>
                                        </div>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-1"></label>
                                    <div class="col-md-4">
                                        <div class="input-group input-large date-picker input-daterange" data-date="<?php echo date("Y-m-d");?>" data-date-format="yyyy-mm-dd">
                                            <input type="text" class="form-control" name="from_date" value="">
												<span class="input-group-addon">
												to </span>
                                            <input type="text" class="form-control" name="to_date" value="">
                                        </div>
                                        <span class="help-block">
											Select date range </span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-offset-1 col-md-3 ">
                                        <button class="btn blue btn-sm" type="submit" ><i class="fa fa-search"></i> Search</button>
                                    </div>
                                </div>
                            </form>
                            <hr>

                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-database font-green-sharp"></i>
                                    <span class="caption-subject font-green-sharp bold">Found {{ $countData }} Record(s).</span>
                                </div>

                                <div class="text-right">
                                    <div><a href="{{URL::to('_admin/orders?&export=true&type=1'.$strParam)}}"><button class="btn blue btn-sm" type="submit" ><i class="fa fa-file"></i> Excel Total Price</button></a> <a href="{{URL::to('_admin/orders?&export=true&type=2'.$strParam)}}"><button class="btn blue btn-sm" type="submit" ><i class="fa fa-file"></i> Excel Promotion Code</button></a> <a href="{{URL::to('_admin/orders?&export=true&type=5'.$strParam)}}"><button class="btn blue btn-sm" type="submit" ><i class="fa fa-file"></i> Excel Product</button></a> <a href="{{URL::to('_admin/orders?&export=true&type=3'.$strParam)}}"><button class="btn green btn-sm" type="submit" ><i class="fa fa-file"></i> Print Address</button></a></div>
                                </div>
                            </div>
                            <div class="portlet-body">
                                {!! Form::open(array('url' => '_admin/status-change' , 'method' => 'POST' , 'class' => 'form-horizontal')) !!}
                                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                <div class="form-group">
                                    <label class="control-label col-md-2">เปลี่ยนสถานะ</label>
                                    <div class="col-md-3">
                                        <select name="status_s" class="form-control select2-container form-control select2me" data-placeholder="Select...">
                                            <option value="">Status</option>
                                            @foreach($status as $value)
                                                @if( $value->status_id > 2  && $value->status_id < 8)
                                                <option value="{{ $value->status_id }}" @if($value->status_id==input::get('status')) selected @endif>{{ $value->status_name_th }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>


                                    <div class="col-md-3">
                                        <button class="btn blue btn-sm" type="submit" >OK</button>
                                    </div>

                                    <div class="text-right">
                                        <div>* <span style="font-weight:bold;color:red;">รอการชำระเงิน</span> = ยืนยันการชำระเงินแล้ว</div>
                                    </div>
                                </div>


                                <div class="form-group">
                                        <?php $error_msg = Session::get('error_msg'); ?>
                                        @if (!empty($error_msg))
                                            <div class="form-group font-red" style="margin-bottom: 0;">
                                                <label class="control-label col-md-2"></label>
                                                <div class="col-md-4">
                                                    {{ $error_msg }}
                                                </div>
                                            </div>
                                        @endif
                                </div>


                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>

                                            <th class="col-md-1 text-center"><input type="checkbox" name="selectedAll" id="selectedAll"></th>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('ID',$pkField,$orderBy,$sortBy,$strParam) !!} </th>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('Orders No.','orders_no',$orderBy,$sortBy,$strParam) !!} </th>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('Orders date','orders_date',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-2 text-center">{!! $mainFn->sorting('Fullname','firstname',$orderBy,$sortBy,$strParam) !!}</th>
                                            {{--<th class="col-md-2 text-center font-blue">Fullname</th>--}}

                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('Payment By','payment_by',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('Shipping By','status_id',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('Total price','total_price',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-1">{!! $mainFn->sorting('Status','status_id',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th></th>
                                            <th class="col-md-2 text-center">Tracking No.</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @if($countData > 0)
                                            @foreach($data as $field)
                                                <tr>

                                                    <td class="span1"><div class="text-center">

                                                            @if($field->status_id != 7 and $field->status_id != 6 )
                                                            <input type="checkbox" name="order_chk[]" id="order_chk[]" class="checkbox1" value="{{ $field->$pkField }}"></div>
                                                        @endif
                                                    </td>
                                                    <td class="text-center">{{ $field->$pkField }}</td>
                                                    <td class="text-center">{{ $field->orders_no }}</td>
                                                    {!! Form::close() !!}
                                                    <td>{{ $mainFn->format_date_en($field->orders_date,4) }}</td>
                                                    <td>
                                                        {{ $field->firstname }} {{ $field->lastname }}
                                                    </td>
                                                    <td>                                                        
                                                        @if($field->payment_method_id == 0)
                                                          รอการอนุมัติจาก Admin
                                                        @elseif($field->payment_method_id == 1)
                                                          โอนเงิน
                                                        @elseif($field->payment_method_id == 2)
                                                          Paypal
                                                        @endif
                                                    </td>
                                                    <td class="text-center">
                                                        @if ($field->shipping_method_id == 2)
                                                            Lalamove
                                                            @if($field->status_id == 1 || $field->status_id == 2)
                                                            <br>
                                                            <a href="{{ URL::to($path . '/' . $field->$pkField . '/payment?' . $strParam) }}" class="btn btn-xs btn-circle green">
                                                                แก้ไขวิธีชำระเงิน
                                                            </a>
                                                            @endif
                                                        @else
                                                            ไปรษณีย์
                                                        @endif
                                                    </td>
                                                    <td class="text-right">{{ number_format($field->total_price + $field->shipping_price - $field->discount_price - $field->use_point,2)}}</td>
                                                    @if($field->status_id == 0)
                                                        <td><?php echo" ";?></td>
                                                    @else
                                                        <td>
                                                            <?php
                                                            $data2 = DB::table('payment_inform')
//                                                                    ->leftJoin('payment_inform','orders.orders_id','=','payment_inform.orders_id')
                                                                    ->where('orders_id',$field->$pkField);
                                                            $count2 = $data2->count();
                                                            ?>

                                                                @if($count2 > 0)

                                                                    @if($field->status_id==6)
                                                                        <span style="font-weight:bold;color:{{ $color_status[$field->status_id] }}"> {{ $field->Status->status_name_th }} </span>
                                                                    @elseif($field->status_id==2)
                                                                        <span style="font-weight:bold;color:red;">{{ $field->Status->status_name_th }}</span>
                                                                        @else
                                                                        <span style="color:{{ $color_status[$field->status_id] }}">{{ $field->Status->status_name_th }}</span>
                                                                    @endif
                                                                @else
                                                                    <span style="color:{{ $color_status[$field->status_id] }}">{{ $field->Status->status_name_th }}</span>
                                                                @endif
                                                        </td>
                                                    @endif
                                                    <td class="text-center">

                                                            <a href="{{URL::to('_admin/orders_detail/create?'.$pkField.'='.$field->$pkField)}}" class="btn btn-xs btn-circle blue" target="_blank"><i class="fa fa-search"></i> Detail</a>


                                                        <a href="{{ URL::to($path.'/'.$field->$pkField.'/stock?'.$strParam) }}" class="btn btn-xs btn-circle yellow"><i class="fa fa-barcode"></i> Stock</a>

                                                        
                                                        <a href="{{URL::to('_admin/orders?&export=true&type=4&'.$pkField.'='.$field->$pkField)}}" class="btn btn-xs btn-circle green " target="_blank"><i class="fa fa-file"></i> Print Address</a>
                                                       
                                                    </td>
                                                    <td>
                                                        @if($field->status_id==5 or $field->status_id==6)
                                                            {!! Form::open(array('url' => '_admin/update-tracking' , 'method' => 'POST' , 'class' => 'form-horizontal')) !!}

                                                            <div class="input-group">
                                                                <input type="text" name="tracking_no" class="form-control" value="{{ $field->tracking_no }}">
                                                                <input type="hidden" name="orders_id" class="form-control" value="{{ $field->$pkField }}">
                                                                <div class="input-group-btn">
                                                                    <button class="btn blue btn-sm" type="submit">OK</button>
                                                                </div>
                                                            </div>

                                                            {!! Form::close() !!}
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr><td colspan='12' class='text-center'>No Result.</td></tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            {!! $data->appends(Input::except('page'))->render() !!}
                        </div>
                    </div>
                    <!-- END SAMPLE TABLE PORTLET-->
                </div>
            </div>
            <!-- END PAGE CONTENT INNER -->
        </div>
    </div>
    <!-- END PAGE CONTENT -->
    </div>

    <script>
        $(document).ready(function() {
            $('#selectedAll').on('click',function(){
                if($(this).is(':checked')) {

                    $('.checkbox1').attr('checked', 'checked');
                    $('.checkbox1').parent().addClass('checked');
                }else{
                    $('.checkbox1').removeAttr('checked');
                    $('.checkbox1').parent().removeClass('checked');
                }
            });

        });

    </script>

@endsection
