<?php
$mainFn = new MainFunction(); // New Object Main Function
$filename  ="excel_report.xls";
header("Content-Type: application/vnd.ms-excel; charset=utf-8");
header("Content-Disposition: attachment; filename=report_promotion_code.xls");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private",false);

?>
<html >
<head>
    <meta charset="UTF-8">
</head>


<body>
<TABLE BORDER="1" width="100%">
    @if(!empty($from_date) || !empty($to_date))
        <tr>
            <td colspan="7">วันที่ {{ $mainFn->format_date_th($from_date,2) }} ถึง {{ $mainFn->format_date_th($to_date,2) }}</td>
        </tr>
    @endif
    <tr>
        <th><div align="left">ID</div></th>
        <th><div align="left">Order No.</div></th>
        <th><div align="left">Product Name</div></th>
        <th><div align="left">User</div></th>
        <th><div align="center">Model</div></th>
        <th><div align="center">Size</div></th>
        <th><div align="center">Price (บาท)</div></th>
        <th><div align="center">Status</div></th>
        <th><div align="center">Price Dollar</div></th>
        <th><div align="center">Cost</div></th>
        <th><div align="center">Qty</div></th>
        <th><div align="center">Total (บาท)</div></th>

    </tr>

    <?php

    $or = 0;
    $total = 0;
        $total_price = 0;
        $before_vat = 0;
    $db = DB::table('orders')
            ->leftjoin('orders_detail','orders_detail.orders_id','=','orders.orders_id')
            ->leftjoin('user','user.user_id','=','orders.user_id')
            ->select('orders.orders_no','orders.use_point','orders_detail.qty','orders_detail.product_option_id','orders_detail.product_id','orders.orders_id','orders_detail.product_name_th','orders_detail.unit_price','orders_detail.product_name_en','orders.discount_price','orders_detail.dollar_unit_price','orders_detail.created_at','user.firstname','user.lastname','orders_detail.flag')
            ->whereNull('orders.deleted_at')->whereNull('orders_detail.deleted_at')->whereIn('orders.status_id',[3,4,6,11])
//            ->where('orders.orders_id','22')
//                    ->take('9')
            ->get();
    $total = 0;

    ?>



    @if($countData > 0)
        @foreach($db as $field)
            @if($field->orders_id != $or)
                <?php $or = $field->orders_id;?>

                <tr class="text-center">
                    <td>{{ $field->orders_id }}</td>
                    <td>{{ $field->orders_no }}</td>
                    <td class="text-left">{!! Html::decode($field->product_name_th) !!} ({!! Html::decode($field->product_name_en) !!})</td>
                    <td>{{ $field->firstname }} {{ $field->lastname }}</td>
                    <td colspan="8"></td>
                </tr>
            @endif

            <tr>
                <?php

                    if(!empty($field->product_id)){
                        $sku_q = DB::table('product_option')->select('option_name_th','option_name_en','model')->where('product_id',$field->product_id)->first();
                    }
                ?>
                <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    @if(!empty($sku_q))
                <td><div align="center">{{ $sku_q->model }}</div></td>

                <!-- <td>{!! Html::decode($sku_q->option_name_th) !!} ({!! Html::decode($sku_q->option_name_en) !!})</td> -->
                <td>{!! Html::decode($sku_q->option_name_th) !!}</td>
@endif
                <td><div align="center">{{ number_format($field->unit_price,2) }}</div></td>
                    <td>
                        @if($field->flag==1)
                            <div align="center">Special price</div>
                        @endif
                    </td>
                    <td><div align="center">{{ $field->dollar_unit_price }}</div></td>
                    <?php

                    if(!empty($field->product_option_id)){
                            $product_stock = DB::table('product_stock')->select('cost')->where('product_option_id',$field->product_option_id);
                            $count = $product_stock->count();
                            $product_stock = $product_stock->first();
                    }
                            ?>
                    <td><div align="center">
                            @if($count > 0)
                            {{ $product_stock->cost }}
                                @endif

                        </div></td>
                <td><div align="center">{{ $field->qty }}</div></td>
                <?php

                $total_price = $field->qty*$field->unit_price;
                $total += $total_price;

                $before_vat = $total/1.07;

                $vat = $total-$before_vat;
                ?>

                <td><div align="right">{{ number_format($total_price,2) }}</div></td>
            </tr>



        @endforeach
        <tr>
            <td colspan="10">
                <div align="right">
                    <strong>มูลค่าสินค้ารวมภาษีมูลค่าเพิ่ม / Sub - Total amount</strong> <br>
                    <strong>ส่วนลด / Discount</strong> <br>
                    <strong>ส่วนลดจาก Point</strong> <br>
                    <strong>มูลค่ารวมของสินค้า(ก่อนภาษีมูลค่าเพิ่ม) / Total amount(Before VAT)</strong> <br>
                    <strong>ภาษีมูลค่าเพิ่ม 7% / 7% VAT</strong> <br>
                </div>

            </td>

            <td>
                <div align="right">
                    {{ number_format($total,2) }}<br>
                    {{ number_format($field->discount_price,2) }}<br>
                    {{ number_format($field->use_point,2) }}<br>
                    {{ number_format($before_vat,2) }}<br>
                    {{ number_format($vat,2)}}
                </div>
            </td>
        </tr>

        <tr>

            <td colspan="10">
                <div align="right">
                    <strong>สินค้ารวมภาษี/Total</strong> <br>
                </div>
            </td>

            <td>
                <div align="right">
                    {{ number_format($total-$field->discount_price-$field->use_point,2) }}
                </div>
            </td>


        </tr>
        <tr>
            <td colspan="10">
                <div align="right">
                    <strong>Date/Time</strong> <br>
                </div>
            </td>

            <td>
                <div align="right">
                    {{ $field->created_at }}
                </div>
            </td>
            </tr>
           @else
        <tr><td colspan='10'><div align="center">No Result.</div></td></tr>
    @endif
</TABLE>
</body>
</html>
