<?php
$mainFn = new MainFunction(); // New Object Main Function
$filename  ="excel_report.xls";
header("Content-Type: application/vnd.ms-excel; charset=utf-8");
header("Content-Disposition: attachment; filename=report_promotion_code.xls");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private",false);
?>
<html >
<head>
    <meta charset="UTF-8">
</head>


<body>
<TABLE BORDER="1" width="100%">
    @if(!empty($from_date) || !empty($to_date))
        <tr>
            <td colspan="7">วันที่ {{ $mainFn->format_date_th($from_date,2) }} ถึง {{ $mainFn->format_date_th($to_date,2) }}</td>
        </tr>
    @endif
    <tr>
        <th><div align="left">ID</div></th>
        <th><div align="left">Product Name</div></th>
        <th><div align="center">SKU</div></th>
        <th><div align="center">Price (บาท)</div></th>
        <th><div align="center">Status</div></th>
        <th><div align="center">Price Dollar</div></th>
        <th><div align="center">Qty</div></th>
        <th><div align="center">Code</div></th>
        <th><div align="center">Date/Time</div></th>
    </tr>
    <?php
    $or = 0;
    $db = DB::table('orders')
            ->leftjoin('orders_detail','orders_detail.orders_id','=','orders.orders_id')
            ->leftjoin('product','product.product_id','=','orders_detail.product_id')
            ->leftjoin('log_code_used','orders.orders_id','=','log_code_used.orders_id')
            ->leftjoin('discount_code','log_code_used.discount_code_id','=','discount_code.discount_code_id')
            ->select('orders_detail.qty','product.product_id','orders.orders_id','orders_detail.product_name_th','orders_detail.unit_price','product.product_name_th','product.product_name_en','discount_code.code_name_th','discount_code.code_name_en','orders_detail.dollar_unit_price','orders_detail.created_at','orders_detail.flag')
            ->whereNull('orders.deleted_at')->whereNull('orders_detail.deleted_at')->whereIn('orders.status_id',['11'])
//            ->where('orders.orders_id','2')
            ->get();
    $total = 0;
    ?>

    @if($countData > 0)
        @foreach($db as $field)

            @if($field->orders_id != $or)
                <?php $or = $field->orders_id;?>

                <tr class="text-center">
                    <td>{{ $field->orders_id }}</td>
                    <td class="text-left">{!! Html::decode($field->product_name_th) !!} ({!! Html::decode($field->product_name_en) !!})</td>
                    <td colspan="4"></td>
                </tr>
            @endif
            <tr>
                <?php
                $sku_q = DB::table('product_option')->select('option_name_th','option_name_en','sku')->where('product_id',$field->product_id)->first();
                ?>
                <td></td>
                <td>{!! Html::decode($sku_q->option_name_th) !!} ({!! Html::decode($sku_q->option_name_en) !!})</td>
                <td><div align="center">{{ $sku_q->sku }}</div></td>
                <td><div align="center">{{ number_format($field->unit_price) }}</div></td>
                    <td>
                        @if($field->flag==1)
                            <div align="center">Special price</div>
                        @endif
                    </td>
                    <td><div align="center">{{ $field->dollar_unit_price }}</div></td>
                <td><div align="center">{{ $field->qty }}</div></td>
                <td><div align="center">{!! Html::decode($field->code_name_th) !!} @if(!empty($field->code_name_en))({!! Html::decode($field->code_name_en) !!})@endif</div></td>
                <td><div align="center">{{$field->created_at}}</div></td>
            </tr>
        @endforeach
    @else
        <tr><td colspan='6' class='text-center'>No Result.</td></tr>
    @endif
</TABLE>
</body>
</html>