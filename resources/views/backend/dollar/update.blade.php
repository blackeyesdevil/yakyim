<?php
use App\Http\Controllers\Backend\DollarController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new DollarController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}
$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);


?>

@extends('admin')
@section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>{{ $txt_manage.' '.$titlePage }}</h1><br>
                    <h1>Last Update. {{ $mainFn->format_date_th($data->updated_at,4) }}</h1><br>
                        <a href="{{URL::to('_admin/dollar?&export=true&type=2')}}"><button class="btn blue btn-sm" type="submit" ><i class="fa fa-file"></i> Excel Log</button></a>
                </div>

                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD -->

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body form">
                                <form action="{{URL::to($url_to)}}" enctype="multipart/form-data" method="POST" id="form-banner_promotion" class="form-horizontal">
                                    <input name="_method" type="hidden" value="{{$method}}">

                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                    

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">USD</label>
                                            <div class="col-md-4">
                                                <label class="control-label">1</label>
                                                
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">THB</label>
                                            <div class="col-md-4">
                                                <input type="number" name="dollar"  class="form-control" value="{{ $dollar }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <input type="hidden" name="strParam" value="{{$strParam}}">
                                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                                <button type="reset" class="btn default">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>

@endsection
@section('js')

@endsection
