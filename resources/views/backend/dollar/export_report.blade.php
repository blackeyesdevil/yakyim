<?php
$mainFn = new MainFunction(); // New Object Main Function
$filename  ="excel_report_setcurrency.xls";
header("Content-Type: application/vnd.ms-excel; charset=utf-8");
header("Content-Disposition: attachment; filename=report_setcurrency.xls");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private",false);
?>
<html >
<head>
    <meta charset="UTF-8">
</head>


<body>
<TABLE BORDER="1" width="100%">
    <tr>
        <th><div align="center">ID</div></th>
        <th><div align="center">Dollar</div></th>
        <th><div align="left">Date</div></th>
    </tr>
    <?php
        $db = DB::table('dollar')
            ->leftjoin('dollar_log','dollar_log.dollar_id','=','dollar.dollar_id')
            ->get();
    ?>

        @foreach($db as $field)
            <tr>
                <td><div align="center">{{ $field->dollar_log_id }}</div></td>
                <td><div align="center">{{ $field->dollar }}</div></td>
                <td><div>{{ $mainFn->format_date_th($field->created_at,4) }}</div></td>
            </tr>
        @endforeach
</TABLE>
</body>
</html>