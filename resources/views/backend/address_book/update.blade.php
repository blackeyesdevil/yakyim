<?php
use App\Http\Controllers\Backend\AddressBookController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;
use App\Model\District;
use App\Model\Province;
use App\Model\Amphur;

$objCon = new AddressBookController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;
$tb_name = $objCon->tbName;

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}
$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);
?>



@extends('admin')
@section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>{{ $txt_manage.' '.$titlePage }}</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD -->

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body form">
                                <form action="{{URL::to($url_to)}}" enctype="multipart/form-data" method="POST" id="form-user" class="form-horizontal">
                                    <input name="_method" type="hidden" value="{{$method}}">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                                    <div class="form-body">

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Default</label>
                                            <div class="col-md-4">
                                                <input type='checkbox' name="set_default" id="set_default" class="form-control" value="1" @if($set_default==1) checked @endif>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Billing Shipping</label>
                                            <div class="col-md-4">
                                                <select name="billing_shipping" class="form-control select2-container form-control select2me">
                                                    <option value="1" @if($billing_shipping == 1) selected @endif >Billing</option>
                                                    <option value="2" @if($billing_shipping == 2) selected @endif >Shipping</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Firstname</label>
                                            <div class="col-md-4">
                                                <input type='text' name="firstname" id="firstname" class="form-control" value="{{ $firstname }}">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Lastname</label>
                                            <div class="col-md-4">
                                                <input type='text' name="lastname" id="lastname" class="form-control" value="{{ $lastname }}">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Address</label>
                                            <div class="col-md-4">
                                                <input type='text' name="address" id="address" class="form-control" value="{{ $address }}">
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="control-label col-md-3">Tax ID</label>
                                            <div class="col-md-4">
                                                <input type='text' name="tax_id" id="tax_id" class="form-control" value="{{ $tax_id }}">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Mobile</label>
                                            <div class="col-md-4">
                                                <input type='text' name="mobile" id="mobile" class="form-control" value="{{ $mobile }}">
                                            </div>
                                        </div>

                                        <?php
                                        $amph = Amphur::where('amphur_id',$amphur_id)->get();
                                        $dist = District::where('district_id',$district_id)->get();
                                        ?>

                                        <div class="form-group">
                                            <label class="control-label col-md-3"></label>
                                            <div class="col-md-4">

                                                Province :
                                                <select name="province_id" class="select-province form-control select2-container form-control select2me filter-province" id="province_id" data-section="address">
                                                    <option selected="selected">--Select Province--</option>
                                                    @foreach($province as $provinces)
                                                        <option value="{{ $provinces->province_id }}"  @if($province_id == $provinces->province_id) selected @endif >{{ $provinces->province_name_th }}</option>
                                                    @endforeach

                                                </select><br/><br/>

                                                Amphur :
                                                <select name="amphur_id" class="select-amphur form-control select2-container form-control select2me filter-amphur" data-section="address" id="amphur_id">
                                                    @if($amphur_id != "0")
                                                        @foreach($amph as $a)
                                                            <option value="{{$a->amphur_id}}" @if($amphur_id==$a->amphur_id) selected @endif >{{$a->amphur_name_th}}</option>
                                                        @endforeach
                                                    @else
                                                        <option selected="selected" >--Select Amphur--</option>
                                                    @endif
                                                </select><br/><br/>

                                                District :
                                                <select name="district_id" class="select-district form-control select2-container form-control select2me filter-district" data-section="address" id="district_id">
                                                    @if($district_id != "0")
                                                        @foreach($dist as $d)
                                                            <option value="{{$d->district_id}}" @if($district_id==$d->district_id) selected @endif >{{$d->district_name_th}}</option>
                                                        @endforeach
                                                    @else
                                                        <option selected="selected" >--Select District--</option>
                                                    @endif
                                                </select><br/><br/>
                                                Zip : <input type='text' name="zipcode" id="zipcode" class="form-control filter-zipcode" data-section="address" value="{{ $zipcode }}">
                                            </div>
                                        </div>




                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <input type="hidden" name="address_book_id" value="{{Input::get('address_book_id')}}">
                                                <input type="hidden" name="strParam" value="{{$strParam}}">
                                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                                <button type="reset" class="btn default">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                        </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>

@endsection
@section('js')
    <script src="{{URL::asset('assets/admin/scripts/users.js')}}"></script>

    {{ Html::script('js/front/filter-address.js') }}
    <script>
        $(document).ready(function() {
            filter_address('province','province',null,'{{ $data->province_id or '' }}','address');
            filter_address('province','amphur','{{ $data->province_id or '' }}','{{ $data->amphur_id or '' }}','address');
            filter_address('amphur','district','{{ $data->amphur_id or '' }}','{{ $data->district_id or '' }}','address');
            filter_address('district','zipcode','{{ $data->district_id or '' }}','{{ $data->zipcode or '' }}','address');
        });
    </script>

@endsection