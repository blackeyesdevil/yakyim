<?php
use App\Http\Controllers\Backend\AddressBookController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new AddressBookController(); // New Object Controller
$mainFn = new MainFunction(); // New Object Main Function

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;
$path = $objCon->path;
$tb_name = $objCon->tbName;
$orderBy = Input::get('orderBy');
$sortBy = Input::get('sortBy');
$search = Input::get('search');
$user_id = input::get('user_id');

$a_otherParam = Input::except(['orderBy','sortBy']);
$strParam = $mainFn->parameter($a_otherParam);

$a_billing_shipping = Config::get('menuList.billing_shipping');

?>
@extends('admin')
@section('content')
    <div class="page-container">
        <div class="page-head">
            <div class="container">
                <div class="page-title">
                    <h1>{{ $titlePage }}</h1>
                </div>
            </div>
        </div>

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN SAMPLE TABLE PORTLET-->

                        <div class="portlet light">


                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr >
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('ID',$pkField,$orderBy,$sortBy,$strParam) !!} </th>
                                            <th class="text-center">{!! $mainFn->sorting('Billing / Shipping','billing_shipping',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="text-center">{!! $mainFn->sorting('Firstname - Lastname','firstname',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="text-center">{!! $mainFn->sorting('Address','address',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="text-center">{!! $mainFn->sorting('District','district_id',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="text-center">{!! $mainFn->sorting('Amphur','amphur_id',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="text-center">{!! $mainFn->sorting('Province','province_id',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="text-center">{!! $mainFn->sorting('Zip','zipcode',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-1 text-center font-blue">EDIT</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if($countData > 0)
                                            @foreach($data as $field)
                                                <tr>
                                                    <td class="text-center">{{ $field->$pkField }}</td>
                                                    <td>{{ $a_billing_shipping[$field->billing_shipping] }}</td>
                                                    <td>{!! Html::decode($field->firstname) !!} {!! Html::decode($field->lastname) !!}</td>
                                                    <td>{!! Html::decode($field->address) !!}</td>
                                                    <td>
                                                        <?php $n_district = DB::Table('a_district')->where('district_id',$field->district_id)->first();
                                                        ?>{{  $n_district->district_name_th }}

                                                    </td>

                                                    <td>
                                                        <?php $n_amphur = DB::Table('a_amphur')->where('amphur_id',$field->amphur_id)->first();
                                                        ?>{{  $n_amphur->amphur_name_th }}
                                                    </td>

                                                    <td><?php $n_province = DB::Table('a_province')->where('province_id',$field->province_id)->first();
                                                        ?>{{  $n_province->province_name_th }}
                                                    </td>

                                                    <td>{!! Html::decode($field->zipcode) !!}</td>


                                                    <td class="text-center">
                                                        <a href="{{ URL::to($path.'/'.$field->$pkField.'/edit?'.$strParam) }}" class="btn btn-xs btn-circle green"><i class="fa fa-edit"></i></a>
                                                        <form action="{{URL::to($path.'/'.$field->$pkField)}}" method="post" class="frm-delete">
                                                            <input name="_method" type="hidden" value="delete">
                                                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                                            <button type="submit" class="btn btn-xs btn-circle red" ><i class="fa  fa-trash-o"></i></button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr><td colspan='12' class='text-center'>No Result.</td></tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                {!! $data->appends(Input::except('page'))->render() !!}
                            </div>
                        </div>
                        <!-- END SAMPLE TABLE PORTLET-->
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
@endsection
