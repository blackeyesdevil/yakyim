<?php
use App\Http\Controllers\Backend\OrdersController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new OrdersController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}
$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);
?>
@extends('admin')
@section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>{{ $txt_manage.' '.$titlePage }}</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD -->

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body form">
                                <form action="{{URL::to($url_to)}}" enctype="multipart/form-data" method="POST" class="form-horizontal">
                                    <input name="_method" type="hidden" value="{{$method}}">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">


                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Orders Date</label>
                                            <div class="col-md-4">
                                                {{ $mainFn->format_date_th($orders_date,2) }}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Customer ID</label>
                                            <div class="col-md-4">
                                                <input type="text"  name="user_id"  class="form-control" value="{{ $user_id }}"readonly/>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Billing address</label>
                                            <div class="col-md-4">

                                                    {!! $billing_address !!}

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Shipping address</label>
                                            <div class="col-md-4">

                                                    {!! $shipping_address !!}

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Payment by</label>
                                            <div class="col-md-4">
                                                <select name="payment_by" class="form-control select2-container form-control select2me" disabled>
                                                    <option value="Cash on delivery" @if($payment_by == "Cash on delivery") selected @endif>Cash on delivery</option>
                                                    <option value="ATM" @if($payment_by == "ATM") selected @endif >ATM</option>
                                                    <option value="Internet Banking" @if($payment_by == "Internet Banking") selected @endif >Internet Banking</option>
                                                    <option value="Counter Service" @if($payment_by == "Counter Service") selected @endif >Counter Service</option>
                                                  </select>

                                                {{--<input type="text" name="payment_by"  class="form-control" value="{{ $payment_by }}"/>--}}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Payment Date</label>
                                            <div class="col-md-4">
                                                <input type="text"  name="payment_date" id="payment_date" class="form-control" value="{{ $payment_date }}" data-date-format="yyyy-mm-dd hh:ii" readonly/>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Paysbuy Method</label>
                                            <div class="col-md-4">
                                                <input type="text"  name="paysbuy_method"  class="form-control" value="{{ $paysbuy_method}}" readonly/>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Discount Price</label>
                                            <div class="col-md-4">
                                                <input type="text"  name="discount_price"  class="form-control" value="{{ $discount_price }}" readonly/>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Shipping Price</label>
                                            <div class="col-md-4">
                                                <input type="text"  name="shipping_price"  class="form-control" value="{{ $shipping_price}}" readonly/>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Total Price</label>
                                            <div class="col-md-4">
                                                <input type="nubmer" step="0.01" min="0" name="total_price"  class="form-control" value="{{ $total_price }}" readonly/>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Total Point</label>
                                            <div class="col-md-4">
                                                <input type="nubmer" step="0.01" min="0" name="total_point"  class="form-control" value="{{ $total_point }}" readonly/>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Order Status</label>
                                            <div class="col-md-4">
                                                <select name="status_id" class="form-control select2-container form-control select2me">
                                                    <option value="4">In process</option>
                                                        <option value="11">Shipping Now</option>
                                                    <option value="6">Shipped</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Tracking No.</label>
                                            <div class="col-md-4">
                                                <input type="text"  name="tracking_no"  class="form-control" value="{{ $tracking_no }}"/>
                                            </div>
                                        </div>

                                        <input type="hidden"  name="payment_method_id"  class="form-control" value="{{ $payment_method_id}}" readonly/>
                                        <input type="hidden"  name="shipping_address_id"  class="form-control" value="{{ $shipping_address_id}}" readonly/>
                                        <input type="hidden"  name="billing_address_id"  class="form-control" value="{{ $billing_address_id}}"readonly/>

                                    </div>


                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <input type="hidden" name="strParam" value="{{$strParam}}">
                                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                                <button type="reset" class="btn default">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
    <script src="{{URL::asset('js/elfinder-upload.js')}}"></script>
    <script>
        $(function() {
            $( "#orders_date" ).datetimepicker();

        });
    </script>

@endsection