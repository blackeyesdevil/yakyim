<?php
use App\Http\Controllers\Backend\OrdersController;
use App\Library\MainFunction;

$objCon = new OrdersController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;
$path = $objCon->path;

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}
?>
<script type="text/javascript">
     window.print();
</script>
<style>
    body {
        height: 842px;
        width: 990px;
        margin-left: auto;
        margin-right: auto;
    }
    .label{
        width: 450px;
        height: 150px;
        padding: 10px;
        margin: 5px;

        float: left;

        overflow: hidden;

        outline: 1px dotted;
    }
    .page-break  {
        clear: left;
        display:block;
        page-break-after:always;
    }

    .text-left{
        text-align: left;
    }
    .text-right{
        text-align: right;
    }
</style>


    <div class="label">
        {{--<strong>{{ $value->firstname_th }} {{ $value->lastname_th }} (Tel.{{ $value->mobile }})<br/><br/></strong>--}}
        <p class="text-left">{!! $shipping_address !!}<br /><br/> รายการสั่งซื้อที่ : # {{ $orders_no }}</p>
    </div>


<div class="page-break"></div>

