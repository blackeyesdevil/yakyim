<?php
use App\Http\Controllers\Backend\OrdersWareHouseController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new OrdersWareHouseController(); // New Object Controller
$mainFn = new MainFunction(); // New Object Main Function

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;
$path = $objCon->path;
$tb_name = $objCon->tbName;

$paysbuy = Config()->get('main.paysbuy_method');

$status = App\Model\Status::whereNull('deleted_at')->get();

$orderBy = Input::get('orderBy');
$sortBy = Input::get('sortBy');
$search = Input::get('search');

$a_otherParam = Input::except(['orderBy','sortBy']);
$strParam = $mainFn->parameter($a_otherParam);

?>

@extends('admin')
@section('content')
    <div class="page-container">
        <div class="page-head">
            <div class="container">
                <div class="page-title">
                    <h1>{{ $titlePage }}</h1>
                </div>
            </div>
        </div>

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN SAMPLE TABLE PORTLET-->

                        <div class="portlet light">

                            <form action{{URL::to($path)}} class="form-horizontal" method="GET" >
                                <div class="form-group">
                                    <label class="control-label col-md-1">Search</label>
                                    <div class="col-md-3">
                                        <input class="form-control" type="text" name="search" value="{{ $search }}" >
                                        <span class="help-block">Search by Orders No. , Customer ID</span>
                                    </div>

                                    <div class="col-md-3">
                                        <select name="status" class="form-control select2-container form-control select2me">
                                            <option  value="">Status..</option>
                                            <option value="3" >Paid</option>
                                            <option value="7" >Cancelled</option>
                                            <option value="4">In process</option>
                                            <option value="6">Shipped</option>
                                            <option value="11">Shipping Now</option>
                                        </select>
                                        <span class="help-block">Search by Status Order</span>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <select name="payment_by" class="form-control select2-container form-control select2me" >
                                                <option  value="">Payment..</option>
                                                <option value="Cash on delivery" >Cash on delivery</option>
                                                <option value="ATM"  >ATM</option>
                                                <option value="Internet Banking" >Internet Banking</option>
                                                <option value="Counter Service" >Counter Service</option>
                                            </select>
                                            <span class="help-block">Search by Payment by</span>
                                        </div>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-1"></label>
                                    <div class="col-md-4">
                                        <div class="input-group input-large date-picker input-daterange" data-date="<?php echo date("Y-m-d");?>" data-date-format="yyyy-mm-dd">
                                            <input type="text" class="form-control" name="from_date" value="">
												<span class="input-group-addon">
												to </span>
                                            <input type="text" class="form-control" name="to_date" value="">
                                        </div>
                                        <span class="help-block">
											Select date range </span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-offset-1 col-md-3 ">
                                        <button class="btn blue btn-sm" type="submit" ><i class="fa fa-search"></i> Search</button>
                                    </div>
                                </div>
                            </form>
                            <hr>
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-database font-green-sharp"></i>
                                    <span class="caption-subject font-green-sharp bold">Found {{ $countData }} Record(s).</span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                {!! Form::open(array('url' => '_admin/status-change' , 'method' => 'POST' , 'class' => 'form-horizontal')) !!}
                                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                <div class="form-group">
                                    <label class="control-label col-md-2">เปลี่ยนสถานะ</label>
                                    <div class="col-md-3">
                                        <select name="status_s" class="form-control select2-container form-control select2me" data-placeholder="Select...">
                                            <option value="">Status</option>
                                            <option value="4">In process</option>
                                            <option value="6">Shipped</option>
                                            <option value="11">Shipping Now</option>
                                        </select>
                                    </div>

                                    <div class="col-md-3">
                                        <button class="btn blue btn-sm" type="submit" >OK</button>
                                    </div>
                                </div>

                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>

                                            <th class="col-md-1 text-center"><input type="checkbox" name="selectedAll" id="selectedAll"></th>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('ID',$pkField,$orderBy,$sortBy,$strParam) !!} </th>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('Order No.','orders_no',$orderBy,$sortBy,$strParam) !!} </th>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('Date','orders_date',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-2 text-center">{!! $mainFn->sorting('Fullname','user_id',$orderBy,$sortBy,$strParam) !!}</th>

                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('Payment By','payment_method_id',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('Paysbuy','paysbuy_method',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('Total price','total_price',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-1">{!! $mainFn->sorting('Status','status_id',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-2 text-center">Tracking No.</th>
                                            <th></th>
                                            <th class="col-md-1 text-center">Print Shipping</th>

                                            <th class="caption-subject font-blue-sharp text-center">
                                                Edit
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>



                                        @if($countData > 0)
                                            @foreach($data as $field)
                                                <tr>

                                                    <td class="span1"><div class="text-center">
                                                            <input type="checkbox" name="order_chk[]" id="order_chk[]" class="checkbox1" value="{{ $field->$pkField }}"></div>
                                                    </td>
                                                    <td class="text-center">{{ $field->$pkField }}</td>
                                                    <td class="text-center">{{ $field->orders_no }}</td>
                                                    {!! Form::close() !!}
                                                    <td>{{ $mainFn->format_date_th($field->orders_date,2) }}</td>
                                                    <td>
                                                        {{ $field->firstname_th }} {{ $field->lastname_th }}
                                                    </td>
                                                    <td>
                                                        {{ $field->Payment->name_th }}
                                                    </td>
                                                    <td>
                                                        @if(!empty($field->paysbuy_method))
                                                            {{ $paysbuy[$field->paysbuy_method] }}
                                                        @endif
                                                    </td>
                                                    <td class="text-right">{{ number_format($field->total_price,2)}}</td>

                                                    @if($field->status_id == 0)
                                                        <td><?php echo" ";?></td>
                                                    @else
                                                        <td>{{ $field->Status->status_name_en }}</td>
                                                    @endif

                                                    <td>
                                                        {!! Form::open(array('url' => '_admin/update-tracking' , 'method' => 'POST' , 'class' => 'form-horizontal')) !!}

                                                        <div class="input-group">
                                                            <input type="text" name="tracking_no" class="form-control" value="{{ $field->tracking_no }}">
                                                            <input type="hidden" name="orders_id" class="form-control" value="{{ $field->$pkField }}">
                                                            <div class="input-group-btn">
                                                                <button class="btn blue btn-sm" type="submit">OK</button>
                                                            </div>
                                                        </div>

                                                        {!! Form::close() !!}

                                                    </td>

                                                    <td class="text-center">

                                                        <a href="{{URL::to('_admin/orders_detail/create?'.$pkField.'='.$field->$pkField)}}" class="btn btn-xs btn-circle blue" target="_blank"><i class="fa fa-search"></i></a>
                                                    </td>
                                                    <td class="text-center">

                                                        <a href="{{URL::to($path.'/'.$pkField.'='.$field->$pkField)}}" class="btn btn-xs btn-circle blue" target="_blank"><i class="fa fa-print"></i></a>
                                                    </td>
                                                    <td class="text-center">
                                                        <a href="{{ URL::to($path.'/'.$field->$pkField.'/edit?'.$strParam) }}" class="btn btn-xs btn-circle green"><i class="fa fa-edit"></i></a>


                                                        <form action="{{URL::to($path.'/'.$field->$pkField)}}" method="post" class="frm-delete">
                                                            <input name="_method" type="hidden" value="delete">
                                                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                                            <button type="submit" class="btn btn-xs btn-circle red" ><i class="fa  fa-trash-o"></i></button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach

                                        @else
                                            <tr><td colspan='13' class='text-center'>No Result.</td></tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            {!! $data->appends(Input::except('page'))->render() !!}
                        </div>
                    </div>
                    <!-- END SAMPLE TABLE PORTLET-->
                </div>
            </div>
            <!-- END PAGE CONTENT INNER -->
        </div>
    </div>
    <!-- END PAGE CONTENT -->
    </div>

    <script>
        $(document).ready(function() {
            $('#selectedAll').on('click',function(){
                if($(this).is(':checked')) {

                    $('.checkbox1').attr('checked', 'checked');
                    $('.checkbox1').parent().addClass('checked');
                }else{
                    $('.checkbox1').removeAttr('checked');
                    $('.checkbox1').parent().removeClass('checked');
                }
            });

        });

    </script>

@endsection

