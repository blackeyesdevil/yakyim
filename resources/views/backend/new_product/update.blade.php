<?php
use App\Http\Controllers\Backend\NewProductController;
use App\Library\MainFunction;
use App\Library\CategoryFunction;
use Illuminate\Support\Facades\Input;

$objCon = new NewProductController();
$mainFn = new MainFunction();
$CategoryFn = new CategoryFunction();

$product = App\Model\Product::whereNull('deleted_at')->get();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;
$tb_name = $objCon->tbName;

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}
$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);
?>
@extends('admin')
@section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1><a href="{{URL::to('_admin/product')}}">Product</a> / {{ $txt_manage.' '.$titlePage }}</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD -->


        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body form">
                                <form action="{{URL::to($url_to)}}" enctype="multipart/form-data" method="POST" id="form-product" class="form-horizontal">
                                    <input name="new_sorting" type="hidden" value="{{$new_sorting}}">
                                    <input name="_method" type="hidden" value="{{$method}}">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Status</label>
                                                <div class="col-md-4">
                                                    <select name="status" class="form-control select2-container select2me">
                                                        <option value="0" @if($status == 0)  selected @endif >Disabled</option>
                                                        <option value="1"  @if($status == 1 || $status=='') selected @endif>Enabled</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Product Name</label>
                                                <div class="col-md-4">
                                                    <input type="text" name="product_name_th" id="product_name_th" class="form-control" value="{{ $product_name_th }}">
                                                </div>
                                                <div class="col-md-1">
                                                    ( TH )
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Product Name</label>
                                                <div class="col-md-4">
                                                    <input type="text" name="product_name_en" id="product_name_en" class="form-control" value="{{ $product_name_en }}">
                                                </div>
                                                <div class="col-md-1">
                                                    ( EN )
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Category</label>
                                                <div class="col-md-4">
                                                    <select name="category_id[]" class="form-control select2-container form-control select2me" multiple="multiple">
                                                        {{ $CategoryFn->search_select_category('','','') }}
                                                    </select>
                                                </div>
                                            </div>

                                            @if($countCategory !="")
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">List Category</label>
                                                    <div class="col-md-4">
                                                        @if($countCategory > 0)
                                                            @foreach($product_cat as $cat2)

                                                                <span style="display: inline;">
                                                                    <a id="delete_category-{{$cat2->category_id}}" data-change_field="delete_category" class="delete-category btn btn-xs btn-default"  data-tb_name="{{$tb_name}}" data-value="{{$cat2->category_id}}"><i class="fa fa-times"></i>
                                                                        {{ $CategoryFn->list_category($cat2->category_id) }}
                                                                    </a>
                                                                </span>
                                                            @endforeach
                                                        @else
                                                            No Result.
                                                        @endif
                                                        <input type="hidden" name="product_id" value="{{ $product_id }}">
                                                    </div>
                                                </div>
                                            @endif

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Related Product</label>
                                                <div class="col-md-4">
                                                    <select name="related_id[]" class="form-control select2-container form-control select2me" multiple="multiple">
                                                        @foreach($product as $product)
                                                            <option value="{{ $product->product_id }}" >{{ $product->product_name_th }} @if(!empty($product->product_name_en)) ( {{ $product->product_name_en }} )<?php foreach($product_option as $option) ?> ( {{ $option->model }} ) @endif</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            @if($countProduct !="")
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">List Related Product</label>
                                                    <div class="col-md-4">
                                                        @if($countProduct > 0)
                                                            @foreach($product_related as $cat2)
                                                                <?php
                                                                    $product = DB::table('product')->select('product_name_th')->where('product_id',$cat2->related_id)->first();
                                                                ?>
                                                                <span style="display: inline;">
                                                                    <a id="delete_product-{{$cat2->related_id}}" data-change_field="delete_product" class="delete-product btn btn-xs btn-default"  data-tb_name="{{$tb_name}}" data-value="{{$cat2->related_id}}"><i class="fa fa-times"></i>
                                                                        {{ $product->product_name_th }}
                                                                    </a>
                                                                </span>
                                                            @endforeach
                                                        @else
                                                            No Result.
                                                        @endif
                                                        <input type="hidden" name="product_id" value="{{ $product_id }}">
                                                    </div>
                                                </div>
                                            @endif

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Detail ( TH )</label>
                                                <div class="col-md-9">
                                                    <textarea name="detail_th" id="detail_th" class="form-control ckeditor" data-error-container="#detail_error">{{ $detail_th}}</textarea>
                                                    <div id="detail_error"></div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Detail ( EN )</label>
                                                <div class="col-md-9">
                                                    <textarea name="detail_en" id="detail_en" class="form-control ckeditor">{{ $detail_en}}</textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Size Chart</label>
                                                <div class="col-md-4">
                                                    <select name="size_chart_id" class="form-control select2-container form-control select2me">
                                                        <option value="0"{{ ($size_chart_id == '0')?' selected':'' }}></option>
                                                        @foreach($size_charts as $size_chart)
                                                        <option value="{{ $size_chart->size_chart_id }}"{{ ($size_chart_id == $size_chart->size_chart_id)?' selected':'' }}>{{ $size_chart->chart_name_th }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Price</label>
                                                <div class="col-md-4">
                                                    <input type="number" min="0" step="0.01" name="retail_price" id="retail_price" class="form-control" value="{{ $retail_price }}">
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="control-label col-md-3">Special Price</label>
                                                <div class="col-md-4">
                                                    <input type="number" min="0" step="0.01" name="special_price" id="special_price" class="form-control" value="{{ $special_price }}">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Minimum Quantity</label>
                                                <div class="col-md-4">
                                                    <input type="number" min="1" step="1" name="minimum_qty" id="minimum_qty" class="form-control" value="{{ $minimum_qty }}">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Look Book</label>
                                                <div class="col-md-4">
                                                    <input type="text"  name="look_book" id="look_book" class="form-control" value="{{ $look_book }}">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3"></label>
                                                <div class="col-md-9">
                                                    <div id="thumbnail">
                                                        @if($img_new_product != '')
                                                            <img src="<?php echo URL::asset('uploads/banner_product/'.$img_new_product) ?>" id="img_path2" style="max-width:400px;">
                                                        @else
                                                            <img src="" id="img_path2" style="max-width:400px;">
                                                        @endif

                                                        <input type="hidden" name="img_new_product" id="img_path" value="<?php echo  $img_new_product?>">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Image New Product</label>
                                                <div class="col-md-4">
                                                    @if($img_new_product != '')
                                                        <div id="remove_img" class="btn btn-danger" >Remove</div>
                                                        <div id="upload" class="btn blue" style="display:none;"><i class="icon-picture"></i> Select File</div>
                                                    @elseif ($img_new_product == '')
                                                        <div id="upload" class="btn blue" ><i class="icon-picture"></i> Select File</div>
                                                        <div id="remove_img" class="btn btn-danger" style="display:none;" >Remove</div>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label"> </label>
                                                <div class="col-md-4">
                                                    <span style="color:#F00">* หมายเหตุ</span><br>
                                                    - ไฟล์ต้องเป็นมีขนาดกว้าง 660x880 pixel. <br>
                                                    - นามสกุลของรูปภาพต้อง .png, .jpg เท่านั้น<br>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <input type="hidden" name="strParam" value="{{$strParam}}">
                                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                                <button type="reset" class="btn default">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT INNER -->
        <!-- END PAGE CONTENT -->
    </div>

@endsection
@section('js')
    <script src="{{URL::asset('js/elfinder-upload.js')}}"></script>
    <script src="{{URL::asset('assets/admin/scripts/product.js')}}"></script>
    <script src="{{URL::asset('js/del-category.js')}}"></script>

@endsection
