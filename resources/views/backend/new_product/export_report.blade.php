<?php

$filename  ="excel_report.xls";
header("Content-Type: application/vnd.ms-excel; charset=utf-8");
header("Content-Disposition: attachment; filename=report_stock.xls");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private",false);
$p = 0;
?>
<html >
<head>
    <meta charset="UTF-8">
</head>


<body>
<TABLE BORDER="1" width="100%">
    <tr>
        <th><div align="left">ID</div></th>
        <th><div align="left">Product Name</div></th>
        <th><div align="center">Qty</div></th>
        <th><div align="center">Date/Time</div></th>
    </tr>
    @if($countData > 0)
        @foreach($data as $field)
            @if($p!=$field->product_id)
                <?php $p=$field->product_id;?>
                <tr>
                    <td><div align="center">{{$field->product_id}}</div></td>
                    <td colspan="2">{{$field->product_name_th }} ( {{ $field->product_name_en }} ) </td>
                     <td>
                <div align="right">
                    {{ $field->created_at }}
                </div>
            </td>
                <?php
                $db = DB::table('product_option')->select('option_name_th','qty')->whereNull('deleted_at')->where('product_id',$field->product_id)->get();
                ?>

                @foreach($db as $value)
                    <tr>
                        <td></td>
                        <td>@if(!empty($value->option_name_th)){{ $value->option_name_th }} @else ไม่มีชื่อสินค้า @endif</td>
                        <td><div align="center">{{ $value->qty }}</div></td>
                    </tr>
                    @endforeach

                    </tr>
                    @endif
                @endforeach
            @else
                <tr><td colspan='6' class='text-center'>No Result.</td></tr>
            @endif
</TABLE>
</body>
</html>