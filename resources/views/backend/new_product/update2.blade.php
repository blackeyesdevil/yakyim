<?php
use App\Http\Controllers\Backend\ProductController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new ProductController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;
$tb_name = $objCon->tbName;

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}
$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);
?>
@extends('admin')
@section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1><a href="{{URL::to('_admin/product')}}">Product</a> / {{ $txt_manage.' '.$titlePage }}</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
            <!-- END PAGE HEAD -->


        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body form">
                                <form action="{{URL::to($url_to)}}" enctype="multipart/form-data" method="POST" id="form-product" class="form-horizontal">
                                    <input name="_method" type="hidden" value="{{$method}}">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs nav-justified" role="tablist">
                                        <li role="presentation" class="active">
                                            <a href="#product-tap1" aria-controls="product-tap1" role="tab" data-toggle="tab">
                                                Data
                                            </a>
                                        </li>

                                        <li role="presentation">
                                            <a href="#product-tap2" aria-controls="product-tap2" role="tab" data-toggle="tab">
                                                Category
                                            </a>
                                        </li>


                                        {{--<li role="presentation">--}}
                                            {{--<a href="#product-tap3" aria-controls="product-tap3" role="tab" data-toggle="tab">--}}
                                                {{--Image--}}
                                            {{--</a>--}}
                                        {{--</li>--}}
                                    </ul>

                                    {{--TAB CONTENT--}}

                                    <div class="tab-content tab-outside">

                                        <!-- tab 1 -->
                                        <div role="tabpanel" class="tab-pane fade in active" id="product-tap1">
                                            <div class="row">
                                                <div class="col-md-12">

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Product Name [TH]</label>
                                                            <div class="col-md-4">
                                                                <input type="text" name="product_name_th" id="product_name_th" class="form-control" value="{{ $product_name_th }}">
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Product Name [EN]</label>
                                                            <div class="col-md-4">
                                                                <input type="text" name="product_name_en" id="product_name_en" class="form-control" value="{{ $product_name_en }}">
                                                            </div>
                                                        </div>


                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Detail[TH]</label>
                                                            <div class="col-md-9">
                                                                <textarea name="detail_th" id="detail_th" class="form-control ckeditor" data-error-container="#detail_error">{{ $detail_th}}</textarea>
                                                                <div id="detail_error">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Detail[EN]</label>
                                                            <div class="col-md-9">
                                                                <textarea name="detail_en" id="detail_en" class="form-control ckeditor">{{ $detail_en}}</textarea>
                                                            </div>
                                                        </div>



                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Retail Price</label>
                                                            <div class="col-md-4">
                                                                <input type="number" min="0" step="0.01" name="retail_price" id="retail_price" class="form-control" value="{{ $retail_price }}">
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Minimum Quantity</label>
                                                            <div class="col-md-4">
                                                                <input type="number" min="1" step="1" name="minimum_qty" id="minimum_qty" class="form-control" value="{{ $minimum_qty }}">
                                                            </div>
                                                        </div>


                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Point</label>
                                                            <div class="col-md-4">
                                                                <input type="number" min="0" step="1" name="point" id="point" class="form-control" value="{{ $point }}">
                                                            </div>
                                                        </div>



                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Meta Title</label>
                                                            <div class="col-md-4">
                                                                <input type="text"  name="meta_title" id="meta_title" class="form-control" value="{{ $meta_title}}">
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Meta Keyword</label>
                                                            <div class="col-md-4">
                                                                <input type="text"  name="meta_keyword" id="meta_keyword" class="form-control" value="{{ $meta_keyword}}">
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Meta Description</label>
                                                            <div class="col-md-4">
                                                                <textarea name="meta_description" id="meta_description" class="form-control">{{ $meta_description}}</textarea>
                                                            </div>
                                                        </div>


                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Dimension Length</label>
                                                            <div class="col-md-4">
                                                                <input type="number" min="0" step="0.01" name="dimension_l" id="dimension_l" class="form-control" value="{{ $dimension_l }}">
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Dimension Width</label>
                                                            <div class="col-md-4">
                                                                <input type="number" min="0" step="0.01" name="dimension_w" id="dimension_w" class="form-control" value="{{ $dimension_w }}">
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Dimension Height</label>
                                                            <div class="col-md-4">
                                                                <input type="number" min="0" step="0.01" name="dimension_h" id="dimension_h" class="form-control" value="{{ $dimension_h }}">
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Dimension Class</label>
                                                            <div class="col-md-4">
                                                                <select name="dimension_class_id" class="form-control select2-container form-control select2me" >
                                                                    @foreach($dimension as $field3)
                                                                        <option value="{{$field3->dimension_class_id}}" @if($dimension_class_id == $field3->dimension_class_id) selected @endif >{{$field3->title_en}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Weight</label>
                                                            <div class="col-md-4">
                                                                <input type="number" min="0" step="0.01" name="weight" id="weight" class="form-control" value="{{ $weight }}">
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Weight Class</label>
                                                            <div class="col-md-4">
                                                                <select name="weight_class_id" class="form-control select2-container form-control select2me" >
                                                                    @foreach($weight1 as $field4)
                                                                        <option value="{{$field4->weight_class_id}}" @if($weight_class_id == $field4->weight_class_id) selected @endif >{{$field4->title_en}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Manufacturer</label>
                                                            <div class="col-md-4">
                                                                <select name="manufacturer_id" class="form-control select2-container form-control select2me" >
                                                                    @foreach($manufacturer as $field5)
                                                                        <option value="{{$field5->manufacturer_id}}" @if($manufacturer_id == $field5->manufacturer_id) selected @endif >{{$field5->manufacturer_name_th}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>


                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Available</label>
                                                            <div class="col-md-4">
                                                                <p><input type='date' name="available_date" id="available_date" class="form-control" value="{{ $available_date }}" data-date-format="yyyy-mm-dd"></p>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Status</label>
                                                            <div class="col-md-4">
                                                                <select name="status" class="form-control select2-container select2me">
                                                                    <option value="0" @if($status == 0) selected @endif >Disabled</option>
                                                                    <option value="1"  @if($status == 1) selected @endif>Enabled</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    <hr>

                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Product Option</label>
                                                            <div class="col-md-3">
                                                                <input type='text'  name="option_name_th" id="option_name_th" class="form-control" value="{{$name_th}}" placeholder="Name [TH]"><br>
                                                                <input type='number' step="0.01" min="0" name="model" id="model" class="form-control" value="{{$model}}" placeholder="Model">

                                                            </div>

                                                            <div class="col-md-3">
                                                                <input type='text'  name="option_name_en" id="option_name_en" class="form-control" value="{{$name_en}}" placeholder="Name [EN]">
                                                                <br><input type='number' step="0.01" min="0"   name="sku" id="sku" class="form-control" value="{{$sku}}" placeholder="Sku">

                                                            </div>


                                                            <div class="col-md-3">
                                                                <input type='number' name="qty" id="qty" class="form-control" value="{{$qty}}" placeholder="Quantity">
                                                                <br><input type='number' step="0.01" min="0"  name="ean" id="ean" class="form-control" value="{{$ean}}" placeholder="Ean">
                                                            </div>
                                                    </div>

                                                    <div class="control-group ">
                                                        <div class="controls">
                                                            <div id="thumbnail1" class="text-center">
                                                                @if($photo != '')
                                                                    <img src="{{URL::asset('uploads/product_option/'.$o_product->photo) }}" id="img_path3" style="max-width:400px;">
                                                                @else
                                                                    <img src="" id="img_path3" style="max-width:400px;">
                                                                @endif

                                                                <input type="hidden" name="photo1" id="img_path1" value="<?php echo  $photo?>">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-body">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Photo</label>
                                                            <div class="col-md-4">
                                                                @if($photo != '')
                                                                    <div id="remove_img1" class="btn btn-danger" >Remove</div>
                                                                    <div id="upload1" class="btn blue" style="display:none;"><i class="icon-picture"></i> Select File</div>

                                                                @elseif ($photo == '')
                                                                    <div id="upload1" class="btn blue" ><i class="icon-picture"></i> Select File</div>
                                                                    <div id="remove_img1" class="btn btn-danger" style="display:none;" >Remove</div>

                                                                @endif



                                                            </div>
                                                        </div>
                                                    </div>






                                                </div>
                                            </div>
                                        </div>

                                        <!-- tab 2 -->
                                        <div role="tabpanel" class="tab-pane fade" id="product-tap2">
                                            <div class="row">
                                                <div class="col-md-12">

                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Category</label>
                                                                <div class="col-md-4">
                                                                    <select name="category_id[]" class="form-control select2-container form-control select2me" multiple="multiple">
                                                                        @foreach($category as $cat)
                                                                            <option value="{{$cat->category_id}}" >{{$cat->category_name_th}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            @if($countCategory !="")

                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">List Category</label>
                                                                    <div class="col-md-4">

                                                                        @if($countCategory > 0)
                                                                            @foreach($product_cat as $cat2)

                                                                                <span style="display: inline;">
                                                                                <a id="delete_category-{{$cat2->category_id}}" data-change_field="delete_category" class="delete-category btn btn-xs btn-default"  data-tb_name="{{$tb_name}}" data-value="{{$cat2->category_id}}"><i class="fa fa-times"></i> {{ $cat2->Category->category_name_th}}</a>
                                                                                </span>
                                                                            @endforeach
                                                                            @else
                                                                                No Result.
                                                                        @endif
                                                                        <input type="hidden" name="product_id" value="{{ $product_id }}">
                                                                    </div>
                                                                </div>

                                                                @else
                                                            @endif

                                                </div>
                                            </div>
                                        </div>

                                        <!-- tab 3 -->
                                        {{--<div role="tabpanel" class="tab-pane fade" id="product-tap3">--}}
                                            {{--<div class="row">--}}
                                                {{--<div class="col-md-12">--}}

                                                    {{--<input id="fileupload" type="file" name="files[]" data-url="server/php/" multiple>--}}

                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}

                                    {{--</div>--}}

                                    {{--END TAB CONTENT--}}



                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">

                                                    <input type="hidden" name="strParam" value="{{$strParam}}">
                                                    <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                                    <button type="reset" class="btn default">Reset</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
         <!-- END PAGE CONTENT INNER -->
        <!-- END PAGE CONTENT -->
    </div>




@endsection
@section('js')
    <script src="{{URL::asset('js/elfinder-upload.js')}}"></script>
    <script src="{{URL::asset('assets/admin/scripts/product.js')}}"></script>
    <script src="{{URL::asset('js/del-category.js')}}"></script>

    <script src="{{URL::asset('js/jquery.iframe-transport.js') }}"></script>
    <!-- The basic File Upload plugin -->
    <script src="{{URL::asset('js/jquery.fileupload.js') }}"></script>
    <!-- The File Upload processing plugin -->
    <script src="{{URL::asset('js/jquery.fileupload-process.js') }}"></script>
    <!-- The File Upload image preview & resize plugin -->
    <script src="{{URL::asset('js/jquery.fileupload-image.js') }}"></script>
    <!-- The File Upload audio preview plugin -->
    <script src="{{URL::asset('js/jquery.fileupload-audio.js') }}"></script>
    <!-- The File Upload video preview plugin -->
    <script src="{{URL::asset('js/jquery.fileupload-video.js') }}"></script>
    <!-- The File Upload validation plugin -->
    <script src="{{URL::asset('js/jquery.fileupload-validate.js') }}"></script>
    <!-- The File Upload user interface plugin -->
    <script src="{{URL::asset('js/jquery.fileupload-ui.js') }}"></script>

    <script>
        $(function () {
            'use strict';

            // Initialize the jQuery File Upload widget:
            $('#fileupload').fileupload({
                // Uncomment the following to send cross-domain cookies:
                //xhrFields: {withCredentials: true},
                url: 'server/php/'
            });

            // Enable iframe cross-domain access via redirect option:
            $('#fileupload').fileupload(
                    'option',
                    'redirect',
                    window.location.href.replace(
                            /\/[^\/]*$/,
                            '/cors/result.html?%s'
                    )
            );

            if (window.location.hostname === 'blueimp.github.io') {
                // Demo settings:
                $('#fileupload').fileupload('option', {
                    url: '//jquery-file-upload.appspot.com/',
                    // Enable image resizing, except for Android and Opera,
                    // which actually support image resizing, but fail to
                    // send Blob objects via XHR requests:
                    disableImageResize: /Android(?!.*Chrome)|Opera/
                            .test(window.navigator.userAgent),
                    maxFileSize: 999000,
                    acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i
                });
                // Upload server status check for browsers with CORS support:
                if ($.support.cors) {
                    $.ajax({
                        url: '//jquery-file-upload.appspot.com/',
                        type: 'HEAD'
                    }).fail(function () {
                        $('<div class="alert alert-danger"/>')
                                .text('Upload server currently unavailable - ' +
                                        new Date())
                                .appendTo('#fileupload');
                    });
                }
            } else {
                // Load existing files:
                $('#fileupload').addClass('fileupload-processing');
                $.ajax({
                    // Uncomment the following to send cross-domain cookies:
                    //xhrFields: {withCredentials: true},
                    url: $('#fileupload').fileupload('option', 'url'),
                    dataType: 'json',
                    context: $('#fileupload')[0]
                }).always(function () {
                    $(this).removeClass('fileupload-processing');
                }).done(function (result) {
                    $(this).fileupload('option', 'done')
                            .call(this, $.Event('done'), {result: result});
                });
            }

        });
    </script>

@endsection
