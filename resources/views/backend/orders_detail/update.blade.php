<?php
use App\Http\Controllers\Backend\OrdersDetailController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;
use App\Model\User;

$objCon = new OrdersDetailController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;
$pkField2 = $objCon->pkField2;

$fieldList = $objCon->fieldList;

if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}
$total = '';

?>

@extends('admin')
@section('content')
    <div class="page-container">
        <div class="page-head">
            <div class="container">
                <div class="page-title">
                    <h1><a href="{{URL::to('_admin/orders')}}">Order</a> / {{ $subtitle->orders_no}} </h1>
                </div>
            </div>
        </div>

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->


            <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN SAMPLE TABLE PORTLET-->


                        <div class="portlet light">


                            <div class="portlet-title">
                                <section id="content">

                                    <div id="header-shop">

                                        <h2 style="color:green;">รายการสั่งซื้อหมายเลข #{{ $order->orders_no }}</h2>

                                        <hr>
                                    </div>

                                    <div class="col-sm-12">


                                        <div id="account" class="row">
                                            <?php
                                            $db_user = User::where('user_id',$order->user_id)->get();
                                            ?>

                                            <div class="table-responsive col-sm-6">
                                                <table class="table table-bordered">
                                                    <tbody><tr>
                                                        <th style="font-size:20;">รายละเอียดลูกค้า
                                                        </th>
                                                    </tr>

                                                    <tr>
                                                        <td>
                                                            @foreach($db_user as $value)
                                                                <div class="row">
                                                                    <div class="topic col-xs-5">ชื่อ</div>
                                                                    <div class="value col-xs-7">{{ $value->firstname }} {{ $value->lastname }}</div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="topic col-xs-5">ที่อยู่</div>
                                                                    <div class="value col-xs-7">{{ $value->address }}</div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="topic col-xs-5">เบอร์ติดต่อ</div>
                                                                    <div class="value col-xs-7">{{ $value->mobile }}</div>
                                                                </div>
                                                            @endforeach
                                                        </td>
                                                    </tr>
                                                    </tbody></table>
                                            </div>

                                            <div class="table-responsive col-sm-6">
                                                <table class="table table-bordered">
                                                    <tbody><tr>
                                                        <th style="font-size:20;">รายละเอียดการสั่งซื้อ

                                                            <a href="{{ URL::to('_admin/orders_detail/'.input::get('orders_id')) }}" style="margin-left:10px;width:50px" target="_blank"  class="btn btn-circle yellow btn-xs"><i class="fa fa-print"></i></a>
                                                        </th>
                                                    </tr>

                                                    <tr>
                                                        <td>
                                                            <div class="row">
                                                                <div class="topic col-xs-5">วันที่สั่งซื้อ</div>
                                                                <div class="value col-xs-7">{{ $mainFn->format_date_en($order->orders_date,4) }}</div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="topic col-xs-5">รหัสการสั่งซื้อ</div>
                                                                <div class="value col-xs-7">{{ $order->orders_no }}</div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="topic col-xs-5">วิธีการชำระเงิน</div>
                                                                <div class="value col-xs-7">@if ( $order->payment_method_id ==1) โอนเงิน @elseif($order->payment_method_id ==2) Paypal  @endif </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="topic col-xs-5">สถานะการสั่งซื้อ</div>
                                                                <div class="value col-xs-7">
                                                                    {{ $order->Status['status_name_th'] }}
                                                                    @if($order->status_id == '3')
                                                                        <br>
                                                                        {{ $mainFn->format_date_en($order->payment_date,4) }}
                                                                    @endif

                                                                </div>
                                                            </div>

                                                        </td>
                                                    </tr>
                                                    </tbody></table>
                                            </div>
                                        </div>

                                        <div id="address" class="row">
                                            <div class="col-sm-6">
                                                <div class="block">
                                                    <div class="sub-headline">ที่อยู่สำหรับจัดส่งสินค้า</div>

                                                    <div class="item">
                                                        {!! $order->shipping_address !!}
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="block">
                                                    <div class="sub-headline">ที่อยู่สำหรับใบกำกับภาษี</div>

                                                    <div class="item">
                                                        {!! $order->billing_address !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

<Br>

                                        <div id="summary" class="table-responsive">
                                            <table class="table table-bordered" >
                                                <tbody style="font-size:11pt">
                                                <tr>
                                                    <th class="text-center">#</th>
                                                    <th>รหัสสินค้า</th>
                                                    <th class="text-center">รูปภาพ</th>
                                                    <th colspan="2">รายการสินค้า</th>
                                                    <th>ขนาด</th>
                                                    <th class="text-center">จำนวน</th>
                                                    <th class="text-right">ราคาสินค้าต่อหน่วย</th>
                                                    <th class="text-right">ราคารวม (บาท)</th>
                                                </tr>

                                                @foreach($order_detail as $key => $item)
                                                    <tr>
                                                        <th class="text-center">{{ $key+1 }}</th>
                                                        <th >{{ $item->model }}</th>
                                                        <th class="text-center">
                                                            <?php
                                                                    $photo = DB::table('product_gallery')
                                                                            ->where('product_id',$item->product_id)
                                                                            ->orderBy('product_id','desc')
                                                                            ->select('photo')
                                                                            ->first();

                                                                if ($photo->photo != '' && file_exists('uploads/product/100/'.$photo->photo)){
                                                                    $img_url = URL::asset('uploads/product/100/' . $photo->photo);
                                                                }else{
                                                                    $img_url = '';
                                                                }
                                                            ?>

                                                            <img src="{{ $img_url }}" class="img-responsive" />
                                                        </th>
                                                        <th colspan="2">{{ $item->product_name_th }}</th>
                                                        <th>
                                                            @foreach($product_option as $option)
                                                                @if($option->product_option_id == $item->product_option_id)
                                                                    {{ $option->option_name_th }}
                                                                @endif
                                                            @endforeach
                                                        </th>
                                                        <th class="text-center">{{ $item->qty }}</th>
                                                        <th class="text-right">{{ number_format($item->unit_price,2) }}</th>
                                                        <th class="text-right">{{ number_format($item->qty * $item->unit_price,2) }}</th>
                                                        <?php
                                                                $total_price = $item->qty*$item->unit_price;
                                                                $total += $total_price;

//                                                                $before_vat = $total/1.07;

//                                                                $vat = $total-$before_vat;
                                                        ?>

                                                    </tr>
                                                @endforeach

                                                <tr>
                                                    {{--<th colspan="8" class="text-right">มูลค่าสินค้ารวมภาษีมูลค่าเพิ่ม / Sub - Total amount</th>--}}
                                                    <th colspan="8" class="text-right">มูลค่าสินค้า / Sub - Total amount</th>
                                                    <th class="text-right">{{ number_format($total,2) }}</th>
                                                </tr>

                                                <tr>
                                                    <th colspan="8" class="text-right">ส่วนลด / Discount</th>
                                                    <th class="text-right">{{ number_format($order->discount_price,2) }}</th>
                                                </tr>

                                                <tr>
                                                    <th colspan="8" class="text-right">ส่วนลดจาก Point</th>
                                                    <th class="text-right">{{ number_format($order->use_point,2) }}</th>
                                                </tr>

                                                {{--<tr>--}}
                                                    {{--<th colspan="8" class="text-right">มูลค่ารวมของสินค้า(ก่อนภาษีมูลค่าเพิ่ม) / Total amount(Before VAT)</th>--}}
                                                    {{--<th class="text-right">{{ number_format($before_vat,2) }}</th>--}}
                                                {{--</tr>--}}

                                                {{--<tr>--}}
                                                    {{--<th colspan="8" class="text-right">ภาษีมูลค่าเพิ่ม 7% / 7% VAT</th>--}}
                                                    {{--<th class="text-right">{{ number_format($vat,2) }}</th>--}}
                                                {{--</tr>--}}

                                                <tr>
                                                    <th colspan="8" class="text-right">ค่าจัดส่ง</th>
                                                    <th class="text-right">{{ number_format($order->shipping_price,2) }}</th>
                                                </tr>

                                                <tr>
                                                    <th colspan="8" class="text-right">ราคารวมสุทธิ</th>
                                                    <th class="text-right">{{ number_format($total-$order->discount_price-$order->use_point+ $order->shipping_price,2) }}</th>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>





                                </section>
                            </div>

                        </div>

                        <?php
                                $data2 = DB::table('orders')
                                ->Join('payment_inform','orders.orders_id','=','payment_inform.orders_id')
                                ->where('orders.orders_id',Input::get('orders_id'));
                                $count2 = $data2->count();
                                $data2 = $data2->get();


                        ?>

                        @if($count2 > 0)
                        <div class="portlet light">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="portlet grey-cascade box">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    Inform
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="table-responsive">
                                                    @foreach($data2 as $field)

                                                    <div class="col-md4">
                                                        @if($field->img_name !='' && file_exists('uploads/inform/'.$field->img_name))
                                                            <img src="{{URL::asset('uploads/inform/'.$field->img_name) }}" class="img-responsive">
                                                        @endif

                                                        </div>
                                                        <div class="col-md4">
                                                            <?php
                                                                $data2 = DB::Table('user')->where('user_id',$field->user_id)->get();
                                                                foreach($data2 as $user){
                                                            ?>

                                                                Name : {{ $user->firstname }} {{ $user->lastname }}
                                                                <?php } ?>
                                                            </div>
                                                        <div class="col-md4">
                                                            Date : {{ $field->payment_date }}
                                                            </div>
                                                        <div class="col-md4">
                                                            Price : {{ $field->total_price }}
                                                        </div>

                                                    @endforeach


                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    @endif

                                    <?php
                                    $data3 = DB::table('log_code_used')
//                                            ->leftJoin('log_code_used','orders.orders_id','=','log_code_used.orders_id')
                                            ->Join('discount_code','log_code_used.discount_code_id','=','discount_code.discount_code_id')
                                            ->where('log_code_used.orders_id',Input::get('orders_id'));
                                    $count3 = $data3->count();
                                    $data3 = $data3->get();


                                    ?>

                                    @if($count3 > 0)
                                    <div class="col-md-6 col-sm-6">
                                        <div class="portlet grey-cascade box">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    Code Promotion
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="table-responsive">
                                                    @foreach($data3 as $field)
                                                        <div class="col-md4">
                                                            Code Name : {{ $field->code_name_th }}
                                                        </div>
                                                        <div class="col-md4">
                                                            Code : {{ $field->code }}
                                                        </div>
                                                        <div class="col-md4">
                                                            {{ $field->discount }} {{ $field->discount_type }}
                                                            </div>

                                                        <hr>
                                                    @endforeach


                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                        @endif

                                    <?php
                                        $data4 = DB::table('point_transaction')->where('user_id',$subtitle->user_id);
                                        $count4 = $data4->count();
                                        $data4 = $data4->get();
                                    ?>

                                    @if($count4 > 0)
                                        <div class="col-md-6 col-sm-6">
                                            <div class="portlet grey-cascade box">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        Point Transaction
                                                    </div>
                                                </div>
                                                <div class="portlet-body">
                                                    <div class="table-responsive">
                                                        <table class="table table-hover table-bordered table-striped">
                                                        <tr>
                                                            <td>Detail</td>
                                                            <td>Point</td>
                                                            <td>Total</td>
                                                        </tr>
                                                        @foreach($data4 as $field)
                                                                <tr>
                                                                    <td>{{ $field->detail }}</td>
                                                                    <td>{{ $field->point }}</td>
                                                                    <td>{{ $field->total }}</td>
                                                                </tr>

                                                        @endforeach
                                                        </table>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    @endif


                                </div>

                        </div>

                        </div>
                    </div>
                </div>
                <!-- END SAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE CONTENT INNER -->


    </div>
    </div>
    <!-- END PAGE CONTENT -->
    </div>
@endsection