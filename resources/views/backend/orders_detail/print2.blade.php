<?php
use App\Http\Controllers\Backend\OrdersDetailController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new OrdersDetailController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;
$pkField2 = $objCon->pkField2;

$paysbuy = Config()->get('main.paysbuy_method');

$fieldList = $objCon->fieldList;

if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}

?>

<head>
    <style>

    @media all
    {
    .page-break { display:none; }
    .page-break-no{ display:none; }
    }
    @media print
    {
    .page-break { display:block;height:1px; page-break-before:always; }
    .page-break-no{ display:block;height:1px; page-break-after:avoid; }
    }
    </style>

    </head>



@extends('admin')
@section('content')
    <div class="page-container">
        <div class="page-head">
            <div class="container">
                <div class="page-title">
                    <h1><a href="{{URL::to('_admin/orders')}}">Order</a> / <a href="{{ URL::to('_admin/orders_detail/create?orders_id='. $orders_id) }}">Order Detail</a> </h1>
                </div>
            </div>
        </div>

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN SAMPLE TABLE PORTLET-->
                        <?php
                                $user = DB::Table('user')
                                ->where('user_id',$data->user_id)
                                ->get();

                        ?>

                        <!-- BEGIN PAGE CONTENT INNER -->
                        <div class="portlet light">
                            <div class="portlet-body">
                                <div class="row">
                                    <div class="col-xs-12 invoice-block text-right">
                                        <a class="btn btn-md blue hidden-print margin-bottom-5" onclick="javascript:window.print();">
                                            Print <i class="fa fa-print"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="invoice">
                                    <div class="row invoice-logo">
                                        <div class="invoice-logo-space">
                                            <div class="col-xs-2">
                                                <img src="{{ URL::asset('/images/logo.jpg') }}" class="img-responsive" alt=""/>
                                            </div>
                                            <div class="col-xs-10">
                                                <p style="font-size: 10px">
                                                    บริษัท ยักษ์ ยิ้ม กรุ๊ป จำกัด เลขที่ 111/21 หมู่บ้านธัญญาภิรมย์ ตำบลรังสิต อำเภอธัญบุรี จังหวัดปทุมธานี 12110<br>
                                                    Yakyim Group Ltd 111/21 Thanyaphirom Village Rangsit Thnayaburi Pathumthani<br>
                                                    Tel. 02-191-3955, 084-657-1001   http://www.yakyim.com<br>
                                                    เลขประจำตัวผู้เสียภาษี 0135558001720
                                                    {{--#{{ $data->orders_id }} / {{ $mainFn->format_date_en($data->orders_date,4) }}--}}
                                                </p>

                                            </div>

                                        </div>
                                    </div>
                                    <hr/>

                                    {{--dd--}}
                                    <div class="row">
                                        <div class="col-xs-8">

                                        </div>

                                        <div class="col-xs-4 text-center invoice-payment" style="border-color: #7E7E7E; border: 1px solid ">
                                            <h7>
                                                ใบเสร็จรับเงิน / ใบกำกับภาษี / ใบสั่งของ<br>
                                                RECEIPT/ TAX INVOICE / Delivery Order<br>
                                                ต้นฉบับ
                                            </h7>
                                        </div>


                                    </div>

                                    <br>
                                    {{----}}

                                    <div class="row">

                                        <table class="table table-bordered table-hover">
                                            <tbody>
                                            <tr>
                                                <td rowspan="2">
                                                    <strong>Order Details</strong>
                                                    <br>
                                                    Order Date: {{ $mainFn->format_date_th($data->orders_date,4) }}
                                                    <br>
                                                    Order Code.:  {{ $data->orders_id }}
                                                </td>
                                                <td align="center"><img src="{{URL::asset('uploads/images/or.JPG') }}" class="img-responsive"></td>
                                                <td colspan="2" align="center"><img src="{{URL::asset('uploads/images/t.JPG') }}" class="img-responsive"></td>
                                            </tr>
                                            <tr>
                                                <td><strong>Order No.</strong> {{ $data->orders_no }}</td>
                                                <td align="center"><strong>TRACKING NUMBER: {{ $data->tracking_number }}</strong> </td>
                                                <td align="center"><strong>1 Of 1</strong></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2"><strong>Payment type:</strong> @if($data->payment_method_id==0) รอการอนุมัติจาก Admin @elseif($data->payment_method_id==1) โอนเงิน @elseif($data->payment_method_id==2) Paypal @endif<br>
                                                    @if($data->shipping_method_id==1) ปญ. @elseif($data->shipping_method_id==2) Laramove @endif<br>


                                                </td>
                                                <td align="center">
                                                    {{--<p style="font-size: 12px; font-weight: bold;">--}}
                                                    {{--ZIPCODE<br>--}}
                                                    {{--10500--}}
                                                    {{--</p>--}}
                                                </td>
                                                <td align="center">
                                                    <p style="font-size: 12px; font-weight: bold;">
                                                    Service Type<br>
                                                        @if(!empty($data->paysbuy_method))
                                                            {{ $paysbuy[$data->paysbuy_method] }}
                                                        @endif
                                                    </p>
                                                </td>
                                            </tr>
                                            </tbody>

                                        </table>
                                    </div>

                                    <div class="row">

                                        <table class="table table-bordered table-hover">
                                            <tbody>
                                            <tr>
                                            <th>Billing information:</th>
                                            <th>Shipping information:</th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    {!! $data->billing_address !!}
                                                    <br><br> <strong>Tax id:</strong> {{$data->tax_id}}
                                                </td>
                                                <td>{!! $data->shipping_address !!}</td>
                                            </tr>
                                            </tbody>

                                        </table>
                                    </div>

                                    <div class="row">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                       <td class="bold">
                                                            #
                                                        </td>
                                                        <td class="bold">
                                                            ID
                                                        </td>
                                                        <td class="bold">
                                                            Image
                                                        </td>
                                                        <td class="bold">
                                                            Product
                                                        </td>
                                                        <td class="text-center bold">
                                                            Size
                                                        </td>
                                                        <td class="hidden-480 text-center bold">
                                                            Quantity
                                                        </td>
                                                        <td class="hidden-480 text-center bold">
                                                            Price
                                                        </td>
                                                        <td class="text-center bold">
                                                            Total(THB)
                                                        </td>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                <?php
                                                $orders_detail = DB::Table('orders_detail')
                                                        ->where('orders_id',$data->orders_id)
                                                        ->get();

                                                $total = 0;
                                                ?>

                                                @foreach($orders_detail as $value)

                                                <tr>
                                                    <td>
                                                        {{ $value->orders_detail_id }}
                                                    </td>
                                                    <td>
                                                        {{ $value->model}}
                                                    </td>
                                                    <td>
                                                        <?php
                                                            $photo = DB::table('product_gallery')
                                                                    ->where('product_id',$value->product_id)
                                                                    ->orderBy('product_id','desc')
                                                                    ->select('photo')
                                                                    ->first();

                                                            if ($photo->photo != '' && file_exists('uploads/product/100/'.$photo->photo)){
                                                                $img_url = URL::asset('uploads/product/100/' . $photo->photo);
                                                            }else{
                                                                $img_url = '';
                                                            }
                                                        ?>

                                                        <img src="{{ $img_url }}" class="img-responsive" />
                                                    </td>
                                                    <td>
                                                        {{ $value->product_name_th }} {{ $value->product_name_en }}
                                                    </td>
                                                    <td class="text-center">
                                                        @foreach($product_option as $option)
                                                            @if($option->product_option_id == $value->product_option_id)
                                                                {{ $option->option_name_th }}
                                                            @endif
                                                        @endforeach
                                                    </td>
                                                    <td class="hidden-480 text-center">
                                                        {{ number_format($value->qty) }}
                                                    </td>
                                                    <td class="hidden-480 text-right">
                                                        {{ number_format($value->unit_price,2) }}
                                                    </td>


                                                    <?php

                                                        $total_price = $value->qty*$value->unit_price;
                                                        $total += $total_price;

                                                        $before_vat = $total/1.07;

                                                        $vat = $total-$before_vat;


                                                    ?>

                                                    <td class="hidden-480 text-right" >
                                                        {{ number_format($total_price,2) }}

                                                    </td>
                                                </tr>

                                                @endforeach


                                                <tr>
                                                    <td colspan="7" class="text-right">
                                                        <strong>มูลค่าสินค้ารวมภาษีมูลค่าเพิ่ม / Sub - Total amount</strong> <br>
                                                        <strong>ส่วนลด / Discount</strong> <br>
                                                        <strong>มูลค่ารวมของสินค้า(ก่อนภาษีมูลค่าเพิ่ม) / Total amount(Before VAT)</strong> <br>
                                                        <strong>ภาษีมูลค่าเพิ่ม 7% / 7% VAT</strong> <br>
                                                        <strong>ค่าจัดส่ง</strong> <br>
                                                    </td>
                                                    <td class="text-right">
                                                        {{ number_format($total,2) }}<br>
                                                        {{ number_format($data->discount,2) }}<br>
                                                        {{ number_format($before_vat,2) }}<br>
                                                        {{ number_format($vat,2)}}<br>
                                                        {{ number_format($data->shipping_price,2)}}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="7" class="text-right">
                                                        <strong>สินค้ารวมภาษี/Total</strong> <br>

                                                    </td>
                                                    <td class="text-right">
                                                        {{--{{ number_format($total-$data->discount,2) }}--}}
                                                        {{ number_format($total+$data->shipping_price-$data->discount,2) }}
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <table>
                                                @foreach($point_transaction as $points)
                                                    @if($points->user_id == $data->user_id)
                                                        <?php 
                                                            $point = $points->point;
                                                            $total_point = $points->total; 
                                                        ?>
                                                    @endif
                                                @endforeach
                                            <tr>
                                                <td class="text-right">
                                                    <strong>คะแนนสะสม/point bill : </strong> <br>
                                                </td>
                                                <td class="text-right">
                                                    @if(!empty($point)) {{ number_format($point,2) }} @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-right">
                                                    <strong>ยอดดรวมคะแนนสะสม/ Total point :</strong> <br>

                                                </td>
                                                <td class="text-right">
                                                    @if(!empty($total_point)) {{ number_format($total_point,2) }} @endif
                                                </td>
                                            </tr>
                                        </table>
                                        </div>
                                    </div><br>

                                    <div class="row">
                                        <div class="col-xs-5" style="border-color: #7E7E7E; border: 1px solid; font-size: 11.5px ">
                                            <strong>Delivery date</strong><br>
                                            ได้รับสินค้าตามรายการทั้งหมดไว้ในสภาพเรียบร้อย และยอมรับเงื่อไขการชำระเงินทุกประการ
                                            <br><br>
                                            ____________________________________<br>
                                            <strong>ผู้รับสินค้า / Receiver <br><br> วันที่รับสินค้า / DATE ________________</strong>


                                        </div>
                                        <div class="col-xs-2 ">

                                        </div>

                                        <div class="col-xs-5 text-center" style="border-color: #7E7E7E; border: 1px solid; font-size: 11.5px ">

                                            <br>
                                            <br>
                                            <br>
                                            <br>

                                            ______________________________<br>
                                            <strong>ผู้มีอำนาจลงนาม / Authorized Signature</strong>
                                            <br>
                                            <br>

                                        </div>
                                    </div>

                                    <br>

                                    <div class="row">
                                        <div class="col-xs-12 text-center" style="border-color: #7E7E7E; border: 1px solid; font-size: 11.5px ">

                                            <br>
                                            <strong>หมายเหตุ:</strong> กรณีคุณได้รับสินค้าในสภาพไม่สมบูรณ์ ไม่ถูกต้อง ชำรุด เสียหาย ไม่ตรงตามที่ได้ทำกานสั่งซื้อ คุณสามารถยกเลิกคำสั่งซื้อและส่งคืนสินค้าเพื่อขอรับเงินคืนได้ คุณสามารถดูรายละเอียดและเงื่อนไขเพิ่มเติมได้ที่ www.yakyim.com
                                            <br>
                                            <strong>Terms and Conditions:</strong> If you received a defective, incorrect or damaged item, you may cancel the order and return the item for a full. For more information and full terms and conditions, please visit www.yakyim.com
                                            <br>
                                            <br>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        {{--break page--}}
                        <div class="page-break">&nbsp;</div>
                            <div class="portlet light">

                            <div class="portlet-body">

                                <div class="invoice">
                                    <div class="row invoice-logo">
                                        <div class="invoice-logo-space">
                                            <div class="col-xs-2">
                                                <img src="{{ URL::asset('/images/logo.jpg') }}" class="img-responsive" alt=""/>
                                            </div>
                                            <div class="col-xs-10">
                                                <p style="font-size: 10px">
                                                    บริษัท ยักษ์ ยิ้ม กรุ๊ป จำกัด เลขที่ 111/21 หมู่บ้านธัญญาภิรมย์ ตำบลรังสิต อำเภอธัญบุรี จังหวัดปทุมธานี 12110<br>
                                                    Yakyim Group Ltd 111/21 Thanyaphirom Village Rangsit Thnayaburi Pathumthani<br>
                                                    Tel. 02-191-3955, 084-657-1001   http://www.yakyim.com<br>
                                                    เลขประจำตัวผู้เสียภาษี 0135558001720
                                                    {{--#{{ $data->orders_id }} / {{ $mainFn->format_date_en($data->orders_date,4) }}--}}
                                                </p>

                                            </div>

                                        </div>
                                    </div>
                                    <hr/>

                                    {{--dd--}}
                                    <div class="row">
                                        <div class="col-xs-8">

                                        </div>

                                        <div class="col-xs-4 text-center invoice-payment" style="border-color: #7E7E7E; border: 1px solid ">
                                            <h7>
                                                ใบเสร็จรับเงิน / ใบกำกับภาษี<br>
                                                RECEIPT / TAX INVOICE <br>
                                                สำเนา
                                            </h7>
                                        </div>


                                    </div>

                                    <br>
                                    {{----}}

                                    <div class="row">
                                        <div class="col-xs-4">
                                            <strong>Customer Details</strong>
                                            <ul class="list-unstyled">
                                                @foreach($user as $value)
                                                    <li>
                                                        <strong>ชื่อ / Name: </strong>{{ $value->firstname }} {{ $value->lastname }}
                                                    </li>

                                                    <li>
                                                        <strong>เบอร์ติดต่อ / Telephone: </strong>{{ $value->mobile }}
                                                    </li>

                                                @endforeach
                                            </ul>
                                        </div>
                                        <div class="col-xs-3">
                                            {{--<h3>About:</h3>--}}
                                            {{--<ul class="list-unstyled">--}}
                                            {{--<li>--}}
                                            {{--Drem psum dolor sit amet--}}
                                            {{--</li>--}}

                                            {{--</ul>--}}
                                        </div>
                                        <div class="col-xs-5 invoice-payment">
                                            <strong>Order Details</strong>
                                            <ul class="list-unstyled">
                                                <li>
                                                    <strong>วันที่ / Date: </strong>{{ $mainFn->format_date_en($data->orders_date,2) }}
                                                </li>
                                                <li>
                                                    <strong>เลขที่สั่งซื้อ / Order CODE: </strong>{{ $data->orders_id }}
                                                </li>
                                                <li>
                                                    <strong>วิธีการชำระเงิน / Payment Method: </strong>{{ $data->payment_by }}
                                                </li>
                                            </ul>
                                        </div>
                                    </div>

                                    <br>

                                    <div class="row">

                                        <style>

                                            td{
                                                font-size : 11.5px;
                                            }

                                        </style>

                                        <table class="table" border="1">

                                            <thead>
                                            <tr>
                                                <td class="bold">
                                                    Shipping Information:
                                                </td>
                                                <td class="bold">
                                                    Billing Information:
                                                </td>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            <td class="col-xs-6">
                                                {!! $data->shipping_address !!}
                                            </td>

                                            <td class="col-xs-6">
                                                {!! $data->billing_address !!}
                                                <br><br> <strong>Tax id:</strong> {{$data->tax_id}}
                                            </td>
                                            </tbody>

                                        </table>

                                    </div>

                                    <div class="row">
                                        <div class="col-xs-12">
                                            <table class="table table-striped table-hover">
                                                <thead>
                                                <tr>
                                                    <td class="bold">
                                                        #
                                                    </td>
                                                    <td class="bold">
                                                        ID
                                                    </td>
                                                    <td class="bold">
                                                        Image
                                                    </td>
                                                    <td class="bold">
                                                        Product
                                                    </td>
                                                    <td class="text-center bold">
                                                        Size
                                                    </td>
                                                    <td class="hidden-480 text-center bold">
                                                        Quantity
                                                    </td>
                                                    <td class="hidden-480 text-center bold">
                                                        Price
                                                    </td>
                                                    <td class="text-center bold">
                                                        Total(THB)
                                                    </td>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                <?php
                                                $orders_detail = DB::Table('orders_detail')
                                                        ->where('orders_id',$data->orders_id)
                                                        ->get();

                                                $total = 0;
                                                ?>

                                                @foreach($orders_detail as $value)
                                                    <tr>
                                                        <td>
                                                            {{ $value->orders_detail_id }}
                                                        </td>
                                                        <td>
                                                            {{ $value->model}}
                                                        </td>
                                                        <td>
                                                            <?php
                                                                $photo = DB::table('product_gallery')
                                                                        ->where('product_id',$value->product_id)
                                                                        ->orderBy('product_id','desc')
                                                                        ->select('photo')
                                                                        ->first();

                                                                if ($photo->photo != '' && file_exists('uploads/product/100/'.$photo->photo)){
                                                                    $img_url = URL::asset('uploads/product/100/' . $photo->photo);
                                                                }else{
                                                                    $img_url = '';
                                                                }
                                                            ?>

                                                            <img src="{{ $img_url }}" class="img-responsive" />
                                                        </td>
                                                        <td>
                                                            {{ $value->product_name_th }} {{ $value->product_name_en }}
                                                        </td>
                                                        <td class="text-center">
                                                            @foreach($product_option as $option)
                                                                @if($option->product_option_id == $value->product_option_id)
                                                                    {{ $option->option_name_th }}
                                                                @endif
                                                            @endforeach
                                                        </td>
                                                        <td class="hidden-480 text-center">
                                                            {{ number_format($value->qty) }}
                                                        </td>
                                                        <td class="hidden-480 text-right">
                                                            {{ number_format($value->unit_price,2) }}
                                                        </td>
                                                        <?php

                                                            $total_price = $value->qty*$value->unit_price;
                                                            $total += $total_price;

                                                            $before_vat = $total/1.07;

                                                            $vat = $total-$before_vat;
                                                        ?>
                                                        <td class="hidden-480 text-right" >
                                                            {{ number_format($total_price,2) }}
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                <tr>
                                                    <td colspan="7" class="text-right">
                                                        <strong>มูลค่าสินค้ารวมภาษีมูลค่าเพิ่ม / Sub - Total amount</strong> <br>
                                                        <strong>ส่วนลด / Discount</strong> <br>
                                                        <strong>มูลค่ารวมของสินค้า(ก่อนภาษีมูลค่าเพิ่ม) / Total amount(Before VAT)</strong> <br>
                                                        <strong>ภาษีมูลค่าเพิ่ม 7% / 7% VAT</strong> <br>
                                                        <strong>ค่าจัดส่ง</strong> <br>
                                                    </td>
                                                    <td class="text-right">
                                                        {{ number_format($total_price,2) }}<br>
                                                        {{ number_format($data->discount,2) }}<br>
                                                        {{ number_format($before_vat,2) }}<br>
                                                        {{ number_format($vat,2)}}<br>
                                                        {{ number_format($data->shipping_price,2)}}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="7" class="text-right">
                                                        <strong>สินค้ารวมภาษี/Total</strong> <br>
                                                    </td>
                                                    <td class="text-right">
                                                        {{--{{ number_format($total-$data->discount,2) }}--}}
                                                        {{ number_format($total+$data->shipping_price-$data->discount,2) }}
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <table>
                                                @foreach($point_transaction as $points)
                                                    @if($points->user_id == $data->user_id)
                                                        <?php 
                                                            $point = $points->point;
                                                            $total_point = $points->total; 
                                                        ?>
                                                    @endif
                                                @endforeach
                                            <tr>
                                                <td class="text-right">
                                                    <strong>คะแนนสะสม/point bill : </strong> <br>
                                                </td>
                                                <td class="text-right">
                                                    @if(!empty($point)) {{ number_format($point,2) }} @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-right">
                                                    <strong>ยอดดรวมคะแนนสะสม/ Total point :</strong> <br>

                                                </td>
                                                <td class="text-right">
                                                    @if(!empty($total_point)) {{ number_format($total_point,2) }} @endif
                                                </td>
                                            </tr>
                                        </table>
                                        </div>
                                    </div><br>

                                    <div class="row">
                                        <div class="col-xs-5" style="border-color: #7E7E7E; border: 1px solid; font-size: 11.5px ">
                                            <strong>Delivery date</strong><br>
                                            ได้รับสินค้าตามรายการทั้งหมดไว้ในสภาพเรียบร้อย และยอมรับเงื่อไขการชำระเงินทุกประการ
                                            <br><br>
                                            ____________________________________<br>
                                            <strong>ผู้รับสินค้า / Receiver <br><br> วันที่รับสินค้า / DATE ________________</strong>


                                        </div>
                                        <div class="col-xs-2 ">

                                        </div>

                                        <div class="col-xs-5 text-center" style="border-color: #7E7E7E; border: 1px solid; font-size: 11.5px ">

                                            <br>
                                            <br>
                                            <br>
                                            <br>

                                            ______________________________<br>
                                            <strong>ผู้มีอำนาจลงนาม / Authorized Signature</strong>
                                            <br>
                                            <br>

                                        </div>
                                    </div>

                                    <br>

                                    <div class="row">
                                        <div class="col-xs-12 text-center" style="border-color: #7E7E7E; border: 1px solid; font-size: 11.5px ">

                                            <br>
                                            <strong>หมายเหตุ:</strong> กรณีคุณได้รับสินค้าในสภาพไม่สมบูรณ์ ไม่ถูกต้อง ชำรุด เสียหาย ไม่ตรงตามที่ได้ทำกานสั่งซื้อ คุณสามารถยกเลิกคำสั่งซื้อและส่งคืนสินค้าเพื่อขอรับเงินคืนได้ คุณสามารถดูรายละเอียดและเงื่อนไขเพิ่มเติมได้ที่ www.yakyim.com
                                            <br>
                                            <strong>Terms and Conditions:</strong> If you received a defective, incorrect or damaged item, you may cancel the order and return the item for a full. For more information and full terms and conditions, please visit www.yakyim.com
                                            <br>
                                            <br>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <!-- END PAGE CONTENT INNER -->


                </div>
                <!-- END PAGE CONTENT -->
            </div>
            <!-- END PAGE CONTAINER -->

        </div>
    </div>

@endsection
