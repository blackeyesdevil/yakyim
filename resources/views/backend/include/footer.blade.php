<div class="page-footer">
    <div class="container">
        &copy; 2016 <a href="http://yakyim.com/" target="_blank">yakyim</a>, all rights reserved.
    </div>
</div>
<div class="scroll-to-top">
    <i class="icon-arrow-up"></i>
</div>