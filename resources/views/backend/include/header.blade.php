<div class="page-header">
    <!-- BEGIN HEADER TOP -->
    <div class="page-header-top">
        <div class="container">
            <!-- BEGIN LOGO -->
            <div class="page-logo">
                {{--<a href="{{URL::to('_admin')}}"><img src="{{ URL::asset('assets/admin/layout3/img/logo_bkksol.jpg') }}" alt="logo" class="logo-default"></a>--}}

                <a href="{{ URL::route('home') }}" target="_blank"><img src="{{ URL::asset('/images/logo.jpg') }}" alt="logo" class="logo-default" width="90"></a>
            </div>
            <!-- END LOGO -->
            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <a href="javascript:;" class="menu-toggler"></a>
            <!-- END RESPONSIVE MENU TOGGLER -->
            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="top-menu hidden-xs">
                <ul class="nav navbar-nav pull-right">
                    <li class="dropdown dropdown-user dropdown-dark">
                        <a href="javascript:;" >
                            <span class="username username-hide-mobile">Hello, {{auth()->guard('admin')->user()->firstname}}</span>
                        </a>
                    </li>
                    <li class="droddown dropdown-separator">
                        <span class="separator"></span>
                    </li>
                    <li class="dropdown dropdown-user dropdown-dark">
                        <a href="{{URL::to('_admin/logout')}}" >
                            <span class="username username-hide-mobile"><i class="fa fa-sign-out"></i> Logout</span>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
    </div>
    <!-- END HEADER TOP -->
    <!-- BEGIN HEADER MENU -->
    <div class="page-header-menu">
        <div class="container">

            <!-- BEGIN MEGA MENU -->
            <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
            <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
            <div class="hor-menu ">
                <ul class="nav navbar-nav">
                    {{--<li class="menu-dropdown classic-menu-dropdown">--}}
                        {{--<a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">--}}
                            {{--<i class="fa fa-bold"></i> Banner <i class="fa fa-angle-down"></i>--}}
                        {{--</a>--}}

                        {{--<ul class="dropdown-menu pull-left">--}}

                            {{--<li><a href="{{ URL::to('_admin/banner_home') }}"> Banner Home</a></li>--}}
                                {{--<li class=" dropdown-submenu">--}}
                                    {{--<a href="#"> Banner Promotion</a>--}}
                                    {{--<ul class="dropdown-menu">--}}
                                        {{--<li class=" "><a href="{{ URL::to('_admin/banner_promotion') }}"> Banner Promotion</a></li>--}}
                                        {{--<li class=" "><a href="{{ URL::to('_admin/new_page') }}"> Content Promotion</a></li>--}}
                                    {{--</ul>--}}
                                {{--</li>--}}
                            {{--<li><a href="{{ URL::to('_admin/banner_product_pants') }}"> Banner Product Pants</a></li>--}}
                            {{--<li><a href="{{ URL::to('_admin/banner_product_shirts') }}"> Banner Product Shirts</a></li>--}}
                            {{--<li><a href="{{ URL::to('_admin/banner_product_sleepware') }}"> Banner Product Sleepware</a></li>--}}
                            {{--<li><a href="{{ URL::to('_admin/banner_product_accessories') }}"> Banner Product Accessories</a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}

                    <li><a href="{{ URL::to('_admin') }}"><i class="fa fa-home"></i> Dashboard</a></li>

                    <li class="menu-dropdown classic-menu-dropdown">
                        <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
                            <i class="fa fa-file"></i> Banner<i class="fa fa-angle-down"></i>
                        </a>

                        <ul class="dropdown-menu pull-left">
                            <li class=" "><a href="{{ URL::to('_admin/banner_top/1/edit') }}"> Banner Top</a></li>
                            <li class=" "><a href="{{ URL::to('_admin/banner_promotion/2/edit') }}"> Banner Promotion</a></li>
                        </ul>
                    </li>

                    <li class="menu-dropdown classic-menu-dropdown">
                        <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
                            <i class="fa fa-shopping-cart"></i> Product <i class="fa fa-angle-down"></i>
                        </a>

                        <ul class="dropdown-menu pull-left">

                            <li><a href="{{ URL::to('_admin/category') }}"> Category</a></li>
                            <li><a href="{{ URL::to('_admin/sub_category') }}">Sub Category</a></li>

                            <li class=" dropdown-submenu">
                                <a href="#">Product</a>
                                <ul class="dropdown-menu">
                                    <li><a href="{{ URL::to('_admin/product') }}"> All Products</a></li>
                                    <li class=" "><a href="{{ URL::to('_admin/new_product') }}"> New Arrivals</a></li>
                                </ul>
                            </li>

                            {{--<li class=" dropdown-submenu">--}}
                                {{--<a href="#">Product</a>--}}
                                {{--<ul class="dropdown-menu">--}}
                                    {{--<li><a href="{{ URL::to('_admin/product') }}"> All Products</a></li>--}}
                                    {{--<li class=" "><a href="{{ URL::to('_admin/product_new') }}"> New Arrivals</a></li>--}}
                                    {{--<li class=" "><a href="{{ URL::to('_admin/product_recommend') }}"> Recommended Products</a></li>--}}
                                {{--</ul>--}}
                            {{--</li>--}}
                            <li><a href="{{ URL::to('_admin/size_chart') }}"> Size Chart</a></li>
                        </ul>
                    </li>

                    <li class="menu-dropdown classic-menu-dropdown">
                        <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
                            <i class="fa fa-reorder"></i> Orders<i class="fa fa-angle-down"></i>
                        </a>

                        <ul class="dropdown-menu pull-left">
                          <li><a href="{{ URL::to('_admin/orders') }}"> Orders</a></li>
                          <li><a href="{{ URL::to('_admin/orders_return') }}"> Refund Product</a></li>
                        </ul>
                    </li>


                    <li><a href="{{ URL::to('_admin/warehouse') }}"><i class="fa fa-h-square"></i> Warehouse</a></li>

                    <li><a href="{{ URL::to('_admin/contact_msg') }}"><i class="fa fa-envelope-o"></i> Contact </a></li>
                    <li><a href="{{ URL::to('_admin/user') }}"><i class="fa fa-user"></i> Customer</a></li>
                    <li class="menu-dropdown classic-menu-dropdown">
                        <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
                            <i class="fa fa-plus-square"></i> Others <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu pull-left">
                            <li><a href="{{ URL::to('_admin/discount_code') }}"> Discount Code</a></li>
                            <li><a href="{{ URL::to('_admin/birthday_discount') }}"> Birthday Discount</a></li>
                            <li><a href="{{ URL::to('_admin/shipping_rate/1/edit') }}"> Shipping Rate</a></li>
                            <li><a href="{{ URL::to('_admin/dollar/1/edit') }}"> Set Currency</a></li>
                            <li><a href="{{ URL::to('_admin/point_condition/1/edit') }}"> Set Point</a></li>
                            <li><a href="{{ URL::to('_admin/subscribe') }}"> Subscribe</a></li>
                        </ul>
                    </li>
                    <li class="visible-xs-block"><a href="logout"><i class="fa fa-sign-out"></i> Logout</a></li>

                </ul>
            </div>
            <!-- END MEGA MENU -->
        </div>
    </div>
    <!-- END HEADER MENU -->
</div>
