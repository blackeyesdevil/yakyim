<?php
use App\Http\Controllers\Backend\ProductOptionController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new ProductOptionController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}
$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);
?>



@extends('admin')
@section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1><a href="{{URL::to('_admin/product')}}">Product</a> / <a href="{{ URL::to('_admin/product_option?product_id='.$product_id) }}">Product Option</a> / {{ $txt_manage.' '.$titlePage }}</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD -->

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body form">
                                <form action="{{URL::to($url_to)}}" enctype="multipart/form-data" method="POST" id="form" class="form-horizontal">
                                    <input name="_method" type="hidden" value="{{$method}}">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">




                                    <div class="form-body">

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Option Name</label>
                                            <div class="col-md-4">
                                                <input type='text'  name="option_name_th" id="option_name_th" class="form-control" value="{{ $option_name_th }}" >
                                            </div>
                                            <div class="col-md-1">
                                                ( TH )
                                                </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Option Name</label>
                                            <div class="col-md-4">
                                                <input type='text'  name="option_name_en" id="option_name_en" class="form-control" value="{{ $option_name_en }}" >
                                            </div>
                                            <div class="col-md-1">
                                                ( EN )
                                            </div>
                                        </div>

                                        {{--<div class="form-group">--}}
                                            {{--<label class="control-label col-md-3">Quantity</label>--}}
                                            {{--<div class="col-md-4">--}}
                                                {{--<input type='number' name="qty" id="qty" class="form-control" value="{{ $qty }}" >--}}
                                            {{--</div>--}}
                                        {{--</div>--}}

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Model</label>
                                            <div class="col-md-4">
                                                <input type='text' name="model" id="model" class="form-control" value="{{ $model }}" >
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <input type="hidden" name="product_id" value="{{Input::get('product_id')}}">
                                                <input type="hidden" name="strParam" value="{{$strParam}}">
                                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                                <button type="reset" class="btn default">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
    <script src="{{URL::asset('js/elfinder-upload.js')}}"></script>


@endsection
@section('js')

    <script>
        function remove_img(id) {
                var r = confirm('Are you sure you want to delete');
                if(r == true){
                    $("#img_photo_"+id).removeAttr('src');
                    $("#photo_"+id).hide();
                    $("#head_photo_"+id).hide();
                }else{
                    return false;
                }
        }

    </script>
    <script src="{{URL::asset('assets/admin/scripts/product_option.js')}}"></script>
@endsection
