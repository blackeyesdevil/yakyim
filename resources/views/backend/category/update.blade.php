<?php
use App\Http\Controllers\Backend\CategoryController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new CategoryController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }

    $img_name = $data->img_name;
    $img_right_name = $data->img_right_name;
    $img_bg_name = $data->img_bg_name;
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";
    }

    $img_name = "";
    $img_right_name = "";
    $img_bg_name = "";
}

$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);
?>



@extends('admin')
@section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1><a href="{{URL::to('_admin/category')}}">Category</a> / {{ $txt_manage.' '.$titlePage }}</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD -->

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body form">
                                <form action="{{URL::to($url_to)}}" enctype="multipart/form-data" method="POST" id="form-category" class="form-horizontal">
                                    <input name="_method" type="hidden" value="{{$method}}">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">



                                    <div class="form-body">

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Category Name <span style="color:#F00">*</span></label>
                                            <div class="col-md-4">
                                                <input type='text' name="category_name_th" id="category_name_th" class="form-control" value="{{ $category_name_th }}" >
                                            </div>
                                            <div class="col-md-1">
                                                ( TH )
                                            </div>
                                        </div>

                                        @if (!empty($errors->first('category_name_th')))
                                        <div class="form-group">
                                            <label class="control-label col-md-3"></label>
                                            <div class="col-md-4" style="color:#F00">
                                                {{ $errors->first('category_name_th') }}
                                            </div>
                                        </div>
                                        @endif



                                        <div class="form-group">
                                            <label class="control-label col-md-3">Category Name <span style="color:#F00">*</span></label>
                                            <div class="col-md-4">
                                                <input type='text' name="category_name_en" id="category_name_en" class="form-control" value="{{ $category_name_en }}" >
                                            </div>
                                            <div class="col-md-1">
                                                ( EN )
                                            </div>
                                        </div>

                                        @if (!empty($errors->first('category_name_en')))
                                        <div class="form-group">
                                            <label class="control-label col-md-3"></label>
                                            <div class="col-md-4" style="color:#F00">
                                                {{ $errors->first('category_name_en') }}
                                            </div>
                                        </div>
                                        @endif

                                        <input type='hidden' name="parent_category_id" id="parent_category_id" class="form-control" value="0" >

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Home Page Title</label>
                                            <div class="col-md-4">
                                                <input type='text' name="title_home_th" id="title_home_th" class="form-control" value="{{ $title_home_th }}" >
                                            </div>
                                            <div class="col-md-1">
                                                ( TH )
                                            </div>
                                        </div>



                                        <div class="form-group">
                                            <label class="control-label col-md-3">Home Page Title</label>
                                            <div class="col-md-4">
                                                <input type='text' name="title_home_en" id="title_home_en" class="form-control" value="{{ $title_home_en }}" >
                                            </div>
                                            <div class="col-md-1">
                                                ( EN )
                                            </div>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">Title ( TH )</label>
                                            <div class="col-md-9">
                                                <textarea name="title_th" id="title_th" class="form-control ckeditor" data-error-container="#title_th_error">{{ $title_th}}</textarea>
                                                <div id="detail_error">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">Title ( EN )</label>
                                            <div class="col-md-9">
                                                <textarea name="title_en" id="title_en" class="form-control ckeditor" data-error-container="#title_en_error">{{ $title_en }}</textarea>
                                                <div id="detail_error">
                                            </div>
                                        </div>
                                    </div>

                                    <!-- START BANNER IMAGE -->
                                    <?php
                                        $field = 'img_name';
                                        $label = 'Banner (Home Page)';
                                        $upload_path = 'category';
                                    ?>
                                    <div id="banner-img">
                                        <div class="form-group">
                                            <label class="control-label col-md-3"></label>
                                            <div class="col-md-9">
                                                <div class="img_thumbnail">
                                                    @if($$field != '')
                                                        <img src="<?php echo URL::asset('uploads/' . $upload_path . '/' . $$field) ?>" class="img_path" style="max-width:200px;">
                                                    @else
                                                        <img src="" class="img_path" style="max-width:400px;">
                                                    @endif

                                                    <input type="hidden" name="{{ $field }}" class="img_hidden" value="<?php echo $$field; ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">{{ $label }}</label>
                                            <div class="col-md-4">
                                                @if($$field != '')
                                                    <div class="btn btn-danger remove_img" >Remove</div>
                                                    <div class="btn blue upload" data-file="{{ $field }}" style="display:none;"><i class="icon-picture"></i> Select File</div>
                                                @elseif ($$field == '')
                                                    <div class="btn blue upload" data-file="{{ $field }}"><i class="icon-picture"></i> Select File</div>
                                                    <div class="btn btn-danger remove_img" style="display:none;" >Remove</div>
                                                @endif

                                                <input type="hidden" name="img_del" class="img_del" value="n">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label"> </label>
                                            <div class="col-md-4">
                                                <span style="color:#F00">* หมายเหตุ</span><br>
                                                <span style="">
        							      		    - รูปควรมีขนาด 650 x 460 pixel. <br>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END BANNER IMAGE -->

                                    <!-- START RIGHT SIDE IMAGE -->
                                    <?php
                                        $field = 'img_right_name';
                                        $label = 'Category Image (Right Side)';
                                        $upload_path = 'category_right';
                                    ?>
                                    <div id="right-img">
                                        <div class="form-group">
                                            <label class="control-label col-md-3"></label>
                                            <div class="col-md-9">
                                                <div class="img_thumbnail">
                                                    @if($$field != '')
                                                        <img src="<?php echo URL::asset('uploads/' . $upload_path . '/' . $$field) ?>" class="img_path" style="max-width:200px;">
                                                    @else
                                                        <img src="" class="img_path" style="max-width:400px;">
                                                    @endif

                                                    <input type="hidden" name="{{ $field }}" class="img_hidden" value="<?php echo $$field; ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">{{ $label }}</label>
                                            <div class="col-md-4">
                                                @if($$field != '')
                                                    <div class="btn btn-danger remove_img" >Remove</div>
                                                    <div class="btn blue upload" data-file="{{ $field }}" style="display:none;"><i class="icon-picture"></i> Select File</div>
                                                @elseif ($$field == '')
                                                    <div class="btn blue upload" data-file="{{ $field }}"><i class="icon-picture"></i> Select File</div>
                                                    <div class="btn btn-danger remove_img" style="display:none;" >Remove</div>
                                                @endif

                                                <input type="hidden" name="img_right_del" class="img_del" value="n">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label"> </label>
                                            <div class="col-md-4">
                                                <span style="color:#F00">* หมายเหตุ</span><br>
                                                <span style="">
        							      		    - รูปควรมีขนาด 507 x 460 pixel. <br>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END RIGHT SIDE IMAGE -->

                                    <!-- START BACKGROUND IMAGE -->
                                    <?php
                                        $field = 'img_bg_name';
                                        $label = 'Category Background';
                                        $upload_path = 'category_bg';
                                    ?>
                                    <div id="bg-img">
                                        <div class="form-group">
                                            <label class="control-label col-md-3"></label>
                                            <div class="col-md-9">
                                                <div class="img_thumbnail">
                                                    @if($$field != '')
                                                        <img src="<?php echo URL::asset('uploads/' . $upload_path . '/' . $$field) ?>" class="img_path" style="max-width:200px;">
                                                    @else
                                                        <img src="" class="img_path" style="max-width:400px;">
                                                    @endif

                                                    <input type="hidden" name="{{ $field }}" class="img_hidden" value="<?php echo $$field; ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">{{ $label }}</label>
                                            <div class="col-md-4">
                                                @if($$field != '')
                                                    <div class="btn btn-danger remove_img" >Remove</div>
                                                    <div class="btn blue upload" data-file="{{ $field }}" style="display:none;"><i class="icon-picture"></i> Select File</div>
                                                @elseif ($$field == '')
                                                    <div class="btn blue upload" data-file="{{ $field }}"><i class="icon-picture"></i> Select File</div>
                                                    <div class="btn btn-danger remove_img" style="display:none;" >Remove</div>
                                                @endif

                                                <input type="hidden" name="img_bg_del" class="img_del" value="n">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label"> </label>
                                            <div class="col-md-4">
                                                <span style="color:#F00">* หมายเหตุ</span><br>
                                                <span style="">
        							      		    - รูปควรมีขนาด 1600 x 460 pixel. <br>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END BANNER IMAGE -->

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <input type="hidden" name="strParam" value="{{$strParam}}">
                                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                                <button type="reset" class="btn default">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>


@endsection
@section('js')
    {{ Html::script('js/elfinder-upload-new.js') }}
    {{ Html::script('assets/admin/scripts/category.js') }}
@endsection
