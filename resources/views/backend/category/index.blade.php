<?php
use App\Http\Controllers\Backend\CategoryController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;
        use App\Model\Category;

$objCon = new CategoryController(); // New Object Controller
$mainFn = new MainFunction(); // New Object Main Function
$parent_name = new Category();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;
$path = $objCon->path;
$tb_name = $objCon->tbName;

$orderBy = Input::get('orderBy');
$sortBy = Input::get('sortBy');
$search = Input::get('search');

$a_otherParam = Input::except(['orderBy','sortBy']);
$strParam = $mainFn->parameter($a_otherParam);
?>
@extends('admin')
@section('content')
    <div class="page-container">
        <div class="page-head">
            <div class="container">
                <div class="page-title">
                    <h1>{{ $titlePage }}</h1>
                </div>
            </div>
        </div>

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN SAMPLE TABLE PORTLET-->

                        <div class="portlet light">

                            <form action{{URL::to($path)}} class="form-horizontal" method="GET" >
                                <div class="form-group">
                                    <label class="control-label col-md-1">Search</label>
                                    <div class="col-md-3">
                                        <input class="form-control" type="text" name="search" value="{{ $search }}" >
                                        <span class="help-block">Search by Name</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-1 col-md-3 ">
                                        <button class="btn blue btn-sm" type="submit" ><i class="fa fa-search"></i> Search</button>
                                    </div>
                                </div>
                            </form>
                            <hr>

                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-database font-green-sharp"></i>
                                    <span class="caption-subject font-green-sharp bold">Found {{ $countData }} Record(s).</span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr >
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('ID',$pkField,$orderBy,$sortBy,$strParam) !!} </th>
                                            <th class="text-center">{!! $mainFn->sorting('Category Name (TH)','category_name_en',$orderBy,$sortBy,$strParam) !!}</th>

                                            <th class="col-md-2 text-center font-blue">Banner (Home Page)</th>
                                            <th class="text-center font-blue">Category Image (Right Side)</th>
                                            <th class="text-center font-blue">Category Background</th>
                                            <th class="col-md-2 text-center font-blue">{!! $mainFn->sorting('Sorting','sorting',$orderBy,$sortBy,$strParam) !!}</th>
                                            {{--<th class="col-md-2 text-center font-blue">More option</th>--}}

                                            <th class="col-md-1 text-center">
                                                <!-- <a href="{{ URL::to($path.'/create') }}" class="btn btn-circle blue btn-xs"><i class="fa fa-plus"></i> Add</a> -->
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @if($countData > 0)
                                            @foreach($data as $field)
                                                <tr>
                                                    <td class="text-center">{{ $field->$pkField }}</td>

                                                    <td>{!! Html::decode($field->category_name_th) !!}</td>

                                                    <td class="text-center">
                                                        @if($field->img_name !='' && file_exists('uploads/category/100/'.$field->img_name))
                                                            <img src="{{URL::asset('uploads/category/100/'.$field->img_name) }}" class="img-responsive">
                                                        @endif
                                                    </td>

                                                    <td class="text-center">
                                                        @if($field->img_right_name !='' && file_exists('uploads/category_right/100/'.$field->img_right_name))
                                                            <img src="{{URL::asset('uploads/category_right/100/'.$field->img_right_name) }}" class="img-responsive">
                                                        @endif
                                                    </td>

                                                    <td class="text-center">
                                                        @if($field->img_bg_name !='' && file_exists('uploads/category_bg/100/'.$field->img_bg_name))
                                                            <img src="{{URL::asset('uploads/category_bg/100/'.$field->img_bg_name) }}" class="img-responsive">
                                                        @endif
                                                    </td>

                                                    @if($field->parent_category_id =='0')
                                                           <td> {!! Form::open(array('url'=>'_admin/sequence/category' , 'method' => 'post' , 'id' => 'form' , 'class' => 'form-horizontal'  )) !!}

                                                          <div class="input-group">
                                                              <input type="text" class="form-control" name="new_sorting" value="{{$field->sorting}}">
                                                              <div class="input-group-btn">
                                                                  <input type="hidden" name="{{$pkField}}" value="{{$field->$pkField}}">
                                                                  <input type="hidden" name="old_sorting" value="{{$field->sorting}}">;
                                                                  <button class="btn" type="submit"><i class="glyphicon glyphicon-resize-horizontal"></i></button>
                                                              </div>
                                                          </div>
                                                          {!! Form::close() !!}
                                                      </td>
                                                    @else
                                                        <td><?php echo' '?></td>
                                                        @endif

                                                    {{--<td class="text-center">--}}
                                                        {{--<div class="btn-group">--}}
                                                            {{--<button class="btn btn-sm blue dropdown-toggle" type="button" data-toggle="dropdown">--}}
                                                                {{--More <i class="fa fa-angle-down"></i>--}}
                                                            {{--</button>--}}
                                                            {{--<ul class="dropdown-menu" role="menu">--}}
                                                                {{--<li>--}}
                                                                    {{--<a href="special_category?{{$pkField}}={{$field->$pkField}}">--}}
                                                                        {{--Special Category </a>--}}
                                                                {{--</li>--}}

                                                            {{--</ul>--}}
                                                        {{--</div>--}}
                                                    {{--</td>--}}

                                                    <td class="text-center">
                                                        <a href="{{ URL::to($path.'/'.$field->$pkField.'/edit?'.$strParam) }}" class="btn btn-xs btn-circle green"><i class="fa fa-edit"></i></a>



                                                        <form action="{{URL::to($path.'/'.$field->$pkField)}}" method="post" class="frm-delete">
                                                            <input name="_method" type="hidden" value="delete">
                                                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                                            <button type="submit" class="btn btn-xs btn-circle red" ><i class="fa  fa-trash-o"></i></button>
                                                        </form>



                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr><td colspan='8' class='text-center'>No Result.</td></tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                {!! $data->appends(Input::except('page'))->render() !!}
                            </div>
                        </div>
                        <!-- END SAMPLE TABLE PORTLET-->
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
@endsection
@section('js')
    <script src="{{URL::asset('js/change-status.js')}}"></script>
@endsection
