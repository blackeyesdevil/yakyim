<?php
use App\Http\Controllers\Backend\SizeChartDetailController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new SizeChartDetailController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }

    $img_name = $data->img_name;
    $img_right_name = $data->img_right_name;
    $img_bg_name = $data->img_bg_name;
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";
    }

    $img_name = "";
    $img_right_name = "";
    $img_bg_name = "";
}

$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);
?>



@extends('admin')
@section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1><a href="{{URL::to('_admin/size_chart')}}">Size Chart</a> / <a href="{{ URL::to('_admin/size_chart_detail?size_chart_id='. $size_chart_id) }}">Size Chart Detail</a> / {{ $txt_manage.' '.$titlePage }}</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD -->

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body form">
                                <form action="{{URL::to($url_to)}}" enctype="multipart/form-data" method="POST" id="form-category" class="form-horizontal">
                                    <input name="_method" type="hidden" value="{{$method}}">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                    <input type="hidden" name="size_chart_id" value="{{ $parent_id }}">

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Part Name <span style="color:#F00">*</span></label>
                                            <div class="col-md-4">
                                                <select name="part" class="form-control">
                                                    @foreach ($parts as $key => $item)
                                                    <option value="{{ $key }}"@if($key==$part) selected @endif>{{ $item }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        @if (!empty($errors->first('part')))
                                        <div class="form-group">
                                            <label class="control-label col-md-3"></label>
                                            <div class="col-md-4" style="color:#F00">
                                                {{ $errors->first('part') }}
                                            </div>
                                        </div>
                                        @endif

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Size Name <span style="color:#F00">*</span></label>
                                            <div class="col-md-4">
                                                <input type='text' name="size_name" id="size_name" class="form-control" value="{{ $size_name }}" >
                                            </div>
                                        </div>

                                        @if (!empty($errors->first('size_name')))
                                        <div class="form-group">
                                            <label class="control-label col-md-3"></label>
                                            <div class="col-md-4" style="color:#F00">
                                                {{ $errors->first('size_name') }}
                                            </div>
                                        </div>
                                        @endif

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Value <span style="color:#F00">*</span></label>
                                            <div class="col-md-4">
                                                <input type='text' name="value" id="value" class="form-control" value="{{ $value }}" >
                                            </div>
                                        </div>

                                        @if (!empty($errors->first('value')))
                                        <div class="form-group">
                                            <label class="control-label col-md-3"></label>
                                            <div class="col-md-4" style="color:#F00">
                                                {{ $errors->first('value') }}
                                            </div>
                                        </div>
                                        @endif
                                    </div>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <input type="hidden" name="strParam" value="{{$strParam}}">
                                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                                <button type="reset" class="btn default">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>


@endsection
