<?php
use App\Http\Controllers\Backend\OrdersReturnController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new OrdersReturnController(); // New Object Controller
$mainFn = new MainFunction(); // New Object Main Function

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;
$path = $objCon->path;

$orderBy = Input::get('orderBy');
$sortBy = Input::get('sortBy');
$search = Input::get('search');

$a_otherParam = Input::except(['orderBy','sortBy']);
$strParam = $mainFn->parameter($a_otherParam);

?>
@extends('admin')
@section('content')
    <div class="page-container">
        <div class="page-head">
            <div class="container">
                <div class="page-title">

                </div>
            </div>
        </div>

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN SAMPLE TABLE PORTLET-->

                        <div class="portlet light">

                          <div class="portlet box">
                            <div class="portlet-title">
                              <div class="caption" style="color:black;margin-bottom:20px;">
                                Order Return Detail
                              </div>
                            </div>
                            <div class="table-responsive col-sm-6">
                                <table class="table table-bordered">
                                    <tbody><tr>
                                        <th style="font-size:20;">รายละเอียดลูกค้า
                                        </th>
                                    </tr>

                                    <tr>
                                        <td>
                                          @foreach($datas as $data)
                                                <?php $img_name_slip = $data->image_name;?>
                                                <div class="row">
                                                    <div class="topic col-xs-5">ชื่อ</div>
                                                    <div class="topic col-xs-7">{{$data->firstname}} {{$data->lastname}}</div>

                                                </div>

                                                <div class="row">
                                                    <div class="topic col-xs-5">เบอร์ติดต่อ</div>
                                                    <div class="topic col-xs-7">{{$data->mobile}}</div>
                                                </div>
                                          @endforeach
                                        </td>
                                    </tr>
                                    </tbody></table>
                            </div>

                                    <div class="portlet-body">
                                      <div class="table-responsive">
                                          <table class="table table-hover table-bordered table-striped">
                                              <thead>
                                                <tr>
                                                  <th class="text-center">#</th>
                                                  <th class="text-center">Product Image</th>
                                                  <th class="text-center">Slip Image</th>
                                                  <th class="text-center">Product Name</th>
                                                  <th class="text-center">Qty</th>
                                                    <th class="text-center">Unit Price</th>
                                                  <th class="text-center">Reason</th>

                                                </tr>
                                              </thead>
                                              @foreach($data_return as $key => $value1)
                                                @foreach($order_detail as $value2)
                                                  @if($value1['product_id'] == $value2['product_id'])
                                                    <tr>
                                                      <td class="text-center">{{$key+1}}</td>
                                                      <td class="text-center">
                                                        <?php
                                                          if ($value1->img_product !='' && file_exists('uploads/product/'.$value1->img_product))
                                                              $img_url = URL::asset('uploads/product/' . $value1->img_product);
                                                          else
                                                              $img_url = URL::asset('images/logo.jpg');
                                                         ?>
                                                         <img src="{{$img_url}}" class="img-responsive" style="height:200px;">
                                                      </td>
                                                      <td class="text-center">
                                                        <?php
                                                          if ($img_name_slip !='' && file_exists('uploads/refund_product/'.$img_name_slip))
                                                              $img_url = URL::asset('uploads/refund_product/' .  $img_name_slip);
                                                          else
                                                              $img_url = URL::asset('images/logo.jpg');
                                                         ?>
                                                         <a href="{{$img_url}}" target="_blank">
                                                          <img src="{{$img_url}}" class="img-responsive" style="height:200px;">
                                                        </a>
                                                      </td>
                                                      <td class="text-center">{{$value2['product_name_th']}}</td>
                                                      <td class="text-center">{{$value1['qty']}}</td>
                                                        <td class="text-center">{{$value2['unit_price']}}</td>
                                                      <td class="text-center">{{$value1['reason']}}</td>

                                                    </tr>
                                                    @endif
                                                  @endforeach
                                                @endforeach
                                              <tbody>
                                              </tbody>
                                            </table>

                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                        <!-- END SAMPLE TABLE PORTLET-->
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
@endsection
