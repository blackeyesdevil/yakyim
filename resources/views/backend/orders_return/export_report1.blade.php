<?php
$mainFn = new MainFunction(); // New Object Main Function
$filename  ="excel_report.xls";
header("Content-Type: application/vnd.ms-excel; charset=utf-8");
header("Content-Disposition: attachment; filename=report_Product_return.xls");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private",false);
?>
<html >
<head>
    <meta charset="UTF-8">
</head>


<body>
<TABLE BORDER="1" width="100%">
    <tr>
        <th><div align="left">ID</div></th>
        <th><div align="left">Return Date</div></th>
        <th><div align="left">Product Name</div></th>
        <th><div align="center">Size</div></th>
        <th><div align="center">Price (บาท)</div></th>
        <th><div align="center">Qty</div></th>
        <th><div align="center">Reason</div></th>
    </tr>
      <?php $i = 1; ?>
    @if($countData > 0)
      @foreach($data as $value)
      <?php
        $db_return_detail = DB::table('return_product_detail')->where('orders_id', $value->orders_id)->get();
        $db_product_detail = DB::table('orders_detail')->where('orders_id', $value->orders_id)->get();
       ?>
        @foreach($db_return_detail as $re_detail)
           <?php
             $db_order_detail_id = DB::table('orders_detail')->where('orders_detail_id', $re_detail->orders_detail_id)->first();
             $size = DB::table('product_option')->where('product_option_id',$db_order_detail_id->product_option_id)->first();
             $db_product = DB::table('product')->where('product_id', $re_detail->product_id)->first();

           ?>
           <tr>
             <td colspan='1' class='text-center'>{{$i}}</td>
             <td colspan='1' class='text-center'>{{$re_detail->created_at}}</td>
             <td colspan='1' class='text-center'>{{$db_product->product_name_th}}</td>
             <td colspan='1' class='text-center'>{{$size->option_name_th}}</td>
             <td colspan='1' class='text-center'>{{$db_product->retail_price}}</td>
             <td colspan='1' class='text-center'>{{$re_detail->qty}}</td>
             <td colspan='1' class='text-center'>{{$re_detail->reason}}</td>

            </tr>

        @endforeach
        <?php $i = $i+1; ?>

      @endforeach

      {{--{{ json_encode($db_detail)}}--}}
    @else
        <tr><td colspan='7' class='text-center'>No Result.</td></tr>
    @endif
</TABLE>
</body>
</html>
