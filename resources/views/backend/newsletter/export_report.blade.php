<?php
$mainFn = new MainFunction(); // New Object Main Function

$filename  ="excel_report.xls";
header("Content-Type: application/vnd.ms-excel; charset=utf-8");
header("Content-Disposition: attachment; filename=report_printing.xls");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private",false);

?>
<html >
<head>
    <meta charset="UTF-8">
</head>


<body>
<TABLE BORDER="1" width="100%">
    <tr>
        <th><div align="left">ID</div></th>
        <th><div align="left">Email</div></th>
    </tr>
    @if($countData > 0)
        @foreach($data as $field)
            <tr>
                <td>{{ $field->newsletter_id }}</td>
                <td>{{ $field->email }}</td>
            </tr>
        @endforeach
    @else
        <tr><td colspan='6' class='text-center'>No Result.</td></tr>
    @endif
</TABLE>
</body>
</html>