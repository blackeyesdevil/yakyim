<?php
use App\Http\Controllers\Backend\ProductController;
use App\Library\MainFunction;
use App\Library\CategoryFunction;
use Illuminate\Support\Facades\Input;

$objCon = new ProductController(); // New Object Controller
$mainFn = new MainFunction(); // New Object Main Function
$CategoryFn = new CategoryFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;
$path = $objCon->path;
$tb_name = $objCon->tbName;
$category_list = "";

$orderBy = Input::get('orderBy');
$sortBy = Input::get('sortBy');
$search = Input::get('search');

$a_otherParam = Input::except(['orderBy','sortBy']);
$strParam = $mainFn->parameter($a_otherParam);
$PK = "";
?>


@extends('admin')
@section('content')
    <div class="page-container">
        <div class="page-head">
            <div class="container">
                <div class="page-title">
                    <h1>{{ $titlePage }}</h1>
                </div>
            </div>
        </div>

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN SAMPLE TABLE PORTLET-->

                        <div class="portlet light">

                            <form action{{URL::to($path)}} class="form-horizontal"  method="GET" >
                                <div class="form-group">

                                    <label class="control-label col-md-1">Search</label>
                                    <div class="col-md-3">

                                        <input class="form-control" type="text" name="search" value="{{ $search }}" placeholder="Name , Model">

                                        <input type="hidden" name="stock_status_id" value="{{Input::get('stock_status_id')}}"><br>
                                        <button class="btn blue btn-sm" type="submit" ><i class="fa fa-search"></i> Search</button>

                                    </div>

                                    <div class="col-md-3">
                                        <select name="new_product" class="form-control select2-container form-control select2me">
                                            <option  value="">Product status..</option>
                                            <option value="1">New Product</option>
                                        </select>
                                    </div>

                                    <div class="col-md-3">
                                        <select name="recommend" class="form-control select2-container form-control select2me">
                                            <option  value="" >Product recommend..</option>
                                            <option value="1" >Recommend</option>

                                        </select>
                                    </div>

                                </div>

                            </form>

                            <?php $error_msg = Session::get('error_msg'); ?>
                            @if (!empty($error_msg))
                                <div class="form-group font-red" style="margin-bottom: 0;">
                                    <label class="control-label col-md-2"></label>
                                    <div class="col-md-4">
                                        {{ $error_msg }}
                                    </div>
                                </div>
                            @endif


                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-database font-green-sharp"></i>
                                    <span class="caption-subject font-green-sharp bold">Found {{ $countData }} Record(s).</span>
                                </div>
                                <div class="text-right">
                                    <a href="{{URL::to('_admin/product?&export=true'.$strParam)}}"><button class="btn blue btn-sm" type="submit" ><i class="fa fa-file"></i> Excel Stock</button></a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                {!! Form::open(array('url' => '_admin/tag_product' , 'method' => 'post' ,'id'=>'form-tag-product', 'class' => 'form-horizontal')) !!}

                                <!-- <div class="form-group">
                                    <div class="col-md-12 text-right">
                                        {{--* New Product <span style="cursor:auto;color: #ffcc00"><i class="fa  fa-star"></i></span><br>--}}
                                        {{--* Recommend Product <span style="cursor:auto;color: green;"><i class="fa  fa-pencil"></i></span><br>--}}
                                        {{-- * Copy Product <span class="btn btn-xs btn-circle yellow" style="cursor:auto;"><i class="fa  fa-file"></i></span> --}}
                                    </div>
                                </div> -->


                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            {{--<th class="text-center"><input type="checkbox" name="selectedAll" id="selectedAll"></th>--}}
                                            <th class="text-center">{!! $mainFn->sorting('ID',$pkField,$orderBy,$sortBy,$strParam) !!} </th>
                                            <th class="col-md-2 text-center font-blue">Image</th>
                                            <th class="text-center">{!! $mainFn->sorting('Name [TH]','product_name_th',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="text-center">{!! $mainFn->sorting('Name [EN]','product_name_en',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="text-center">{!! $mainFn->sorting('Price','retail_price',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-2 text-center font-blue">Category</th>
                                            <th class="col-md-1 text-center font-blue">More option</th>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('New Product','new_product',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('Status','status',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-1 text-center">
                                                <a href="{{ URL::to($path.'/create') }}" class="btn btn-circle blue btn-xs"><i class="fa fa-plus"></i> Add</a>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @if($countData > 0)
                                            @foreach($data as $field)

                                                @if($field->$pkField != $PK)
                                                    <?php
                                                        $PK = $field->$pkField;
                                                    ?>
                                                    <tr>
                                                        {{--<td class="span1"><div class="text-center">--}}
                                                                {{--<input type="checkbox" name="product_chk[]" id="product_chk[]" class="checkbox1" value="{{ $field->$pkField }}"></div>--}}
                                                        {{--</td>--}}
                                                            <td class="text-center">{{ $field->$pkField }}
                                                                {{--@if($field->recommend == 1)--}}
                                                                    {{--<br><i style="color: green;" class="fa fa-pencil"></i>--}}
                                                                {{--@endif--}}
                                                                {{--@if($field->new_product == 1)--}}
                                                                    {{--<br><i style="color: #ffcc00" class="fa fa-star"></i>--}}
                                                                {{--@endif--}}
                                                            </td>
                                                        {!! Form::close() !!}

                                                        <td class="text-center">
                                                            @if ($field->photo !='' && file_exists('uploads/product/100/'.$field->logo))
                                                                <?php
                                                                    $img_url = URL::asset('uploads/product/100/' . $field->photo->photo);
                                                                ?>
                                                                    <img src="{{ $img_url }}" class="img-responsive" />
                                                            @endif
                                                        </td>


                                                        <td>{!! Html::decode($field->product_name_th) !!}</td>
                                                        <td>{!! Html::decode($field->product_name_en) !!}</td>
                                                        <td class="text-right">{{number_format($field->retail_price,2)}} </td>


                                                        <td> @foreach($field->categorys as $category)


                                                                {{--@if($category->parent_category_id != 0)--}}
                                                                <?php $category_list .= ', '.$CategoryFn->list_category($category->category_id); ?>
                                                                {{--@endif--}}

                                                            @endforeach
                                                            {{ substr($category_list,1) }}
                                                            <?php $category_list = ""; ?>
                                                        </td>

                                                        <td class="text-center">
                                                            <a href="product_option?{{$pkField}}={{$field->$pkField}}" class="btn btn-circle blue btn-xs"><i class="fa fa-ellipsis-h"></i> Product Option</a>
                                                            <a href="product_gallery?{{$pkField}}={{$field->$pkField}}" class="btn btn-circle yellow btn-xs"><i class="fa fa-image"></i> Product Gallery</a>

                                                        </td>
                                                         @if($field->new_product == 0)
                                                            <td><div class="text-center"><a id="new_product-{{$field->$pkField}}" class=" change-active fa fa-close text-red" data-tb_name="{{$tb_name}}" data-pk_field="{{$pkField}}" data-v_pk_field="{{$field->$pkField}}" data-change_field="new_product"  data-value="{{$field->new_product}}"></a></div></td>
                                                        @else
                                                            <td><div class="text-center"><a id="new_product-{{$field->$pkField}}" class=" change-active fa fa-check text-green" data-tb_name="{{$tb_name}}" data-pk_field="{{$pkField}}" data-v_pk_field="{{$field->$pkField}}" data-change_field="new_product"  data-value="{{$field->new_product}}"></a></div></td>
                                                        @endif


                                                        @if($field->status == 0)
                                                            <td><div class="text-center"><a id="status-{{$field->$pkField}}" class=" change-active fa fa-close text-red" data-tb_name="{{$tb_name}}" data-pk_field="{{$pkField}}" data-v_pk_field="{{$field->$pkField}}" data-change_field="status"  data-value="{{$field->status}}"></a></div></td>
                                                        @else
                                                            <td><div class="text-center"><a id="status-{{$field->$pkField}}" class=" change-active fa fa-check text-green" data-tb_name="{{$tb_name}}" data-pk_field="{{$pkField}}" data-v_pk_field="{{$field->$pkField}}" data-change_field="status"  data-value="{{$field->status}}"></a></div></td>
                                                        @endif
                                                        <td class="text-center">
                                                            <a href="{{ URL::to($path.'/'.$field->$pkField.'/edit?'.$strParam) }}" class="btn btn-xs btn-circle green"><i class="fa fa-edit"></i></a>

                                                            {{-- {!! Form::open(array('url' => '_admin/copy_product' , 'method' => 'post' ,'id'=>'form-copy-product', 'class' => 'form-horizontal')) !!} --}}
                                                            {{-- <input name="product_id" type="hidden" value="{{ $field->$pkField }}"> --}}
                                                            {{-- <button type="submit" class="btn btn-xs btn-circle yellow" ><i class="fa  fa-file"></i></button> --}}
                                                            {{-- {!! Form::close() !!} --}}

                                                            <form action="{{URL::to($path.'/'.$field->$pkField)}}" method="post" class="frm-delete">
                                                                <input name="_method" type="hidden" value="delete">
                                                                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                                                <button type="submit" class="btn btn-xs btn-circle red" ><i class="fa  fa-trash-o"></i></button>
                                                            </form>
                                                        </td>
                                                    </tr>

                                                    @endif
                                            @endforeach
                                        @else
                                            <tr><td colspan='12' class='text-center'>No Result.</td></tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>

                                {!! $data->appends(Input::except('page'))->render() !!}
                            </div>
                        </div>
                        <!-- END SAMPLE TABLE PORTLET-->
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>

    <script>
        $(document).ready(function() {
            $('#selectedAll').on('click',function(){
                if($(this).is(':checked')) {

                    $('.checkbox1').attr('checked', 'checked');
                    $('.checkbox1').parent().addClass('checked');
                }else{
                    $('.checkbox1').removeAttr('checked');
                    $('.checkbox1').parent().removeClass('checked');
                }
            });

        });

    </script>

@endsection
@section('js')
    <script src="{{URL::asset('js/change-status.js')}}"></script>

@endsection
