<?php
use App\Http\Controllers\Backend\UserController;
use App\Library\MainFunction;
$mainFn = new MainFunction(); // New Object Main Function

$filename  ="excel_report_salable1.xls";
header("Content-Type: application/vnd.ms-excel; charset=utf-8");
header("Content-Disposition: attachment; filename=excel_report_salable1.xls");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private",false);
$total = '';

?>
<html >
<head>
    <meta charset="UTF-8">
</head>
<body>
<TABLE BORDER="1" width="100%">
    <div align="center"><b> สรุปสินค้าขายไม่ได้(ไม่มีความเคลื่อนไหวภายใน 2 สัปดาห์) </b></div>
    <tr>
        <th><div align="left">ลำดับที่</div></th>
        <th><div align="left">รหัสสินค้า</div></th>
        <th><div align="left">ชื่อสินค้า</div></th>
        <th><div align="left">ขนาด</div></th>
        <th><div align="left">จำนวน</div></th>
        <th><div align="left">ต้นทุน</div></th>
        <th><div align="left">จำนวนเงิน</div></th>
    </tr>
    @if($countData > 0)
        @foreach($data as $key => $field)
            <tr>
                <td><div align="left">{{$key+1}}</td>
                <td><div align="left">{{$field->model}}</div></td>
                <td><div align="left">{{$field->product_name_th}}</div></td>
                <td><div align="left">{{$field->option_name_th}}</div></td>
                <td><div align="left">{{$field->qty}}</div></td>
                <td><div align="left"></div></td>
                <td></td>
            </tr>
        @endforeach
    @else
        <tr><td colspan='7' class='text-center'>No Result.</td></tr>
    @endif
</TABLE>
</body>
</html>
