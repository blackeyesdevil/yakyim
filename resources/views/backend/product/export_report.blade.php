<?php

$filename  ="excel_report.xls";
header("Content-Type: application/vnd.ms-excel; charset=utf-8");
header("Content-Disposition: attachment; filename=report_stock.xls");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private",false);
$p = 0;
?>
<html >
<head>
    <meta charset="UTF-8">
</head>


<body>
<TABLE BORDER="1" width="100%">
    <tr>
        <th><div align="left">ID</div></th>
        <th><div align="left">Product Name</div></th>
        <th><div align="center">Price (บาท)</div></th>
        <th><div align="center">Special Price (บาท)</div></th>
        <th><div align="center">Qty</div></th>
        <th><div align="center">Date/Time</div></th>
    </tr>
    @if($countData > 0)
        @foreach($data as $field)
            @if($p!=$field->product_id)
                <?php $p=$field->product_id;?>
                <tr>
                    <td><div align="center">{{$field->product_id}}</div></td>
                    <td>{{$field->product_name_th }} ( {{ $field->product_name_en }} ) </td>
                    <td align="center">{{ $field->retail_price }}</td>
                    <td align="center">{{ $field->special_price }}</td>
                    <td></td>
                     <td>
                <div align="right">
                    {{ $field->created_at }}
                </div>
            </td>
                <?php
                $db = DB::table('product_option')->select('option_name_th','qty','model')->whereNull('deleted_at')->where('product_id',$field->product_id)->get();
                ?>

                @foreach($db as $value)
                    <tr>
                        <td></td>
                        <td align="left" colspan="3">@if(!empty($value->option_name_th)){{ $value->option_name_th }} @else ไม่มีชื่อสินค้า @endif
                            [ {{ $value->model }} ]</td>
                        <td><div align="center">{{ $value->qty }}</div></td>
                        <td></td>
                    </tr>
                    @endforeach

                    </tr>
                    @endif
                @endforeach
            @else
                <tr><td colspan='6' class='text-center'>No Result.</td></tr>
            @endif
</TABLE>
</body>
</html>