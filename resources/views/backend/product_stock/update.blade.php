<?php
use App\Http\Controllers\Backend\ProductStockController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new ProductStockController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;
$type = Input::get('type');
//$warehouse_id = Input::get('warehouse_id');

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}
$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);

        if(!empty(input::get('product_option_id'))){
            $product_option_id = input::get('product_option_id');
        }

$db_warehouse = DB::table('warehouse')->where('warehouse_id',Input::get('warehouse_id'))->select('warehouse_name')->first();
$warehouse_name = $db_warehouse->warehouse_name;

?>

@extends('admin')
@section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1> <a href="{{URL::to('_admin/warehouse')}}">Warehouse</a> / <a href="{{URL::to('/_admin/product_stock?warehouse_id='.Input::get('warehouse_id'))}}">ProductStock</a> / @if($type==1)เพิ่ม@elseลบ@endifสินค้าใน Stock</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD -->

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body form">
                                <form action="{{URL::to($url_to)}}" enctype="multipart/form-data" method="POST" id="product_category" class="form-horizontal">
                                    <input name="_method" type="hidden" value="{{$method}}">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                                    <div class="form-body">

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Model</label>
                                            <div class="col-md-3">
                                                <select name="product_option_id" class="form-control select2-container form-control select2me" >
                                                    @foreach($ProductOption as $field)
                                                        <option value="{{$field->product_option_id}}" @if($product_option_id == $field->product_option_id) selected @endif >{{$field->model}} ({{$field->option_name_th}})</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Qty</label>
                                            <div class="col-md-1">
                                                <input type="number" name="qty" id="qty" class="form-control" value="{{ $qty }}">
                                            </div>
                                        </div>

                                        @if (!empty($errors->first('msg')))
                                            <div class="form-group">
                                                <label class="control-label col-md-3"></label>
                                                <div class="col-md-4" style="color:#F00">
                                                    {{ $errors->first('msg')}}
                                                </div>
                                            </div>
                                        @endif

                                        @if(Input::get('warehouse_id')==1)
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Cost</label>
                                            <div class="col-md-1">
                                                <input type="number" name="cost" id="cost" class="form-control" value="{{ $cost }}">
                                            </div>
                                        </div>
                                            @endif
                                    </div>

                                    <input type="hidden" name="type" id="type" class="form-control" value="{{ Input::get('type') }}">


                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <input type="hidden" name="warehouse_id" id="warehouse_id" class="form-control" value="{{ input::get('warehouse_id') }}">
                                                <input type="hidden" name="strParam" value="{{$strParam}}">
                                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                                <button type="reset" class="btn default">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>

    <script>
        $(function() {
            $( "#stock_date" ).datetimepicker();

        });
    </script>
@endsection

@section('js')
    <script src="{{URL::asset('assets/admin/scripts/product_stock.js')}}"></script>
@endsection