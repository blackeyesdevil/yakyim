<?php
use App\Http\Controllers\Backend\UserController;
use App\Library\MainFunction;
$mainFn = new MainFunction(); // New Object Main Function

$filename  ="excel_report.xls";
header("Content-Type: application/vnd.ms-excel; charset=utf-8");
header("Content-Disposition: attachment; filename=report_warehouse.xls");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private",false);
$total = '';

?>
<html >
<head>
    <meta charset="UTF-8">
</head>


<body>
    <?php  $qty=0; $cost=0; $sale_price=0; ?>
    @foreach($data as $field)
        <?php 
            $retail_price = DB::table('product')
                ->where('product_id', $field->product_id)->first()->retail_price;
            $qty += $field->qty; 
            $cost += $field->cost;
            $sale_price += $retail_price;
        ?>
    @endforeach
<TABLE BORDER="1" width="100%">
    <tr>
        <th>จำนวน</th>
        <th align="right">{{ number_format($qty) }} &nbsp; ตัว</th>
    </tr>
    <tr>
        <th>ต้นทุน</th>
        <th align="right">{{ number_format($cost) }} &nbsp;บาท</th>
    </tr>
    <tr>
        <th>ราคาขาย</th>
        <th align="right">{{ number_format($sale_price) }} &nbsp;บาท</th>
    </tr>
    <tr>
        <th><div align="left">รหัสสินค้า</div></th>
        <th><div align="left">ชื่อ</div></th>
        <th><div align="left">จำนวน</div></th>
        <th><div align="left">ต้นทุน</div></th>
        <th><div align="left">ราคาขาย</div></th>
        <th><div align="left">ราคาขาย (พิเศษ)</div></th>
    </tr>
    @if($countData > 0)
        @foreach($data as $field)
            <?php 
                $data_product = DB::table('product')
                ->where('product_id', $field->product_id)
                ->select('product_name_th','retail_price','special_price')->first();
            ?>
            <tr>
                <td>{{ $field->model }}</td>
                <td>{{ $data_product->product_name_th }}</td>
                <td>{{ number_format($field->qty) }}</td>
                <td>{{ number_format($field->cost) }}</td>
                <td>{{ number_format($data_product->retail_price) }}</td>
                <td>{{ number_format($data_product->special_price) }}</td>
            </tr>
        @endforeach
    @else
        <tr><td colspan='6' class='text-center'>No Result.</td></tr>
    @endif
</TABLE>
</body>
</html>
