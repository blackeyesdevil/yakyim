<?php
use App\Http\Controllers\Backend\ProductStockController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new ProductStockController(); // New Object Controller
$mainFn = new MainFunction(); // New Object Main Function

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;
$path = $objCon->path;
$tb_name = $objCon->tbName;
$orderBy = Input::get('orderBy');
$sortBy = Input::get('sortBy');
$search = Input::get('search');
$warehouse_id = Input::get('warehouse_id');

$a_otherParam = Input::except(['orderBy','sortBy']);
$strParam = $mainFn->parameter($a_otherParam);

$db_warehouse = DB::table('warehouse')->where('warehouse_id',$warehouse_id)->select('warehouse_name')->first();
$warehouse_name = $db_warehouse->warehouse_name;

?>
@extends('admin')
@section('content')
    <div class="page-container">
        <div class="page-head">
            <div class="container">
                <div class="page-title">
                    <h1><a href="{{URL::to('_admin/warehouse')}}">Warehouse</a> / {{ $warehouse_name }}</h1>
                </div>
            </div>
        </div>

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN SAMPLE TABLE PORTLET-->

                        <div class="portlet light">

                            <form action{{URL::to($path)}} class="form-horizontal" method="GET" >
                                <div class="form-group">
                                    <label class="control-label col-md-1">Search</label>
                                    <div class="col-md-3">
                                        <input class="form-control" type="text" name="search" value="{{ $search }}" >
                                        <span class="help-block">Search by Model</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-1 col-md-3 ">
                                        <input class="form-control" type="hidden" name="warehouse_id" value="{{ input::get('warehouse_id') }}" >
                                        <button class="btn blue btn-sm" type="submit" ><i class="fa fa-search"></i> Search</button>
                                    </div>
                                </div>
                            </form>
                            <hr>

                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-database font-green-sharp"></i>
                                    <span class="caption-subject font-green-sharp bold">Found {{ $countData }} Record(s).</span>
                                </div>
                                <div class="text-right col-md-10">

                                    <div class="form-group">
                                        <div class="col-md-2">
                                            <a href="{{ URL::to($path.'/create?type=1&warehouse_id='.$warehouse_id) }}" class="btn green btn-sm"><i class="fa fa-plus"></i> Import Products</a>
                                        </div>
                                        <div class="col-md-2">
                                            @if($warehouse_id!=1)<a href="{{ URL::to($path.'/create?type=2&warehouse_id='.$warehouse_id) }}" class="btn red btn-sm"><i class="fa fa-minus"></i> Remove Products</a>@endif
                                        </div>
                                        <div class="col-md-2">
                                            @if($warehouse_id!=1)

                                                {!! Form::open(array('url' => '_admin/postRestock', 'method' => 'post' , 'class' => 'restock')) !!}
                                                <input type="hidden" name="warehouse_id" value="{{ $warehouse_id }}">
                                                <button type="submit" class="btn blue btn-sm restock"><i class="fa  fa-rotate-left"></i> Return All Products</button>

                                                {!! Form::close() !!}
                                            @endif
                                        </div>
                                    </div>


                                </div>
                            </div>

                            <div class="text-right">
                                <a href="{{URL::to('_admin/product_stock?warehouse_id='.Input::get('warehouse_id').'&export=true'.$strParam)}}"><button class="btn blue btn-sm" type="submit" ><i class="fa fa-file"></i> Excel</button></a>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr >
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('ID',$pkField,$orderBy,$sortBy,$strParam) !!} </th>
                                            <th class="col-md-4">{!! $mainFn->sorting('Model','model',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-1  text-center">{!! $mainFn->sorting('Qty','qty',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-1  text-center">{!! $mainFn->sorting('Cost','cost',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-1"></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if($countData > 0)
                                            @foreach($data as $field)
                                                <tr>
                                                    <td class="text-center">{{ $field->$pkField }}</td>
                                                    <td>{{ $field->option_name_th }} ( {{ $field->model }} )</td>
                                                    <td class="text-center">{{ $field->qty }}</td>
                                                    <td class="text-center">{{ $field->cost }}</td>
                                                    <td class="text-center">
                                                        <a href="{{ URL::to('_admin/product_option/'.$field->product_option_id.'/edit?'.$strParam) }}" class="btn btn-xs btn-circle green"><i class="fa fa-edit"></i></a>

                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr><td colspan='7' class='text-center'>No Result.</td></tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                {!! $data->appends(Input::except('page'))->render() !!}
                            </div>
                        </div>
                        <!-- END SAMPLE TABLE PORTLET-->
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
@endsection
@section('js')

    <script>
        $(".restock").on("submit", function(){
            return confirm("Do you want to restock this ?");
        });
    </script>


@endsection
