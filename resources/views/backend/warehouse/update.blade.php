<?php
use App\Http\Controllers\Backend\WareHouseController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new WareHouseController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }

} else
{
    foreach($fieldList as $value)
    {
        $$value = "";
    }

}

$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);
?>

@extends('admin')
@section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1><a href="{{URL::to('_admin/warehouse')}}">Warehouse</a> / {{ $txt_manage.' '.$titlePage }}</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD -->

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body form">
                                <form action="{{URL::to($url_to)}}" enctype="multipart/form-data" method="POST" id="form-category" class="form-horizontal">
                                    <input name="_method" type="hidden" value="{{$method}}">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Warehouse Name <span style="color:#F00">*</span></label>
                                            <div class="col-md-4">
                                                <input type='text' name="warehouse_name" id="warehouse_name" class="form-control" value="{{ $warehouse_name }}" >
                                            </div>
                                        </div>

                                        @if (!empty($errors->first('warehouse_name')))
                                        <div class="form-group">
                                            <label class="control-label col-md-3"></label>
                                            <div class="col-md-4" style="color:#F00">
                                                {{ $errors->first('warehouse_name') }}
                                            </div>
                                        </div>
                                        @endif

                                    </div>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <input type="hidden" name="strParam" value="{{$strParam}}">
                                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                                <button type="reset" class="btn default">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
@endsection
@section('js')
    <script src="{{URL::asset('assets/admin/scripts/warehouse.js')}}"></script>
@endsection
