<?php
use App\Http\Controllers\Backend\ProductRecommendController;
use App\Library\MainFunction;
use App\Library\CategoryFunction;
use Illuminate\Support\Facades\Input;

$objCon = new ProductRecommendController(); // New Object Controller
$mainFn = new MainFunction(); // New Object Main Function
$CategoryFn = new CategoryFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;
$path = $objCon->path;
$tb_name = $objCon->tbName;
$category_list = "";

$orderBy = Input::get('orderBy');
$sortBy = Input::get('sortBy');
$search = Input::get('search');

$a_otherParam = Input::except(['orderBy','sortBy']);
$strParam = $mainFn->parameter($a_otherParam);

?>


@extends('admin')
@section('content')
    <div class="page-container">
        <div class="page-head">
            <div class="container">
                <div class="page-title">
                    <h1>{{ $titlePage }}</h1>
                </div>
            </div>
        </div>

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN SAMPLE TABLE PORTLET-->

                        <div class="portlet light">


                            <div class="portlet-body">

                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th class="text-center">{!! $mainFn->sorting('ID',$pkField,$orderBy,$sortBy,$strParam) !!} </th>
                                            <th class="col-md-2 text-center font-blue">Image</th>
                                            <th class="text-center">{!! $mainFn->sorting('Name [TH]','product_name_th',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="text-center">{!! $mainFn->sorting('Name [EN]','product_name_en',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="text-center">{!! $mainFn->sorting('Price','retail_price',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-2 text-center font-blue">Category</th>
                                            <th class="col-md-2 text-center font-blue">More Option</th>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('Status','status',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-2 text-center">{!! $mainFn->sorting('Sorting','recommend_sorting',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-1 text-center">
                                                <a href="{{ URL::to($path.'/create') }}" class="btn btn-circle blue btn-xs"><i class="fa fa-plus"></i> Add</a>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @if($countData > 0)
                                            @foreach($data as $field)
                                                <tr>
                                                    @if($field->recommend == 0)
                                                    <td class="text-center">{{ $field->$pkField }}</td>
                                                    @else
                                                    <td class="text-center">{{ $field->$pkField }}<i style="color:green;" class="fa fa-pencil"></i></td>

                                                    @endif
                                                    <td class="text-center">
                                                        <?php
                                                        if (!empty($field->photo))
                                                          $img_url = URL::asset('uploads/product_option/250/' . $field->photo->photo);
                                                        else
                                                          $img_url = URL::asset('images/no-pic.jpg');
                                                        ?>
                                                        <img src="{{ $img_url }}" class="img-responsive" />
                                                    </td>


                                                    <td>{!! Html::decode($field->product_name_th) !!}</td>
                                                    <td>{!! Html::decode($field->product_name_en) !!}</td>
                                                    <td class="text-right">{{number_format($field->retail_price,2)}} </td>


                                                    <td> @foreach($field->categorys as $category)


                                                            {{--@if($category->parent_category_id != 0)--}}
                                                                <?php $category_list .= ', '.$CategoryFn->list_category($category->category_id); ?>
                                                            {{--@endif--}}

                                                        @endforeach
                                                            {{ substr($category_list,1) }}
                                                        <?php $category_list = ""; ?>
                                                    </td>

                                                    <td class="text-center">
                                                        <a href="product_option?{{$pkField}}={{$field->$pkField}}" class="btn btn-circle blue btn-xs"><i class="fa fa-ellipsis-h"></i> Product Option</a>
                                                    </td>

                                                    @if($field->status == 0)
                                                        <td><div class="text-center"><a id="status-{{$field->$pkField}}" class=" change-active fa fa-close text-red" data-tb_name="{{$tb_name}}" data-pk_field="{{$pkField}}" data-v_pk_field="{{$field->$pkField}}" data-change_field="status"  data-value="{{$field->status}}"></a></div></td>
                                                    @else
                                                        <td><div class="text-center"><a id="status-{{$field->$pkField}}" class=" change-active fa fa-check text-green" data-tb_name="{{$tb_name}}" data-pk_field="{{$pkField}}" data-v_pk_field="{{$field->$pkField}}" data-change_field="status"  data-value="{{$field->status}}"></a></div></td>
                                                    @endif

                                                        <td>
                                                            {!! Form::open(array('url'=>'_admin/sequence/productrecommend' , 'method' => 'post' , 'id' => 'form' , 'class' => 'form-horizontal'  )) !!}

                                                            <div class="input-group">
                                                                <input type="text" class="form-control" name="new_sorting" value="{{$field->recommend_sorting}}">
                                                                <div class="input-group-btn">
                                                                    <input type="hidden" name="{{$pkField}}" value="{{$field->$pkField}}">
                                                                    <input type="hidden" name="old_sorting" value="{{$field->recommend_sorting}}">;
                                                                    <button class="btn" type="submit"><i class="glyphicon glyphicon-resize-horizontal"></i></button>
                                                                </div>
                                                            </div>
                                                            {!! Form::close() !!}
                                                        </td>
                                                    {{--<td class="text-center"><a href="{{URL::to('_admin/product_option_group?'.$pkField.'='.$field->$pkField)}}" class="btn btn-xs btn-circle yellow"><i class="fa fa-ellipsis-h"></i></a></td>--}}

                                                    <td class="text-center">
                                                        <a href="{{ URL::to($path.'/'.$field->$pkField.'/edit?'.$strParam) }}" class="btn btn-xs btn-circle green"><i class="fa fa-edit"></i></a>
                                                        <form action="{{URL::to($path.'/'.$field->$pkField)}}" method="post" class="frm-delete">
                                                            <input name="_method" type="hidden" value="delete">
                                                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                                            <button type="submit" class="btn btn-xs btn-circle red" ><i class="fa  fa-trash-o"></i></button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr><td colspan='11' class='text-center'>No Result.</td></tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>

                                {!! $data->appends(Input::except('page'))->render() !!}
                            </div>
                        </div>
                        <!-- END SAMPLE TABLE PORTLET-->
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>

@endsection
@section('js')
    <script src="{{URL::asset('js/change-status.js')}}"></script>
    <script src="{{URL::asset('assets/admin/scripts/tag-product.js')}}"></script>

@endsection
