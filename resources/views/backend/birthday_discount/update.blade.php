<?php
use App\Http\Controllers\Backend\BirthdayDiscountController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new BirthdayDiscountController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        //$$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}
$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);
?>



@extends('admin')
@section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>{{ $txt_manage.' '.$titlePage }}</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD -->

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body form">
                                <form action="{{URL::to($url_to)}}" enctype="multipart/form-data" method="POST" id="form-discount_code" class="form-horizontal">
                                    <input name="_method" type="hidden" value="{{$method}}">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                                    <div class="form-body">
                                        <label class="control-label"> <h3><b>สมาชิกปกติ</b></h3> </label>
                                        <div class="form-group">
                                            <label class="control-label col-md-3"> <b> โอนเงิน : </b></label>
                                            <div class="col-md-4">
                                                <label class="control-label"> ราคารวมสินค้า (บาท) </label>
                                                <input type='number'  name="min_price[1]" class="form-control" value="{{ $data[0]->min_price }}" min="0"/>
                                            </div>
                                            <div class="col-md-4">
                                                <label class="control-label"> ส่วนลด (%) </label>
                                                <input type='number'  name="discount[1]" class="form-control" value="{{ $data[0]->discount }}" min="0" max="100"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3"> <b> Paypal : </b></label>
                                            <div class="col-md-4">
                                                <label class="control-label"> ราคารวมสินค้า (บาท) </label>
                                                <input type='number'  name="min_price[2]" class="form-control" value="{{ $data[1]->min_price }}" min="0"/>
                                            </div>
                                            <div class="col-md-4">
                                                <label class="control-label"> ส่วนลด (%) </label>
                                                <input type='number'  name="discount[2]" class="form-control" value="{{ $data[1]->discount }}" min="0" max="100"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-body">
                                        <label class="control-label"> <h3><b> VIP </b></h3></label>
                                        <div class="form-group">
                                            <label class="control-label col-md-3"> <b> โอนเงิน : </b> </label>
                                            <div class="col-md-4">
                                                <label class="control-label"> ราคารวมสินค้า (บาท) </label>
                                                <input type='number'  name="min_price[3]" class="form-control" value="{{ $data[2]->min_price }}" min="0"/>
                                            </div>
                                            <div class="col-md-4">
                                                <label class="control-label"> ส่วนลด (%) </label>
                                                <input type='number'  name="discount[3]" class="form-control" value="{{ $data[2]->discount }}" min="0" max="100"/>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3"> <b> Paypal : </b> </label>
                                            <div class="col-md-4">
                                                <label class="control-label"> ราคารวมสินค้า (บาท) </label>
                                                <input type='number'  name="min_price[4]" id="code" class="form-control" value="{{ $data[3]->min_price }}" min="0"/>
                                            </div>
                                            <div class="col-md-4">
                                                <label class="control-label"> ส่วนลด (%) </label>
                                                <input type='number'  name="discount[4]" class="form-control" value="{{ $data[3]->discount }}" min="0" max="100"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                                <button type="reset" class="btn default">Reset</button>
                                            </div>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
    <script src="{{URL::asset('js/elfinder-upload.js')}}"></script>
    <script>
        $(function() {
            $( "#show_date,#start_date,#end_date" ).datetimepicker();

        });
    </script>

@endsection
@section('js')
    <script src="{{URL::asset('assets/admin/scripts/discount_code.js')}}"></script>
@endsection