<?php
use App\Http\Controllers\Backend\BannerPromotionController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new BannerPromotionController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }

    $img_name = $data->img_name;
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";
    }

    $img_name = "";
}

$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);
?>



@extends('admin')
@section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>{{ $txt_manage.' '.$titlePage }}</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD -->

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body form">
                                <form action="{{URL::to($url_to)}}" enctype="multipart/form-data" method="POST" id="form-category" class="form-horizontal">
                                    <input name="_method" type="hidden" value="{{$method}}">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                                    <div class="form-body">

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Active</label>
                                            <div class="col-md-4">
                                                <input type="checkbox" name="status" id="status" class="form-control" value="1" @if($status==1) checked @endif>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Title</label>
                                            <div class="col-md-4">
                                                <input type='text' name="title_th" id="title_th" class="form-control" value="{{ $title_th }}" >
                                            </div>
                                            <div class="col-md-1">
                                                ( TH )
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Title</label>
                                            <div class="col-md-4">
                                                <input type='text' name="title_en" id="title_en" class="form-control" value="{{ $title_en }}" >
                                            </div>
                                            <div class="col-md-1">
                                                ( EN )
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Detail ( TH )</label>
                                            <div class="col-md-9">
                                                <textarea name="detail_th" id="detail_th" class="form-control ckeditor" data-error-container="#detail_error">{{ $detail_th}}</textarea>
                                                <div id="detail_error">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Detail ( EN )</label>
                                            <div class="col-md-9">
                                                <textarea name="detail_en" id="detail_en" class="form-control ckeditor">{{ $detail_en}}</textarea>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="control-label col-md-3">Link</label>
                                            <div class="col-md-4">
                                                <input type='text' name="link" id="link" class="form-control" value="{{ $link }}" >
                                            </div>
                                        </div>

                                    </div>

                                    <!-- START BANNER IMAGE -->
                                    <?php
                                    $field = 'img_name';
                                    $label = 'Banner Promotion';
                                    $upload_path = 'banner_promotion';
                                    ?>
                                    <div id="banner-img">
                                        <div class="form-group">
                                            <label class="control-label col-md-3"></label>
                                            <div class="col-md-9">
                                                <div class="img_thumbnail">
                                                    @if($$field != '')
                                                        <img src="<?php echo URL::asset('uploads/' . $upload_path . '/' . $$field) ?>" class="img_path" style="max-width:200px;">
                                                    @else
                                                        <img src="" class="img_path" style="max-width:400px;">
                                                    @endif

                                                    <input type="hidden" name="{{ $field }}" class="img_hidden" value="<?php echo $$field; ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">{{ $label }}</label>
                                            <div class="col-md-4">
                                                @if($$field != '')
                                                    <div class="btn btn-danger remove_img" >Remove</div>
                                                    <div class="btn blue upload" data-file="{{ $field }}" style="display:none;"><i class="icon-picture"></i> Select File</div>
                                                @elseif ($$field == '')
                                                    <div class="btn blue upload" data-file="{{ $field }}"><i class="icon-picture"></i> Select File</div>
                                                    <div class="btn btn-danger remove_img" style="display:none;" >Remove</div>
                                                @endif

                                                <input type="hidden" name="img_del" class="img_del" value="n">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label"> </label>
                                            <div class="col-md-4">
                                                <span style="color:#F00">* หมายเหตุ</span><br>
                                                <span style="">
        							      		    - รูปควรมีขนาด 1600 x 232 pixel. <br>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END BANNER IMAGE -->


                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <input type="hidden" name="strParam" value="{{$strParam}}">
                                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                                <button type="reset" class="btn default">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>


@endsection
@section('js')
    {{ Html::script('js/elfinder-upload-new.js') }}
    {{ Html::script('assets/admin/scripts/banner_promotion.js') }}



@endsection
