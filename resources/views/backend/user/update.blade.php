<?php
use App\Http\Controllers\Backend\UserController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;
use App\Model\District;
use App\Model\Province;
use App\Model\Amphur;
$objCon = new UserController();
$mainFn = new MainFunction();

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;

$fieldList = $objCon->fieldList;
if(isset($data))
{
    foreach($fieldList as $value)
    {
        $$value = $data->$value;

    }
} else
{
    foreach($fieldList as $value)
    {
        $$value = "";

    }
}
$a_otherParam = Input::except([]);
$strParam = $mainFn->parameter($a_otherParam);
?>

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css">
<script>
    $(function() {
        $( "#datepicker" ).datepicker();

    });
</script>

@extends('admin')
@section('content')
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1><a href="{{URL::to('_admin/user')}}">User</a> / {{ $txt_manage.' '.$titlePage }}</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD -->

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body form">
                                <form action="{{URL::to($url_to)}}" enctype="multipart/form-data" method="POST" id="form-user" class="form-horizontal">
                                    <input name="_method" type="hidden" value="{{$method}}">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                                    <div class="form-body">

                                        <?php
                                        $data_point = DB::table('point_transaction')->select('total')->orderBy('point_trans_id','desc')->whereNUll('deleted_at')->where($pkField,$id)->first();
                                        ?>
                                            @if(!empty($data_point->total))
                                                <?php $total = $data_point->total; ?>
                                            @else
                                                <?php $total = 0; ?>
                                                @endif

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Point</label>
                                            <div class="col-md-4">
                                                <input type='text' class="form-control" value="{{ $total }}" readonly>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">User No.</label>
                                            <div class="col-md-4">
                                                <input type='text' name="user_no" id="user_no" class="form-control" value="{{ $data->user_no }}" readonly>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">VIP</label>
                                            <div class="col-md-4">
                                                <input type='checkbox' name="user_type" id="user_type" class="form-control" value="1" @if($user_type==1) checked @endif>
                                            </div>
                                        </div>

                                        @if($verified==1)

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Verified</label>
                                            <div class="col-md-4">
                                                 <i class="fa fa-check text-green"></i> {{ $mainFn->format_date_th($data->verified_date,4) }}
                                            </div>
                                        </div>

                                        @endif

                                            <input type='hidden' name="verified" id="verified" class="form-control" value="{{ $verified }}" readonly>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">First Name</label>
                                            <div class="col-md-4">
                                                <input type='text' name="firstname" id="firstname" class="form-control" value="{{ $firstname }}" >
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Last Name</label>
                                            <div class="col-md-4">
                                                <input type='text' name="lastname" id="lastname" class="form-control" value="{{ $lastname }}" >
                                            </div>
                                        </div>

                                        <div class="form-body">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Birth date</label>
                                                <div class="col-md-4">
                                                    <p><input type='text' name="birth_date" id="datepicker" class="form-control" value="{{ $birth_date }}" data-date-format="yyyy-mm-dd" required></p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Email</label>
                                            <div class="col-md-4">
                                                <input type='text' name="email" id="email" class="form-control" value="{{ $email }}" required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Password</label>
                                            <div class="col-md-4">
                                                <input type='password' name="password_new" id="password_new" class="form-control" value="" >
                                                <input type='hidden' name="password_old" id="password_old" class="form-control" value="{{ $password }}" >
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Address</label>
                                            <div class="col-md-4">
                                               <textarea name="address" id="address" class="form-control">{{ $address }}</textarea>
                                            </div>
                                        </div>

                                        <?php
                                            $amph = Amphur::where('amphur_id',$amphur_id)->get();
                                            $dist = District::where('district_id',$district_id)->get();
                                        ?>

                                        <div class="form-group">
                                            <label class="control-label col-md-3"></label>
                                            <div class="col-md-4">

                                                Province :
                                                <select name="province_id" class="select-province form-control select2-container form-control select2me filter-province" id="province_id" data-section="address">
                                                    <option selected="selected">--Select Province--</option>
                                                    @foreach($province as $provinces)
                                                        <option value="{{ $provinces->province_id }}"  @if($province_id == $provinces->province_id) selected @endif >{{ $provinces->province_name_th }}</option>
                                                    @endforeach

                                                </select><br/><br/>

                                                Amphur :
                                                <select name="amphur_id" class="select-amphur form-control select2-container form-control select2me filter-amphur" data-section="address" id="amphur_id">
                                                    @if($amphur_id != "0")
                                                        @foreach($amph as $a)
                                                            <option value="{{$a->amphur_id}}" @if($amphur_id==$a->amphur_id) selected @endif >{{$a->amphur_name_th}}</option>
                                                        @endforeach
                                                    @else
                                                        <option selected="selected" >--Select Amphur--</option>
                                                    @endif
                                                </select><br/><br/>

                                                District :
                                                <select name="district_id" class="select-district form-control select2-container form-control select2me filter-district" data-section="address" id="district_id">
                                                    @if($district_id != "0")
                                                        @foreach($dist as $d)
                                                            <option value="{{$d->district_id}}" @if($district_id==$d->district_id) selected @endif >{{$d->district_name_th}}</option>
                                                        @endforeach
                                                    @else
                                                        <option selected="selected" >--Select District--</option>
                                                    @endif
                                                </select><br/><br/>
                                                Zip : <input type='text' name="zipcode" id="zipcode" class="form-control filter-zipcode" data-section="address" value="{{ $zipcode }}">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Line</label>
                                            <div class="col-md-4">
                                                <input type='text' name="line" id="line" class="form-control" value="{{ $data->line }}" readonly>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Facebook</label>
                                            <div class="col-md-4">
                                                <input type='text' name="facebook" id="facebook" class="form-control" value="{{ $data->facebook }}" readonly>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Mobile</label>
                                            <div class="col-md-4">
                                                <input type='text' name="mobile" id="mobile" class="form-control" value="{{ $mobile }}" >
                                            </div>
                                        </div>

                                    </div>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <input type="hidden" name="strParam" value="{{$strParam}}">
                                                <button type="submit" class="btn green">{{ $txt_manage }}</button>
                                                <button type="reset" class="btn default">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>

@endsection
@section('js')
    <script src="{{URL::asset('assets/admin/scripts/users.js')}}"></script>

    {{ Html::script('js/front/filter-address.js') }}
    <script>
        $(document).ready(function() {
            filter_address('province','province',null,'{{ $data->province_id or '' }}','address');
            filter_address('province','amphur','{{ $data->province_id or '' }}','{{ $data->amphur_id or '' }}','address');
            filter_address('amphur','district','{{ $data->amphur_id or '' }}','{{ $data->district_id or '' }}','address');
            filter_address('district','zipcode','{{ $data->district_id or '' }}','{{ $data->zipcode or '' }}','address');
        });
    </script>

@endsection
