<?php
use App\Http\Controllers\Backend\UserController;
use App\Library\MainFunction;
use Illuminate\Support\Facades\Input;

$objCon = new UserController(); // New Object Controller
$mainFn = new MainFunction(); // New Object Main Function

$titlePage = $objCon->titlePage;
$pkField = $objCon->pkField;
$path = $objCon->path;
$tb_name = $objCon->tbName;

$orderBy = Input::get('orderBy');
$sortBy = Input::get('sortBy');
$search = Input::get('search');

$a_otherParam = Input::except(['orderBy','sortBy']);
$strParam = $mainFn->parameter($a_otherParam);

?>
@extends('admin')
@section('content')

    <div class="page-container">
        <div class="page-head">
            <div class="container">
                <div class="page-title">
                    <h1>{{ $titlePage }}</h1>
                </div>
            </div>
        </div>

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN SAMPLE TABLE PORTLET-->

                        <div class="portlet light">

                            <form action{{URL::to($path)}} class="form-horizontal" method="GET" >

                                <div class="form-group">
                                    <label class="control-label col-md-1"></label>
                                    <div class="col-md-4">
                                        <div class="input-group input-large date-picker input-daterange" data-date="<?php echo date("Y-m-d");?>" data-date-format="yyyy-mm-dd">
                                            <input type="text" class="form-control" name="from_date" value="">
												<span class="input-group-addon">
												to </span>
                                            <input type="text" class="form-control" name="to_date" value="">
                                        </div>
                                        <span class="help-block">
											Select date range </span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-1">Search</label>
                                    <div class="col-md-3">
                                        <input class="form-control" type="text" name="search" value="{{ $search }}" >
                                        <span class="help-block">Search by Firstname , Lastname , Email</span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-1"></label>                                    
                                    <div class="col-md-3">
                                        <select class="form-control" name="user_type">
                                            <option value=""> Search by User Type </option>
                                            <option value="0"> Normal </option>
                                            <option value="1"> VIP </option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-offset-1 col-md-3 ">
                                        <button class="btn blue btn-sm" type="submit" ><i class="fa fa-search"></i> Search</button>
                                    </div>
                                </div>
                            </form>
                            <hr>

                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-database font-green-sharp"></i>
                                    <span class="caption-subject font-green-sharp bold">Found {{ $countData }} Record(s).</span>
                                </div>
                                <div class="text-right">
                                   <a href="{{URL::to('_admin/user?&export=true&type=1'.$strParam)}}"><button class="btn blue btn-sm" type="submit" ><i class="fa fa-file"></i> Excel Customer</button></a>
                                   {{-- <a href="{{URL::to('_admin/user?&export=true&type=2'.$strParam)}}"><button class="btn blue btn-sm" type="submit" ><i class="fa fa-file"></i> Excel Member Top</button></a> --}}
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr >
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('ID',$pkField,$orderBy,$sortBy,$strParam) !!} </th>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('User Type','user_type',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="text-center">{!! $mainFn->sorting('Name','firstname',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-2 text-center">{!! $mainFn->sorting('Email','email',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('Mobile','mobile',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="col-md-1 text-center">{!! $mainFn->sorting('Point','point',$orderBy,$sortBy,$strParam) !!}</th>
                                            <th class="text-center font-blue">EDIT</th>
                                            <th class="col-md-2 text-center font-blue">Check Verified</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if($countData > 0)
                                            @foreach($data as $field)
                                                <tr>
                                                    <td class="text-center">{{ $field->$pkField }}</td>

                                                    @if($field->user_type == 1)
                                                        <td><div style="color:yellow" class="text-center"><span class="change-active fa fa-star"></span></div></td>
                                                    @else
                                                        <td><div class="text-center"></div></td>
                                                    @endif

                                                    <td>{!! Html::decode($field->firstname).' '.($field->lastname) !!}</td>
                                                    <td>{!! Html::decode($field->email) !!}</td>
                                                    <td>{{ $field->mobile }}</td>
                                                    <td>
                                                        <?php
                                                            $data_point = DB::table('point_transaction')->select('total')->orderBy('point_trans_id','desc')->whereNUll('deleted_at')->where($pkField,$field->$pkField)->first();
                                                        ?>
                                                        @if(!empty($data_point->total))
                                                                {{ $data_point->total }}
                                                            @endif

                                                    </td>

                                                    <td class="text-center">
                                                        <a href="{{ URL::to($path.'/'.$field->$pkField.'/edit?'.$strParam) }}" class="btn btn-xs btn-circle green"><i class="fa fa-edit"></i></a>
                                                    </td>
                                                    <td class="text-center">
                                                        @if($field->verified == 0)
                                                            <form action="{{URL::to($path.'/verified')}}" method="post">
                                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                                <input type="hidden" name="{{ $pkField }}" value="{{ $field->$pkField }}">
                                                                <button type="submit" class="btn btn-xs btn-circle red" onclick="return confirm('คุณต้องการยืนยันระบุตัวตนใช่หรือไม่')";><i class="fa fa-times-circle"></i></button>
                                                            </form>

                                                            @else

                                                            <span class="btn btn-xs btn-circle green" style="cursor:default;"><i class="fa  fa-check-circle"></i></span> <br>@if($field->verified_date!='0000-00-00 00:00:00')( {{ $mainFn->format_date_th($field->verified_date,4) }} ) @endif

                                                            @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr><td colspan='10' class='text-center'>No Result.</td></tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                {!! $data->appends(Input::except('page'))->render() !!}
                            </div>
                        </div>
                        <!-- END SAMPLE TABLE PORTLET-->
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
@endsection
@section('js')
    <script src="{{URL::asset('js/change-status.js')}}"></script>
@endsection