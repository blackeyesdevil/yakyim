<?php
use App\Http\Controllers\Backend\UserController;
use App\Library\MainFunction;
$mainFn = new MainFunction(); // New Object Main Function

$filename  ="excel_report.xls";
header("Content-Type: application/vnd.ms-excel; charset=utf-8");
header("Content-Disposition: attachment; filename=report_member_top.xls");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private",false);
$total = '';

?>
<html >
<head>
    <meta charset="UTF-8">
</head>


<body>
<TABLE BORDER="1" width="100%">
    <tr>
        <th><div align="left">ลำดับ</div></th>
        <th><div align="left">ชื่อ - นามสกุล</div></th>
        <th><div align="left">Line ID</div></th>
        <th><div align="left">จำนวน Order</div></th>
        <th><div align="left">ยอดสั่งซื้อ</div></th>
    </tr>

    @foreach($get_all_data as $key => $value)
            <tr>
                <td class="text-center">{{ $key+1 }}</td>
                <td>{{ $value->firstname }} {{ $value->lastname }}</td>
                <td>{{ $value->line }}</td>
                <td>{{ $value->count_orders }}</td>
                <td colspan="5" class="text-center">{{ number_format($value->total_price, 2) }} THB</td>
            </tr>
    @endforeach
</TABLE>
</body>
</html>
