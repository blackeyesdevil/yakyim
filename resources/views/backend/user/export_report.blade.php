<?php
use App\Http\Controllers\Backend\UserController;
use App\Library\MainFunction;
$mainFn = new MainFunction(); // New Object Main Function

$filename  ="excel_report.xls";
header("Content-Type: application/vnd.ms-excel; charset=utf-8");
header("Content-Disposition: attachment; filename=report_customer.xls");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private",false);
$total = '';

?>
<html >
<head>
    <meta charset="UTF-8">
</head>


<body>
<?php 
    $countVIP = DB::table('user')
            ->where('user_type',1)
            ->whereNull('deleted_at')
            ->count();
    $countNomal = DB::table('user')
            ->where('user_type',0)
            ->whereNull('deleted_at')
            ->count();
?>
<TABLE BORDER="1" width="100%">
    <tr>
        <th align="left">จำนวนลูกค้าทั้งหมด</th>
        <th align="left">{{ $countData }}</th>
    </tr>
    <tr>
        <th align="left">VIP (คน)</th>
        <th align="left">{{ $countVIP }}</th>
    </tr>
    <tr>
        <th align="left">ลูกค้าทั่วไป (คน)</th>
        <th align="left">{{ $countNomal }}</th>
    </tr>
    <tr>
        <th><div align="left">ID</div></th>
        <th><div align="left">Use No.</div></th>
        <th><div align="left">Type</div></th>
        <th><div align="left">Verified</div></th>
        <th><div align="left">Verified Date</div></th>
        <th><div align="left">Firstname</div></th>
        <th><div align="left">Lastname</div></th>
        <th><div align="left">Birth Date</div></th>
        <th><div align="left">Mobile</div></th>
        <th><div align="left">Email</div></th>
        <th><div align="left">Password</div></th>
        <th><div align="left">Facebook</div></th>
        <th><div align="left">Line</div></th>
        <th><div align="left">Address</div></th>
        <th><div align="left">District</div></th>
        <th><div align="left">Amphur</div></th>
        <th><div align="left">Province</div></th>
        <th><div align="left">Zipcode</div></th>
        <th><div align="left">Points</div></th>
    </tr>
    @if($countData > 0)
        @foreach($data as $field)
            <tr>
                <td>{{$field->user_id}}</td>
                <td>{{$field->user_no}}</td>
                <td>
                    @if($field->user_type=="1")
                        VIP
                    @endif
                </td>
                <td>{{$field->verified}}</td>
                <td>{{$mainFn->format_date_th($field->verified_date,4)}}</td>
                <td>{{$field->firstname}}</td>
                <td>{{$field->lastname}}</td>
                <td>{{$mainFn->format_date_th($field->birth_date,2)}}</td>
                <td>{{$field->mobile}}</td>
                <td>{{$field->email}}</td>
                <td>{{$field->password}}</td>
                <td>{{$field->facebook}}</td>
                <td>{{$field->line}}</td>
                <td>{{$field->address}}</td>

                <td>
                    @if($field->district_id != '0')
                        <?php $name = DB::table('a_district')->where('district_id',$field->district_id)->select('district_name_th')->first(); ?>
                        {{ $name->district_name_th }}
                    @endif
                </td>
                <td>
                    @if($field->amphur_id != '0')
                        <?php $name = DB::table('a_amphur')->where('amphur_id',$field->amphur_id)->select('amphur_name_th')->first(); ?>
                        {{ $name->amphur_name_th }}
                    @endif
                </td>
                <td>
                    @if($field->province_id != '0')
                        <?php $name = DB::table('a_province')->where('province_id',$field->province_id)->select('province_name_th')->first(); ?>
                        {{ $name->province_name_th }}
                    @endif
                </td>
                <td>{{$field->zipcode}}</td>
                <td>{{$field->total_point}}</td>
            </tr>
        @endforeach
    @else
        <tr><td colspan='6' class='text-center'>No Result.</td></tr>
    @endif
</TABLE>
</body>
</html>
