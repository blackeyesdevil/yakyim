<html >
<head>
    <meta charset="UTF-8">
</head>
<body>
    <tr><th colspan="6" align="center" ><b> สรุปสินค้าขายไม่ได้(ไม่มีความเคลื่อนไหวภายใน 2 สัปดาห์) </b></th></tr>
    <tr>
        <th align="center">ลำดับที่</th>
        <th align="center">รหัสสินค้า</th>
        <th align="center">ชื่อสินค้า</th>
        <th align="center">ขนาด</th>
        <th align="center">จำนวน</th>
        {{-- <th align="center">ต้นทุน</th> --}}
        {{-- <th align="center">จำนวนเงิน</th> --}}
        <th align="center">Date to Add Stock</th>
    </tr>
    <?php $i=1; ?>
    @foreach($data as $field)
        <?php
            // $cost = DB::table('product_stock')->where('product_option_id',$field->product_option_id)->select('cost')->get();
            // $count_cost = $cost->count();
            // $c_cost = 0;
            // if($count_cost > 0){
            //     $c_cost = $cost->cost;
            // }else{
            //     $c_cost = 0;
            // }
        ?>
        <tr>
            <td align="center">{{$i++}}</td>
            <td align="center">{{$field->model}}</td>
            <td>{{$field->product_name_th}}</td>
            <td align="center">{{$field->option_name_th}}</td>
            <td align="center">{{ number_format($field->qty) }}</td>
            {{-- <td align="right">{{ number_format($c_cost) }}</td> --}}
            {{-- <td align="right">{{ number_format($c_cost*$field->qty) }}</td> --}}
            <td align="center">{{$field->created_at}}</td>
        </tr>
    @endforeach
</body>
</html>
