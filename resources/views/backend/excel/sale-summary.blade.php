<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>

<tr>
    <th align="center">Date</th>
    <th align="center">Paid Order</th>
    <th align="right">Average Value/Order</th>
    <th align="right">Sale Summary</th>
</tr>
@foreach($sales as $item)
<tr>
    <td align="center">{{ $item['orders_date'] }}</td>
    <td align="center">{{ $item['paid_order'] }}</td>
    <td align="right">{{ number_format($item['average'],2) }}</td>
    <td align="right">{{ number_format($item['sale_summary'],2) }}</td>
</tr>
@endforeach

</body>