
<html >
<head>
    <meta charset="UTF-8">
</head>
<body>
    <tr>
        <th align="center">ลำดับที่</th>
        <th align="center">รหัสสินค้า</th>
        <th align="center">ชื่อสินค้า</th>
        <th align="center">ขนาด</th>
        <th align="center">จำนวน</th>
        <th align="center">ราคาขาย</th>
        <th align="center">ยอดขาย</th>
    </tr>
    @foreach($data as $key => $field)
        <tr>
            <td align="center">{{$key+1}}</td>
            <td align="center">{{$field->model}}</td>
            <td>{{$field->product_name_th}}</td>
            <td align="center">{{$field->option_name_th}}</td>
            <td align="center">{{ number_format($field->qty) }}</td>
            <td align="right">{{ number_format($field->unit_price) }}</td>
            <td align="right">{{ number_format($field->price) }}</td>
        </tr>
    @endforeach
</body>
</html>
