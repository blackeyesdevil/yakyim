<html >
<head>
    <meta charset="UTF-8">
</head>
<body>
    <tr>
        <th class="text-center">ลำดับ</th>
        <th class="text-center">ชื่อ - นามสกุล</th>
        <th class="text-center">Line ID</th>
        <th class="text-center">จำนวน Order</th>
        <th class="text-center">ยอดสั่งซื้อ</th>
    </tr>

    @foreach($data as $key => $value)
        <tr>
            <td class="text-center">{{ $key+1 }}</td>
            <td>{{ $value->firstname }} {{ $value->lastname }}</td>
            <td>{{ $value->line }}</td>
            <td>{{ $value->count_orders }}</td>
            <td align="right">{{ number_format($value->total_price, 2) }} THB</td>
        </tr>
    @endforeach
</body>
</html>
