<?php

return [

    'home' => 'หน้าแรก',
    'about-us' => 'เกี่ยวกับเรา',
    'blog' => 'บล็อก',
    'contact-us' => 'ติดต่อเรา',
    'cart' => 'ตะกร้าสินค้า',
    'checkout' => 'ดำเนินการสั่งซื้อ',
    'my-account' => 'บัญชีของฉัน',
    'policy' => 'ข้อตกลงการใช้งาน',
    'return' => 'นโยบายการเปลี่ยนสินค้า / คืนสินค้า',
    'delivery' => 'การจัดส่งสินค้า',
    'promotion' => 'โปรโมชั่น',

    'read-more' => 'อ่านต่อ',
    'post-on' => 'โพสต์เมื่อ',
    'prev' => 'ก่อนหน้า',
    'next' => 'ถัดไป',

    'related-products' => 'สินค้าที่เกี่ยวข้อง',
    'related-products-slogan' => 'เราขอแนะนำสินค้าตามไลฟ์สไตล์ของคุณดังนี้',

    'search-result' => 'ผลการค้นหาสำหรับ ',
    'product' => 'สินค้า',

    'company-profile' => 'ประวัติยักษ์ยิ้ม',
    'services' => 'บริการของเรา',
    'tracking-order' => 'ติดตามการสั่งซื้อ',
    'how-to' => 'คู่มือการใช้งาน',
    'member-register' => 'วิธีการสมัครสมาชิก',
    'payment' => 'วิธีการชำระเงิน',
    'status-check' => 'ตรวจสอบสถานะ',
    'sizing-chart' => 'ตารางขนาดสินค้า',
    'change-product' => 'เปลี่ยน/คืนสินค้า',
    'discount' => 'การใช้ส่วนลด',
    'shipping' => 'การขนส่ง',
    'change-address' => 'แก้ไขที่อยู่',

    'newsletter' => 'จดหมายข่าว',
    'delivery-services' => 'บริการจัดส่ง',
    'payment' => 'การชำระเงิน',

    'size-chart-size' => 'ขนาด (นิ้ว)',

    'th-font' => ' th',
];
