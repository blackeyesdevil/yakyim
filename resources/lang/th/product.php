<?php

return [
    'home' => 'หน้าหลัก',
    'our-products' => 'สินค้าของเรา',
    'our-products-slogan' => 'เราพร้อมมอบสินค้าที่ดีที่สุดเพื่อไลฟ์สไตล์ของคุณ',
    'product-category' => 'หมวดหมู่สินค้า',
    'product-categories' => 'หมวดหมู่สินค้า',
    'best-seller' => 'สินค้ายอดนิยม',
    'grid' => 'ตาราง',
    'list' => 'รายการ',
    'sort-by' => 'เรียงตาม',
    'sort-by-newest' => 'สินค้าล่าสุด',
    'sort-by-name' => 'ชื่อสินค้า',
    'sort-by-price' => 'ราคา',
    'add-to-cart' => 'เพิ่มลงตะกร้า',
    'color' => 'สี',
    'qty' => 'จำนวน',
    'no-products' => 'ไม่พบสินค้าในหมวดหมู่นี้',

    'description' => 'คำอธิบาย',
    'reviews' => 'รีวิว',
    'tags' => 'แท็ก',
    'no-tags' => 'สินค้าชนิดนี้ยังไม่มีแท็ก',

    'recommended' => 'สินค้าแนะนำ',
    'recommended-slogan' => 'สินค้าอื่นๆ ที่น่าสนใจ',

    'view-detail' => 'รายละเอียดสินค้า',

    // Cart
    'shopping-cart' => 'ตะกร้าสินค้า',
    'product-name' => 'ชื่อสินค้า',
    'price' => 'ราคา',
    'quantity' => 'จำนวน',
    'sub-total' => 'ราคารวม',
    'remove' => 'ลบสินค้า',
    'update-qty' => 'อัพเดตจำนวน',
    'total-price' => 'ราคารวม',
    'discount-price' => 'ราคาลด',
    'shipping-price' => 'ค่าจัดส่ง',
    'grand-total' => 'ยอดสุทธิ',
    'no-item-in-cart' => 'ยังไม่มีสินค้าในตะกร้า',
    'continue-shopping' => 'กลับไปเลือกสินค้า',
    'proceed-to-checkout' => 'ดำเนินการชำระเงิน',
    'currency-sign' => '฿',

    // PRODUCT=>
    'shirts-dropdown-category'=> 'ประเภท',
    'shirts-dropdown-size'=> 'ขนาด',
    'size-chart' => 'ตารางขนาดสินค้า',
    'complete-your-outfit' => 'สินค้าอื่นๆ',

    'sold-out' => 'สินค้าหมด'
];
