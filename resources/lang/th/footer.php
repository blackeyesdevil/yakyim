<?php

return [

    'subscribe-text' => 'ลงทะเบียนรับข่าวสารวันนี้เพื่อรับสิทธิประโยชน์พิเศษมากมาย',
    'subscribe-placeholder' => 'อีเมลของคุณ',
    'subscribe-btn' => 'ลงทะเบียน',

    'about-us' => 'เกี่ยวกับเรา',
    'corporate-info' => 'ประวัติความเป็นมา',
    'privacy-policy' => 'นโยบายความเป็นส่วนตัว',
    'contact-us' => 'ติดต่อเรา',
    'sitemap' => 'แผนผังเว็บไซต์',

    'faq' => 'ความช่วยเหลือ',
    'how-to-order' => 'วิธีการสั่งซื้อ',
    'payment-method' => 'ช่องทางการชำระเงิน',
    'delivery' => 'การส่งสินค้า',
    'return-policy' => 'นโยบายการเปลี่ยน/คืนสินค้า',

    'services' => 'บริการของเรา',
    'order-tracking' => 'ติดตามการสั่งซื้อสินค้า',

    'payment' => 'การชำระเงิน',
    'delivery-services' => 'การส่งสินค้า',

];
