<?php

return [

    'member-login' => 'สมาชิก',
    'checkout-as-guest' => 'ดำเนินการสั่งซื้อโดยไม่สมัครสมาชิก',
    'checkout-as-member' => 'เข้าสู่ระบบหรือสมัครสมาชิก เพื่อรับสิทธิประโยชน์พิเศษ',
    'checkout-as-member-2' => 'ดำเนินการสั่งซื้อโดยใช้ชื่อผู้ซื้อ ',

    'billing_information' => 'รายละเอียดใบวางบิล',
    'billing_firstname' => 'ชื่อ',
    'billing_lastname' => 'นามสกุล',
    'billing_address' => 'ที่อยู่',
    'billing_zipcode' => 'รหัสไปรษณีย์',
    'billing_district' => 'แขวง / ตำบล',
    'billing_amphur' => 'เขต / อำเภอ',
    'billing_province' => 'จังหวัด',
    'billing_tel' => 'เบอร์โทรศัพท์',
    'billing_tax' => 'หมายเลขประจำตัวผู้เสียภาษีฯ',
    'billing_same' => 'ใช้ที่อยู่ใบวางบิลเดียวกับที่อยู่จัดส่ง',
    'billing_new' => 'สร้างที่อยู่ใบวางบิลใหม่',
    'billing_edit' => 'แก้ไขใบวางบิล',

    'shipping_information' => 'รายละเอียดการจัดส่ง',
    'shipping_old' => 'ใช้ที่อยู่จัดส่งที่ฉันเคยใช้ก่อนหน้านี้',
    'shipping_yourself' => 'ลูกค้ามารับสินค้าด้วยตัวเอง',
    'shipping_new' => 'สร้างที่อยู่จัดส่งใหม่',
    'shipping_firstname' => 'ชื่อ',
    'shipping_lastname' => 'นามสกุล',
    'shipping-address' => 'ที่อยู่',
    'shipping_zipcode' => 'รหัสไปรษณีย์',
    'shipping_district' => 'แขวง / ตำบล',
    'shipping_amphur' => 'เขต / อำเภอ',
    'shipping_province' => 'จังหวัด',
    'shipping_tel' => 'เบอร์โทรศัพท์',

    'payment-method' => 'การจัดส่ง & การชำระเงิน',
    'payment-paysbuy' => 'ชำระเงินผ่านระบบ Paysbuy',
    'payment-transfer' => 'ชำระเงินโดยการโอนผ่านบัญชีธนาคาร',
    'payment-cod' => 'ชำระเงินปลายทางทั่วประเทศ',

    'shipping-1' => 'ไปรษณีย์ไทย',
    'shipping-2' => 'ลาล่ามูฟ',
    'shipping-express' => 'ค่าจัดส่งคำนวณตามระยะทาง เฉพาะกรุงเทพและปริมณฑล',

    'payment-1' => 'โอนเงิน',
    'payment-2' => 'เพย์พาล',

    'order-confirmation' => 'ยืนยันการสั่งซื้อ',
    'have-a-discount-code' => 'คุณมีรหัสส่วนลดแล้ว?',
    'enter-code' => 'กรอกรหัสส่วนลดที่นี่',
    'apply-code' => 'ตรวจสอบ',
    'current-code' => 'รหัสส่วนลดที่กำลังใช้งาน',
    'remove' => 'ลบรหัส',

    'go-back' => '&laquo; ย้อนกลับ',
    'continue' => 'ดำเนินการต่อ &raquo;',
    'finish-checkout' => 'ยืนยันการสั่งซื้อ &raquo;',

    'thankyou-headline' => 'ขอบคุณที่ร่วมอุดหนุนสินค้าของเรา',
    'your-orderno-is' => 'รหัสการสั่งซื้อของคุณคือ',
    'you-will-receive-an-email' => 'คุณจะได้รับอีเมลเกี่ยวกับรายละเอียดการสั่งซื้อนี้',

    'total-price' => 'รวม',
    'discount-price' => 'ส่วนลด',
    'shipping-price' => 'ค่าจัดส่ง',
    'use-point' => 'ใช้คะแนนสะสม',
    'grand-total' => 'ทั้งหมด',

    'discount-birthday' => 'ใช้ส่วนลดวันเกิด',
    'discount-code' => 'ใช้รหัสส่วนลด',
    'discount-user-no' => 'ใช้รหัสสมาชิกของเพื่อน',

    'points' => 'คะแนน',
    'user-point' => 'คุณมีคะแนนสะสม',
    'use-point-text' => 'ใช้คะแนนสะสมชำระค่าสินค้า'

];
