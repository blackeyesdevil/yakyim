<?php
    return [
        'create' => 'สร้างบัญชีผู้ใช้',
        'firstname' => 'ชื่อ',
        'lastname' => 'นามสกุล',
        'email' => 'อีเมล',
        'password' => 'รหัสผ่าน',
        'password_min' => 'อย่างน้อย 6 ตัว',
        'birth_date' => 'วันเกิด',
        'mobile' => 'เบอร์โทรศัพท์',
        'subscribe' => 'สมัครรับข่าวสาร',
        'condition' => 'ฉันได้อ่านข้อตกลง และยอมรับ',
        'btn_create' => 'สร้างบัญชี',

        'login' => 'เข้าสู่ระบบ',
        'btn_signin' => 'เข้าสู่ระบบ',

        'forget' => 'ลืมรหัสผ่าน'
    ];
?>
