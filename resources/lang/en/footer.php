<?php

return [

    'subscribe-text' => 'Subscribe to our e-newsletter and get special promotions',
    'subscribe-placeholder' => 'Your E-mail Address',
    'subscribe-btn' => 'Subscribe',

    'hot-line' => 'HOT LINE',

    'about-us' => 'About Us',
    'corporate-info' => 'Corporate Info',
    'privacy-policy' => 'Privacy Policy',
    'contact-us' => 'Contact Us',
    'sitemap' => 'Sitemap',

    'faq' => 'FAQ',
    'how-to-order' => 'How to Order',
    'payment-method' => 'Payment Method',
    'delivery' => 'Delivery',
    'return-policy' => 'Exchange/Return Policy',

    'services' => 'Services',
    'order-tracking' => 'Order Tracking',

    'payment' => 'Payment',
    'delivery-services' => 'Delivery Services',

    'copyright' => 'Copyright &copy; 2016 Loft Bangkok. All Right Reserved.',
    'registration-number' => 'E-Commerce Registration Number',

];
