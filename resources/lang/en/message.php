<?php

return [

    'inform_invalid_total_price' => 'Invalid Total Price',
    'inform_success' => 'Successfully Informed. Your order has been sent to the next process.',

    'sign-in-first' => 'Please sign in first.',
    'confirm-email-first' => 'Please confirm your email address first.',
    'same-email' => 'This email address has already been used.',

    'register-success' => 'Please Check your email address to confirm this registration.',
    'confirm-email-success' => 'Email confirmed successfully.',

    'wishlist-added-success' => 'Item has already been added to the wishlist.',
    'wishlist-login-first' => 'Please login first',

    'cart-product-not-enough' => 'Not Enough Product',
    'cart-choose-at-least-one' => 'Please choose at least one item.',
    'cart-add-success' => 'The product has been added to the cart.',
    'cart-have-max-item' => 'You cannot exceed the maximum amount of this product',
    'cart-product-closed' => 'This product is temporarily unavailable.',
    'cart-out-of-stock' => 'This product is currently out of stock.',
    'cart-update-amount-success' => 'Amount updated successfully.',
    'cart-remove-success' => 'Product removed successfully.',

    'checkout-input-field' => 'Please input all field.',
    'checkout-select-lalamove' => 'Waiting for a response from admin.',
    'checkout-select-payment' => 'Please select payment method.',

    'discount-min-price' => 'Do not use this discount code. The orders given below.',
    'discount-max-price' => 'Do not use this discount code. The orders exceeded.',
    'discount-not-found' => 'This discount code can not be found in the system.',
    'discount-member-not-found' => 'This member number can not be found in the system.',
    'discount-for-vip' => 'The discount code is available only VIP member.',
    'discount-for-normal' => 'The discount code is available only normal member.',
    'discount-empty' => 'Discount code out',
    'discount-bank-transfer' => 'The discount code is valid only payment by bank transfer only.',
    'discount-paypal' => 'The discount code is valid only payment by PayPal only.',
    'discount-used' => 'This discount code is already taken.',
    'discount-normal-price' => 'The discount code is only valid on regular priced items only.',
    'discount-expire' => 'Discount code expires',
    'discount-correct' => 'The discount code is valid',
    'discount-me' => 'You can not use your member number',

    'discount-can-use-point' => 'You can use the reward points',
    'discount-can-not-use-point' => 'You can not use the reward points',

    'recaptcha-req' => 'Please verify that you are not an automated program.',
    'check-email-to-set-password' => 'Please check email to set your password.',
    'email-not-found' => 'This email was not found.',
    'email-not-found-please-register' => 'This email was not found, please register.',
    'please-verify' => 'Please verify your email address.',
    'resend-email' => 'Resend email',
    'wrong-password' => 'The password is wrong.',
    'wrong-email-or-password' => 'The email or password is incorrect.',

    'update-success' => 'Update Successfully',
    'reset-password-success' => 'Reset Password Successfully',

    'amount-greater-equal' => 'The amount should be greater or equal to ',
    'amount-greater-stock' => 'The amount is greater than the number of this item in our stock.',
    'add-to-cart-success' => 'The item is successfully added to the cart.',
    'amount-at-least-one' => 'The amount must be greater than one.',
    'code-req' => 'The code is required.',
    'code-correct' => 'This code is correct.',
    'code-min-max-range1' => 'Total Price is',
    'code-min-max-range2' => 'to',
    'code-min-max-range3' => 'only',
    'code-unavailable' => 'This code is unavailable.',
    'code-limit' => 'This code is unavailable. This code can only be used a limited amount of times.',
    'code-expired' => 'This code is already expired.',
    'code-not-found' => 'Cannot find this code.',

    'remove-success' => 'Item removed successfully',

];
