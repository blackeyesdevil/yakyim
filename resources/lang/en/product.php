<?php

return [
    'home' => 'Home',
    'our-products' => 'Our Products',
    'our-products-slogan' => 'We deliver you the best product based on your everyday lifestyle',
    'product-category' => 'Product Category',
    'product-categories' => 'Product Categories',
    'best-seller' => 'Best Seller',
    'grid' => 'Grid',
    'list' => 'List',
    'sort-by' => 'Sort By',
    'sort-by-newest' => 'Newest',
    'sort-by-name' => 'Name',
    'sort-by-price' => 'Price',
    'add-to-cart' => 'Add to Cart',
    'color' => 'Color',
    'qty' => 'QTY',
    'no-products' => 'No products in this category',

    'description' => 'Description',
    'reviews' => 'Reviews',
    'tags' => 'Tags',
    'no-tags' => 'This product has no tags.',

    'recommended' => 'Recommended',
    'recommended-slogan' => 'Customers who bought this item also bought',

    'view-detail' => 'View Detail',

    // Cart
    'shopping-cart' => 'Shopping Cart',
    'product-name' => 'product name',
    'price' => 'Price',
    'quantity' => 'quantity',
    'sub-total' => 'subtotal',
    'remove' => 'Remove',
    'update-qty' => 'update qty',
    'total-price' => 'Total Price',
    'discount-price' => 'Discount Price',
    'shipping-price' => 'Shipping Price',
    'grand-total' => 'Grand Total',
    'no-item-in-cart' => 'There is no item in the cart',
    'continue-shopping' => 'continue shopping',
    'proceed-to-checkout' => 'proceed to checkout',
    'currency-sign' => '$',

     // PRODUCT=>shirts
    'shirts-dropdown-category'=> 'CATEGORY',
    'shirts-dropdown-size'=> 'SIZE',
    'size-chart' => 'Size Chart',
    'complete-your-outfit'=> 'Complete Your Outfit',

    'sold-out' => 'Sold Out'
];
