<?php

return [

    'home' => 'Home',
    'about-us' => 'About Us',
    'product' => 'Products',
    'blog' => 'Blog',
    'contact-us' => 'Contact Us',
    'cart' => 'Shopping Cart',
    'checkout' => 'Check Out',
    'my-account' => 'My Account',
    'policy' => 'Privacy Policy',
    'return' => 'Exchange / Return Policy',
    'delivery' => 'Delivery',
    'promotion' => 'Promotion',

    'read-more' => 'Read More',
    'post-on' => 'Post on',
    'prev' => 'Prev',
    'next' => 'Next',

    'related-products' => 'Related Products',
    'related-products-slogan' => 'These products are recommended based on your everyday lifestyle',

    'search-result' => 'Displaying results for ',
    'product' => 'Product',

    'company-profile' => 'Company Profile',
    'services' => 'Services',
    'tracking-order' => 'Tracking Order',
    'how-to' => 'How To',
    'member-register' => 'Member Register',
    'payment' => 'Payment',
    'status-check' => 'Status Checking',
    'sizing-chart' => 'Sizing Chart',
    'change-product' => 'Change/Return Product',
    'discount' => 'Discount',
    'shipping' => 'Shipping',
    'change-address' => 'Change Address',

    'newsletter' => 'Newsletter',
    'delivery-services' => 'Delivery Services',
    'payment' => 'Payment',

    'size-chart-size' => 'Size (inch)',

    'th-font' => '',
];
