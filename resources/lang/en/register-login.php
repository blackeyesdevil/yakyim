<?php
    return [
        'create' => 'Create an Account',
        'firstname' => 'FIRST NAME',
        'lastname' => 'LAST NAME',
        'email' => 'EMAIL',
        'password' => 'PASSWORD',
        'password_min' => 'minimum 6 characters',
        'birth_date' => 'DATE OF BIRTH',
        'mobile' => 'MOBILE',
        'subscribe' => 'Sign up for our newsletter!',
        'condition' => 'I agree to the',
        'btn_create' => 'CREATE',

        'login' => 'SIGN IN',
        'btn_signin' => 'SIGN IN',

        'forget' => 'Forget Password'
    ];
?>
