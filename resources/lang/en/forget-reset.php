<?php
return[
    'forget' => 'FORGET YOUR PASSWORD?',
    'detail' => 'Please enter your email address below. You will recieve a link to reset your password',
    'email' => 'EMAIL',
    'send' => 'SEND',
    'back-to-login' => 'BACK TO LOGIN',

    'reset' => 'RESET YOUR PASSWORD',
    'reset-detail' => 'Please enter your new password.',

];
?>