<?php

return [

    'member-login' => 'Member Login',
    'checkout-as-guest' => 'Check out as Guest',
    'checkout-as-member' => 'Check out as Member',
    'checkout-as-member-2' => 'Check out as Member - Logged in as ',

    'billing_information' => 'Billing Information',
    'billing_firstname' => 'First name',
    'billing_lastname' => 'Last name',
    'billing_address' => 'Address',
    'billing_zipcode' => 'ZIP Code',
    'billing_district' => 'District',
    'billing_amphur' => 'Amphur',
    'billing_province' => 'Province',
    'billing_tel' => 'Telephone',
    'billing_tax' => 'Tax ID',
    'billing_same' => 'Use the shipping address as the billing address.',
    'billing_new' => 'Create a new billing address',
    'billing_edit' => 'Edit billing address',

    'shipping_information' => 'Shipping Information',
    'shipping_old' => 'Use the shipping address I recently used',
    'shipping_yourself' => 'The customer receives an item yourself.',
    'shipping_new' => 'Create a new shipping address',
    'shipping_firstname' => 'First name',
    'shipping_lastname' => 'Last name',
    'shipping_address' => 'Address',
    'shipping_zipcode' => 'ZIP Code',
    'shipping_district' => 'District',
    'shipping_amphur' => 'Amphur',
    'shipping_province' => 'Province',
    'shipping_tel' => 'Telephone',

    'payment-method' => 'Shipping & Payment Method',
    'payment-paysbuy' => 'Paysbuy Account',
    'payment-transfer' => 'Bank Transfer',
    'payment-cod' => 'Cash on Delivery',

    'shipping-1' => 'Thailand Post',
    'shipping-2' => 'Lalamove',
    'shipping-express' => 'Express delivery within one day only Bangkok.',

    'payment-1' => 'Bank Transfer',
    'payment-2' => 'Paypal',

    'order-confirmation' => 'Order confirmation',
    'have-a-discount-code' => 'Have a discount code?',
    'enter-code' => 'Enter your code here',
    'apply-code' => 'Check',
    'current-code' => 'Your current code',
    'remove' => 'Remove',

    'go-back' => '&laquo; Back',
    'continue' => 'Continue &raquo;',
    'finish-checkout' => 'Check Out &raquo;',

    'thankyou-headline' => 'Thank you for shopping with us',
    'your-orderno-is' => 'Your order number is',
    'you-will-receive-an-email' => 'You will receive an email about this order information',

    'total-price' => 'Total',
    'discount-price' => 'Discount',
    'shipping-price' => 'Shipping',
    'use-point' => 'Use Point',
    'grand-total' => 'Grand Price',

    'discount-birthday' => 'Use birth day discount',
    'discount-code' => 'Use discount code',
    'discount-user-no' => 'Use member no',

    'points' => 'point(s)',
    'user-point' => 'Your reward points',
    'use-point-text' => 'Use point payment'
];
