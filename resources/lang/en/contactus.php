<?php
return [
    'address' => 'Address',
    'phones' => 'Phones',
    'email' => 'Email',
    'line' => 'Line ID',

    // 'company-address' => '111/21 Thanyaphirom Village<br>Rangsit Thnayaburi Pathumthani',
    'company-address' => '53/79 Rangsitcondo moo 3 Hongsakula, Khlong Nueng, Khlong Luang Pathumthani 12120',

    'tel' => 'Tel.',
    'email-label' => 'Information:',

    'contact-form' => 'Contact Us',
    'form-subject' => 'Subject heading',
    'form-name' => 'Your Name',
    'form-email' => 'Your Email',
    'form-msg' => 'Your Message',
    'form-mobile' => 'Your Mobile Number',
    'form-submit' => 'Send Message',

];
