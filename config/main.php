<?php
    return [
        'title' =>
            [
                '1' => [ 'th' => 'นาย', 'en' => 'Mr.'],
                '2' => [ 'th' => 'นาง', 'en' => 'Mrs.' ],
                '3' => [ 'th' => 'นางสาว', 'en' => 'Miss' ]
            ],
        'gender' =>
            [
                '1' => ['th' => 'หญิง', 'en' => 'Female'],
                '2' => ['th' => 'ชาย', 'en' => 'Male']
            ],
        'marital_status' =>
            [
                '1' => ['th' => 'โสด', 'en' => 'Single'],
                '2' => ['th' => 'แต่งงานแล้ว', 'en' => 'Married']
            ],
        'paysbuy_method' => ['01' => 'บัญชีเพย์สบาย','02' => 'บัตรเครดิต', '03' => 'เพล์พาล', '04' => 'อเมริกัน เอ็กซ์เพรส','05' => 'ออนไลน์ ไดเร็กเดบิต', '06' => 'เคาน์เตอร์เซอร์วิส' ],
        'bank_name' => [ 'th' => 'ธนาคารไทยพาณิชย์', 'en' => 'ธนาคารไทยพาณิชย์' ],
        'branch_name' => [ 'th' => 'สาขาสยามสแควร์', 'en' => 'สาขาสยามสแควร์' ],
        'acc_name' => [ 'th' => 'ชื่อบัญชี บริษัท สยาม สเปเชียลลิตี้ จำกัด', 'en' => 'ชื่อบัญชี บริษัท สยาม สเปเชียลลิตี้ จำกัด' ],
        'acc_no' => [ 'th' => 'เลขที่บัญชี 038-257428-8', 'en' => 'เลขที่บัญชี 038-257428-8'],


    ]
?>