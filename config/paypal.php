<?php
return array(
    // set your paypal credential
    'client_id' => 'AQsoeRpzWiRQgHkedmdgrLbDJlj6R-kVNTDUvVN470OcBFnYsfmrdHSBK6XtropJZqKI8PE3Dk9Pivh4',
    'secret' => 'EA4p3pdSsHQohLq0GOR_3-d6uXs9ESoZOKFkC1zYkkAbtX0TlVvhq3HHhNluYukkU_HPO__PZ7IA1eGd',

    /**
     * SDK configuration
     */
    'settings' => array(
        /**
         * Available option 'sandbox' or 'live'
         */
        'mode' => 'sandbox',

        /**
         * Specify the max request time in seconds
         */
        'http.ConnectionTimeOut' => 30,

        /**
         * Whether want to log to a file
         */
        'log.LogEnabled' => true,

        /**
         * Specify the file that want to write on
         */
        'log.FileName' => storage_path() . '/logs/paypal.log',

        /**
         * Available option 'FINE', 'INFO', 'WARN' or 'ERROR'
         *
         * Logging is most verbose in the 'FINE' level and decreases as you
         * proceed towards ERROR
         */
        'log.LogLevel' => 'FINE'
    ),
);

?>