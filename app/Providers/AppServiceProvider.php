<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Model\Dollar;
use App\Model\Cart;
use App\Model\Wishlist;
use App\Model\PointTransaction;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function ($view) {
            $lang = session()->get('locale');
            // Get Currency
            $dollar = Dollar::first();
            $currency = $dollar->dollar;

            // Get Cart Qty.
            $objCart = new Cart();
            $resultCart = $objCart->cart();
            $cart_qty = $resultCart['total_qty'];
            if($cart_qty <= 0) $cart_qty = '';

            // Get Wish List and Point
            $wishlist_qty = '';
            $count_point = 0;
            if(session()->has('s_user_id')){
                $wishlist_qty = Wishlist::where('user_id',session()->get('s_user_id'))->count();
                if($wishlist_qty <= 0) $wishlist_qty = '';

                $user_point = PointTransaction::select('total')->where('user_id',session()->get('s_user_id'))->orderBy('point_trans_id','desc')->first();
                if(!empty($user_point))
                    $count_point = $user_point->total;
            }


            $view->with(['lang' => $lang,'currency' => $currency,'cart_qty' => $cart_qty,'wishlist_qty' => $wishlist_qty, 'count_point' => $count_point]);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
