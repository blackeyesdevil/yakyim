<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Newsletter extends Model
{
    use SoftDeletes;
    protected $table = 'newsletter';
    protected $primaryKey = 'newsletter_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}

?>