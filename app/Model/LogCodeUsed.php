<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LogCodeUsed extends Model
{
    use SoftDeletes;
    protected $table = 'log_code_used';
    protected $primaryKey = 'log_code_used_id';
    protected $fillable = ['discount_code_id','discount_code','user_id','orders_id'];

    public $timestamps = true;
    protected $dates = ['deleted_at'];



}

?>