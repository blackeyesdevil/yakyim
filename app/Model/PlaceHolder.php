<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PlaceHolder extends Model
{
    use SoftDeletes;
    protected $table = 'place_holder';
    protected $primaryKey = 'place_holder_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}

?>
