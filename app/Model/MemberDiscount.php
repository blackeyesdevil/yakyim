<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MemberDiscount extends Model
{
    use SoftDeletes;
    protected $table = 'member_discount';
    protected $primaryKey = 'member_discount_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}

?>