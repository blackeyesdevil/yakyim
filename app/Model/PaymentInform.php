<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentInform extends Model
{
    use SoftDeletes;
    protected $table = 'payment_inform';
    protected $primaryKey = 'payment_inform_id';
    protected $fillable = ['orders_id','payment_date','total_price','img_name','bank'];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}


?>