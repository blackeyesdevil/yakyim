<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Wishlist extends Model
{
    use SoftDeletes;
    protected $table = 'wishlist';
    protected $primaryKey = 'wishlist_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}

?>