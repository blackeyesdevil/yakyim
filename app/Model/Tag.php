<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tag extends Model
{
    use SoftDeletes;
    protected $table = 'tag';
    protected $primaryKey = 'tag_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function ProductTag(){
        return $this->hasMany('App\Model\ProductTag','product_id');
    }
    public function Tag(){
        return $this->hasMany('App\Model\Tag','product_id');
    }

    public function BlogTag(){
        return $this->hasMany('App\Model\BlogTag','blog_id');
    }

}


?>