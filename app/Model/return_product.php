<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Return_product extends Model
{
    use SoftDeletes;
    protected $table = 'return_product';
    protected $primaryKey = 'order_return_id';
    protected $fillable = ['orders_id','image_name','return_date','detail','status','order_return_detail'];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}

?>
