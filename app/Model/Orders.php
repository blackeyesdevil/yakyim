<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Orders extends Model
{
    use SoftDeletes;
    protected $table = 'orders';
    protected $primaryKey = 'orders_id';
    protected $fillable = ['orders_no','orders_date','user_id','billing_address_id','billing_address','shipping_address_id','shipping_address','shipping_method_id','payment_method_id','paysbuy_method','payment_date','total_price','total_point','dollar_total_price','dollar_discount_price','discount_price','shipping_price','dollar_shipping_price','status_id','status_return_product','use_point','dollar_use_point'];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function User(){
        return $this->belongsTo('App\Model\User' ,'user_id');
    }
    public function Status(){
        return $this->belongsTo('App\Model\Status' ,'status_id');
    }
    public function Payment(){
        return $this->belongsTo('App\Model\PaymentMethod' ,'payment_method_id');
    }



}

?>
