<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Cart extends Model
{
    use SoftDeletes;
    protected $table = 'cart';
    protected $primaryKey = 'cart_id';
    protected $fillable = ['session_id','user_id','product_id','product_option_id','qty'];
    public $timestamps = true;
    protected $dates = ['deleted_at'];


    public function cart(){
        $user_id = session()->get('s_user_id');
        $carts = Cart::select('*','cart.qty as cart_qty')
            ->join('product_option','product_option.product_option_id','=','cart.product_option_id')
            ->join('product','product.product_id','=','product_option.product_id');
        if (!empty($user_id)) {
            $carts = $carts->where('user_id', $user_id);
        } else {
            $carts = $carts->where('session_id', session()->getId());
        }
        $carts = $carts->get();

        $total_price = 0;
        $total_normal_price=0;
        $total_qty = 0;
        $normal_price = 0;
        foreach($carts as $key => $cart){
            $cart_qty = $cart->cart_qty;
            $p_price = $cart->retail_price;

            if($cart->special_price > 0){
                $p_price = $cart->special_price;
            }else{
                $normal_price = $p_price;
            }

            $p_total_price = $cart_qty * $p_price;
            $n_total_price = $cart_qty * $normal_price;

            $total_price +=  $p_total_price;
            $total_normal_price += $n_total_price;

            $total_qty += $cart_qty;
        }

        $result = ['carts'=>$carts, 'total_price'=>$total_price, 'normal_price' => $total_normal_price, 'total_qty' => $total_qty ];
        return $result;
    }
}

?>
