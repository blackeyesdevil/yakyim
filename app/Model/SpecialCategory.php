<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SpecialCategory extends Model
{
    use SoftDeletes;
    protected $table = 'special_category';
    protected $primaryKey = 'special_category_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function Category(){
        return $this->belongsTo('App\Model\Category' ,'category_id');
    }


}

?>