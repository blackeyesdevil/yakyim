<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductGallery extends Model
{
    use SoftDeletes;
    protected $table = 'product_gallery';
    protected $primaryKey = 'product_gallery_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}

?>
