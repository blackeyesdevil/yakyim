<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductStock extends Model
{
    use SoftDeletes;
    protected $table = 'product_stock';
    protected $primaryKey = 'product_stock_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    
    public function ProductOption(){
        return $this->belongsTo('App\Model\ProductOption' ,'product_option_id');
    }
}

?>
