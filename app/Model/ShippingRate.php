<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class ShippingRate extends Model
{
    use SoftDeletes;
    protected $table = 'shipping_rate';
    protected $primaryKey = 'shipping_rate_id';
    protected $fillable = ['min_price','max_price','shipping_price'];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function shipping_price($total_price){
        $shipping_price = 0;
        $shipping_rate = ShippingRate::where('max_price','>=',$total_price)->orderBy('max_price','desc')->first();
        if(!empty($shipping_rate)){
            $shipping_price = $shipping_rate->shipping_price;
        }
        if(session()->get('s_user_type') == 1){
            $shipping_price = 0;
        }
        return $shipping_price;
    }

}

?>