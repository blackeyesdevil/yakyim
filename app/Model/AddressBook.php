<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AddressBook extends Model
{
    use SoftDeletes;
    protected $table = 'address_book';
    protected $primaryKey = 'address_book_id';
    protected $fillable = ['user_id','billing_shipping','firstname','lastname','tax_id','mobile','address','district_id','amphur_id','province_id','zipcode','set_default'];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function pattern($id){
        $lang = session()->get('locale');
        $address = AddressBook::find($id);
        $address_text = '';
        $address_text .= $address->firstname .' '. $address->lastname.' ('.$address->mobile.')<br>';
        $address_text .= $address->address.'<br>';
        $address_text .= $address->District['district_name_'.$lang].' '.$address->Amphur['amphur_name_'.$lang].' '.$address->Province['province_name_'.$lang].' '.$address->zipcode.'<br>';
        if($address->billing_shipping == 1){
            $address_text .= '<b>TAX ID: </b>'.$address->tax_id.'<br>';
        }
//        $address_text .= '<b>TEL. </b>'.$address->mobile;

        return $address_text;
    }
    public function User(){
        return $this->belongsTo('App\Model\User' ,'user_id');
    }
    public function Province(){
        return $this->belongsTo('App\Model\Province' ,'province_id');
    }
    public function Amphur(){
        return $this->belongsTo('App\Model\Amphur' ,'amphur_id');
    }
    public function District(){
        return $this->belongsTo('App\Model\District' ,'district_id');
    }



}

?>