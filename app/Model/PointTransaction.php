<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PointTransaction extends Model
{
    use SoftDeletes;
    protected $table = 'point_transaction';
    protected $primaryKey = 'point_trans_id';
    protected $fillable = ['user_id','detail','point','total'];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}


?>
