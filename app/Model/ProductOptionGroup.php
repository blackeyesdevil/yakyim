<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductOptionGroup extends Model
{
    use SoftDeletes;
    protected $table = 'product_option_group';
    protected $primaryKey = 'product_option_group_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];



    public function Product(){
        return $this->belongsTo('App\Model\Product' ,'product_id');
    }


    public function OptionGroup(){
        return $this->belongsTo('App\Model\OptionGroup' ,'option_group_id');
    }

}

?>