<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Warehouse extends Model
{
    use SoftDeletes;
    protected $table = 'warehouse';
    protected $primaryKey = 'warehouse_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}

?>