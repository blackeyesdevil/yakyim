<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Zipcode extends Model
{
    use SoftDeletes;
    protected $table = 'a_zipcode';
    protected $primaryKey = 'zipcode_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}

?>