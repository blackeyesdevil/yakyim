<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderTracking extends Model
{
    use SoftDeletes;
    protected $table = 'orders_tracking';
    protected $primaryKey = 'orders_tracking_id';
    protected $fillable = ['orders_id','status_id'];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function OptionGroup(){
        return $this->belongsTo('App\Model\OptionGroup' ,'option_group_id');
    }
    public function Status(){
        return $this->belongsTo('App\Model\Status' ,'status_id');
    }
}


?>