<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Review extends Model
{
    use SoftDeletes;
    protected $table = 'review';
    protected $primaryKey = 'review_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function Product(){
        return $this->belongsTo('App\Model\Product' ,'product_id');
    }

}

?>