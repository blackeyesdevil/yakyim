<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SizeChartDetail extends Model
{
    use SoftDeletes;
    protected $table = 'size_chart_detail';
    protected $primaryKey = 'size_chart_detail_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function SizeChart(){
        return $this->belongsTo('App\Model\SizeChart' ,'size_chart_id');
    }
}

?>
