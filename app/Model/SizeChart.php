<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SizeChart extends Model
{
    use SoftDeletes;
    protected $table = 'size_chart';
    protected $primaryKey = 'size_chart_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}

?>
