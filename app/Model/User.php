<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model
{
    use SoftDeletes;
    protected $table = 'user';
    protected $primaryKey = 'user_id';
    protected $fillable = ['user_no','verified','verified_date','firstname','lastname','birth_date','email','password','address','district_id','amphur_id','province_id','zipcode','mobile','line','facebook'];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}
?>