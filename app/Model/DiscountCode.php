<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DiscountCode extends Model
{
    use SoftDeletes;
    protected $table = 'discount_code';
    protected $primaryKey = 'discount_code_id';
    protected $fillable = ['code_name_th','code_name_en','code','allot', 'reserved', 'used','per_use','min_price','max_price','discount_type','discount','discount_max','show_date','start_date','end_date','status','flag','payment_method','normal_price'];
    public $timestamps = true;
    protected $dates = ['deleted_at'];




}

?>