<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PointCondition extends Model
{
    use SoftDeletes;
    protected $table = 'point_condition';
    protected $primaryKey = 'point_condition_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}

?>