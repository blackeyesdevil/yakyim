<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Product extends Model
{
    use SoftDeletes;
    protected $table = 'product';
    protected $primaryKey = 'product_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function getFirstImage($obj_product){
        foreach($obj_product as $item) {
            $item->img_name = null;
            $item->img_name2 = null;
            $photo = ProductGallery::whereNull('deleted_at')->where('product_id',$item->product_id)->take(2)->get();
            if(count($photo) > 0){
                foreach($photo as $key =>$p){
                    if($key == 0)
                        $item->img_name = $p->photo;
                    else
                        $item->img_name2 = $p->photo;
                }
            }
        }
    }

    public function JoinSpecial($id = null){
        $current_time = Carbon::now();
        $data = "(select `special_id`,`product_id`,`price` from `special` where 1 or ( `start_date` <= '$current_time' and `end_date` >= '$current_time')";
        if($id != null){ $data .= " and `product_id` = '".$id."'"; }
        $data .= " order by `special_id` desc limit 1) as `special` ";
        return $data;
    }
    public function Special($product_id){
        $special_price = Special::where('product_id',$product_id)->where('start_date','<=',Carbon::now())->where('end_date','>=',Carbon::now())->orderBy('price','asc')->first();
        return $special_price;
    }

    public function SpecialPrice($product_id,$retail_price){
        $s_price = ''; $t_price = ''; $m_price = '';
        // Special Price Product
        $special_product = Special::where('product_id',$product_id)->where('start_date','<=',Carbon::now())->where('end_date','>=',Carbon::now())->orderBy('price','desc')->first();
        if(!empty($special_product)){
            $s_price = $special_product->price;
        }

        // // Special Price From Tag
        // $special_tag = SpecialTag::where('start_date','<=',Carbon::now())->where('end_date','>=',Carbon::now())->orderBy('price','desc')->first();
        // if(!empty($special_tag)){
        //     $tag_id = $special_tag->tag_id;
        //     // หา product ที่อยู่ใน tag นี้
        //     $product_tag = ProductTag::where('tag_id',$tag_id)->get();
        //     if(count($product_tag) > 0){
        //         $a_product_tag = array();
        //         foreach($product_tag as $item){
        //             $a_product_tag[] = $item->product_id;
        //             $a_product_tag_type[$product_id] = $special_tag->special_type;
        //             $a_product_tag_price[$product_id] = $special_tag->price;
        //         }
        //         if(in_array($product_id, $a_product_tag)){
        //             if($a_product_tag_type[$product_id] == 'fix') {
        //                 $t_price = $retail_price - $a_product_tag_price[$product_id];
        //             } else {
        //                 $t_price = $retail_price - ($retail_price * $a_product_tag_price[$product_id] / 100);
        //             }
        //         }
        //     }
        // }

        // Discount Price From Member Discount
        $member_discount = MemberDiscount::where('start_date','<=',Carbon::now())->where('end_date','>=',Carbon::now())->where('status','1')->orderBy('priority','asc')->first();
        if(!empty($member_discount)){
            $m_price = $retail_price - ($retail_price * $member_discount->percent_discount / 100);
        }

        // Start Compare Special Price
        $arr_special_price = [];
        if(!empty($s_price)) $arr_special_price[] = $s_price;
        if(!empty($t_price)) $arr_special_price[] = $t_price;
        if(!empty($m_price)) $arr_special_price[] = $m_price;

        $special_price = min($arr_special_price);
        if($special_price < 0) $special_price = 0;

        $special_price = round($special_price);

        return $special_price;
    }

    public function StockStatus(){
        return $this->belongsTo('App\Model\StockStatus' ,'stock_status_id');
    }

    public function WeightClass(){
        return $this->belongsTo('App\Model\WeightClass' ,'weight_class_id');
    }

    public function DimensionClass(){
        return $this->belongsTo('App\Model\DimensionClass' ,'dimension_class_id');
    }

    public function Manufacturer(){
        return $this->belongsTo('App\Model\Manufacturer' ,'manufacturer_id');
    }

    public function ProductOption(){
        return $this->hasMany('App\Model\ProductOption','product_id');
    }




public function checkQty($amount){
        if ($amount < $this->minimum_qty){
            return array('status' => 0, 'msg' => 'The amount should be greater or equal to ' . $this->minimum_qty);
        }
        else if ($amount > $this->product_qty){
            return array('status' => 0, 'msg' => 'The amount is greater than the number of this item in our stock.');
        }
    }
}

?>
