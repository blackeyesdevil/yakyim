<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StockLog extends Model
{
    use SoftDeletes;
    protected $table = 'stock_log';
    protected $primaryKey = 'stock_log_id';
    protected $fillable = ['warehouse_id','orders_id','product_option_id','qty','cost','sale_price','dollar_sale_price','total','type'];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}

?>
