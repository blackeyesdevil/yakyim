<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Dollar extends Model
{
    use SoftDeletes;
    protected $table = 'dollar';
    protected $primaryKey = 'dollar_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    function cal_dollar($thb_price,$currency){
        $dollar_price = round($thb_price / $currency,2);
        return $dollar_price;
    }

}

?>