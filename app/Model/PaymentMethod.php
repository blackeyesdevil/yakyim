<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentMethod extends Model
{
    use SoftDeletes;
    protected $table = 'payment_method';
    protected $primaryKey = 'payment_method_id';
    protected $fillable = ['name_th','name_en'];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function Status(){
        return $this->hasMany('App\Model\Status' ,'status_id');
    }
}

?>