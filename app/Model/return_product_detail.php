<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Return_product_detail extends Model
{
    use SoftDeletes;
    protected $table = 'return_product_detail';
    protected $primaryKey = 'order_return_detail_id';
    protected $fillable = ['order_return_id','orders_id','orders_detail_id','product_id','reason','qty'];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}

?>
