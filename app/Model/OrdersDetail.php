<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrdersDetail extends Model
{
    use SoftDeletes;
    protected $table = 'orders_detail';
    protected $primaryKey = 'orders_detail_id';
    protected $fillable = ['orders_id','product_id','product_option_id','product_name_th','product_name_en','qty','unit_price','dollar_unit_price','model','flag'];
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function Orders(){
        return $this->belongsTo('App\Model\Orders' ,'orders_id');
    }

    public function User(){
        return $this->belongsTo('App\Model\User' ,'user_id');
    }
 
}

?>