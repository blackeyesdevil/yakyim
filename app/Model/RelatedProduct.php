<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RelatedProduct extends Model
{
    use SoftDeletes;
    protected $table = 'related_product';
    protected $primaryKey = 'related_product_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}

?>