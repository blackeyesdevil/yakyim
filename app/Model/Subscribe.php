<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subscribe extends Model
{
    use SoftDeletes;
    protected $table = 'subscribe';
    protected $primaryKey = 'subscribe_id';
    protected $fillable = ['email'];
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}

?>