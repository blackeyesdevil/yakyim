<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MemberCondition extends Model
{
    use SoftDeletes;
    protected $table = 'member_condition';
    protected $primaryKey = 'member_condition_id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}

?>