<?php
namespace App\Library;

use App\Model\User;
use App\Model\Product;
use App\Model\ProductOption;
use App\Model\Orders;
use App\Model\OrdersDetail;
use App\Model\LogCodeUsed;
use App\Model\DiscountCode;
use App\Model\PointTransaction;
use App\Model\PointCondition;
use App\Model\OrderTracking;
use Mail;
use Carbon\Carbon;

class OrderFunction {
    public function sendMailUser($orders_id,$subject){
        $objGal = new Product();
        $orders = Orders::find($orders_id);
        $orders_detail = OrdersDetail::where('orders_id',$orders->orders_id)->get();
        $objGal->getFirstImage($orders_detail); 

        $orders_no = $orders->orders_no;
        $user_data = User::find($orders->user_id);
        $user_email = $user_data->email;
        $user_name = $user_data->firstname.' '.$user_data->lastname;

        $log_code = LogCodeUsed::where('orders_id',$orders_id)->first();

        Mail::send('emails.order-detail', ['orders' => $orders, 'orders_detail' => $orders_detail, 'log_code' => $log_code], function ($m) use ($subject,$user_email,$user_name,$orders_no){
            $m->from('no-reply@yakyim.com','Webmaster YakYim');
            $m->to($user_email, $user_name);
            $m->subject($subject.', Order Number '.$orders_no);
        });
    }
    public function sendMailUserCancel($orders_id,$subject){
        $objGal = new Product();
        $orders = Orders::find($orders_id);
        $orders_detail = OrdersDetail::where('orders_id',$orders->orders_id)->get();
        $objGal->getFirstImage($orders_detail);

        $orders_no = $orders->orders_no;
        $user_data = User::find($orders->user_id);
        $user_email = $user_data->email;
        $user_name = $user_data->firstname.' '.$user_data->lastname;

        Mail::send('emails.order-cancelled', ['orders' => $orders, 'orders_detail' => $orders_detail], function ($m) use ($subject,$user_email,$user_name,$orders_no){
            $m->from('no-reply@yakyim.com','Webmaster YakYim');
            $m->to($user_email, $user_name);
            $m->subject($subject.', Order Number '.$orders_no);
        });
    }
    public function returnStock($orders_id){
        $orders_detail = OrdersDetail::where('orders_id',$orders_id)->get();
        if(count($orders_detail) > 0){
            foreach($orders_detail as $item){
                $product_option = ProductOption::find($item->product_option_id);
                $product_option->qty = $product_option->qty + $item->qty;
                $product_option->save();
            }
        }
    }
    public function usedCode($orders_id){
        $log_code = LogCodeUsed::where('orders_id',$orders_id)->first();
        if(!empty($log_code)){
            $discount_code_id = $log_code->discount_code_id;
            $discount_code = $log_code->discount_code;
            $discount_data = DiscountCode::where('discount_code_id',$discount_code_id);
            if($discount_code_id != '0' && $discount_code_id != '1' && $discount_code_id != '2' && $discount_code_id != '3' && $discount_code_id != '4'){
                $discount_data = $discount_data->where('code',$discount_code);
            }
            $discount_data = $discount_data->first();
            if(!empty($discount_data)){
                if($discount_code_id != '0' && $discount_code_id != '1' && $discount_code_id != '2' && $discount_code_id != '3' && $discount_code_id != '4') {
                    $reserved = $discount_data->reserved - 1;
                    $used = $discount_data->used + 1;
                    DiscountCode::where('discount_code_id', $discount_code_id)->where('code', $discount_code)->update(['reserved' => $reserved, 'used' => $used]);
                }
            }
        }
    }
    public function returnCode($orders_id){
        $log_code = LogCodeUsed::where('orders_id',$orders_id)->first();
        if(!empty($log_code)){
            $log_id = $log_code->log_code_used_id;
            $discount_code_id = $log_code->discount_code_id;
            $discount_code = $log_code->discount_code;
            $discount_data = DiscountCode::where('discount_code_id',$discount_code_id);
            if($discount_code_id != '0' && $discount_code_id != '1' && $discount_code_id != '2' && $discount_code_id != '3' && $discount_code_id != '4'){
                $discount_data = $discount_data->where('code',$discount_code);
            }
            $discount_data = $discount_data->first();
            if(!empty($discount_data)){
                if($discount_code_id != '0' && $discount_code_id != '1' && $discount_code_id != '2' && $discount_code_id != '3' && $discount_code_id != '4'){
                    $reserved = $discount_data->reserved - 1;
                    DiscountCode::where('discount_code_id',$discount_code_id)->where('code',$discount_code)->update(['reserved' => $reserved]);
                }
            }
            LogCodeUsed::where('log_code_used_id',$log_id)->delete();
        }
    }

    // point ที่ได้รับจากการซื้อสินค้า
    public function point($orders_id){
        $orders = Orders::find($orders_id);
        $orders_no = $orders->orders_no;
        $normal_price_total=0;

        $orders_detail = OrdersDetail::where('orders_id',$orders_id)->where('flag','0')->get();

        if(count($orders_detail) > 0){
            foreach($orders_detail as $detail_item){              
                $normal_price_total += $detail_item->unit_price * $detail_item->qty;
            }

            $grand_total = $normal_price_total - $orders->discount_price - $orders->use_point;

            $user_id = $orders->user_id;

            $point_condition = PointCondition::first();
            $min_price = $point_condition->min_price;
            $price = $point_condition->price;
            $point = $point_condition->point;

            if($grand_total >= $min_price ){
                $member_point = round($grand_total * $point / $price);
                $transaction = PointTransaction::where('user_id',$user_id)->orderBy('point_trans_id','desc')->first();
                if(!empty($transaction)){
                    $total_point = $transaction->total + $member_point;
                }else{
                    $total_point = $member_point;
                }
                // insert point transaction
                PointTransaction::create(['user_id'=>$user_id, 'point'=>$member_point, 'total'=>$total_point, 'detail' => 'Ref. Order Number '.$orders_no]);
                // update total point in order
                $orders->total_point = $member_point;
                $orders->save();
            }
        }

    }

    // คืน point ที่ได้รับจากการซื้อสินค้า
    public function returnPoint($orders_id){
        $orders = Orders::find($orders_id);
        $user_id = $orders->user_id;
        $orders_no = $orders->orders_no;
        $point = $orders->total_point;
//        $orders->total_point = 0;
//        $orders->save();
        if($point > 0){
            $transaction = PointTransaction::where('user_id',$user_id)->orderBy('point_trans_id','desc')->first();
            if(!empty($transaction)){
                $total_point = $transaction->total - $point;
            }else{
                $total_point = 0;
            }
            PointTransaction::create(['user_id'=>$user_id, 'point'=> -$point, 'total'=>$total_point, 'detail' => 'Fail, Ref. Order Number '.$orders_no]);
        }
    }

    // point ที่ใช้ซื้อสินค้า
    public function usePoint($orders_id,$use_point){
        $orders = Orders::find($orders_id);
        $user_id = $orders->user_id;
        $orders_no = $orders->orders_no;
        $user_point = PointTransaction::select('total')->where('user_id',$user_id)->orderBy('point_trans_id','desc')->first();
        if(!empty($user_point)){
            $points = $user_point->total;
            $total_point = $points - $use_point;

            PointTransaction::create(['user_id' => $user_id, 'point' => -$use_point, 'total' => $total_point, 'detail' => 'Ref. Order Number '.$orders_no]);
        }
    }
    // คืน point ที่ใช้ซื้อสินค้า
    public function returnUsePoint($orders_id){
        $orders = Orders::find($orders_id);
        $user_id = $orders->user_id;
        $orders_no = $orders->orders_no;
        $point = $orders->use_point;

        if($point > 0){
            $transaction = PointTransaction::where('user_id',$user_id)->orderBy('point_trans_id','desc')->first();
            if(!empty($transaction)){
                $total_point = $transaction->total + $point;
            }else{
                $total_point = 0;
            }
            PointTransaction::create(['user_id'=>$user_id, 'point'=> $point, 'total'=>$total_point, 'detail' => 'Return, Ref. Order Number '.$orders_no]);
        }
    }

    public function sendMailWh($orders_no){
        Mail::send('emails.payment-success', ['orders_no' => $orders_no], function ($m) use ($orders_no) {
            $m->from('no-reply@yakyim.com','Webmaster YakYim');
            $m->to(env('MAIL_WH'), 'Webmaster YakYim');
            $m->subject('Payment Success, Order Number '.$orders_no);
        });
    }
    public function sendMailWhPending($orders_no){
        Mail::send('emails.payment-pending', ['orders_no' => $orders_no], function ($m) use ($orders_no) {
            $m->from('no-reply@yakyim.com','Webmaster YakYim');
            $m->to(env('MAIL_WH'), 'Webmaster YakYim');
            $m->subject('PayPal Payment Status Is Pending, Order Number '.$orders_no);
        });
    }
    public function sendMailAcc($orders_no){
        Mail::send('emails.payment-success', ['orders_no' => $orders_no], function ($m) use ($orders_no) {
            $m->from('no-reply@yakyim.com','Webmaster YakYim');
            $m->to(env('MAIL_ACCOUNT'), 'Account');
            $m->subject('Payment Success, Order Number '.$orders_no);
        });
    }
    public function orderTracking($orders_id,$status_id){
        OrderTracking::create(['orders_id' => $orders_id, 'status_id' => $status_id]);
    }

    public function memberVIP($orders_id){
        $orders = Orders::find($orders_id);
        $orders_no = $orders->orders_no;
        $grand_total = $orders->total_price - $orders->discount_price + $orders->shipping_price - $orders->use_point;

        $user_id = $orders->user_id;
        if($grand_total >= 8000){
            User::where('user_id',$user_id)->update(['user_type'=>'1']);
            session()->put('s_user_type','1');
        }
    }

    public function sendMailCan($orders_id,$subject){
        $orders = Orders::find($orders_id);
        $orders_no = $orders->orders_no;
        $user_data = User::find($orders->user_id);
        $user_email = $user_data->email;
        $user_name = $user_data->firstname.' '.$user_data->lastname;
        Mail::send('emails.order-cancelled', ['orders' => $orders, 'orders_no' => $orders_no ], function ($m) use ($subject,$user_email,$user_name,$orders_no){
            $m->from('no-reply@yakyim.com','Webmaster YakYim');
            $m->to($user_email, $user_name);
            $m->subject($subject.', Order Number '.$orders_no);
        });
    }

    public function friendPoint($orders_id){
        // หาว่า order นี้ ใช้รหัสสมาชิกเป็นส่วนลดมั๊ย
        $log_code = LogCodeUsed::where('orders_id',$orders_id)->first();
        if(!empty($log_code)){
            // ข้อมูลเจ้าของรหัสสมาชิก
            $user_no = $log_code->discount_code;
            $data_user = User::where('user_no',$user_no)->first();

            if(!empty($data_user)){
                $user_id = $data_user->user_id;
                $user_email = $data_user->email;
                $user_name = $data_user->firstname.' '.$data_user->lastname;

                // หาส่วนลดว่าเท่าไหร่
                $orders = Orders::find($orders_id);
                $orders_no = $orders->orders_no;
                $discount_price = $orders->discount_price;

                // ส่วนลด 1 บาท = 1 point
                $member_point = round($discount_price);

                // Point Transaction
                $transaction = PointTransaction::where('user_id',$user_id)->orderBy('point_trans_id','desc')->first();
                if(!empty($transaction)){
                    $total_point = $transaction->total + $member_point;
                }else{
                    $total_point = $member_point;
                }

                // insert point transaction
                PointTransaction::create(['user_id'=>$user_id, 'point'=>$member_point, 'total'=>$total_point, 'detail' => 'Friend Ref. Order Number '.$orders_no]);

                // Send Mail
                Mail::send('emails.friend-point', ['member_point' => $member_point], function ($m) use ($orders_no, $user_email,$user_name) {
                    $m->from('no-reply@yakyim.com','Webmaster YakYim');
                    $m->to($user_email, $user_name);
                    $m->subject('คุณได้รับคะแนนสะสม จากเพื่อนของคุณ!');
                });
            }
        }
    }

}
?>
