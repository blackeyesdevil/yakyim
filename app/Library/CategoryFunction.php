<?php
namespace App\Library;

use DB;
use Request;
use URL;
use Intervention\Image\ImageManagerStatic as Image;
use Config;
use Storage;

class CategoryFunction {

    public function loop_category($query) {

        $check = 0;
        $list_cat = "";
        $selCategory = $query;
        foreach($selCategory as $fieldCate)
        {
            $category_name = $fieldCate->category_name_th;
            $path = $category_name;
            $parent = $fieldCate->parent_category_id;

            while($parent != 0)
            {
                $query2 = DB::table('category')
                    ->select('category_id','parent_category_id','category_name_th')
                    ->whereNull('deleted_at')
                    ->where('category_id',$parent)
                    ->orderBy('category_name_th','asc')->get();

                foreach($query2 as $fieldSubCate)
                {
                    $category_name2 = $fieldSubCate->category_name_th;
                    // New path and parent
                    $path = $category_name2 ." > " . $path ;
                    $parent = $fieldSubCate->parent_category_id;
                }
            }
            $list_cat[$fieldCate->category_id] = $path;
        }

        // เรียงค่าใน array จากน้อยไปมาก
        asort ($list_cat);
        //return $list_cat;
        return $list_cat;
    }

    public function list_category($category_id){
        $list_category_value = "";
        $query = DB::table('category')
            ->select('category_id','parent_category_id','category_name_th')
            ->whereNull('deleted_at')->where('category_id',$category_id)->get();
        $list_cat = $this->loop_category($query);
        foreach($list_cat as $key => $value){
            $list_category_value .= $value;
        }
        return $list_category_value;
    }



    public function search_select_category($search_category_id,$category_id,$root){
        // $search_category_id from search select box
        // $root 0 is parent_category_id=0
        $option = '';
        $query = DB::table('category')
            ->select('category_id','parent_category_id','category_name_th')
            ->whereNull('deleted_at');
        if(!empty($category_id))
        {
            $query = $query->where('category_id','!=',$category_id);
        }
        if(!empty($root) || $root=='0')
        {
            $query = $query->where('parent_category_id','0');
        }
        $query = $query->orderBy('category_name_th','asc')->get();
        $list_cat = $this->loop_category($query);
        foreach($list_cat as $key => $value){
            $select = "";
            if($search_category_id == $key ) $select = "selected";
            $option .= "<option value=\"".$key."\" ".$select.">".$value."</option>";

        }
        echo $option;

//        return $option;
//        return $list_cat;
    }
}
?>