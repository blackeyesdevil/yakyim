<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Model\DiscountCode;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;

class BirthdayDiscountController extends Controller
{
    public $model = 'App\Model\DiscountCode';
    public $titlePage = 'Birthday Discount';
    public $tbName = 'discount_code';
    public $pkField = 'discount_code_id';
    public $fieldList = array('code_name_th','code_name_en','code','allot','per_use','min_price','max_price','discount_type','discount','discount_max','show_date','start_date','end_date','status');
    public $a_search = array('');
    public $path = '_admin/birthday_discount';
    public $page = 'birthday_discount';
    public $viewPath = 'backend/birthday_discount';

    public function __construct()
    {
        $this->middleware('admin');
    }

    // ----------------------------------------- Show All List Page
    public function index()
    {
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Update";
        Session::put('referUrl',URL::previous());

        $model = $this->model;
        $data = $model::whereIn('discount_code_id',[1,2,3,4])->get();

        return view($this->viewPath.'/update',compact('data','url_to','method','txt_manage'));
    }

    // ----------------------------------------- View Add Page
    public function create()
    {

        
    }

    // ----------------------------------------- Record Data
    public function store(Request $request)
    {
        $model = $this->model;
        $arr_min_price = $request->min_price;
        $arr_discount = $request->discount;
        foreach ($arr_min_price as $key => $value) {
            $input_all['min_price'] = $value;
            $input_all['discount'] = $arr_discount[$key];
            $model::where('discount_code_id',$key)->update($input_all);
        }
        return Redirect::Back();
    }

    // ----------------------------------------- Show Data : ID
    public function show($id)
    {

    }

    // ----------------------------------------- View Update Page
    public function edit($id)
    {

    }

    // ----------------------------------------- Record Update Data
    public function update(Request $request, $id )
    {
        // $objFn = new MainFunction();

        // $model = $this->model; 
        // $data = $model::find($id);
        // $id = $objFn->db_update($data, $this->pkField, $request, $this->fieldList); // add $id
        // return Redirect::Back();
    }

    // ----------------------------------------- Delete Data
    public function destroy($id)
    {

    }
}
