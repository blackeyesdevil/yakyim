<?php
namespace App\Http\Controllers\Backend;

use App\Model\ProductCategory;
use Illuminate\Http\Request;
use App\Model\Category;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;
use Validator;

class CategoryController extends Controller
{
    public $model = 'App\Model\Category';
    public $titlePage = 'Category';
    public $tbName = 'category';
    public $pkField = 'category_id';
    public $fieldList = array('category_name_th','category_name_en','parent_category_id','title_home_th', 'title_home_en','title_th', 'title_en');
    public $a_search = array('category_name_th','category_name_en');
    public $path = '_admin/category';
    public $page = 'category';
    public $viewPath = 'backend/category';

    public function __construct()
    {
        $this->middleware('admin');
    }

    // ----------------------------------------- Show All List Page
    public function index()
    {
        $perPage = Config::get('mainConfig.perPage');
        $orderBy = Input::get('orderBy');
        if(empty($orderBy)) $orderBy = ('sorting');
        $sortBy = Input::get('sortBy');
        if(empty($sortBy)) $sortBy = 'desc';

        $search = Input::get('search');

        $model = $this->model;


        $data = new $model;
        $data = $model::where('parent_category_id',0);


        if(!empty($search))
        {
            $data = $data->where(function ($query) use($search){
                foreach($this->a_search as $field)
                {
                    $query = $query->orWhere($field, 'like', '%'.$search.'%');
                }
            });
        }
        $countData = $data->count();
        $data = $data
            ->orderBy($orderBy,$sortBy)
            ->paginate($perPage);
        $data->setPath($this->page);
        $data->lastPage();

 //      $category_id = Input::get('category_id');
//        $parent_category_name = $model::select("category_name")
//            ->where("parent_category_id",$category_id)
//            ->get();




        return view($this->viewPath.'/index',compact('data','countData'));
    }

    // ----------------------------------------- View Add Page
    public function create()
    {
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";
        $model = $this->model;

        $category = $model::get();
//        $category_id = $category[0]->category_id;

        $category_id = Input::get('category_id');
        return view($this->viewPath.'/update',compact('url_to','method','txt_manage','category','category_id'));
    }

    // ----------------------------------------- Record Data
    public function store(Request $request)
    {
        $objFn = new MainFunction();
        $model = $this->model;
        $data = new $model;
        $strParam = $request->strParam;

        $request->img_name = $request->img_name;

        $validator = Validator::make($request->all(), [
            'category_name_th' => 'required',
            'category_name_en' => 'required'
        ]);

        if ($validator->fails()){
            return Redirect::back()->withErrors($validator)->withInput();
        }
        else{
            $id = $objFn->db_add($data,$this->pkField,$request,$this->fieldList);

            // BANNER IMAGE
            if (Input::hasFile('img_name')) { // เพิ่มตรงนี้
                $photo = $request->file('img_name');                    // get image from form
                $new_name = date('YmdHis').'-'.rand(10000, 99999);                             // set new name
                $path = public_path('uploads/category');           // set path
                $extension = $photo->getClientOriginalExtension();      // get extension
                $filename = $new_name . "." . $extension;               // set filename
                $destinationPath = $path;
                $objFn->img_full_resize($photo, $destinationPath, $filename);   //
                $objFn->image_resize($photo, $destinationPath, 100, $filename);   // resize image
                $objFn->image_resize($photo, $destinationPath, 650, $filename);   // resize image
                $data = $model::find($id);
                $data->img_name = $filename;
                $data->save();
            }

            // RIGHT SIDE IMAGE
            if (Input::hasFile('img_right_name')) { // เพิ่มตรงนี้
                $photo = $request->file('img_right_name');                    // get image from form
                $new_name = date('YmdHis').'-'.rand(10000, 99999);                             // set new name
                $path = public_path('uploads/category_right');           // set path
                $extension = $photo->getClientOriginalExtension();      // get extension
                $filename = $new_name . "." . $extension;               // set filename
                $destinationPath = $path;
                $objFn->img_full_resize($photo, $destinationPath, $filename);   //
                $objFn->image_resize($photo, $destinationPath, 100, $filename);   // resize image
                $objFn->image_resize($photo, $destinationPath, 460, $filename);   // resize image
                $data = $model::find($id);
                $data->img_right_name = $filename;
                $data->save();
            }

            // BACKGROUND IMAGE
            if (Input::hasFile('img_bg_name')) { // เพิ่มตรงนี้
                $photo = $request->file('img_bg_name');                    // get image from form
                $new_name = date('YmdHis').'-'.rand(10000, 99999);                             // set new name
                $path = public_path('uploads/category_bg');           // set path
                $extension = $photo->getClientOriginalExtension();      // get extension
                $filename = $new_name . "." . $extension;               // set filename
                $destinationPath = $path;
                $objFn->img_full_resize($photo, $destinationPath, $filename);   //
                $objFn->image_resize($photo, $destinationPath, 100, $filename);   // resize image
                $objFn->image_resize($photo, $destinationPath, 995, $filename);   // resize image
                $objFn->image_resize($photo, $destinationPath, 1600, $filename);   // resize image
                $data = $model::find($id);
                $data->img_bg_name = $filename;
                $data->save();
            }

            $parent_category_id = $request->parent_category_id;

            if($parent_category_id == '0'){
                $check_sorting = $model::select("sorting")
                    ->orderBy('sorting','desc')
                    ->first();

                $data->sorting = $check_sorting->sorting+1;




                $data->save();
            } else{
                $data->sorting = 0;
                $data->save();


            }
        }

        return Redirect::to($this->path.'?'.$strParam);
    }

    // ----------------------------------------- Show Data : ID
    public function show($id)
    {

    }

    // ----------------------------------------- View Update Page
    public function edit($id)
    {
        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = "Update";
        Session::put('referUrl',URL::previous());

        $model = $this->model;
        $data = $model::find($id);
        $category_id = $data->parent_category_id;

        $category = $model::all();

        return view($this->viewPath.'/update',compact('data','url_to','method','txt_manage','category','category_id'));
    }

    // ----------------------------------------- Record Update Data
    public function update(Request $request, $id )
    {
        $objFn = new MainFunction();
        $strParam = $request->strParam;

        $model = $this->model;
        $data = $model::find($id);
        $request->img_name = $request->img_name;

        $validator = Validator::make($request->all(), [
            'category_name_th' => 'required',
            'category_name_en' => 'required'
        ]);

        if ($validator->fails()){
            return Redirect::back()->withErrors($validator)->withInput();
        }
        else{
            $id = $objFn->db_update($data,$this->pkField,$request,$this->fieldList); // add $id

            // BANNER IMAGE
            if (Input::hasFile('img_name')) { // test P.
                $photo = $request->file('img_name');                    // get image from form
                $new_name = date('YmdHis').'-'.rand(10000, 99999);                             // set new name
                $old_name = $data->img_name;                            // get old name
                $path = public_path('uploads/category');           // set path
                $objFn->del_storage($path,$old_name);                   // delete old picture in storage
                $objFn->del_storage($path.'/100',$old_name);                   // delete old picture in storage
                $objFn->del_storage($path.'/650',$old_name);                   // delete old picture in storage
                $extension = $photo->getClientOriginalExtension();      // get extension
                $filename = $new_name . "." . $extension;               // set filename
                $destinationPath = $path;
                $objFn->img_full_resize($photo, $destinationPath, $filename);   // resize image
                $objFn->image_resize($photo, $destinationPath, 100, $filename);   // resize image
                $objFn->image_resize($photo, $destinationPath, 650, $filename);   // resize image
                $data = $model::find($id);
                $data->img_name = $filename;
                $data->save();
            }
            else if ($request->img_del == 'y'){
                $old_name = $data->img_name;                            // get old name
                $path = public_path('uploads/category');           // set path
                $objFn->del_storage($path,$old_name);                   // delete old picture in storage
                $objFn->del_storage($path.'/100',$old_name);                   // delete old picture in storage
                $objFn->del_storage($path.'/650',$old_name);                   // delete old picture in storage

                $data->img_name = '';
                $data->save();
            }

            // RIGHT SIDE IMAGE
            if (Input::hasFile('img_right_name')) { // test P.
                $photo = $request->file('img_right_name');                    // get image from form
                $new_name = date('YmdHis').'-'.rand(10000, 99999);                             // set new name
                $old_name = $data->img_right_name;                            // get old name
                $path = public_path('uploads/category_right');           // set path
                $objFn->del_storage($path,$old_name);                   // delete old picture in storage
                $objFn->del_storage($path.'/100',$old_name);                   // delete old picture in storage
                $objFn->del_storage($path.'/460',$old_name);                   // delete old picture in storage
                $extension = $photo->getClientOriginalExtension();      // get extension
                $filename = $new_name . "." . $extension;               // set filename
                $destinationPath = $path;
                $objFn->img_full_resize($photo, $destinationPath, $filename);   // resize image
                $objFn->image_resize($photo, $destinationPath, 100, $filename);   // resize image
                $objFn->image_resize($photo, $destinationPath, 460, $filename);   // resize image
                $data = $model::find($id);
                $data->img_right_name = $filename;
                $data->save();
            }
            else if ($request->img_right_del == 'y'){
                $old_name = $data->img_right_name;                            // get old name
                $path = public_path('uploads/category_right');           // set path
                $objFn->del_storage($path,$old_name);                   // delete old picture in storage
                $objFn->del_storage($path.'/100',$old_name);                   // delete old picture in storage
                $objFn->del_storage($path.'/460',$old_name);                   // delete old picture in storage

                $data->img_right_name = '';
                $data->save();
            }

            // BACKGROUND IMAGE
            if (Input::hasFile('img_bg_name')) { // test P.
                $photo = $request->file('img_bg_name');                    // get image from form
                $new_name = date('YmdHis').'-'.rand(10000, 99999);                             // set new name
                $old_name = $data->img_bg_name;                            // get old name
                $path = public_path('uploads/category_bg');           // set path
                $objFn->del_storage($path,$old_name);                   // delete old picture in storage
                $objFn->del_storage($path.'/100',$old_name);                   // delete old picture in storage
                $objFn->del_storage($path.'/995',$old_name);                   // delete old picture in storage
                $objFn->del_storage($path.'/1600',$old_name);                   // delete old picture in storage
                $extension = $photo->getClientOriginalExtension();      // get extension
                $filename = $new_name . "." . $extension;               // set filename
                $destinationPath = $path;
                $objFn->img_full_resize($photo, $destinationPath, $filename);   // resize image
                $objFn->image_resize($photo, $destinationPath, 100, $filename);   // resize image
                $objFn->image_resize($photo, $destinationPath, 995, $filename);   // resize image
                $objFn->image_resize($photo, $destinationPath, 1600, $filename);   // resize image
                $data = $model::find($id);
                $data->img_bg_name = $filename;
                $data->save();
            }
            else if ($request->img_bg_del == 'y'){
                $old_name = $data->img_bg_name;                            // get old name
                $path = public_path('uploads/category_bg');           // set path
                $objFn->del_storage($path,$old_name);                   // delete old picture in storage
                $objFn->del_storage($path.'/100',$old_name);                   // delete old picture in storage
                $objFn->del_storage($path.'/995',$old_name);                   // delete old picture in storage
                $objFn->del_storage($path.'/1600',$old_name);                   // delete old picture in storage

                $data->img_bg_name = '';
                $data->save();
            }
        }

        return Redirect::to($this->path.'?'.$strParam);
    }

    // ----------------------------------------- Delete Data
    public function destroy($id)
    {
        Session::put('referUrl',URL::previous());
        $model = $this->model;

        ProductCategory::where('category_id', $id)->delete();


        $data2 = $model::select('img_name', 'img_right_name', 'img_bg_name')
            ->where("category_id",$id)
            ->first();

        $set_paths = array(
            'uploads/category/',
            'uploads/category/100/',
            'uploads/category/650/'
        );

        $set2_paths = array(
            'uploads/category_right/',
            'uploads/category_right/100/',
            'uploads/category_right/460/'
        );

        $set3_paths = array(
            'uploads/category_bg/',
            'uploads/category_bg/100/',
            'uploads/category_bg/995/',
            'uploads/category_bg/1600/'
        );

        foreach ($set_paths as $path){
            $file = $data2->img_name;

            if ($file != '' && file_exists($path.$file)){
                unlink($path.$file);
            }
        }

        foreach ($set2_paths as $path){
            $file = $data2->img_right_name;

            if ($file != '' && file_exists($path.$file)){
                unlink($path.$file);
            }
        }

        foreach ($set3_paths as $path){
            $file = $data2->img_bg_name;

            if ($file != '' && file_exists($path.$file)){
                unlink($path.$file);
            }
        }

        $data = $model::select("parent_category_id")
            ->where("category_id",$id)
            ->first();
        if($data){

            $categories = Category::where('parent_category_id',$id)->get();
            foreach($categories as $category){
                $category->parent_category_id = 0;
                $category->save();

            }


        }

        $model::find($id)->delete();


        return Redirect::to(Session::get('referUrl'));
    }


}
