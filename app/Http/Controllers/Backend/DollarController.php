<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Model\Dollar;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;

class DollarController extends Controller
{
    public $model = 'App\Model\Dollar';
    public $titlePage = 'Set Baht : 1 Dollar';
    public $tbName = 'dollar';
    public $pkField = 'dollar_id';
    public $fieldList = array('dollar');
    public $a_search = array('');
    public $path = '_admin/dollar';
    public $page = 'dollar';
    public $viewPath = 'backend/dollar';

    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
        $model = $this->model;
        $data = $model::leftjoin('user','user.user_id','=','orders.user_id')->whereNull('orders.deleted_at');

        return view($this->viewPath.'/export_report',compact('data','countData','from_date','to_date'));
    }

    // ----------------------------------------- View Update Page
    public function edit($id)
    {
        $url_to = $this->path . '/' . $id;
        $method = 'PUT';
        $txt_manage = "Update";
        Session::put('referUrl', URL::previous());

        $model = $this->model;
        $data = $model::find($id);
        return view($this->viewPath . '/update', compact('data', 'url_to', 'method', 'txt_manage'));
    }

    // ----------------------------------------- Record Update Data
    public function update(Request $request, $id)
    {
        $objFn = new MainFunction();
        $strParam = $request->strParam;

        $model = $this->model;
        $data = $model::find($id);
        $id = $objFn->db_update($data, $this->pkField, $request, $this->fieldList); // add $id

        DB::table('dollar_log')->insert(['dollar_id' => $id , 'dollar' => $request->dollar , 'created_at' => date('Y-m-d H:i:s')]);

        return Redirect::Back();

    }

    // ----------------------------------------- Delete Data
    public function destroy($id)
    {


    }

}
