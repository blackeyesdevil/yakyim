<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Model\ProductStock;
use App\Model\ProductOption;
use App\Model\StockLog;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;
// use Excel;

class WareHouseController extends Controller
{
    public $model = 'App\Model\Warehouse';
    public $titlePage = 'Warehouse';
    public $tbName = 'warehouse';
    public $pkField = 'warehouse_id';
    public $fieldList = array('warehouse_name');
    public $a_search = array('warehouse_name');
    public $path = '_admin/warehouse';
    public $page = 'warehouse';
    public $viewPath = 'backend/warehouse';

    public function __construct()
    {
        $this->middleware('admin');
    }

    // ----------------------------------------- Show All List Page
    public function index(Request $request)
    {
        $perPage = Config::get('mainConfig.perPage');
        $orderBy = Input::get('orderBy');
        if (empty($orderBy)) $orderBy = $this->pkField;
        $sortBy = Input::get('sortBy');
        if (empty($sortBy)) $sortBy = 'desc';
        $search = Input::get('search');

        $model = $this->model;
        $data = new $model;

        if (!empty($search)) {
            $data = $data->where(function ($query) use ($search) {
                foreach ($this->a_search as $field) {
                    $query = $query->orWhere($field, 'like', '%' . $search . '%');
                }
            });
        }

        $countData = $data->count();
        $data = $data
            ->orderBy($orderBy, $sortBy)
            ->paginate($perPage);
        $data->setPath($this->page);
        $data->lastPage();

        return view($this->viewPath . '/index', compact('data', 'countData'));
    }

    // ----------------------------------------- View Add Page
    public function create()
    {
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";

        return view($this->viewPath . '/update', compact('url_to', 'method', 'txt_manage'));
    }

    // ----------------------------------------- Record Data
    public function store(Request $request)
    {
        $objFn = new MainFunction();

        $strParam = $request->strParam;
        $model = $this->model;
        $data = new $model;
        $id = $objFn->db_add($data, $this->pkField, $request, $this->fieldList);

        return Redirect::to($this->path . '?' . $strParam);
    }

    // ----------------------------------------- Show Data : ID
    public function show($id)
    {

    }

    // ----------------------------------------- View Update Page
    public function edit($id)
    {
        $url_to = $this->path . '/' . $id;
        $method = 'PUT';
        $txt_manage = "Update";
        Session::put('referUrl', URL::previous());

        $model = $this->model;
        $data = $model::find($id);

        if($id==1){
            return Redirect::back();
        }else{
            return view($this->viewPath . '/update', compact('data', 'url_to', 'method', 'txt_manage'));
        }

    }

    // ----------------------------------------- Record Update Data
    public function update(Request $request, $id)
    {
        $objFn = new MainFunction();
        $strParam = $request->strParam;

        $model = $this->model;
        $data = $model::find($id);
        $objFn->db_update($data, $this->pkField, $request, $this->fieldList); // add $id

        return Redirect::to($this->path . '?' . $strParam);

    }

    // ----------------------------------------- Delete Data
    public function destroy($id)
    {
        Session::put('referUrl', URL::previous());
        $model = $this->model;

        $warehouse_id = $id;
        $data_stock = ProductStock::whereNull('deleted_at')->where('warehouse_id',$warehouse_id)->get();

        foreach ($data_stock as $value){

            $sql_log = StockLog::select('total')->where('warehouse_id','1')->whereNull('deleted_at')->where('product_option_id',$value->product_option_id)->orderBy('stock_log_id','desc')->first();

            if(!empty($sql_log->total)){
                $totalLog = $sql_log->total;
            }else{
                $totalLog = 0;
            }

            $total = $totalLog+$value->qty;
            StockLog::insert(['warehouse_id' => '1' , 'product_option_id' => $value->product_option_id , 'qty' => $value->qty ,  'total' => $total , 'cost' => $value->cost , 'type' => '2' , 'created_at' => date('Y-m-d H:i:s')] );

            $sql_log2 = StockLog::select('total')->where('warehouse_id',$warehouse_id)->whereNull('deleted_at')->where('product_option_id',$value->product_option_id)->orderBy('stock_log_id','desc')->first();

            if(!empty($sql_log2->total)){
                $totalLog2 = $sql_log2->total;
            }else{
                $totalLog2 = 0;
            }

            $total2 = $totalLog2-$value->qty;
            StockLog::insert(['warehouse_id' => $warehouse_id , 'product_option_id' => $value->product_option_id , 'qty' => "-".$value->qty ,  'total' => $total2 , 'cost' => $value->cost , 'type' => '2' , 'created_at' => date('Y-m-d H:i:s')] );

            ProductStock::insert(['warehouse_id' => '1' , 'product_option_id' => $value->product_option_id , 'qty' => $value->qty , 'cost' => $value->cost , 'created_at' => date('Y-m-d H:i:s')] );

            ProductStock::where('product_stock_id',$value->product_stock_id)->forceDelete();
            $data_option = ProductOption::where('product_option_id',$value->product_option_id)->select('qty')->first();
            $total_option = $data_option->qty+$value->qty;
            ProductOption::where('product_option_id',$value->product_option_id)->update(['qty' => $total_option]);

        }

        $model::find($id)->delete();

        return Redirect::to(Session::get('referUrl'));

    }
}