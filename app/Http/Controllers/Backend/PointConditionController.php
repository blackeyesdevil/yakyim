<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Model\PointCondition;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;

class PointConditionController extends Controller
{
    public $model = 'App\Model\PointCondition';
    public $titlePage = 'PointCondition';
    public $tbName = 'point_condition';
    public $pkField = 'point_condition_id';
    public $fieldList = array('min_price','price','point');
    public $a_search = array('');
    public $path = '_admin/point_condition';
    public $page = 'point_condition';
    public $viewPath = 'backend/point_condition';

    public function __construct()
    {
        $this->middleware('admin');
    }

    // ----------------------------------------- View Update Page
    public function edit($id)
    {
        $url_to = $this->path . '/' . $id;
        $method = 'PUT';
        $txt_manage = "Update";
        Session::put('referUrl', URL::previous());

        $model = $this->model;
        $data = $model::find($id);
        return view($this->viewPath . '/update', compact('data', 'url_to', 'method', 'txt_manage'));
    }

    // ----------------------------------------- Record Update Data
    public function update(Request $request, $id)
    {
        $objFn = new MainFunction();
        $strParam = $request->strParam;

        $model = $this->model;
        $data = $model::find($id);
        $id = $objFn->db_update($data, $this->pkField, $request, $this->fieldList); // add $id

        return Redirect::Back();

    }

    // ----------------------------------------- Delete Data
    public function destroy($id)
    {


    }

}
