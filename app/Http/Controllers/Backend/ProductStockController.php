<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Model\ProductStock;
use App\Model\ProductOption;
use App\Model\StockLog;
use App\Model\WareHouse;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;
use Validator;
// use Excel;

class ProductStockController extends Controller
{
    public $model = 'App\Model\ProductStock';
    public $titlePage = 'ProductStock';
    public $tbName = 'product_stock';
    public $pkField = 'product_stock_id';
    public $fieldList = array('warehouse_id','product_option_id','qty','cost');
    public $a_search = array('model');
    public $path = '_admin/product_stock';
    public $page = 'product_stock';
    public $viewPath = 'backend/product_stock';

    public function __construct()
    {
        $this->middleware('admin');
    }

    // ----------------------------------------- Show All List Page
    public function index(Request $request)
    {
        $perPage = Config::get('mainConfig.perPage');
        $orderBy = Input::get('orderBy');
        if (empty($orderBy)) $orderBy = $this->pkField;
        $sortBy = Input::get('sortBy');
        if (empty($sortBy)) $sortBy = 'desc';
        $search = Input::get('search');
        $warehouse_id = Input::get('warehouse_id');

        $model = $this->model;
        $data = $model::leftjoin('product_option','product_option.product_option_id','=','product_stock.product_option_id')
            ->select('product_stock.qty','product_stock.cost','product_option.model','product_option.option_name_th','product_option.product_option_id','product_stock.product_stock_id','product_option.product_id')->where('product_stock.warehouse_id',$warehouse_id);

        if (!empty($search)) {
            $data = $data->where(function ($query) use ($search) {
                foreach ($this->a_search as $field) {
                    $query = $query->orWhere($field, 'like', '%' . $search . '%');
                }
            });
        }
        

        if(!empty(Input::get('export')) && Input::get('export')=="true"){
            // $data = $data->groupBy('product_option.model');
            $countData = $data->count();
            $data = $data->get();

            return view($this->viewPath.'/export_report',compact('data','countData'));

            // Excel::create('Filename', function($excel) use ($data){
            //     $excel->setTitle('Stock online');
            //     $excel->sheet('Stock', function($sheet) use ($data){
            //         $sheet->rows(array(
            //             array('รหัสสินค้า','ชื่อ','จำนวน','ต้นทุน','ราคาขาย')
            //         ));
            //         $data = $data->get();
            //         foreach ($data as $key => $value) {
            //             $sheet->rows(array(
            //                 array($value->model,'ชื่อ',$value->qty,$value->cost,'ราคาขาย')
            //             ));
            //         }

            //     });
            // })->export('xls');
        }

        $countData = $data->count();
        $data = $data
            ->orderBy($orderBy, $sortBy)
            ->paginate($perPage);
        $data->setPath($this->page);
        $data->lastPage();



        return view($this->viewPath . '/index', compact('data', 'countData'));
    }

    // ----------------------------------------- View Add Page
    public function create()
    {
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";
        $warehouse_id = input::get('warehouse_id');
        $type = input::get('type');

        if($warehouse_id!=1){
            if($type==1){
                $ProductOption = ProductStock::leftjoin('product_option','product_option.product_option_id','=','product_stock.product_option_id')->select('product_option.option_name_th','product_option.model','product_option.product_option_id')->groupBy('product_stock.product_option_id')->whereNull('product_stock.deleted_at')->get();
            }else if($type==2){
                $ProductOption = ProductStock::leftjoin('product_option','product_option.product_option_id','=','product_stock.product_option_id')->select('product_option.option_name_th','product_option.model','product_option.product_option_id')->groupBy('product_stock.product_option_id')->whereNull('product_stock.deleted_at')->where('warehouse_id',$warehouse_id)->get();
            }

        }else{
            $ProductOption = ProductOption::whereNull('deleted_at')->get();
        }

        return view($this->viewPath . '/update', compact('url_to', 'method', 'txt_manage','ProductOption'));
    }

    // ----------------------------------------- Record Data
    public function store(Request $request)
    {
        $objFn = new MainFunction();

        $strParam = $request->strParam;
        $model = $this->model;
        $warehouse_id = $request->warehouse_id;

        $data = new $model;

        $qty_option = ProductOption::select('qty')->whereNull('deleted_at')->where('product_option_id',$request->product_option_id)->first();

        if($warehouse_id == 1){
            $total_log_qty = '';
            $sql_log = StockLog::select('qty')->whereNull('deleted_at')->where('warehouse_id',$warehouse_id)->where('product_option_id',$request->product_option_id)->get();
            foreach ($sql_log as $value){
                $total_log_qty += $value->qty;
            }

            if($request->type==1) { // 1 = +
                $total_qty = $qty_option->qty + $request->qty;
                $qty_log = $request->qty;
                if (!empty($total_log_qty)){
                    $total_log_qty = $total_log_qty+$request->qty;
                }else{
                    $total_log_qty = $request->qty;
                }

                StockLog::insert(['warehouse_id' => $warehouse_id , 'product_option_id' => $request->product_option_id , 'qty' => $qty_log ,  'total' => $total_log_qty , 'cost' => $request->cost , 'type' => '1' , 'created_at' => date('Y-m-d H:i:s')] );
                ProductStock::insert(['warehouse_id' => $warehouse_id , 'product_option_id' => $request->product_option_id , 'qty' => $request->qty , 'cost' => $request->cost , 'created_at' => date('Y-m-d H:i:s')] );

            }

            ProductOption::where('product_option_id',$request->product_option_id)->update(['qty' => $total_qty]);

        }else {
            $qty = $request->qty;

            $product_option_qty = ProductOption::select('qty')
                                            ->where('product_option_id', $request->product_option_id)
                                            ->first();

            if(empty($product_option_qty))
                $product_option_qty = 0;
            else
                $product_option_qty = $product_option_qty->qty;

            $product_stock_qty = StockLog::select('total')
                                            ->where('warehouse_id', $warehouse_id)
                                            ->where('product_option_id', $request->product_option_id)
                                            ->orderBy('stock_log_id', 'desc')
                                            ->first();
            if(empty($product_stock_qty))
                $product_stock_qty = 0;
            else
                $product_stock_qty = $product_stock_qty->total;

            $online_stock_qty = StockLog::select('total')
                                            ->where('warehouse_id', '1')
                                            ->where('product_option_id', $request->product_option_id)
                                            ->orderBy('stock_log_id', 'desc')
                                            ->first();

            if(empty($online_stock_qty))
                $online_stock_qty = 0;
            else
                $online_stock_qty = $online_stock_qty->total;



            if($request->type == 1){
                if ($qty > $product_option_qty || $qty > $online_stock_qty){
                    return Redirect::back()->withErrors(['msg' => 'Not enough products']);
                }
            }else{
                if ($qty > $product_stock_qty){
                    return Redirect::back()->withErrors(['msg' => 'Not enough products']);
                }
            }


            if($request->type == 1) { // In case you want to import products from the Online Warehouse

                // Decrease products in the Online Warehouse and ProductOption's Quantity
                $stocks = ProductStock::where('warehouse_id', '1')
                                        ->where('product_option_id', $request->product_option_id)
                                        ->orderBy('product_stock_id', 'asc')->get();

                $tmp_qty = $qty;
                foreach ($stocks as $stock){

                    if ($tmp_qty == 0)
                        break;


                    $new_item = new ProductStock;
                    $new_item->warehouse_id = $warehouse_id;
                    $new_item->product_option_id = $request->product_option_id;

                    if ($stock->qty > $tmp_qty){

                        $stock->decrement('qty', $tmp_qty);
                        $online_stock_qty -= $tmp_qty;
                        $product_stock_qty += $tmp_qty;

                        $log = new StockLog;
                        $log->warehouse_id = '1';
                        $log->product_option_id = $request->product_option_id;
                        $log->qty = "-".$tmp_qty;
                        $log->cost = $stock->cost;
                        $log->total = $online_stock_qty;
                        $log->type = '4';
                        $log->save();

                        $new_item->qty = $tmp_qty;
                        $new_item->cost = $stock->cost;
                        $new_item->save();

                        $log = new StockLog;
                        $log->warehouse_id = $warehouse_id;
                        $log->product_option_id = $request->product_option_id;
                        $log->qty = $tmp_qty;
                        $log->cost = $stock->cost;
                        $log->total = $product_stock_qty;
                        $log->type = '3';
                        $log->save();

                        $tmp_qty = 0;

                        break;
                    }
                    else {

                        $stock->forceDelete();
                        $online_stock_qty -= $stock->qty;
                        $product_stock_qty += $stock->qty;
                        $tmp_qty -= $stock->qty;

                        $log = new StockLog;
                        $log->warehouse_id = '1';
                        $log->product_option_id = $request->product_option_id;
                        $log->qty = "-".$stock->qty;
                        $log->cost = $stock->cost;
                        $log->total = $online_stock_qty;
                        $log->type = '4';
                        $log->save();

                        $new_item->qty = $stock->qty;
                        $new_item->cost = $stock->cost;
                        $new_item->save();

                        $log = new StockLog;
                        $log->warehouse_id = $warehouse_id;
                        $log->product_option_id = $request->product_option_id;
                        $log->qty = $stock->qty;
                        $log->cost = $stock->cost;
                        $log->total = $product_stock_qty;
                        $log->type = '3';
                        $log->save();
                    }
                }

                ProductOption::where('product_option_id', $request->product_option_id)
                            ->decrement('qty', $qty);
            }else{        // In case you want to remove products
                $stocks = ProductStock::where('warehouse_id', $warehouse_id)
                                        ->where('product_option_id', $request->product_option_id)
                                        ->orderBy('product_stock_id', 'asc')->get();

                $tmp_qty = $qty;
                foreach ($stocks as $stock){
                    if ($tmp_qty == 0)
                        break;

                    $new_item = new ProductStock;
                    $new_item->warehouse_id = 1;
                    $new_item->product_option_id = $request->product_option_id;

                    if ($stock->qty > $tmp_qty){

//                        $stock->decrement('qty', $tmp_qty);
//                        $product_stock_qty -= $tmp_qty;
                       //$tmp_qty = 0;

                        $stock->decrement('qty', $tmp_qty);
                        $online_stock_qty += $tmp_qty;
                        $product_stock_qty -= $tmp_qty;

                        $log = new StockLog;
                        $log->warehouse_id = '1';
                        $log->product_option_id = $request->product_option_id;
                        $log->qty = $tmp_qty;
                        $log->cost = $stock->cost;
                        $log->total = $online_stock_qty;
                        $log->type = '1';
                        $log->save();

                        $new_item->qty = $tmp_qty;
                        $new_item->cost = $stock->cost;
                        $new_item->save();

                        $log = new StockLog;
                        $log->warehouse_id = $warehouse_id;
                        $log->product_option_id = $request->product_option_id;
                        $log->qty = "-".$tmp_qty;
                        $log->cost = $stock->cost;
                        $log->total = $product_stock_qty;
                        $log->type = '2';
                        $log->save();

                        break;
                    }
                    else {
                        $stock->forceDelete();

                        $tmp_qty -= $stock->qty;
                        $product_stock_qty -= $stock->qty;
                        $online_stock_qty += $stock->qty;

                        $log = new StockLog;
                        $log->warehouse_id = $warehouse_id;
                        $log->product_option_id = $request->product_option_id;
                        $log->qty = "-".$stock->qty;
                        $log->cost = $stock->cost;
                        $log->total = $product_stock_qty;
                        $log->type = '2';
                        $log->save();

//                        echo "-".$stock->qty.">>".$stock->cost.">>".$product_stock_qty."w2<br>";


//                                echo $stock->qty.">>".$stock->cost."<br>";
                        $new_item->qty = $stock->qty;
                        $new_item->cost = $stock->cost;
                        $new_item->save();

                        $log = new StockLog;
                        $log->warehouse_id = '1';
                        $log->product_option_id = $request->product_option_id;
                        $log->qty = $stock->qty;
                        $log->cost = $stock->cost;
                        $log->total = $online_stock_qty;
                        $log->type = '1';
                        $log->save();

//                        echo $stock->qty.">>".$stock->cost.">>".$online_stock_qty."w1<br>";

//                        echo "<hr>";


                    }
                }

//                return exit();

                ProductOption::where('product_option_id', $request->product_option_id)
                    ->increment('qty', $qty);

            }
        }

        return Redirect::to($this->path . '?' . $strParam);
    }

    // ----------------------------------------- Show Data : ID
    public function show($id)
    {

    }

    // ----------------------------------------- View Update Page
    public function edit($id)
    {
        $url_to = $this->path . '/' . $id;
        $method = 'PUT';
        $txt_manage = "Update";
        Session::put('referUrl', URL::previous());

        $model = $this->model;
        $data = $model::find($id);


        return view($this->viewPath . '/update', compact('data', 'url_to', 'method', 'txt_manage'));
    }

    // ----------------------------------------- Record Update Data
    public function update(Request $request, $id)
    {
        $objFn = new MainFunction();
        $strParam = $request->strParam;

        $model = $this->model;
        $data = $model::find($id);
        $id = $objFn->db_update($data, $this->pkField, $request, $this->fieldList); // add $id

        return Redirect::back();

    }

    // ----------------------------------------- Delete Data
    public function destroy($id)
    {
        Session::put('referUrl', URL::previous());
        $model = $this->model;

        $stocks = ProductStock::where('product_stock_id', $id)->select('qty','product_option_id','warehouse_id','cost')->first();
        $option = ProductOption::where('product_option_id',$stocks->product_option_id)->select('qty')->first();

        $qty = $option->qty-$stocks->qty;

        ProductOption::where('product_option_id',$stocks->product_option_id)->update(['qty' => $qty]);

        $data_stock = StockLog::where('warehouse_id',$stocks->warehouse_id)->select('total')->orderBy('stock_log_id','desc')->first();
        $stock_qty = $data_stock->total-$stocks->qty;

        StockLog::insert(['warehouse_id' => '1' , 'product_option_id' => $stocks->product_option_id , 'qty' => $stocks->qty , 'cost' => $stocks->cost , 'total' => $stock_qty , 'type' => '2' , 'created_at' => date('Y-m-d H:i:s')] );

        $model::find($id)->delete();

        return Redirect::to(Session::get('referUrl'));

    }

    public function postRestock(Request $request)
    {

        $warehouse_id = $request->warehouse_id;
        $data_stock = ProductStock::whereNull('deleted_at')->where('warehouse_id',$warehouse_id)->get();

        foreach ($data_stock as $value){

            $sql_log = StockLog::select('total')->where('warehouse_id','1')->whereNull('deleted_at')->where('product_option_id',$value->product_option_id)->orderBy('stock_log_id','desc')->first();

            if(!empty($sql_log->total)){
                $totalLog = $sql_log->total;
            }else{
                $totalLog = 0;
            }

            $total = $totalLog+$value->qty;
            StockLog::insert(['warehouse_id' => '1' , 'product_option_id' => $value->product_option_id , 'qty' => $value->qty ,  'total' => $total , 'cost' => $value->cost , 'type' => '1' , 'created_at' => date('Y-m-d H:i:s')] );

            $sql_log2 = StockLog::select('total')->where('warehouse_id',$warehouse_id)->whereNull('deleted_at')->where('product_option_id',$value->product_option_id)->orderBy('stock_log_id','desc')->first();

            if(!empty($sql_log2->total)){
                $totalLog2 = $sql_log2->total;
            }else{
                $totalLog2 = 0;
            }

            $total2 = $totalLog2-$value->qty;
            StockLog::insert(['warehouse_id' => $warehouse_id , 'product_option_id' => $value->product_option_id , 'qty' => "-".$value->qty ,  'total' => $total2 , 'cost' => $value->cost , 'type' => '2' , 'created_at' => date('Y-m-d H:i:s')] );

            ProductStock::insert(['warehouse_id' => '1' , 'product_option_id' => $value->product_option_id , 'qty' => $value->qty , 'cost' => $value->cost , 'created_at' => date('Y-m-d H:i:s')] );

            ProductStock::where('product_stock_id',$value->product_stock_id)->forceDelete();
            $data_option = ProductOption::where('product_option_id',$value->product_option_id)->select('qty')->first();
            $total_option = $data_option->qty+$value->qty;
            ProductOption::where('product_option_id',$value->product_option_id)->update(['qty' => $total_option]);


            //            echo "Log On :".$total."<br>";
            //            echo "Log Off :".$total2."<br>";
            //            echo "Add: ".$value->qty." | ".$value->product_option_id."<br>";
            //            echo "Delete: ".$value->product_stock_id."<br>";
            //            echo "<hr>";
            //            echo $value->product_option_id."|".$total_option;
        }

        return redirect()->back();


    }

}
