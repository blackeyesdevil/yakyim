<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Model\User;
use App\Model\PointTransaction;
use App\Model\District;
use App\Model\Amphur;
use App\Model\Province;
use App\Model\Orders;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;
use Hash;
// use Excel;

class UserController extends Controller
{
    public $model = 'App\Model\User';
    public $titlePage = 'User';
    public $tbName = 'user';
    public $pkField = 'user_id';
    public $fieldList = array('user_type','verified','verified_date','firstname','lastname','birth_date','email','password','address','district_id','amphur_id','province_id','zipcode','mobile');
    public $a_search = array('firstname','lastname','email');
    public $path = '_admin/user';
    public $page = 'user';
    public $viewPath = 'backend/user';

    public function __construct()
    {
        $this->middleware('admin');
    }

    // ----------------------------------------- Show All List Page
    public function index()
    {
        $perPage = Config::get('mainConfig.perPage');
        $orderBy = Input::get('orderBy');
        if(empty($orderBy)) $orderBy = $this->pkField;
        $sortBy = Input::get('sortBy');
        if(empty($sortBy)) $sortBy = 'desc';

        $search = Input::get('search');

        $model = $this->model;
        $data = $model::whereNull('deleted_at');

        if(!empty($search))
        {
            $data = $data->where(function ($query) use($search){
                foreach($this->a_search as $field)
                {
                    $text = explode(' ',$search);

                    foreach ($text as $value){
                        $query = $query->orWhere($field, 'like', '%'.$value.'%');
                    }

                }
            });
        }

        if(!empty(Input::get('from_date')) && !empty(Input::get('to_date')) )
            $data = $data->whereBetween('created_at',[Input::get('from_date').' 00:00:00',Input::get('to_date').' 23:59:59']);

        if(Input::get('user_type') != '')
            $data = $data->where('user_type',Input::get('user_type'));

        if(!empty(Input::get('export')) && Input::get('export')=="true"){
            $type = Input::get('type');
            $countData = $data->count();
            $data = $data
                ->orderBy($orderBy, $sortBy);
            $data = $data->take(Config::get('mainConfig.setTake'))->get();
            foreach($data as $point){
              $data_transaction =  PointTransaction::where('user_id', $point->user_id)->orderBy('point_trans_id','desc')->limit(1)->first();
              $point->total_point = $data_transaction['total'];

            }
            if($type==1){
                return view($this->viewPath.'/export_report',compact('data','countData','countVIP','countNomal'));
            }else if($type==2){
//                $get_all_data = Orders::join('user', 'user.user_id', '=', 'orders.user_id')
//                        ->join('orders_detail', 'orders_detail.orders_id', '=', 'orders.orders_id')
//                        ->select('orders.orders_id','orders.user_id','orders.total_price','orders.discount_price','orders.shipping_price','user.firstname','user.lastname','user.line',DB::raw('SUM(orders_detail.qty) as qty'))
//                        ->get();
//                $success_buy =$this->memberRankByPrice($get_all_data);

                $get_all_data = Orders::leftjoin('user', 'user.user_id', '=', 'orders.user_id')
//                        ->join('orders_detail', 'orders_detail.orders_id', '=', 'orders.orders_id')
                        ->select('orders.orders_id','orders.user_id','orders.total_price','orders.discount_price','orders.shipping_price','user.firstname','user.lastname','user.line',DB::raw('SUM(orders.total_price) as total_price'),DB::raw('COUNT(orders.orders_id) as count_orders '))
                    ->whereNull('orders.deleted_at')
                    ->orderBy('total_price','desc')
                    ->groupBy('user_id')
                        ->get();

                return view($this->viewPath.'/export_report_member',compact('data','countData','get_all_data'));
            }
        }else{
            $countData = $data->count();
            $data = $data
                ->orderBy($orderBy, $sortBy)
                ->paginate($perPage);
            $data->setPath($this->page);
            $data->lastPage();

            return view($this->viewPath.'/index',compact('data','countData'));
        }

    }

    // ----------------------------------------- View Add Page
    public function create()
    {

    }

    // ----------------------------------------- Record Data
    public function store(Request $request)
    {

    }

    // ----------------------------------------- Show Data : ID
    public function show($id)
    {

    }

    // ----------------------------------------- View Update Page
    public function edit($id)
    {
        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = "Update";
        Session::put('referUrl',URL::previous());

        $model = $this->model;
        $data = $model::find($id);

        $district = District::all();
        $district_id = Input::get('district_id');

        $amphur = Amphur::all();
        $amphur_id = Input::get('amphur_id');

        $province = Province::all();
        $province_id = Input::get('province_id');

        return view($this->viewPath.'/update',compact('data','url_to','method','txt_manage','district','district_id','amphur','amphur_id','province','province_id','id'));
    }

    // ----------------------------------------- Record Update Data
    public function update(Request $request, $id )
    {
        $objFn = new MainFunction();
        $strParam = $request->strParam;

            $model = $this->model;
            $data = $model::find($id);

            if (!empty($request->password_new)) {
                $request->password = Hash::make($request->password_new);
            } else {
                $request->password = $request->password_old;
            }

            $objFn->db_update($data, $this->pkField, $request, $this->fieldList);

            return Redirect::to($this->path . '?' . $strParam);
    }

    // ----------------------------------------- Delete Data
    public function destroy($id)
    {
        Session::put('referUrl',URL::previous());
        $model = $this->model;
        $model::find($id)->delete();

        return Redirect::to(Session::get('referUrl'));
    }
    public function memberRankByPrice($get_all_data){

      $member= [];
      foreach ($get_all_data as  $getMember) {
        $name = $getMember['firstname']." ".$getMember['lastname'];
      $array_ini = array(
          $name => array(
            "membername" => '',
             "line" => '',
              "total_order" => '',
            "price" => 0
        ));
        $member = array_merge($member, $array_ini);
      }
      // echo $get_all_data;
      foreach ($get_all_data as  $getMember) {
        $sum_price = $getMember['total_price'] - $getMember['discount_price'] + $getMember['shipping_price'];
        $name = $getMember['firstname']." ".$getMember['lastname'];
         $line  = $getMember['line'];
        $aa  = $member[$name]['price'] + $sum_price;

          $array_current = array(
            $name => array(
              "membername" => $name,
               "line" => $line,
                "total_order" => '',
              "price" => $aa
          ));
          $member = array_merge($member, $array_current);

      }
      usort($member, array($this, 'sortHightoLow'));
      return json_encode($member);
    }

    public function postCheckVerified(Request $request)
    {
        $user_id = $request->user_id;
        $data = User::find($user_id);
        $data->verified = 1;
        $data->verified_date = date('Y-m-d H:i:s');
        $data->save();

        return redirect()->back();
    }

    public function sortHightoLow($a, $b){
      if ($a['price']==$b['price']) return 0;
      return ($a['price']>$b['price'])?-1:1;
    }
}
