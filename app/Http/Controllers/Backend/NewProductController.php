<?php
namespace App\Http\Controllers\Backend;

use App\Model\Category;
use App\Model\Discount;
use App\Model\ProductCategory;
use App\Model\ProductOption;
use App\Model\ProductGallery;
use App\Model\SizeChart;
use App\Model\Special;
use Illuminate\Http\Request;
use App\Model\Product;
use App\Model\ProductRelated;
use App\Model\Tag;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;
use Excel;

class NewProductController extends Controller
{
    public $model = 'App\Model\Product';
    public $titlePage = 'New Product';
    public $tbName = 'product';
    public $pkField = 'product_id';
    public $fieldList = array('product_name_en','product_name_th', 'detail_th','detail_en','retail_price','special_price','minimum_qty', 'status','look_book','size_chart_id','img_new_product','new_sorting');
    public $fieldList2 = array('product_id', 'option_name_th', 'option_name_en','qty', 'model');
    public $a_search = array('product.product_name_en','product.product_name_th','product_option.model');
    public $path = '_admin/new_product';
    public $path2 = '_admin/product_option';
    public $page = 'new_product';
    public $viewPath = 'backend/new_product';

    public function __construct()
    {
        $this->middleware('admin');
    }

    // ----------------------------------------- Show All List Page
    public function index(Request $request)
    {
        $perPage = Config::get('constants.perPage');
        $orderBy = Input::get('orderBy');
        if (empty($orderBy)) $orderBy = $this->pkField;
        $sortBy = Input::get('sortBy');
        if (empty($sortBy)) $sortBy = 'desc';

        $option_group_id = Input::get('option_group_id');
        $search = Input::get('search');
        $new_product = Input::get('new_product');
        $recommend = Input::get('recommend');

        $model = $this->model;
        $data = $model::leftjoin('product_option','product_option.product_id','=','product.product_id')->where('new_product','1');
//        $data = new $model;

        if (!empty($search)) {
            $data = $data->where(function ($query) use ($search) {
                foreach ($this->a_search as $field) {
                    $query = $query->orWhere($field, 'like', '%' . $search . '%');
                }
            });
        }

        if(!empty(Input::get('export')) && Input::get('export')=="true"){
            $type = Input::get('type');
            $countData = $data->count();
            $data = $data
                ->orderBy('product.'.$orderBy, $sortBy);
            $data = $data->take(Config::get('mainConfig.setTake'))->get();
            return view($this->viewPath.'/export_report',compact('data','countData'));
            
        }else{
            $countData = $data->count();
            $data = $data
                ->orderBy('product.'.$orderBy, $sortBy)
//                ->groupBy('product_id')
                ->paginate($perPage);
            $data->setPath($this->page);
            $data->lastPage();

            foreach ($data as $item) {
                // $item->photo = ProductOption::join('product_option_gallery', 'product_option_gallery.product_option_id' , '=' , 'product_option.product_option_id')
                //     ->where('product_id', $item->product_id)
                //     ->whereNotNull('product_option_gallery.photo')
                //     ->whereNull('product_option.deleted_at')
                //     ->whereNull('product_option_gallery.deleted_at')
                //     ->first();

                $item->photo = ProductGallery::where('product_id', $item->product_id)
                              ->whereNotNull('photo')
                              ->first();
            }


            foreach ($data as $category) {
                $category_data = ProductCategory::join('category', 'product_category.category_id', '=', 'category.category_id')->where('product_id', $category->product_id)->get();
                $category->categorys = $category_data;


            }

            return view($this->viewPath . '/index', compact('data', 'countData', 'option_group_id', 'tag_name'));
        }

    }

    // ----------------------------------------- View Add Page
    public function create()
    {
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";

        $category = Category::all();
        $countCategory = "";

        $size_charts = SizeChart::all();
        $product_option = ProductOption::all();

        $countProduct = "";

        return view($this->viewPath . '/update', compact('url_to', 'method', 'txt_manage','category','countCategory','countProduct','size_charts','product_option'));
    }

    // ----------------------------------------- Record Data
    public function store(Request $request)
    {
        $objFn = new MainFunction();
        $model = $this->model;
        $data = new $model;

        $id = $objFn->db_add($data, $this->pkField, $request, $this->fieldList);

        $product_id = $id;
        $category_id = $request->input('category_id');
        $related_id = $request->input('related_id');

        if (!empty($category_id)) {

            foreach ($category_id as $cat) {

                $product_category = new ProductCategory();

                $product_category->product_id = $product_id;
                $product_category->category_id = $cat;
                $product_category->save();


                $data_cat = DB::table('category')->select('count_product','category_id')->where('category_id',$cat)->first();

                $count = $data_cat->count_product+1;
                DB::table('category')->where('category_id',$data_cat->category_id)->update(['count_product' => $count]);

            }
        }


        if(!empty($related_id)){

            foreach($related_id as $related){

                $product_related = new ProductRelated();

                $product_related->product_id = $product_id;
                $product_related->related_id = $related;
                $product_related->save();
            }

        }


        if (Input::hasFile('img_path')) { // เพิ่มตรงนี้
            $photo = $request->file('img_path');                    // get image from form
            $new_name = date('YmdHis').'-'.rand(10000, 99999);                             // set new name
            $path = public_path('uploads/banner_product');           // set path
            $extension = $photo->getClientOriginalExtension();      // get extension
            $filename = $new_name . "." . $extension;               // set filename
            $destinationPath = $path;
            $objFn->img_full_resize($photo, $destinationPath, $filename);   //
            $objFn->image_resize($photo, $destinationPath,100, $filename);   // resize image
            $objFn->image_resize($photo, $destinationPath,635, $filename);   // resize image
            $data = $model::find($id);
            $data->img_new_product = $filename;
            $data->save();
        }

        return Redirect::to($this->path2 . '/create?product_id=' . $product_id);
    }

    // ----------------------------------------- Show Data : ID
    public function show($id)
    {

    }

    // ----------------------------------------- View Update Page
    public function edit($id)
    {
        $url_to = $this->path . '/' . $id;
        $method = 'PUT';
        $txt_manage = "Update";
        Session::put('referUrl', URL::previous());
        $product_option = ProductOption::all();

        $model = $this->model;
        $data = $model::find($id);

        $product_id = $id;
        $product_cat = ProductCategory::select('category_id')->where('product_id', $product_id)->get();

        if (count($product_cat) == 0) {
            $category = Category::all();
        } else {

            $category = Category::whereNotIn('category_id', $product_cat)->get();
        }
        $countCategory = $category->count();
        $product_related = ProductRelated::select('related_id')->where('product_id', $product_id)->get();
        $countProduct = $product_related->count();

        $size_charts = SizeChart::all();

        return view($this->viewPath . '/update', compact('data', 'url_to', 'method', 'txt_manage','name_th','name_en','product_related','countProduct','category','product_cat','countCategory','product_id','size_charts','product_option'));
    }

    // ----------------------------------------- Record Update Data
    public function update(Request $request, $id)
    {
        $objFn = new MainFunction();
        $strParam = $request->strParam;

        $model = $this->model;
        $data = $model::find($id);
        $objFn->db_update($data, $this->pkField, $request, $this->fieldList);

        $product_id = $id;
        $category_id = $request->input('category_id');
        $related_id = $request->input('related_id');

        if (!empty($category_id)) {

            foreach ($category_id as $cat) {

                $count_pc = DB::table('product_category')->where('category_id',$cat)->where('product_id',$id)->count();

                if($count_pc==0){
                    $product_category = new ProductCategory();

                    $product_category->product_id = $product_id;
                    $product_category->category_id = $cat;
                    $product_category->save();

                    $data_cat = DB::table('category')->select('count_product','category_id')->where('category_id',$cat)->first();

                    $count = $data_cat->count_product+1;
                    DB::table('category')->where('category_id',$data_cat->category_id)->update(['count_product' => $count]);
                }

            }
        }

        if(!empty($related_id)){

            foreach($related_id as $related){

                $product_related = new ProductRelated();

                $product_related->product_id = $product_id;
                $product_related->related_id = $related;
                $product_related->save();
            }

        }

        if (Input::hasFile('img_path')) { // test P.
            $photo = $request->file('img_path');                    // get image from form
            $new_name = date('YmdHis').'-'.rand(10000, 99999);                             // set new name
            $old_name = $data->img_new_product;                            // get old name
            $path = public_path('uploads/banner_product');           // set path
            $objFn->del_storage($path,$old_name);                   // delete old picture in storage
            $objFn->del_storage($path.'/100/',$old_name);                   // delete old picture in storage
            $extension = $photo->getClientOriginalExtension();      // get extension
            $filename = $new_name . "." . $extension;               // set filename
            $destinationPath = $path;
            $objFn->img_full_resize($photo, $destinationPath, $filename);   // resize image
            $objFn->image_resize($photo, $destinationPath,100, $filename);   // resize image
            $objFn->image_resize($photo, $destinationPath,635, $filename);   // resize image
            $data = $model::find($id);
            $data->img_new_product = $filename;
            $data->save();
        }

        return Redirect::to($this->path . '?' . $strParam);

    }

    // ----------------------------------------- Delete Data
    public function destroy($id)
    {
        Session::put('referUrl', URL::previous());
        $model = $this->model;

        $data_cat = DB::table('product_category')->select('category_id')->where('product_id',$id)->get();

        foreach ($data_cat as $value){
            $data_cat = DB::table('category')->select('count_product','category_id')->where('category_id',$value->category_id)->first();
            $count = $data_cat->count_product-1;
            DB::table('category')->where('category_id',$data_cat->category_id)->update(['count_product' => $count]);
        }

        $data_product = DB::table('related_product')->select('related_id')->where('product_id',$id)->get();

        foreach ($data_product as $value){
            ProductRelated::where('related_id', $value->related_id)->delete();
        }

//        $data = $model::select("photo","product_option_gallery_id")
//            ->leftjoin('product_option','product_option.product_id','=','product.product_id')
//            ->leftjoin('product_option_gallery','product_option_gallery.product_option_id','=','product_option.product_option_id')
//            ->where("product.product_id",$id)
//            ->get();
//
//        foreach ($data as $value){
//
//            $path = ('uploads/product_option/');           //// set path
//            $path2 = ('uploads/product_option/250/');           //// set path
//            $path3 = ('uploads/product_option/500/');           //// set path
//
//            $file = "$value->photo";
//            if($file != '') {
//                if (unlink($path.$file))    { }                    // delete image in public path
//                if (unlink($path2.$file))   { }                 // delete resize image
//                if (unlink($path3.$file))   { }              // delete resize image
//            }
//
//            DB::table('product_option_gallery')->where('product_option_gallery_id',$value->product_option_gallery_id)->delete();
//
//
//        }

        $model::find($id)->delete();

//        ProductTag::where('product_id', $id)->delete();
        ProductCategory::where('product_id', $id)->delete();
//        Special::where('product_id', $id)->delete();
        ProductOption::where('product_id', $id)->delete();

        return Redirect::to(Session::get('referUrl'));
    }

    //----------------------------------------- tag product

    public function postTagProduct(Request $request)
    {

        if ($request->add_tag) {                                     // click add tag

            $product_chk = $request->input('product_chk');
            $tag_name = Input::get('tag_name');

            if (!empty($tag_name)) {                                //check input tag name
                $tag = Tag::where('tag_name_th', $tag_name)->first();
                if (count($tag) == 0) {                            //if don't have tag name in database
                    $set_tag = new Tag();                          //save tag
                    $set_tag->tag_name_th = $tag_name;
                    $set_tag->tag_name_en = $tag_name;
                    $set_tag->save();
                }
            }

            if (!empty($product_chk)) {
                foreach ($product_chk as $p_tag) {

                    if (!empty($tag_name)) {

                        $tag_find = Tag::where('tag_name_th', $tag_name)->first();
                        $set_data = Tag::find($tag_find->tag_id);
                        $product = ProductTag::where('product_id', $p_tag)->where('tag_id', $set_data->tag_id)->first();

                        if (count($product) == 0) {
                            $product_tag = new ProductTag();
                            $product_tag->product_id = $p_tag;
                            $product_tag->tag_id = $set_data->tag_id;
                            $product_tag->save();
                        }
                    } else {

                        return redirect()->back();

                    }
                }
            }

            return redirect()->back();

        }else if($request->ok_set) {
            $products = $request->input('product_chk');
            $new_product = $request->new_product;
            $recommend = $request->recommend;

            if(!empty($products)) {

                if ($new_product != "") {
                    if ($recommend != "") {

                        foreach ($products as $product_id) {

                            Product::where('product_id', $product_id)->update(['new_product' => $new_product , 'recommend'=> $recommend]);

                        }

                    } else {

                        foreach ($products as $product_id) {

                            Product::where('product_id', $product_id)->update(['new_product' => $new_product]);

                        }
                    }
                } else if ($recommend != "") {

                    foreach ($products as $product_id) {

                        Product::where('product_id', $product_id)->update(['recommend'=> $recommend]);

                    }

                } else {

                    return redirect()->back();
                }



            }else{

                return redirect()->back();
            }

            return redirect()->back();

        }else {     // click del tag


            $product_chk = $request->input('product_chk');
            $tag_name = Input::get('tag_name');

            if (!empty($tag_name)) {                                        //check input tag name
                $tag = Tag::where('tag_name_th', $tag_name)->first();
                if (count($tag) == 0) {                                     //if don't have tag name in database
                    return redirect()->back();
                }
            }

            if (!empty($product_chk)) {
                foreach ($product_chk as $d_tag) {

                    if (!empty($tag_name)) {

                        $tag_find = Tag::where('tag_name_th', $tag_name)->first();
                        $del_data = Tag::find($tag_find->tag_id);
                        $product = ProductTag::where('product_id', $d_tag)->where('tag_id', $del_data->tag_id)->get();

                        if (count($product) <= 1) {
                            $product_tag = new ProductTag();
                            $product_tag->product_id = $d_tag;
                            $product_tag->tag_id = $del_data->tag_id;
                            ProductTag::where('product_id', $d_tag)->where('tag_id', $del_data->tag_id)->delete();
                        }
                    } else {

                        return redirect()->back();
                    }
                }
            }

            return redirect()->back();
        }
    }

    public function postCopyProduct(Request $request)
    {
        $model = $this->model;
        $fieldList = $this->fieldList;
        $fieldList2 = $this->fieldList2;
        $id = $request->product_id;

        $data_product = $model::where('product_id',$id)->first();
        $product = new Product();

        foreach ($fieldList as $value){
            $product->$value = $data_product->$value;
        }

        $product->save();
        $id_product = $product->product_id;

//        $data_product_option = DB::table('product_option')->where('product_id',$id)->get();
//        $product_option = new ProductOption();
//
//        foreach ($data_product_option as $option){
//
//            foreach ($fieldList2 as $value){
//                $product_option->$value = $option->$value;
//            }
//            $product_option->product_id = $id_product;
//            $product_option->save();
//        }

        $data_category = DB::table('product_category')->where('product_id',$id)->get();

        foreach ($data_category as $value2){
            $category = new ProductCategory;
            $category->product_id = $id_product;
            $category->category_id = $value2->category_id;
            $category->save();

            $data_cat = DB::table('category')->select('count_product','category_id')->where('category_id',$value2->category_id)->first();

            $count = $data_cat->count_product+1;
            DB::table('category')->where('category_id',$data_cat->category_id)->update(['count_product' => $count]);

        }

        return redirect()->back();

    }
}
