<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Model\SizeChartDetail;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;
use Validator;

class SizeChartDetailController extends Controller
{
    public $model = 'App\Model\SizeChartDetail';
    public $titlePage = 'Size Chart Detail';
    public $tbName = 'size_chart_detail';
    public $pkField = 'size_chart_detail_id';
    public $fieldList = array('size_chart_id', 'part', 'size_name', 'value');
    public $a_search = array('size_chart.chart_name_th');
    public $path = '_admin/size_chart_detail';
    public $page = 'size_chart_detail';
    public $viewPath = 'backend/size_chart_detail';

    public function __construct()
    {
        $this->middleware('admin');
    }

    // ----------------------------------------- Show All List Page
    public function index(Request $request)
    {

        $perPage = Config::get('mainConfig.perPage');
        $orderBy = Input::get('orderBy');
        if(empty($orderBy)) $orderBy = $this->pkField;
        $sortBy = Input::get('sortBy');
        if(empty($sortBy)) $sortBy = 'desc';

        $search = Input::get('search');

        $size_chart_id = Input::get('size_chart_id');
        $model = $this->model;

        $data = new $model;
        $data = $model::leftjoin('size_chart','size_chart.size_chart_id','=','size_chart_detail.size_chart_id')->where('size_chart.size_chart_id', $size_chart_id);

        if(!empty($search))
        {
            $data = $data->where(function ($query) use($search){
                foreach($this->a_search as $field)
                {
                    $query = $query->orWhere($field, 'like', '%'.$search.'%');
                }
            });
        }
        $countData = $data->count();
        $data = $data
            ->orderBy($orderBy,$sortBy)
            ->paginate($perPage);
        $data->setPath($this->page);
        $data->lastPage();

        $parts = Config::get('constants.part');

 //      $category_id = Input::get('category_id');
//        $parent_category_name = $model::select("category_name")
//            ->where("parent_category_id",$category_id)
//            ->get();




        return view($this->viewPath.'/index',compact('data','countData', 'size_chart_id', 'parts'));
    }

    // ----------------------------------------- View Add Page
    public function create(Request $request)
    {
        $parent_id = $request->size_chart_id;
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";
        $model = $this->model;

        $parts = Config::get('constants.part')['th'];

        return view($this->viewPath.'/update',compact('url_to','method','txt_manage', 'parent_id', 'parts'));
    }

    // ----------------------------------------- Record Data
    public function store(Request $request)
    {
        $objFn = new MainFunction();
        $model = $this->model;
        $data = new $model;
        $strParam = $request->strParam;

        $validator = Validator::make($request->all(), [
            'part' => 'required',
            'size_name' => 'required',
            'value' => 'required'
        ]);

        if ($validator->fails()){
            return Redirect::back()->withErrors($validator)->withInput();
        }
        else{
            $id = $objFn->db_add($data,$this->pkField,$request,$this->fieldList);

            $data->save();
        }

        return Redirect::to($this->path.'?'.$strParam);
    }

    // ----------------------------------------- Show Data : ID
    public function show($id)
    {

    }

    // ----------------------------------------- View Update Page
    public function edit(Request $request, $id)
    {
        $parent_id = $request->size_chart_id;
        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = "Update";
        Session::put('referUrl',URL::previous());

        $model = $this->model;
        $data = $model::find($id);

        $parts = Config::get('constants.part')['th'];

        return view($this->viewPath.'/update',compact('parent_id','data','url_to','method','txt_manage','parts'));
    }

    // ----------------------------------------- Record Update Data
    public function update(Request $request, $id )
    {
        $objFn = new MainFunction();
        $strParam = $request->strParam;

        $model = $this->model;
        $data = $model::find($id);

        $validator = Validator::make($request->all(), [
            'part' => 'required',
            'size_name' => 'required',
            'value' => 'required'
        ]);

        if ($validator->fails()){
            return Redirect::back()->withErrors($validator)->withInput();
        }
        else{
            $id = $objFn->db_update($data,$this->pkField,$request,$this->fieldList); // add $id
            $data->save();
        }

        return Redirect::to($this->path.'?'.$strParam);
    }

    // ----------------------------------------- Delete Data
    public function destroy($id)
    {
        Session::put('referUrl',URL::previous());
        $model = $this->model;

        $model::find($id)->delete();

        return Redirect::to(Session::get('referUrl'));
    }


}
