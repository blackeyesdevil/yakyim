<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Model\Orders;
use App\Model\Status;
use App\Library\MainFunction;
use App\Library\OrderFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;
use Mail;

class OrdersWareHouseController extends Controller
{
    public $model = 'App\Model\Orders';
    public $titlePage = 'Orders ( WareHouse )';
    public $tbName = 'orders';
    public $pkField = 'orders_id';
    public $fieldList = array('orders_no','orders_date','user_id', 'billing_address_id', 'billing_address', 'shipping_address_id', 'shipping_address', 'payment_method_id','paysbuy_method', 'payment_by','payment_date', 'total_price','discount_price','shipping_price','total_point','status_id','tracking_no');
    public $a_search = array('orders_no','user_id');
    public $path = '_admin/orders_warehouse';
    public $page = 'orders_warehouse';
    public $viewPath = 'backend/orders_warehouse';

    public function __construct()
    {
        $this->middleware('admin');
    }

    // ----------------------------------------- Show All List Page
    public function index()
    {
        $perPage = Config::get('mainConfig.perPage');
        $orderBy = Input::get('orderBy');
        if (empty($orderBy)) $orderBy = $this->pkField;
        $sortBy = Input::get('sortBy');
        if (empty($sortBy)) $sortBy = 'desc';
        $user_id = input::get('user_id');
        $search = Input::get('search');
        $status = Input::get('status');
        $payment = Input::get('payment_by');

        $model = $this->model;
        $data = $model::leftjoin('user','user.user_id','=','orders.user_id')->whereNull('orders.deleted_at')->WhereIn('orders.status_id',[3,4,6,7,8,9,10]);

        if (!empty($search)) {
            $data = $data->where(function ($query) use ($search) {
                foreach ($this->a_search as $field) {
                    $query = $query->orWhere($field, 'like', '%' . $search . '%');
                }
            });
        }

        if(!empty($status)){
            $data = $data->Where('orders.status_id', 'like', '%' . $status . '%');
        }

        if(!empty($payment)){
            $data = $data->Where('orders.payment_by', 'like', '%' . $payment . '%');
        }

        if(!empty(Input::get('from_date')) && !empty(Input::get('to_date')) )
            $data = $data->whereBetween('orders_date',[Input::get('from_date').' 00:00:00',Input::get('to_date').' 00:00:00']);

        $countData = $data->count();
        $data = $data
            ->orderBy($orderBy, $sortBy)
            ->paginate($perPage);
        $data->setPath($this->page);
        $data->lastPage();

        return view($this->viewPath . '/index', compact('data','countData','user_id'));
    }

    // ----------------------------------------- View Add Page
    public function create()
    {
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";

        $data2 = Status::all();


        return view($this->viewPath . '/update', compact('url_to', 'method', 'txt_manage','data2'));
    }

    // ----------------------------------------- Record Data
    public function store(Request $request)
    {
        $objFn = new MainFunction();
        $model = $this->model;
        $data = new $model;
        $id = $objFn->db_add($data, $this->pkField, $request, $this->fieldList);


        return Redirect::to($this->path);
    }

    // ----------------------------------------- Show Data : ID
    public function show($id)
    {

        $url_to = $this->path.'/'.$id;
        Session::flash('referUrl',URL::previous());

        $id2 = substr($id,10);
        $data = DB::Table('orders')->whereNull('deleted_at')->where('orders_id',$id2)->first();

        return view($this->viewPath.'/print',compact('data','url_to','id'));


    }

    // ----------------------------------------- View Update Page
    public function edit($id)
    {
        $url_to = $this->path . '/' . $id;
        $method = 'PUT';
        $txt_manage = "Update";
        Session::put('referUrl', URL::previous());

        $data2 = Status::all();


        $model = $this->model;
        $data = $model::find($id);
        return view($this->viewPath . '/update', compact('data', 'url_to', 'method', 'txt_manage','data2'));
    }

    // ----------------------------------------- Record Update Data
    public function update(Request $request, $id)
    {
        $objFn = new MainFunction();
        $objFnOrder = new OrderFunction();
        $strParam = $request->strParam;

        $status = Input::get('status_id');

        if($status==3) {
            $objFnOrder->point($id);
            $objFnOrder->sendMailWh($id);
            $objFnOrder->sendMailUser($id, "การชำระเงินเสร็จเรียบร้อย");
        }

        if($status==7) {
            $objFnOrder->sendMailCancel($id);
        }
        Orders::where('orders_id', $id)->update(['status_id' => $status]);

        return Redirect::to($this->path.'?'.$strParam);
    }

    // ----------------------------------------- Delete Data
    public function destroy($id)
    {
        Session::put('referUrl', URL::previous());
        $model = $this->model;
        $model::find($id)->delete();

        return Redirect::to(Session::get('referUrl'));
    }
//-------------------------------------------------------
    public function chk_status(Request $request)
    {
        $objFnOrder = new OrderFunction();

        $order_chk = $request->order_chk;
        $status = Input::get('status_s');

        if (!empty($status)) {

            if (!empty($order_chk)) {

                foreach ($order_chk as $order_id) {

                    if($status==3) {
                        $objFnOrder->point($order_id);
                        $objFnOrder->sendMailWh($order_id);
                        $objFnOrder->sendMailUser($order_id, "การชำระเงินเสร็จเรียบร้อย");
                    }

                    if($status==7) {
                        $objFnOrder->sendMailCancel($order_id);
                    }
                        Orders::where('orders_id', $order_id)->update(['status_id' => $status]);
                }

            }else{

                return redirect()->back();
            }
        }

        return redirect()->back();




//        $model = $this->model;
//
//        $data = $model::find($value);
//        $data->STAUS = "Y";
//        $data->save();


       // return Redirect::to($this->path);

    }

    public function update_tracking(Request $request)
    {
        
        $objFnOrder = new OrderFunction();

        $orders_id = $request->orders_id;
        $tracking_no = $request->tracking_no;

        $objFnOrder->sendMailUser($orders_id, "แจ้ง Tracking No.");
        Orders::where('orders_id', $orders_id)->update(['status_id' => "11" , 'tracking_no' => $tracking_no]);

        return redirect()->back();

    }

    //-------------------------------------------


//return redirect()->back();


}

