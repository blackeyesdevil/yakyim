<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Model\Orders;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;
use Mail;
use Excel;
class DashboardController extends Controller
{
    public $model = 'App\Model\Orders';
    public $titlePage = 'Orders';
    public $tbName = 'orders';
    public $pkField = 'orders_id';
    public $fieldList = array('orders_no','orders_date','user_id', 'billing_address_id', 'billing_address', 'shipping_address_id', 'shipping_address', 'payment_method_id','paysbuy_method', 'payment_by','payment_date', 'total_price','discount_price','shipping_price','total_point','status_id','tracking_no');
    public $a_search = array('');
    public $path = '_admin/orders';
    public $page = 'orders';
    public $viewPath = 'backend';

    public function __construct()
    {
        $this->middleware('admin');
    }

    // ----------------------------------------- Show All List Page
    public function index()
    {
        $count = '';

        $data_product_qty = DB::table('product')->leftjoin('product_option','product_option.product_id','=','product.product_id')->select('product_option.option_name_th','product.product_id','product.product_name_th','product.minimum_qty','product_option.qty','product_option.product_option_id');

        $data_product = DB::table('product')->select('product_id','product_name_th','minimum_qty')->whereNull('deleted_at');
        $count_product = $data_product->count();
        $data_product = $data_product->get();


        foreach ($data_product as $value){
            $data_product_option = DB::table('product_option')->where('product_id',$value->product_id)->where('qty','<=',$value->minimum_qty)->select('qty');
            $count = $data_product_option->count();
            $data_product_option = $data_product_option->get();

            if($count > 0) {

                foreach ($data_product_option as $value2) {
                    if (!empty($value2)) {
                        $data_product_id[] = $value->product_id;
                    }
                }

            }

        }

        if($count > 0) {
            $data_product_qty = $data_product_qty->WhereIn('product.product_id', $data_product_id)->whereNull('product.deleted_at')->get();
        }

        $model = $this->model;
        $data_order = $model::whereNull('deleted_at')->select('orders_id','orders_no','orders_date','total_price','user_id','status_id');
        $count_order = $data_order->count();
        $data_order = $data_order->orderBy('orders_id','desc')->take(10)->get();

        $total_order = DB::table('orders')->WhereNull('deleted_at')->where('status_id','=','2')->count();

        $total_paid = DB::table('orders')->WhereNull('deleted_at')->whereIn('status_id',[3,4,6,11]);
        $count_paid = $total_paid->count();
        $total_paid = $total_paid->get();

        $price = "";
        $total_price = "";

        if($count_paid != 0){
            foreach ($total_paid as $value2){
                $price += $value2->total_price;
            }
            $total_price = $price/$count_paid;
        }


        // -------------------------------------------------
        // Top 10 member buy product
        $get_all_data =$model::join('user', 'user.user_id', '=', 'orders.user_id')
                                ->select('orders.orders_id','orders.user_id','orders.total_price','orders.discount_price','orders.shipping_price','user.firstname','user.lastname','user.email')
                                ->get();
        $success_buy =$this->memberRankByPrice($get_all_data);

        // -------------------------------------------------
        // Top 10 product view

        $data_product_view = DB::table('product')->select('product_id','product_name_th','product_name_en','view')->whereNull('deleted_at');
        $get_product_views = $data_product_view->orderBy('view','desc')->take(10)->get();

        $data_product_sell = DB::table('orders_detail')->join('product_option','product_option.product_option_id','=','orders_detail.product_option_id')
                              ->select('orders_detail.product_id','orders_detail.product_option_id','orders_detail.product_name_th','orders_detail.qty','product_option.option_name_th')
                              ->whereNull('orders_detail.deleted_at');
        $get_product_sell = $data_product_sell->get();
        $get_product_sells =$this->productTopSell($get_product_sell);
        // $get_product_sells = json_encode($get_product_sells);
        // echo json_encode($get_product_sell);

        // -------------------------------------------------
        // Daily Sales
        $daily_sales = [];
        for ($i=9; $i>=0; $i--){
            $date = strtotime('-' . $i . ' days', strtotime(date('Y-m-d')));

            $order = DB::table('orders')
                                ->select(DB::raw('SUM(total_price+shipping_price-discount_price-use_point) as sum, orders_date'))
                                ->where('orders_date', 'like', date('Y-m-d', $date) . "%")
                                ->whereIn('status_id', array('3', '4', '5', '6'))
                                ->groupBy(DB::raw('CAST(ORDERS_DATE AS DATE)'))
                                ->first();

            $key = 9-$i;
            $daily_sales[$key]['date'] = date('j M', $date);
            if (!empty($order))
                $daily_sales[$key]['sum'] = $order->sum;
            else
                $daily_sales[$key]['sum'] = 0;
        }

        // Monthly Sales
        $monthly_sales = [];
        for ($i=11; $i>=0; $i--){
            $month = strtotime('-' . $i . ' months', strtotime(date('Y-m-d')));
            $order = DB::table('orders')
                                ->select(DB::raw('SUM(total_price+shipping_price-discount_price-use_point) as sum , orders_date'))
                                ->where('orders_date', 'like', '%-' . date('m', $month) . '-%')
                                ->whereIn('status_id', array('3', '4', '5', '6'))
                                ->groupBy(DB::raw('year(orders_date),month(orders_date)'))
                                ->first();

            $key = 11-$i;
            $monthly_sales[$key]['month'] = date('M Y', $month);
            if (!empty($order))
                $monthly_sales[$key]['sum'] = $order->sum;
            else
                $monthly_sales[$key]['sum'] = 0;
        }

        // Yearly Sales
        $yearly_sales = [];
        for ($i=5; $i>=0; $i--){
            $year = strtotime('-' . $i . ' years', strtotime(date('Y-m-d')));
            $order = DB::table('orders')
                                ->select(DB::raw('SUM(total_price+shipping_price-discount_price-use_point) as sum, orders_date'))
                                ->where('orders_date', 'like', date('Y', $year) . '-%')
                                ->whereIn('status_id', array('3', '4', '5', '6'))
                                ->groupBy(DB::raw('year(orders_date)'))
                                ->first();

            $key = 5-$i;
            $yearly_sales[$key]['year'] = date('Y', $year);
            if (!empty($order))
                $yearly_sales[$key]['sum'] = $order->sum;
            else
                $yearly_sales[$key]['sum'] = 0;
        }

        $product_soldout = DB::table('product')
            ->leftjoin('product_option','product_option.product_id','=','product.product_id')
            ->where('qty','=',0)
            ->whereNull('product.deleted_at')
            ->select('product.product_name_th','product.product_name_en','product_option.option_name_th','product_option.model','product_option.product_option_id')
            ->get();

        return view($this->viewPath . '/index', compact('data_product_qty','data_order','total_order','count_product','count_paid','total_price','count','count_order','get_all_data','success_buy','get_product_views','get_product_sells','daily_sales','monthly_sales','yearly_sales','product_soldout'));
    }

    public function exportSummary($type){
        // $type = daily, monthly, yearly
        $statusPaid = ['3', '4', '5', '6'];
        if($type == 'daily'){
            Excel::create('daily-summary-report', function($excel) use ($statusPaid) {
                $daily_sales = [];
                for ($i=9; $i>=0; $i--){
                    $date = strtotime('-' . $i . ' days', strtotime(date('Y-m-d')));
                    $order = DB::table('orders')
                        ->select(DB::raw('SUM(total_price+shipping_price-discount_price-use_point) as sale_summary, orders_date, sum(orders_id) as paid_order'))
                        ->where('orders_date', 'like', date('Y-m-d', $date) . "%")
                        ->whereIn('status_id', $statusPaid)
                        ->groupBy(DB::raw('date(orders_date)'))
                        ->first();

                    $key = 9-$i;
                    $daily_sales[$key]['orders_date'] = date('d/m/Y', $date);
                    if (!empty($order)){
                        $daily_sales[$key]['paid_order'] = $order->paid_order;
                        $daily_sales[$key]['sale_summary'] = $order->sale_summary;
                        $daily_sales[$key]['average'] = $order->sale_summary / $order->paid_order;
                    }
                    else{
                        $daily_sales[$key]['paid_order'] = 0;
                        $daily_sales[$key]['sale_summary'] = 0;
                        $daily_sales[$key]['average'] = 0;
                    }
                }

                // Set the title
                $excel->sheet('Daily Summary Report', function($sheet) use ($daily_sales) {
                    $sheet->loadView('backend.excel.sale-summary',[ "sales" => $daily_sales ]);
                });
            })->download('xlsx');
        }
        if($type == 'monthly'){
            Excel::create('monthly-summary-report', function($excel) use ($statusPaid) {
                $monthly_sales = [];
                for ($i=11; $i>=0; $i--){
                    $month = strtotime('-' . $i . ' months', strtotime(date('Y-m-d')));
                    $order = DB::table('orders')
                        ->select(DB::raw('SUM(total_price+shipping_price-discount_price-use_point) as sale_summary , orders_date, sum(orders_id) as paid_order'))
                        ->where('orders_date', 'like', '%-' . date('m', $month) . '-%')
                        ->whereIn('status_id', $statusPaid)
                        ->groupBy(DB::raw('year(orders_date),month(orders_date)'))
                        ->first();

                    $key = 11-$i;
                    $monthly_sales[$key]['orders_date'] = date('M Y', $month);
                    if (!empty($order)) {
                        $monthly_sales[$key]['paid_order'] = $order->paid_order;
                        $monthly_sales[$key]['sale_summary'] = $order->sale_summary;
                        $monthly_sales[$key]['average'] = $order->sale_summary / $order->paid_order;
                    } else {
                        $monthly_sales[$key]['paid_order'] = 0;
                        $monthly_sales[$key]['sale_summary'] = 0;
                        $monthly_sales[$key]['average'] = 0;
                    }
                }
                // Set the title
                $excel->sheet('Monthly Summary Report', function($sheet) use ($monthly_sales) {
                    $sheet->loadView('backend.excel.sale-summary',[ "sales" => $monthly_sales ]);
                });
            })->download('xlsx');
        }
        if($type == 'yearly'){
            Excel::create('yearly-summary-report', function($excel) use ($statusPaid) {
                $yearly_sales = [];
                for ($i=5; $i>=0; $i--){
                    $year = strtotime('-' . $i . ' years', strtotime(date('Y-m-d')));
                    $order = DB::table('orders')
                        ->select(DB::raw('SUM(total_price+shipping_price-discount_price-use_point) as sale_summary , orders_date, sum(orders_id) as paid_order'))
                        ->where('orders_date', 'like', date('Y', $year) . '-%')
                        ->whereIn('status_id', $statusPaid)
                        ->groupBy(DB::raw('year(orders_date)'))
                        ->first();

                    $key = 5-$i;
                    $yearly_sales[$key]['orders_date'] = date('Y', $year);
                    if (!empty($order)) {
                        $yearly_sales[$key]['paid_order'] = $order->paid_order;
                        $yearly_sales[$key]['sale_summary'] = $order->sale_summary;
                        $yearly_sales[$key]['average'] = $order->sale_summary / $order->paid_order;
                    } else {
                        $yearly_sales[$key]['paid_order'] = 0;
                        $yearly_sales[$key]['sale_summary'] = 0;
                        $yearly_sales[$key]['average'] = 0;
                    }
                }

                // Set the title
                $excel->sheet('Yearly Summary Report', function($sheet) use ($yearly_sales) {
                    $sheet->loadView('backend.excel.sale-summary', [ "sales" => $yearly_sales ]);
                });
            })->download('xlsx');
        }
        if($type == 'top'){
            Excel::create('excel-top-saller', function($excel){
                $excel->setTitle('Excel Top Saller');
                $excel->sheet('excel-top-saller', function($sheet){
                  $data = DB::table('orders_detail')
                          ->leftjoin('product_option','product_option.product_option_id','=','orders_detail.product_option_id')
                          ->select('orders_detail.product_id','orders_detail.model','orders_detail.product_option_id','orders_detail.product_name_th','orders_detail.qty','product_option.option_name_th','orders_detail.unit_price',DB::raw('SUM(orders_detail.qty) as qty, SUM(orders_detail.unit_price*orders_detail.qty) as price'))
                          ->whereNull('orders_detail.deleted_at')
                          ->orderBy('qty','desc');
                    $data = $data->groupBy('orders_detail.model');
                    $data = $data->get();
                    $sheet->loadView('backend.excel.top_saller', [ "data" => $data ]);
                });
            })->export('xlsx');
        }
        if($type == 'unsaleable'){
            Excel::create('excel-unsaleable', function($excel){
                $excel->setTitle('Excel Unsaleable');
                $excel->sheet('excel-unsaleable', function($sheet){
                    $date_time = Carbon::now()->subDay(14);
                    $date_str = date('Y-m-d', strtotime($date_time)).' 00:00:00';
                    $stock_log = DB::table('stock_log')->select('product_option_id')->where('created_at', '>=', $date_str)->get();
                    if(!empty($stock_log) && $stock_log != ''){
                        $a_stock = [];
                        foreach ($stock_log as $value) {
                            $a_stock[] = $value->product_option_id;
                        }
                        $product = DB::table('product')
                            ->leftjoin('product_option','product_option.product_id','=','product.product_id')
                            // ->leftjoin('product_stock','product_stock.product_option_id','=','product_option.product_option_id')
                            ->select('product_option.product_option_id','product_option.model','product.product_name_th','product_option.option_name_th','product_option.qty','product.created_at')
                            // ->select('product_option.model','product.product_name_th','product_option.option_name_th','product_stock.cost','product_option.qty')
                            ->whereNotIn('product_option.product_option_id', $a_stock)
                            ->orderBy('product.created_at','asc')
                            ->get();
                        $sheet->loadView('backend.excel.unsaleble', [ "data" => $product ]);
                    }else{
                        $product = DB::table('product')
                            ->leftjoin('product_option','product_option.product_id','=','product.product_id')
                            // ->leftjoin('product_stock','product_stock.product_option_id','=','product_option.product_option_id')
                            ->select('product_option.product_option_id','product_option.model','product.product_name_th','product_option.option_name_th','product_option.qty','product.created_at')
                            // ->select('product_option.model','product.product_name_th','product_option.option_name_th','product_stock.cost','product_option.qty')
                            ->orderBy('product.created_at','asc')
                            ->get();
                        $sheet->loadView('backend.excel.unsaleble', [ "data" => $product ]);

                    }
                });
            })->export('xlsx');
        }
        if($type == 'member'){
            Excel::create('excel-member-top', function($excel){
                $excel->setTitle('Excel Member Top');
                $excel->sheet('excel-member-top', function($sheet){
                    $get_all_data = Orders::leftjoin('user', 'user.user_id', '=', 'orders.user_id')
                        ->select('orders.orders_id','orders.user_id','orders.total_price','orders.discount_price','orders.shipping_price','user.firstname','user.lastname','user.line',DB::raw('SUM(orders.total_price) as total_price'),DB::raw('COUNT(orders.orders_id) as count_orders '))
                        ->whereNull('orders.deleted_at')
                        ->orderBy('total_price','desc')
                        ->groupBy('user_id')
                        ->get();

                    $sheet->loadView('backend.excel.member_top', [ "data" => $get_all_data ]);
                });
            })->export('xlsx');
        }
    }

    public function memberRankByPrice($get_all_data){

      $member= [];
      foreach ($get_all_data as  $getMember) {
        $name = $getMember['firstname']." ".$getMember['lastname'];
      $array_ini = array(
          $name => array(
            "membername" => '',
            "price" => 0
        ));
        $member = array_merge($member, $array_ini);
      }
      // echo $get_all_data;
      foreach ($get_all_data as  $getMember) {
        $sum_price = $getMember['total_price'] - $getMember['discount_price'] + $getMember['shipping_price'];
        $name = $getMember['firstname']." ".$getMember['lastname'];
        $aa  = $member[$name]['price'] + $sum_price;

          $array_current = array(
            $name => array(
              "membername" => $name,
              "price" => $aa
          ));
          $member = array_merge($member, $array_current);

      }
      usort($member, array($this, 'sortHightoLow'));
      // $result = [];
      // $named_array = array(
      //   "Nuttavorn" => array(
      //     "foo" => "bar"
      // ));
      //
      // $named_array_2 = array(
      //   "nut2222222" => array(
      //     "foo" => "bar"
      //   ));
      // $result = array_merge($result, $named_array);
      // $result = array_merge($result, $named_array_2);
      // $result['Nuttavorn']['foo'] = 'teeettet';
      return json_encode($member);
    }
    public function sortHightoLow($a, $b){
      if ($a['price']==$b['price']) return 0;
      return ($a['price']>$b['price'])?-1:1;
    }
    public function sortHightoLowView($a, $b){
      if ($a['view']==$b['view']) return 0;
      return ($a['view']>$b['view'])?-1:1;
    }
    public function productTopSell($get_product_sell){
      $product = [];
      $product2 = [];
      foreach($get_product_sell as $sell){
        $str_key  ="'$sell->product_id$sell->product_option_id'";
        $array_current = array(
          $str_key => array(
            'product_id' => $sell->product_id,
            'product_option_id' => $sell->product_option_id,
            'product_name_th' => $sell->product_name_th,
            'option_name_th' => $sell->option_name_th,
            'view' => 0
        ));
        $product2 = array_merge($product2, $array_current);
      }

      foreach($get_product_sell as $sell){
      $str_key  ="'$sell->product_id$sell->product_option_id'";
      $view = $sell->qty + $product2[$str_key]['view'];

      $array_current = array(
        $str_key => array(
          'product_id' => $sell->product_id,
          'product_option_id' => $sell->product_option_id,
          'product_name_th' => $sell->product_name_th,
          'option_name_th' => $sell->option_name_th,
          'view' => $view
      ));
      $product2 = array_merge($product2, $array_current);

      }
      // echo json_encode($product2);
      // echo json_encode($get_product_sell);
      // echo '<br>';
      usort($product2, array($this, 'sortHightoLowView'));
      return json_encode($product2);
    }
}
