<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Model\SpecialTag;
use App\Model\Tag;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;

class SpecialTagController extends Controller
{
    public $model = 'App\Model\SpecialTag';
    public $titlePage = 'Special Tag';
    public $tbName = 'special_tag';
    public $pkField = 'special_tag_id';
    public $fieldList = array('tag_id','price','special_type','start_date','end_date');
    public $a_search = array('tag_id');
    public $path = '_admin/special_tag';
    public $page = 'special_tag';
    public $viewPath = 'backend/special_tag';

    public function __construct()
    {
        $this->middleware('admin');
    }

    // ----------------------------------------- Show All List Page
    public function index()
    {
        $perPage = Config::get('mainConfig.perPage');
        $orderBy = Input::get('orderBy');
        if(empty($orderBy)) $orderBy = $this->pkField;
        $sortBy = Input::get('sortBy');
        if(empty($sortBy)) $sortBy = 'desc';

        $search = Input::get('search');

        $model = $this->model;

        $tag_id = Input::get('tag_id');

        $subtitle = Tag::find($tag_id);

        $data = $model::where('tag_id',$tag_id);


        if(!empty($search))
        {
            $data = $data->where(function ($query) use($search){
                foreach($this->a_search as $field)
                {
                    $query = $query->orWhere($field, 'like', '%'.$search.'%');
                }
            });
        }
        $countData = $data->count();
        $data = $data
            ->orderBy($orderBy,$sortBy)
            ->paginate($perPage);
        $data->setPath($this->page);
        $data->lastPage();


        return view($this->viewPath.'/index',compact('data','countData','tag_id','subtitle'));
    }

    // ----------------------------------------- View Add Page
    public function create()
    {
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";


        $tag_id = Input::get('tag_id');
        $a_tag_name = Tag::select("tag_name_th","tag_id")->where('tag_id',$tag_id)->first();
        $tag_name_th = $a_tag_name->tag_name_th;
//        return $tag_name;

//        $product_id = Input::get('product_id');

//
//        if($product_id !=""){
//            $a_product_name = Product::select("product_name","product_id")->where('product_id',$product_id)->first();
//            $product_name = $a_product_name->product_name;
//            $product_id1 = $a_product_name->product_id;
//
//            return view($this->viewPath.'/update',compact('url_to','method','txt_manage','product_id1','product_id','product_name'));
//
//        }else{
//
//            $product = Product::all();
//            $product_id1 = " ";
//            return view($this->viewPath.'/update',compact('url_to','method','txt_manage','product_id1','product_id','product'));
//        }
//        return $product_name;

        return view($this->viewPath.'/update',compact('url_to','method','txt_manage','tag_name_th','tag_id'));





    }

    // ----------------------------------------- Record Data
    public function store(Request $request)
    {
        $objFn = new MainFunction();
        $model = $this->model;
        $data = new $model;
         $objFn->db_add($data,$this->pkField,$request,$this->fieldList);
        $strParam = $request->strParam;






        return Redirect::to($this->path.'?'.$strParam);
    }

    // ----------------------------------------- Show Data : ID
    public function show($id)
    {

    }

    // ----------------------------------------- View Update Page
    public function edit($id)
    {
        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = "Update";
        Session::put('referUrl',URL::previous());


        $model = $this->model;
        $data = $model::find($id);
//        $product_id = Input::get('product_id');
//
//        if($product_id !=""){
//            $a_product_name = Product::select("product_name")->where('product_id',$product_id)->first();
//            $product_name = $a_product_name->product_name;
//            $product_id1 = $a_product_name->productid;
//
//            return view($this->viewPath.'/update',compact('data','url_to','method','txt_manage','product_id1','product_id','product_name'));
//
//        }else{
//            $product = Product::all();
//            $product_id1 = " ";
//
//            return view($this->viewPath.'/update',compact('data','url_to','method','txt_manage','product_id1','product_id','product'));
//
//        }
        $tag_id = Input::get('tag_id');
        $a_tag_name = Tag::select("tag_name_th","tag_id")->where('tag_id',$tag_id)->first();
        $tag_name_th = $a_tag_name->tag_name_th;



        return view($this->viewPath.'/update',compact('data','url_to','method','txt_manage','tag_name_th','tag_id'));



    }

    // ----------------------------------------- Record Update Data
    public function update(Request $request, $id )
    {
        $objFn = new MainFunction();
        $strParam = $request->strParam;

        $model = $this->model;
        $data = $model::find($id);
        $objFn->db_update($data,$this->pkField,$request,$this->fieldList);

        return Redirect::to($this->path.'?'.$strParam);
    }

    // ----------------------------------------- Delete Data
    public function destroy($id)
    {
        Session::put('referUrl',URL::previous());
        $model = $this->model;
        $model::find($id)->delete();

        return Redirect::to(Session::get('referUrl'));
    }
}

