<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Model\Orders;
use App\Model\OrdersDetail;
use App\Model\ProductOption;
use App\Model\ProductStock;
use App\Model\OrderTracking;
use App\Model\StockLog;
use App\Model\Status;
use App\Model\User;
use App\Library\MainFunction;
use App\Library\OrderFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;
use Mail;

class OrdersController extends Controller
{
    public $model = 'App\Model\Orders';
    public $titlePage = 'Orders';
    public $tbName = 'orders';
    public $pkField = 'orders_id';
    public $fieldList = array('orders_no','orders_date','user_id', 'billing_address_id', 'billing_address', 'shipping_address_id', 'shipping_address', 'payment_method_id','paysbuy_method', 'payment_by','payment_date', 'total_price','discount_price','shipping_price','total_point','status_id','tracking_no');
    public $a_search = array('orders.orders_no','user.firstname','user.lastname');
    public $path = '_admin/orders';
    public $page = 'orders';
    public $viewPath = 'backend/orders';

    public function __construct()
    {
        $this->middleware('admin');
    }

    // ----------------------------------------- Show All List Page
    public function index()
    {
        $perPage = Config::get('mainConfig.perPage');
        $orderBy = Input::get('orderBy');
        if (empty($orderBy)) $orderBy = $this->pkField;
        $sortBy = Input::get('sortBy');
        if (empty($sortBy)) $sortBy = 'desc';
        $user_id = input::get('user_id');
        $search = Input::get('search');
        $status = Input::get('status');
        $payment = Input::get('payment_by');
        $orders_id = Input::get('orders_id');

        $model = $this->model;
        $data = $model::leftjoin('user','user.user_id','=','orders.user_id')->whereNull('orders.deleted_at');

        if (!empty($search)) {
            $data = $data->where(function ($query) use ($search) {
                foreach ($this->a_search as $field) {
                    $query = $query->orWhere($field, 'like', '%' . $search . '%');
                }
            });
        }

        if(!empty($status)){
            $data = $data->Where('orders.status_id', 'like', '%' . $status . '%');
        }

        if(!empty($payment)){
            $data = $data->Where('orders.payment_method_id', $payment);
        }
        else if($payment == '0'){
            $data = $data->Where('orders.payment_method_id', $payment);
        }

        if(!empty(Input::get('from_date')) && !empty(Input::get('to_date')) ){
          if(Input::get('from_date') == Input::get('to_date')){
              $data = $data->whereBetween('orders_date',[Input::get('from_date').' 00:00:00',Input::get('to_date').' 23:59:59']);
          }else{
              $data = $data->whereBetween('orders_date',[Input::get('from_date').' 00:00:00',Input::get('to_date').' 00:00:00']);
          }
        }

        if(!empty(Input::get('export')) && Input::get('export')=="true"){
            $type = input::get('type');
            $from_date = input::get('from_date');
            $to_date = input::get('to_date');

            if($type==4){
                $data = $data->where('orders.orders_id',$orders_id);
                $type = 3;
            }
            if($type==5){
                $data = $data->leftJoin('orders_detail','orders.orders_id','=','orders_detail.orders_id')
                        ->select('orders_detail.model','orders_detail.product_name_th','orders_detail.unit_price','orders.discount_price','orders.orders_id','orders_detail.qty','orders_detail.product_option_id','orders_detail.qty','orders.status_id');
                // $data = $data->groupBy('orders_detail.model');
                $type = 5;
            }

            $countData = $data->count();
            $data = $data
                ->orderBy($orderBy, $sortBy);
            $data = $data->take(Config::get('mainConfig.setTake'))->get();
//            $data = $data->take(5)->get();

            if($type==1){
                return view($this->viewPath.'/export_report_price',compact('data','countData','from_date','to_date'));
            }else if($type==2){
                return view($this->viewPath.'/export_report',compact('data','countData','from_date','to_date'));
            }else if($type==3){
                return view($this->viewPath.'/export_address',compact('data','countData','from_date','to_date'));
            }else if($type==5){
                return view($this->viewPath.'/export_product',compact('data','countData','from_date','to_date'));
            }
        }else{

            $countData = $data->count();
            $data = $data
                ->orderBy($orderBy, $sortBy)
                ->paginate($perPage);
            $data->setPath($this->page);
            $data->lastPage();

            return view($this->viewPath . '/index', compact('data', 'countData', 'user_id'));
        }
    }

    // ----------------------------------------- View Add Page
    public function create()
    {
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";

        $data2 = Status::all();


        return view($this->viewPath . '/update', compact('url_to', 'method', 'txt_manage','data2'));
    }

    // ----------------------------------------- Record Data
    public function store(Request $request)
    {
        $objFn = new MainFunction();
        $model = $this->model;
        $data = new $model;
        $id = $objFn->db_add($data, $this->pkField, $request, $this->fieldList);


        return Redirect::to($this->path);
    }

    // ----------------------------------------- Show Data : ID
    public function show($id)
    {

        $url_to = $this->path.'/'.$id;
        Session::flash('referUrl',URL::previous());

        $id2 = substr($id,10);
        $data = DB::Table('orders')->whereNull('deleted_at')->where('orders_id',$id2)->first();

        return view($this->viewPath.'/print',compact('data','url_to','id'));


    }

    // ----------------------------------------- View Update Page
    public function edit($id)
    {
        $url_to = $this->path . '/' . $id;
        $method = 'PUT';
        $txt_manage = "Update";
        Session::put('referUrl', URL::previous());

        $data2 = Status::all();


        $model = $this->model;
        $data = $model::find($id);
        return view($this->viewPath . '/update', compact('data', 'url_to', 'method', 'txt_manage','data2'));
    }

    // ----------------------------------------- Record Update Data
    public function update(Request $request, $id)
    {
        $objFn = new MainFunction();
        $objFnOrder = new OrderFunction();
        $strParam = $request->strParam;

        $status = Input::get('status_id');


        if($status==3) {
            $objFnOrder->point($id);
            $objFnOrder->sendMailWh($id);
            $objFnOrder->sendMailUser($id, "การชำระเงินเสร็จเรียบร้อย");
        }

        if($status==4) {
            $objFnOrder->sendMailWh($id);
            $objFnOrder->sendMailUser($id, "อยู่ในระหว่างดำเนินการ");
        }

        if($status==5) {
            $objFnOrder->sendMailWh($id);
            $objFnOrder->sendMailUser($id, "สินค้าอยู่ในระหว่างการจัดส่ง");
        }

        if($status==6) {
            $objFnOrder->sendMailWh($id);
            $objFnOrder->sendMailUser($id, "สินค้าถูกจัดส่งเรียบร้อยแล้ว");
        }

        if($status==7) {
            $objFnOrder->sendMailCan($id, "รายการสั่งซื้อถูกยกเลิก");
        }

        Orders::where('orders_id', $id)->update(['status_id' => $status]);

        return Redirect::to($this->path.'?'.$strParam);
    }

    // ----------------------------------------- Delete Data
    public function destroy($id)
    {
        Session::put('referUrl', URL::previous());
        $model = $this->model;
        $model::find($id)->delete();

        return Redirect::to(Session::get('referUrl'));
    }
//-------------------------------------------------------
    public function chk_status(Request $request)
    {
        $objFnOrder = new OrderFunction();

        $order_chk = $request->order_chk;
        $status = Input::get('status_s');

        if (!empty($status)) {

            if (!empty($order_chk)) {

                foreach ($order_chk as $order_id) {
                    $data_status = DB::table('orders')->select('status_id')->where('orders_id', $order_id)->first();

                    $status_old = $data_status->status_id;
                    $orders = Orders::find($order_id);
                    $orders->status_id = $status;
                    $orders->save();

                    if ($status == 3) {
                        if ($status_old == 1 || $status_old == 2) {

                            $order_tracking = new OrderTracking;
                            $order_tracking->orders_id = $order_id;
                            $order_tracking->status_id = $status;
                            $order_tracking->save();
                            // return $objFnOrder->point($order_id);
                            $objFnOrder->point($order_id);
                            $objFnOrder->sendMailWh($order_id);
                            $objFnOrder->sendMailUser($order_id, "การชำระเงินเสร็จเรียบร้อย");
                            $objFnOrder->friendPoint($order_id);

                        } else {
                            return redirect()->back()->with('error_msg', 'This order is not status. #' . $order_id);
                        }
                    }

                    if ($status == 4) {
                        $order_tracking = new OrderTracking;
                        $order_tracking->orders_id = $order_id;
                        $order_tracking->status_id = $status;
                        $order_tracking->save();

                        $objFnOrder->sendMailWh($order_id);
                        $objFnOrder->sendMailUser($order_id, "อยู่ในระหว่างดำเนินการ");
                    }

                    if ($status == 5) {
                        $data_orders_detail = DB::table('orders_detail')->select('scanned', 'qty')->where('orders_id', $order_id)->get();

                        foreach ($data_orders_detail as $value) {
                            if ($value->scanned != $value->qty) {

                                return redirect()->back()->with('error_msg', 'ยัง Scan สินค้าไม่ครบ #' . $order_id);

                            }

                            $order_tracking = new OrderTracking;
                            $order_tracking->orders_id = $order_id;
                            $order_tracking->status_id = $status;
                            $order_tracking->save();

                            $objFnOrder->sendMailWh($order_id);
                            $objFnOrder->sendMailUser($order_id, "สินค้าอยู่ในระหว่างการจัดส่ง");

                        }

                    }

                    if ($status == 6) {
                        $data_orders_detail = DB::table('orders_detail')->select('scanned', 'qty')->where('orders_id', $order_id)->get();

                        foreach ($data_orders_detail as $value) {
                            if ($value->scanned != $value->qty) {
                                return redirect()->back()->with('error_msg', 'ยัง Scan สินค้าไม่ครบ #' . $order_id);
                            }
                        }

                        $order_tracking = new OrderTracking;
                        $order_tracking->orders_id = $order_id;
                        $order_tracking->status_id = $status;
                        $order_tracking->save();

                        $objFnOrder->sendMailWh($order_id);
                        $objFnOrder->sendMailUser($order_id, "สินค้าถูกจัดส่งเรียบร้อยแล้ว");


                    }

                    if ($status == 7) {
                        $order_tracking = new OrderTracking;
                        $order_tracking->orders_id = $order_id;
                        $order_tracking->status_id = $status;
                        $order_tracking->save();

                        $data_orders = Orders::leftjoin('orders_detail', 'orders_detail.orders_id', '=', 'orders.orders_id')->where('orders.orders_id', $order_id)->select('orders.total_point', 'orders.use_point', 'orders.user_id', 'orders_detail.qty', 'orders_detail.product_option_id', 'orders.status_id')->get();

                        foreach ($data_orders as $value) {
                            if ($status_old == 2 || $status_old == 3) { //ชำระเงินแล้ว

                                $objFnOrder->returnPoint($order_id);
                                $objFnOrder->returnCode($order_id);
                                $objFnOrder->returnStock($order_id);
                                $objFnOrder->sendMailUserCancel($order_id, "ยกเลิกการสั่งซื้อ");

                            } else if ($status_old == 4 || $status_old == 5) { //อยู่ในระหว่างการดำเนินการ , สินค้าอยู่ในระหว่างการจัดส่ง
                                $objFnOrder->returnPoint($order_id);
                                $objFnOrder->returnCode($order_id);
                                $objFnOrder->returnStock($order_id);

                                $data_product_stock = DB::table('stock_log')->orderBy('stock_log_id', 'asc')->where('product_option_id', $value->product_option_id)->where('qty', '<', '0')->where('orders_id', $order_id)->first();

                                DB::table('product_stock')->insert(['warehouse_id' => $data_product_stock->warehouse_id,
                                    'product_option_id' => $data_product_stock->product_option_id,
                                    'qty' => $value->qty,
                                    'cost' => $data_product_stock->cost,
                                    'created_at' => date('Y-m-d H:i:s')]);

                                $data_stock_total = DB::table('stock_log')->select('total')->whereNUll('deleted_at')->orderBy('stock_log_id', 'desc')->where('product_option_id', $value->product_option_id)->first();
                                $data_stock = DB::table('stock_log')->orderBy('stock_log_id', 'asc')->where('product_option_id', $value->product_option_id)->first();

                                $total_stock = $data_stock_total->total + $value->qty;
                                DB::table('stock_log')->insert(['warehouse_id' => $data_stock->warehouse_id,
                                    'orders_id' => $data_stock->orders_id,
                                    'product_option_id' => $value->product_option_id,
                                    'qty' => $value->qty,
                                    'cost' => $data_product_stock->cost,
                                    'sale_price' => $data_stock->sale_price,
                                    'dollar_sale_price' => $data_stock->dollar_sale_price,
                                    'total' => $total_stock,
                                    'type' => '5',
                                    'created_at' => date('Y-m-d H:i:s')]);

                                $objFnOrder->sendMailUserCancel($order_id, "ยกเลิกการสั่งซื้อ");

                            }


                        }
                    }
                    // return $status;
                    if($status==6) {
                      Orders::where('orders_id', $order_id)->update(['status_id' => $status, 'status_return_product' => 1]);
                    }else{
                      Orders::where('orders_id', $order_id)->update(['status_id' => $status, 'status_return_product' => 0]);
                    }
//                    $orders = Orders::find($order_id);
//                    $orders->status_id = $status;
//                    $orders->save();

                }

                return redirect()->back();

            } else {

                return redirect()->back();
            }
        } else {

            return redirect()->back();
        }
    }


    public function update_tracking(Request $request)
    {
        $objFnOrder = new OrderFunction();

        $orders_id = $request->orders_id;
        $tracking_no = $request->tracking_no;

        Orders::where('orders_id', $orders_id)->update(['tracking_no' => $tracking_no]);
        $subject = "แจ้ง Tracking No. $tracking_no ";
        $objFnOrder->sendMailUser($orders_id, $subject);

        return redirect()->back();

    }

    public function stock($orders_id){
        $path = $this->path;

        $products = OrdersDetail::select('orders_detail.*', 'product_option.option_name_th', 'product_option.option_name_en', 'product_option.model')
                            ->join('product_option', 'orders_detail.product_option_id', '=', 'product_option.product_option_id')
                            ->where('orders_id', $orders_id)
                            ->whereNull('orders_detail.deleted_at')
                            ->whereNull('product_option.deleted_at')
                            ->get();

        return view('backend.orders.stock', compact('path', 'orders_id', 'products'));
    }

    public function updateStock(Request $request, $orders_id){
        $path = $this->path;

        $barcode = $request->barcode; // Barcode = Model Number

        $data_orders = Orders::select('status_id','user_id')->where('orders_id',$orders_id)->first();
        $product_option = ProductOption::where('model', $barcode)->first();

        if($data_orders->status_id > 2) {
            if (!empty($product_option)) {
                $product_option_id = $product_option->product_option_id;
                $orders_detail = OrdersDetail::where('orders_id', $orders_id)
                    ->where('product_option_id', $product_option_id)
                    ->first();

                if (!empty($orders_detail)) {
                    if ($orders_detail->qty > $orders_detail->scanned) {     // If scanning product is not completed yet

                        $current_stock = ProductStock::where('warehouse_id', '1')
                            ->where('product_option_id', $product_option_id)
                            ->sum('qty');

                        if ($current_stock > 0) {    // If this item is still available in the stock

                            $option = ProductOption::find($product_option_id);
                            $option->qty = $product_option->qty - 1;
                            $option->save();

                            $stock = ProductStock::where('warehouse_id', '1')
                                ->where('product_option_id', $product_option_id)
                                ->orderBy('product_stock_id', 'asc')
                                ->first();

                            // Remove one from the stock & Increase a scanned in the orders detail
                            if ($stock->qty == 1)
                                $stock->delete();
                            else
                                $stock->decrement('qty');

                            OrdersDetail::where('orders_id', $orders_id)
                                ->where('product_option_id', $product_option_id)->increment('scanned');

                            // Create a log
                            $log = new StockLog;
                            $log->warehouse_id = 1;
                            $log->orders_id = $orders_id;
                            $log->product_option_id = $product_option_id;
                            $log->qty = -1;
                            $log->cost = $stock->cost;
                            $log->sale_price = $orders_detail->unit_price;
                            $log->dollar_sale_price = $orders_detail->dollar_unit_price;
                            $log->total = $current_stock - 1;
                            $log->type = 3;     // Remove by barcode scanner
                            $log->save();

                            $check_vip = DB::table('orders')
                                ->leftjoin('orders_detail','orders_detail.orders_id','=','orders.orders_id')
                                ->select('orders_detail.unit_price','orders_detail.scanned')
                                ->where('orders.user_id',$data_orders->user_id)
                                ->whereNotIn('orders.status_id',[1,2,7,8,9])
                                ->get();

                            $total = '';
                            foreach ($check_vip as $value){
                                $total += $value->unit_price*$value->scanned;
                            }

                            if($total >= 8000){
                                $user = User::find($data_orders->user_id);
                                $user->user_type = 1;
                                $user->save();
                            }

                            return redirect()->back();
                        } else {
                            // Redirect Back with an error
                            return redirect()->back()->with('error_msg', 'There is not enough product in our stock.');
                        }
                    } else {
                        // Redirect Back with an error
                        return redirect()->back()->with('error_msg', 'This product is already scannned.');
                    }
                } else {
                    return redirect()->back()->with('error_msg', 'Model \'' . $barcode . '\' is not found in this order.');
                }
            } else {
                return redirect()->back()->with('error_msg', 'Model \'' . $barcode . '\' is not found.');
            }
        }else{
            return redirect()->back()->with('error_msg', 'This product is not status Paid.');
        }
    }

    //-------------------------------------------

    public function payment($orders_id){
        $path = $this->path;

        $model = $this->model;
        $data = $model::find($orders_id);

        if (($data->status_id == 1 || $data->status_id == 2) && $data->shipping_method_id == 2)
            return view('backend.orders.payment', compact('path', 'data'));
        else
            return Redirect::to($this->path.'?'.$strParam);
    }

    public function updatePayment(Request $request, $orders_id){
        $strParam = $request->strParam;

        $model = $this->model;
        $data = $model::find($orders_id);

        $shipping_price = $request->shipping_price;
        $payment_method_id = $request->payment_method_id;

        if (($data->status_id == 1 || $data->status_id == 2) && $data->shipping_method_id == 2)
            Orders::where('orders_id', $orders_id)->update(['status_id' => '2', 'shipping_price' => $shipping_price , 'payment_method_id' => $payment_method_id]);

        return Redirect::to($this->path.'?'.$strParam);
    }

//return redirect()->back();


}
