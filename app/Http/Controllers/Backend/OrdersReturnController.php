<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Model\User;
use App\Model\Product;
use App\Model\Orders;
use App\Model\OrdersDetail;
use App\Model\return_product_detail;
use App\Model\ProductOption;
use App\Model\ProductGallery;
use App\Model\ProductStock;
use App\Model\StockLog;
use App\Model\Status;
use App\Library\MainFunction;
use App\Library\OrderFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;
use Mail;


class OrdersReturnController extends Controller
{
    public $model = 'App\Model\return_product';
    public $titlePage = 'Return Product';
    public $tbName = 'return_product';
    public $pkField = 'order_return_id';
    public $fieldList = array('order_id','image_name','return_date', 'detail', 'status');
    public $a_search = array('orders_no');
    public $path = '_admin/orders_return';
    public $page = 'orders_return';
    public $viewPath = 'backend/orders_return';

    public function __construct()
    {
        $this->middleware('admin');
    }

    // ----------------------------------------- Show All List Page
    public function index()
    {
        $perPage = Config::get('mainConfig.perPage');
        $orderBy = Input::get('orderBy');
        if (empty($orderBy)) $orderBy = $this->pkField;
        $sortBy = Input::get('sortBy');
        if (empty($sortBy)) $sortBy = 'desc';

        $user_id = input::get('user_id');
        $search = Input::get('search');

        $model = $this->model;
        $data = $model::join('orders', 'return_product.orders_id', '=', 'orders.orders_id')
                        ->join('user', 'orders.user_id' ,'=', 'user.user_id');
        $data->whereNull('return_product.deleted_at');


        // $data = $model::whereNull('return_product.deleted_at');
        if (!empty($search)) {
            $data = $data->where(function ($query) use ($search) {
                foreach ($this->a_search as $field) {
                    $query = $query->orWhere($field, 'like', '%' . $search . '%');
                }
            });
        }
        // echo json_encode($data);

        if(!empty(Input::get('from_date')) && !empty(Input::get('to_date')) ){
            if(Input::get('from_date') == Input::get('to_date')){
                $data = $data->whereBetween('return_date',[Input::get('from_date').' 00:00:00',Input::get('to_date').' 23:59:59']);
            }else{
                $data = $data->whereBetween('return_date',[Input::get('from_date').' 00:00:00',Input::get('to_date').' 00:00:00']);
            }
        }

        // $datas = $data->get();
        // $data = $model::whereNull('deleted_at')->get();
        if(!empty(Input::get('export')) && Input::get('export')=="true"){
            $countData = $data->count();
            $data = $data
                ->orderBy($orderBy, $sortBy);
            $data = $data->take(Config::get('mainConfig.setTake'))->get();
            //
            return view($this->viewPath.'/export_report',compact('data','countData'));

        }else{
          $countData = $data->count();
          $data = $data
              ->orderBy($orderBy, $sortBy)
              ->paginate($perPage);
          $data->setPath($this->page);
          $data->lastPage();

          return view($this->viewPath . '/index', compact('data', 'countData'));

        }

        // if (!empty($search)) {
        //     $data = $data->where(function ($query) use ($search) {
        //         foreach ($this->a_search as $field) {
        //             $query = $query->orWhere($field, 'like', '%' . $search . '%');
        //         }
        //     });
        // }

        // if(!empty($status)){
        //     $data = $data->Where('orders.status_id', 'like', '%' . $status . '%');
        // }
        //
        // if(!empty($payment)){
        //     $data = $data->Where('orders.payment_by', 'like', '%' . $payment . '%');
        // }
        //
        // if(!empty(Input::get('from_date')) && !empty(Input::get('to_date')) )
        //     $data = $data->whereBetween('orders_date',[Input::get('from_date').' 00:00:00',Input::get('to_date').' 00:00:00']);

        // if(!empty(Input::get('export')) && Input::get('export')=="true"){
        //     $type = input::get('type');
        //     $from_date = input::get('from_date');
        //     $to_date = input::get('to_date');
        //
        //     if($type==1){
        //         $countData = $data->count();
        //         $data = $data
        //             ->orderBy($orderBy, $sortBy);
        //         $data = $data->take(Config::get('mainConfig.setTake'))->get();
        //
        //         return view($this->viewPath.'/export_report_price',compact('data','countData','from_date','to_date'));
        //     }else{
        //         $countData = $data->count();
        //         $data = $data
        //             ->orderBy($orderBy, $sortBy);
        //         $data = $data->take(Config::get('mainConfig.setTake'))->get();
        //
        //         return view($this->viewPath.'/export_report',compact('data','countData','from_date','to_date'));
        //     }
        //
        // }else {
        //     $countData = $data->count();
        //     $data = $data
        //         ->orderBy($orderBy, $sortBy)
        //         ->paginate($perPage);
        //     $data->setPath($this->page);
        //     $data->lastPage();
        //
        //     return view($this->viewPath . '/index', compact('data', 'countData', 'user_id'));
        // }
    }

    // ----------------------------------------- View Add Page
    public function create()
    {
        $url_to = $this->path;
        $order_return_id = Input::get('order_return_id');
        $first = return_product_detail::where('order_return_id', $order_return_id)->first();
        // $datas = Orders::leftjoin('user','user.user_id','=','orders.user_id')->whereNull('orders.deleted_at')->get();
        $datas = Orders::join('return_product', 'return_product.orders_id', '=', 'orders.orders_id')
                        ->join('user', 'orders.user_id' ,'=', 'user.user_id')
                        ->where('return_product.orders_id', $first->orders_id)
                        ->whereNull('orders.deleted_at')->get();
        $data_return = return_product_detail::where('order_return_id', $order_return_id)->get();
        foreach($data_return as $data){
          $product_gallery_detail = ProductGallery::where('product_id',$data->product_id)->first();

          $data->img_product = $product_gallery_detail->photo;
        }


        $orders_id = $first->orders_id;
        $order_detail = OrdersDetail::where('orders_id',$orders_id)->get();
        // $product_detail = ProductGallery::where('product_id',$product_id)->first();
        $method = 'POST';
        return view($this->viewPath . '/detail', compact('url_to', 'method','data_return','order_detail','datas'));
    }

    // ----------------------------------------- Record Data
    public function store(Request $request)
    {
        // $objFn = new MainFunction();
        // $model = $this->model;
        // $data = new $model;
        // $id = $objFn->db_add($data, $this->pkField, $request, $this->fieldList);
        //
        //
        // return Redirect::to($this->path);
    }

    // ----------------------------------------- Show Data : ID
    public function show($id)
    {

        // $url_to = $this->path.'/'.$id;
        // Session::flash('referUrl',URL::previous());
        //
        // $id2 = substr($id,10);
        // $data = DB::Table('orders')->whereNull('deleted_at')->where('orders_id',$id2)->first();
        //
        // return view($this->viewPath.'/print',compact('data','url_to','id'));


    }

    // ----------------------------------------- View Update Page
    public function edit($id)
    {
        // $url_to = $this->path . '/' . $id;
        // $method = 'PUT';
        // $txt_manage = "Update";
        // Session::put('referUrl', URL::previous());
        //
        // $data2 = Status::all();
        //
        //
        // $model = $this->model;
        // $data = $model::find($id);
        // return view($this->viewPath . '/update', compact('data', 'url_to', 'method', 'txt_manage','data2'));
    }

    // ----------------------------------------- Record Update Data
    public function update(Request $request, $id)
    {
        // $objFn = new MainFunction();
        // $objFnOrder = new OrderFunction();
        // $strParam = $request->strParam;
        //
        // $status = Input::get('status_id');
        //
        //
        // if($status==3) {
        //     $objFnOrder->point($id);
        //     $objFnOrder->sendMailWh($id);
        //     $objFnOrder->sendMailUser($id, "การชำระเงินเสร็จเรียบร้อย");
        // }
        //
        // if($status==4) {
        //     $objFnOrder->sendMailWh($id);
        //     $objFnOrder->sendMailUser($id, "อยู่ในระหว่างดำเนินการ");
        // }
        //
        // if($status==5) {
        //     $objFnOrder->sendMailWh($id);
        //     $objFnOrder->sendMailUser($id, "สินค้าอยู่ในระหว่างการจัดส่ง");
        // }
        //
        // if($status==6) {
        //     $objFnOrder->sendMailWh($id);
        //     $objFnOrder->sendMailUser($order_id, "สินค้าถูกจัดส่งเรียบร้อยแล้ว");
        // }
        //
        // if($status==7) {
        //     $objFnOrder->sendMailCan($id, "รายการสั่งซื้อถูกยกเลิก");
        // }
        //
        // Orders::where('orders_id', $id)->update(['status_id' => $status]);
        //
        // return Redirect::to($this->path.'?'.$strParam);
    }

    // ----------------------------------------- Delete Data
    public function destroy($id)
    {
        Session::put('referUrl', URL::previous());
        $model = $this->model;
        $model::find($id)->delete();

        return Redirect::to(Session::get('referUrl'));
    }

    // -----------------------------------------
    public function change_status(Request $request)
    {

        $model = $this->model;
        $order_chk = $request->order_chk;
        $status = Input::get('status_s');
        // echo json_encode($request->all());
        // echo json_encode($order_chk);
        // echo $status;
        if (!empty($status)) {

            if (!empty($order_chk)) {

                foreach ($order_chk as $order_return_id) {

                    if($status==2) {

                      $model::where('order_return_id', $order_return_id)->update(['status' => $status]);
                    }
                    if($status==3) {
                        $total_return = "";
                        $total_qty = "";
                        $total_qty_return = "";

                       $data_return = DB::table('return_product')
                           ->leftjoin('return_product_detail','return_product.order_return_id','=','return_product_detail.order_return_id')
                           ->select('return_product_detail.qty','return_product_detail.orders_id','return_product_detail.orders_detail_id')
                           ->where('return_product.order_return_id',$order_return_id)
                           ->get();
//
                        foreach ( $data_return as $value ){
                            $data_detail = DB::table('orders')
                                ->leftjoin('orders_detail','orders_detail.orders_id','=','orders.orders_id')
                                ->select('orders_detail.unit_price','orders.user_id','orders.orders_no','orders.discount_price','orders.use_point','orders_detail.qty')
                                ->where('orders_detail.orders_detail_id',$value->orders_detail_id)
                                ->first();

                            $total_qty += $data_detail->qty;
                            $total_qty_return += $value->qty;
                            $total_return += $value->qty*$data_detail->unit_price;
                        }

                        $discount = $total_qty_return*(round($data_detail->discount_price/$total_qty,0,PHP_ROUND_HALF_UP));
                        $point = $total_qty_return*(round($data_detail->use_point/$total_qty,0,PHP_ROUND_HALF_UP));

                        $total_return_end = $total_return-$discount-$point;

                        $data_point = DB::table('point_transaction')
                            ->where('user_id',$data_detail->user_id)
                            ->select('total')
                            ->orderBy('point_trans_id','desc')
                            ->first();

                        $total_tran = $total_return_end+$data_point->total;

                        DB::table('point_transaction')->insert(["user_id" => $data_detail->user_id , "detail" => "Ref. Order Number ". $data_detail->orders_no , "point" => $total_return , "total" => $total_tran ,'created_at' => date('Y-m-d H:i:s')]);

                      $model::where('order_return_id', $order_return_id)->update(['status' => $status]);
                    }
                    // if($status==6) {
                      // Orders::where('order_return_id', $order_return_id)->update(['status_id' => $status, 'status_return_product' => 1]);
                    // }else{
                      // Orders::where('order_return_id', $order_return_id)->update(['status_id' => $status, 'status_return_product' => 0]);
                    // }

                }

            }else{

                return Redirect::to($this->path);
            }
        }

        return Redirect::to($this->path);
    }


}
