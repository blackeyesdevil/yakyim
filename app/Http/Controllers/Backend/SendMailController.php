<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Model\Subscribe;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;
use Mail;

class SendMailController extends Controller
{
    public $model = 'App\Model\Subscribe';
    public $titlePage = 'SendMail';
    public $tbName = 'subscribe';
    public $pkField = 'subscribe_id';
    public $fieldList = array('subject','message');
    public $a_search = array('subject');
    public $path = '_admin/send_mail';
    public $page = 'send_mail';
    public $viewPath = 'backend/send_mail';

    public function __construct()
    {
        $this->middleware('admin');
    }

    // ----------------------------------------- View Add Page
    public function create()
    {
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";

        return view($this->viewPath.'/update',compact('url_to','method','txt_manage'));
    }

    // ----------------------------------------- Record Data
    public function store(Request $request)
    {
        $objFn = new MainFunction();
        $model = $this->model;

        $data = $model::whereNull('deleted_at')->get();
        $subject = $request->subject;
        $message = $request->message;

        foreach ($data as $value) {
            $email = $value->email;
            Mail::send('emails.subscribe-back', ['subject' => $subject, 'message2' => $message], function ($m) use ($subject, $email) {
                $m->from(env('MAIL_CONTACT'), 'YakYim Team');
                $m->to($email, $email);
                $m->subject($subject);
            });
        }


        return Redirect::to("_admin/subscribe");
    }

}
