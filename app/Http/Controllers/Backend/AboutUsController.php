<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Model\AboutUs;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;

class AboutUsController extends Controller
{
    public $model = 'App\Model\AboutUs';
    public $titlePage = 'About Us';
    public $tbName = 'about_us';
    public $pkField = 'about_us_id';
    public $fieldList = array('content_th','content_en','img_name');
    public $a_search = array('content_th');
    public $path = '_admin/about_us';
    public $page = 'about_us';
    public $viewPath = 'backend/about_us';

    public function __construct()
    {
        $this->middleware('admin');
    }

    // ----------------------------------------- Show All List Page
    public function index()
    {
        return redirect()->to('_admin/about_us/1/edit');
    }

    // ----------------------------------------- View Add Page
    public function create()
    {
        return redirect()->to('_admin/about_us/1/edit');
    }

    // ----------------------------------------- Record Data
    public function store(Request $request)
    {
        return redirect()->to('_admin/about_us/1/edit');
    }

    // ----------------------------------------- Show Data : ID
    public function show($id)
    {

    }

    // ----------------------------------------- View Update Page
    public function edit($id)
    {
        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = "Update";
        Session::put('referUrl',URL::previous());

        $model = $this->model;
        $data = $model::find($id);
        return view($this->viewPath.'/update',compact('data','url_to','method','txt_manage'));
    }

    // ----------------------------------------- Record Update Data
    public function update(Request $request, $id )
    {
        $objFn = new MainFunction();
        $strParam = $request->strParam;

        $model = $this->model;
        $data = $model::find($id);

        $request->img_name = $request->img_path;

        $id = $objFn->db_update($data,$this->pkField,$request,$this->fieldList); // add $id

        return Redirect::Back();
    }

    // ----------------------------------------- Delete Data
    public function destroy($id)
    {

    }
}

