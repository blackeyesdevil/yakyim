<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Model\AddressBook;
use App\Model\Zipcode;
use App\Model\District;
use App\Model\Amphur;
use App\Model\Province;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;

class AddressBookController extends Controller
{
    public $model = 'App\Model\AddressBook';
    public $titlePage = 'Address Booking';
    public $tbName = 'address_book';
    public $pkField = 'address_book_id';
    public $fieldList = array('billing_shipping','tax_id','firstname','lastname','address','mobile','set_default','district_id','amphur_id','province_id','zipcode');
    public $a_search = array('firstname','lastname');
    public $path = '_admin/address_book';
    public $page = 'address_book';
    public $viewPath = 'backend/address_book';

    public function __construct()
    {
        $this->middleware('admin');
    }

    // ----------------------------------------- Show All List Page
    public function index()
    {
        $perPage = Config::get('mainConfig.perPage');
        $orderBy = Input::get('orderBy');
        if(empty($orderBy)) $orderBy = $this->pkField;
        $sortBy = Input::get('sortBy');
        if(empty($sortBy)) $sortBy = 'desc';
        $user_id = input::get('user_id');
//        $zipcode_id = input::get('zipcode_id');
        $district_id = input::get('district_id');
        $amphur_id = input::get('amphur_id');
        $search = Input::get('search');

        $model = $this->model;


        $data = new $model;
        $data = $model::where('user_id',$user_id);
        if(!empty($search))
        {
            $data = $data->where(function ($query) use($search){
                foreach($this->a_search as $field)
                {
                    $query = $query->orWhere($field, 'like', '%'.$search.'%');
                }
            });
        }
        $countData = $data->count();
        $data = $data
            ->orderBy($orderBy,$sortBy)
            ->paginate($perPage);
        $data->setPath($this->page);
        $data->lastPage();


        return view($this->viewPath.'/index',compact('data','countData','user_id','district_id','amphur_id'));
    }

    // ----------------------------------------- View Add Page
    public function create()
    {
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";



        return view($this->viewPath.'/update',compact('url_to','method','txt_manage'));
    }

    // ----------------------------------------- Record Data
    public function store(Request $request)
    {
        $objFn = new MainFunction();
        $model = $this->model;
        $data = new $model;
        $objFn->db_add($data,$this->pkField,$request,$this->fieldList);

        return Redirect::to($this->path);
    }

    // ----------------------------------------- Show Data : ID
    public function show($id)
    {

    }

    // ----------------------------------------- View Update Page
    public function edit($id)
    {
        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = "Update";
        Session::put('referUrl',URL::previous());

        $model = $this->model;
        $data = $model::find($id);

        $district = District::all();
        $district_id = Input::get('district_id');

        $amphur = Amphur::all();
        $amphur_id = Input::get('amphur_id');

        $province = Province::all();
        $province_id = Input::get('province_id');

//        $a_zip = Zipcode::orderBy('zipcode_id', 'asc')->groupBy('zipcode')->get();
//
//
//        $a_billing_shipping = $model::where('address_book_id',$id)->first();
//        $billing_shipping = $a_billing_shipping->billing_shipping;
//        $a_district = District::all();
//        $a_province = Province::all();
        return view($this->viewPath.'/update',compact('data','url_to','method','txt_manage','district','district_id','amphur','amphur_id','province','province_id'));


//        return view($this->viewPath.'/update',compact('data','url_to','method','txt_manage','a_zip','a_district','n_amphur','billing_shipping','a_province'));
    }

    // ----------------------------------------- Record Update Data
    public function update(Request $request, $id )
    {
        $objFn = new MainFunction();
        $strParam = $request->strParam;

        $model = $this->model;
        $data = $model::find($id);

        $objFn->db_update($data, $this->pkField, $request, $this->fieldList);

        return Redirect::to($this->path . '?' . $strParam);
    }

    // ----------------------------------------- Delete Data
    public function destroy($id)
    {
        Session::put('referUrl',URL::previous());
        $model = $this->model;
        $model::find($id)->delete();

        return Redirect::to(Session::get('referUrl'));
    }
}

