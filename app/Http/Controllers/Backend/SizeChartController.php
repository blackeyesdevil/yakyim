<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Model\SizeChart;
use App\Model\SizeChartDetail;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;
use Validator;

class SizeChartController extends Controller
{
    public $model = 'App\Model\SizeChart';
    public $titlePage = 'Size Chart';
    public $tbName = 'size_chart';
    public $pkField = 'size_chart_id';
    public $fieldList = array('chart_name_th', 'chart_name_en');
    public $a_search = array('chart_name_th','chart_name_en');
    public $path = '_admin/size_chart';
    public $page = 'size_chart';
    public $viewPath = 'backend/size_chart';

    public function __construct()
    {
        $this->middleware('admin');
    }

    // ----------------------------------------- Show All List Page
    public function index()
    {
        $perPage = Config::get('mainConfig.perPage');
        $orderBy = Input::get('orderBy');
        if(empty($orderBy)) $orderBy = ('size_chart_id');
        $sortBy = Input::get('sortBy');
        if(empty($sortBy)) $sortBy = 'desc';

        $search = Input::get('search');

        $model = $this->model;

        $data = new $model;


        if(!empty($search))
        {
            $data = $data->where(function ($query) use($search){
                foreach($this->a_search as $field)
                {
                    $query = $query->orWhere($field, 'like', '%'.$search.'%');
                }
            });
        }

        $countData = $data->count();
        $data = $data
            ->orderBy($orderBy,$sortBy)
            ->paginate($perPage);
        $data->setPath($this->page);
        $data->lastPage();

 //      $category_id = Input::get('category_id');
//        $parent_category_name = $model::select("category_name")
//            ->where("parent_category_id",$category_id)
//            ->get();




        return view($this->viewPath.'/index',compact('data','countData'));
    }

    // ----------------------------------------- View Add Page
    public function create()
    {
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";
        $model = $this->model;

        return view($this->viewPath.'/update',compact('url_to','method','txt_manage'));
    }

    // ----------------------------------------- Record Data
    public function store(Request $request)
    {
        $objFn = new MainFunction();
        $model = $this->model;
        $data = new $model;
        $strParam = $request->strParam;

        $validator = Validator::make($request->all(), [
            'chart_name_th' => 'required',
            'chart_name_en' => 'required'
        ]);

        if ($validator->fails()){
            return Redirect::back()->withErrors($validator)->withInput();
        }
        else{
            $id = $objFn->db_add($data,$this->pkField,$request,$this->fieldList);

            $data->save();
        }

        return Redirect::to($this->path.'?'.$strParam);
    }

    // ----------------------------------------- Show Data : ID
    public function show($id)
    {

    }

    // ----------------------------------------- View Update Page
    public function edit($id)
    {
        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = "Update";
        Session::put('referUrl',URL::previous());

        $model = $this->model;
        $data = $model::find($id);

        return view($this->viewPath.'/update',compact('data','url_to','method','txt_manage'));
    }

    // ----------------------------------------- Record Update Data
    public function update(Request $request, $id )
    {
        $objFn = new MainFunction();
        $strParam = $request->strParam;

        $model = $this->model;
        $data = $model::find($id);

        $validator = Validator::make($request->all(), [
            'chart_name_th' => 'required',
            'chart_name_en' => 'required'
        ]);

        if ($validator->fails()){
            return Redirect::back()->withErrors($validator)->withInput();
        }
        else{
            $id = $objFn->db_update($data,$this->pkField,$request,$this->fieldList); // add $id
            $data->save();
        }

        return Redirect::to($this->path.'?'.$strParam);
    }

    // ----------------------------------------- Delete Data
    public function destroy($id)
    {
        Session::put('referUrl',URL::previous());
        $model = $this->model;

        $model::find($id)->delete();

        return Redirect::to(Session::get('referUrl'));
    }


}
