<?php
namespace App\Http\Controllers\Backend;

use App\Model\AddressBook;
use App\Model\ProductCategory;
use Illuminate\Http\Request;
use App\Model\Category;
use App\Model\User;
use App\Model\Product;
use App\Model\Location;
use App\Model\ContactFrom;
use App\Model\District;
use App\Model\Province;
use App\Model\Amphur;
use App\Model\Zipcode;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;
use Validator;

class ApiController extends Controller
{

    public function __construct()
    {
//        $this->middleware('admin');
    }

    // ----------------------------------------- Show All List Page




    public function changeActive()
    {
        $rules = array('pk_field'=>'required','v_pk_field'=>'required','change_field'=>'required','value'=>'required','tb_name'=>'required');
        $validator = Validator::make(Input::all(),$rules);
        if($validator->fails())
        {
            return Response::json(array('success'=>false,'text'=>'Validate Error'));
        } else {


            if(Input::get('value') == '1')
            {
                $Obj = DB::table(Input::get('tb_name'))
                    ->where(Input::get('pk_field'),Input::get('v_pk_field'))
                    ->update(array(
                        Input::get('change_field') => 0,
                        'updated_at' => date('Y-m-d H:i:s')

                    ));

                echo "0";
            }else{

                $Obj = DB::table(Input::get('tb_name'))
                    ->where(Input::get('pk_field'),Input::get('v_pk_field'))
                    ->update(array(
                        Input::get('change_field') => 1,
                        'updated_at' => date('Y-m-d H:i:s')

                    ));

                echo "1";
            }
        }
    }

    public function delProduct()
    {
        $rules = array('value' => 'required', 'blog_id' => 'required', 'tb_name' => 'required');
        $validator = Validator::make(Input::all(),$rules);
        $value = Input::get('value');
        $blog_id = Input::get('blog_id');
        $tb_name = Input::get('tb_name');

        if($validator->fails())
        {
            return Response::json(array('success'=>false,'text'=>'Validate Error'));
        } else {

            DB::table($tb_name)->where('blog_id', $blog_id)->where('product_id', $value)->delete();

            echo "0";


            }
        }


    public function delCategory()
    {
        $rules = array('value' => 'required', 'product_id' => 'required', 'tb_name' => 'required');
        $validator = Validator::make(Input::all(),$rules);
        $value = Input::get('value');
        $product_id = Input::get('product_id');
        $tb_name = Input::get('tb_name');

        if($validator->fails())
        {
            return Response::json(array('success'=>false,'text'=>'Validate Error'));
        } else {

            DB::table('product_category')->where('product_id', $product_id)->where('category_id', $value)->delete();

            echo "0";


        }
    }

    public function delRelated()
    {

        $rules = array('value' => 'required', 'product_id' => 'required', 'tb_name' => 'required');
        $validator = Validator::make(Input::all(),$rules);
        $value = Input::get('value');
        $product_id = Input::get('product_id');
        $tb_name = Input::get('tb_name');

        if($validator->fails())
        {
            return Response::json(array('success'=>false,'text'=>'Validate Error'));
        } else {

            DB::table('related_product')->where('related_id', $value)->where('product_id', $product_id)->delete();

            echo "0";


        }
    }



    public function Province(Request $request)
    {
        $province_id = $request->province_id;


        if ($province_id != '') {

            $amphurs = Amphur::where('province_id',$province_id)->select('amphur_id','amphur_name_th')->get();

            $a_amphur = "";

            if (count($amphurs) == 0) {

                $a_amphur = "<option value='0' class='form-control select2-container select2me'>Select Amphur</option>";

            }else{

                foreach ($amphurs as $amphur) {
                    $amphur_name_th = $amphur->amphur_name_th;
                    $amphur_id = $amphur->amphur_id;
                    $a_amphur .= "<option value='$amphur_id' class='form-control select2-container select2me'>$amphur_name_th</option>";

                }
            }


            return $a_amphur;

        }else{

            echo "error";

        }

    }
    public function Amphur(Request $request){

        $amphur_id = $request->amphur_id;

        if ($amphur_id != '') {

            $districts = District::where('amphur_id',$amphur_id)->select('district_id','district_name_th')->get();

            $a_district = "";

            if (count($districts) == 0) {

                $a_district = "<option value='0' class='form-control select2-container select2me'>Select District</option>";

            }else{

                foreach ($districts as $district) {
                    $district_name_th = $district->district_name_th;
                    $district_id = $district->district_id;
                    $a_district .= "<option value='$district_id' class='form-control select2-container select2me'>$district_name_th</option>";

                }
            }


            return $a_district;

        }else{

            echo "error";

        }


    }


}


