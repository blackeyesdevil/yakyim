<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Model\Banner;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;
use Validator;

class BannerPromotionController extends Controller
{
    public $model = 'App\Model\Banner';
    public $titlePage = 'Banner Promotion';
    public $tbName = 'banner';
    public $pkField = 'banner_id';
    public $fieldList = array('img_name','title_th','title_en','detail_th', 'detail_en','link','status');
    public $a_search = array('');
    public $path = '_admin/banner_promotion';
    public $page = 'banner_promotion';
    public $viewPath = 'backend/banner_promotion';

    public function __construct()
    {
        $this->middleware('admin');
    }

    // ----------------------------------------- View Update Page
    public function edit($id)
    {
        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = "Update";
        Session::put('referUrl',URL::previous());

        $model = $this->model;
        $data = $model::find($id);

        return view($this->viewPath.'/update',compact('data','url_to','method','txt_manage'));
    }

    // ----------------------------------------- Record Update Data
    public function update(Request $request, $id )
    {
        $objFn = new MainFunction();
        $strParam = $request->strParam;

        $model = $this->model;
        $data = $model::find($id);
        $request->img_name = $request->img_name;

        $validator = Validator::make($request->all(), [
            'title_th' => 'required',
            'title_en' => 'required'
        ]);

        if ($validator->fails()){
            return Redirect::back()->withErrors($validator)->withInput();
        }
        else{
            $id = $objFn->db_update($data,$this->pkField,$request,$this->fieldList); // add $id

            // BANNER IMAGE
            if (Input::hasFile('img_name')) { // test P.
                $photo = $request->file('img_name');                    // get image from form
                $new_name = date('YmdHis').'-'.rand(10000, 99999);                             // set new name
                $old_name = $data->img_name;                            // get old name
                $path = public_path('uploads/banner_promotion');           // set path
                $objFn->del_storage($path,$old_name);                   // delete old picture in storage
                $objFn->del_storage($path.'/100',$old_name);                   // delete old picture in storage
                $objFn->del_storage($path.'/755',$old_name);                   // delete old picture in storage
                $objFn->del_storage($path.'/980',$old_name);                   // delete old picture in storage
                $objFn->del_storage($path.'/1600',$old_name);                   // delete old picture in storage
                $extension = $photo->getClientOriginalExtension();      // get extension
                $filename = $new_name . "." . $extension;               // set filename
                $destinationPath = $path;
                $objFn->img_full_resize($photo, $destinationPath, $filename);   // resize image
                $objFn->image_resize($photo, $destinationPath, 100, $filename);   // resize image
                $objFn->image_resize($photo, $destinationPath, 755, $filename);   // resize image
                $objFn->image_resize($photo, $destinationPath, 980, $filename);   // resize image
                $objFn->image_resize($photo, $destinationPath, 1600, $filename);   // resize image
                $data = $model::find($id);
                $data->img_name = $filename;
                $data->save();
            }
            else if ($request->img_del == 'y'){
                $old_name = $data->img_name;                            // get old name
                $path = public_path('uploads/banner_promotion');           // set path
                $objFn->del_storage($path,$old_name);                   // delete old picture in storage
                $objFn->del_storage($path.'/100',$old_name);                   // delete old picture in storage
                $objFn->del_storage($path.'/755',$old_name);                   // delete old picture in storage
                $objFn->del_storage($path.'/980',$old_name);                   // delete old picture in storage
                $objFn->del_storage($path.'/1600',$old_name);                   // delete old picture in storage

                $data->img_name = '';
                $data->save();
            }


        }

        return Redirect::Back();
    }

    // ----------------------------------------- Delete Data
    public function destroy($id)
    {

    }


}
