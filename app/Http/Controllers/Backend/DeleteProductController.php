<?php
namespace App\Http\Controllers\Backend;

use App\Model\Product;
use App\Model\BlogProduct;
use Illuminate\Http\Request;
use App\Model\NewPage;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;
use Validator;

class DeleteProductController extends Controller
{

    public function __construct()
    {
//        $this->middleware('admin');
    }

    // ----------------------------------------- Show All List Page


    public function d()
    {
return 'a';
        $rules = array('value' => 'required', 'blog_id' => 'required', 'tb_name' => 'required');
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Response::json(array('success' => false, 'text' => 'Validate Error'));
        } else {


            if ((Input::get('value'))) {
                $Obj = BlogProduct::where('blog_id', Input::get('blog_id'))
                    ->where('product_id', Input::get('value'))
                    ->delete();
                echo "1";
            }
        }
    }
}
//--------------------------------------------------
