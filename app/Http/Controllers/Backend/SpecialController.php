<?php
namespace App\Http\Controllers\Backend;

use App\Model\SpecialCategory;
use Illuminate\Http\Request;
use App\Model\Special;
use App\Model\Product;
use App\Model\Category;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;

class SpecialController extends Controller
{
    public $model = 'App\Model\Special';
    public $titlePage = 'Special';
    public $tbName = 'special';
    public $pkField = 'special_id';
    public $fieldList = array('product_id','price','start_date','end_date');
    public $a_search = array('product_id');
    public $path = '_admin/special';
    public $page = 'special';
    public $viewPath = 'backend/special';

 /*   public $model2 = 'App\Model\SpecialCategory';
    public $tbName2 = 'special_category';
    public $pkField2 = 'special_category_id';
    public $fieldList2 = array('category_id','price','special_type','start_date','end_date');
    public $a_search2 = array('category_id');
    public $path2 = '_admin/special_category';
    public $page2 = 'special_category';
    public $viewPath2 = 'backend/special_category';*/

    public function __construct()
    {
        $this->middleware('admin');
    }

    // ----------------------------------------- Show All List Page
    public function index()
    {
        $perPage = Config::get('mainConfig.perPage');
        $orderBy = Input::get('orderBy');
        if(empty($orderBy)) $orderBy = $this->pkField;
        $sortBy = Input::get('sortBy');
        if(empty($sortBy)) $sortBy = 'desc';

        $search = Input::get('search');

        $model = $this->model;

        $product_id = Input::get('product_id');
        $subtitle = Product::find($product_id);


        if($product_id!=''){

            $data = $model::where('product_id',$product_id);
        }else{

            $data = new $model;

        }



        if(!empty($search))
        {
            $data = $data->where(function ($query) use($search){
                foreach($this->a_search as $field)
                {
                    $query = $query->orWhere($field, 'like', '%'.$search.'%');
                }
            });
        }
        $countData = $data->count();
        $data = $data
            ->orderBy($orderBy,$sortBy)
            ->paginate($perPage);
        $data->setPath($this->page);
        $data->lastPage();


        $data2 = db::table('special_category');
        $countData2 = $data2->count();
        $data2 = $data2->get();

        $data3 = db::table('special_tag');
        $countData3 = $data3->count();
        $data3 = $data3->get();



        return view($this->viewPath.'/index',compact('data','data2','data3','countData','countData2','countData3','product_id','subtitle'));
    }

    // ----------------------------------------- View Add Page
    public function create()
    {
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";



        $product_id = Input::get('product_id');


        if($product_id !=""){
            $a_product_name = Product::select("product_name_th","product_id")->where('product_id',$product_id)->first();
            $product_name = $a_product_name->product_name_th;
            $product_id1 = $a_product_name->productid;


            return view($this->viewPath.'/update',compact('url_to','method','txt_manage','product_id1','product_id','product_name'));

        }else{

            $product = Product::all();
            $product_id1 = " ";

            return view($this->viewPath.'/update',compact('url_to','method','txt_manage','product_id1','product_id','product'));
        }


//        return $product_name;



    }

    // ----------------------------------------- Record Data
    public function store(Request $request)
    {
        $objFn = new MainFunction();
        $model = $this->model;
        $data = new $model;
         $objFn->db_add($data,$this->pkField,$request,$this->fieldList);
        $strParam = $request->strParam;






        return Redirect::to($this->path.'?'.$strParam);
    }

    // ----------------------------------------- Show Data : ID
    public function show($id)
    {

    }

    // ----------------------------------------- View Update Page
    public function edit($id)
    {
        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = "Update";
        Session::put('referUrl',URL::previous());


        $model = $this->model;
        $data = $model::find($id);
        $product_id = Input::get('product_id');

        if($product_id !=""){
            $a_product_name = Product::select("product_name_th")->where('product_id',$product_id)->first();
            $product_name = $a_product_name->product_name_th;
            $product_id1 = $a_product_name->productid;

            return view($this->viewPath.'/update',compact('data','url_to','method','txt_manage','product_id1','product_id','product_name'));

        }else{
            $product = Product::all();
            $product_id1 = " ";

            return view($this->viewPath.'/update',compact('data','url_to','method','txt_manage','product_id1','product_id','product'));

        }






    }

    // ----------------------------------------- Record Update Data
    public function update(Request $request, $id )
    {
        $objFn = new MainFunction();
        $strParam = $request->strParam;

        $model = $this->model;
        $data = $model::find($id);
        $objFn->db_update($data,$this->pkField,$request,$this->fieldList);

        return Redirect::to($this->path.'?'.$strParam);
    }

    // ----------------------------------------- Delete Data
    public function destroy($id)
    {
        Session::put('referUrl',URL::previous());
        $model = $this->model;
        $model::find($id)->delete();

        return Redirect::to(Session::get('referUrl'));
    }
}

