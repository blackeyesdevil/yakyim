<?php
namespace App\Http\Controllers\Backend;

use App\Model\Banner;
use App\Model\Category;
use App\Model\Location;
use App\Model\Product;
use App\Model\ProductGallery;

use Illuminate\Http\Request;
use App\Model\NewPage;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;

class SequenceController extends Controller
{

    public function __construct()
    {
//        $this->middleware('admin');
    }


    public function postBanner(Request $request)
    {

        $banner_id = $request->banner_id;
        $new_sorting = $request->new_sorting;
        $old_sorting = $request->old_sorting;

        $data = Banner::where('sorting', $new_sorting)->where('type',$request->type)->first();

        if (count($data) == 0) {

            $change_data2 = Banner::find($banner_id);
            $change_data2->sorting = $new_sorting;
            $change_data2->save();
            return redirect()->back();

        }

        $change_data = Banner::find($data->banner_id);
        $change_data->sorting = $old_sorting;
        $change_data->save();

        $change_data2 = Banner::find($banner_id);
        $change_data2->sorting = $new_sorting;
        $change_data2->save();


        return redirect()->back();


    }
    public function postCategory(Request $request)
    {

        $category_id = $request->category_id;
        $new_sorting = $request->new_sorting;
        $old_sorting = $request->old_sorting;

        $data = Category::where('sorting', $new_sorting)->first();

        if (count($data) == 0) {

            $change_data2 = Category::find($category_id);
            $change_data2->sorting = $new_sorting;
            $change_data2->save();
            return redirect()->back();

        }

        $change_data = Category::find($data->category_id);
        $change_data->sorting = $old_sorting;
        $change_data->save();

        $change_data2 = Category::find($category_id);
        $change_data2->sorting = $new_sorting;
        $change_data2->save();


        return redirect()->back();


    }
    public function postLocation(Request $request)
    {

        $location_id = $request->location_id;
        $new_sorting = $request->new_sorting;
        $old_sorting = $request->old_sorting;

        $data = Location::where('sorting', $new_sorting)->first();

        if (count($data) == 0) {

            $change_data2 = Location::find($location_id);
            $change_data2->sorting = $new_sorting;
            $change_data2->save();
            return redirect()->back();

        }

        $change_data = Location::find($data->location_id);
        $change_data->sorting = $old_sorting;
        $change_data->save();

        $change_data2 = Location::find($location_id);
        $change_data2->sorting = $new_sorting;
        $change_data2->save();


        return redirect()->back();


    }

    public function postProductnew(Request $request)
    {

        $product_id = $request->product_id;
        $new_sorting = $request->new_sorting;
        $old_sorting = $request->old_sorting;

        $data = Product::where('new_sorting', $new_sorting)->where('new_product','1')->first();

        if (count($data) == 0) {

            $change_data2 = Product::find($product_id);
            $change_data2->new_sorting = $new_sorting;
            $change_data2->save();
            return redirect()->back();

        }

        $change_data = Product::find($data->product_id);
        $change_data->new_sorting = $old_sorting;
        $change_data->save();

        $change_data2 = Product::find($product_id);
        $change_data2->new_sorting = $new_sorting;
        $change_data2->save();


        return redirect()->back();


    }

    public function postProductrecommend(Request $request)
    {

        $product_id = $request->product_id;
        $new_sorting = $request->new_sorting;
        $old_sorting = $request->old_sorting;

        $data = Product::where('recommend_sorting', $new_sorting)->where('recommend','1')->first();

        if (count($data) == 0) {

            $change_data2 = Product::find($product_id);
            $change_data2->recommend_sorting = $new_sorting;
            $change_data2->save();
            return redirect()->back();

        }

        $change_data = Product::find($data->product_id);
        $change_data->recommend_sorting = $old_sorting;
        $change_data->save();

        $change_data2 = Product::find($product_id);
        $change_data2->recommend_sorting = $new_sorting;
        $change_data2->save();


        return redirect()->back();


    }

    public function postProductgallery(Request $request)
    {

        $product_gallery_id = $request->product_gallery_id;
        $product_id = $request->product_id;
        $new_sorting = $request->new_sorting;
        $old_sorting = $request->old_sorting;

        $data = ProductGallery::where('sorting', $new_sorting)->where('product_id',$product_id)->first();

        if (count($data) == 0) {
            ProductGallery::where('product_gallery_id',$product_gallery_id)->where('product_id',$product_id)->update(['sorting' => $new_sorting]);
            return redirect()->back();
        }

        ProductGallery::where('product_gallery_id',$data->product_gallery_id)->where('product_id',$data->product_id)->update(['sorting' => $old_sorting]);
        ProductGallery::where('product_gallery_id',$product_gallery_id)->where('product_id',$product_id)->update(['sorting' => $new_sorting]);

        return redirect()->back();


    }

}
    //--------------------------------------------------
