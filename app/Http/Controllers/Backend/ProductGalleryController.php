<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Model\Product;
use App\Model\ProductGallery;
use App\Library\MainFunction;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Redirect;
use URL;
use Session;
use Config;
use Storage;

class ProductGalleryController extends Controller
{
    public $model = 'App\Model\ProductGallery';
    public $titlePage = 'Product Gallery';
    public $tbName = 'product_gallery';
    public $pkField = 'product_gallery_id';
    public $fieldList = array('product_id', 'photo');
    public $a_search = array('product_id');
    public $path = '_admin/product_gallery';
    public $page = 'product_gallery';
    public $viewPath = 'backend/product_gallery';

    public function __construct()
    {
        $this->middleware('admin');
    }

    // ----------------------------------------- Show All List Page
    public function index()
    {
        $perPage = Config::get('mainConfig.perPage');
        $orderBy = Input::get('orderBy');
        if(empty($orderBy)) $orderBy = $this->pkField;
        $sortBy = Input::get('sortBy');
        if(empty($sortBy)) $sortBy = 'desc';

        $search = Input::get('search');

        

        $product_id = Input::get('product_id');
        $model = $this->model;

        $data = new $model;
        $data = $model::where('product_id',$product_id);
        if(!empty($search))
        {
            $data = $data->where(function ($query) use($search){
                foreach($this->a_search as $field)
                {
                    $query = $query->orWhere($field, 'like', '%'.$search.'%');
                }
            });
        }
        $countData = $data->count();
        $data = $data
            ->orderBy($orderBy,$sortBy)
            ->paginate($perPage);
        $data->setPath($this->page);
        $data->lastPage();

        return view($this->viewPath.'/index',compact('data','countData','product_id'));
    }

    // ----------------------------------------- View Add Page
    public function create()
    {
        $url_to = $this->path;
        $method = 'POST';
        $txt_manage = "Add";
        $product_id = Input::get('product_id');

        return view($this->viewPath.'/update',compact('url_to','method','txt_manage'));
    }

    // ----------------------------------------- Record Data
    public function store(Request $request)
    {
        $objFn = new MainFunction();
        $strParam = $request->strParam;

        $model = $this->model;
        $data = new $model;

       $id = $objFn->db_add($data,$this->pkField,$request,$this->fieldList);

//        if($request->hasFile('photo')){
//            foreach($request->file('photo') as $file) {
//
//                $photo = $file;                    // get image from form
//                $path = public_path('uploads/product/');           // set path
//                $new_name = date('YmdHis') . '-' . rand(10000, 99999);
//                $extension = $photo->getClientOriginalExtension();      // get extension
//                $filename = $new_name . "." . $extension;
//                $destinationPath = $path;
//                $objFn->img_full_resize($photo, $destinationPath, $filename);   //
//                $objFn->image_resize($photo, $destinationPath, 100, $filename);   // resize image (width: 100px)
//                $objFn->image_resize($photo, $destinationPath, 340, $filename);   // resize image (width: 340px)
//                $objFn->image_resize($photo, $destinationPath, 420, $filename);   // resize image (width: 420px)
//                $objFn->image_resize($photo, $destinationPath, 570, $filename);   // resize image (width: 570px)
//
//                $data = new ProductGallery();
//                $data->photo = $filename;
//                $data->product_id = $id;
//                $data->created_at = date('Y-m-d H:i:s');
//                $data->save();
//
//            }
//        }

        if (Input::hasFile('photo')) { // เพิ่มตรงนี้
            $photo = $request->file('photo');                    // get image from form
            $new_name = date('YmdHis').'-'.rand(10000, 99999);                             // set new name
            $path = public_path('uploads/product');           // set path
            $extension = $photo->getClientOriginalExtension();      // get extension
            $filename = $new_name . "." . $extension;               // set filename
            $destinationPath = $path;


            $objFn->img_full_resize($photo, $destinationPath, $filename);   //
            $objFn->image_resize($photo, $destinationPath, 100, $filename);   // resize image (width: 100px)
            $objFn->image_resize($photo, $destinationPath, 340, $filename);   // resize image (width: 340px)
            $objFn->image_resize($photo, $destinationPath, 420, $filename);   // resize image (width: 420px)
            $objFn->image_resize($photo, $destinationPath, 570, $filename);   // resize image (width: 570px)

            $data = $model::find($id);
            $data->photo = $filename;
            $data->save();
        }
           

        return Redirect::to($this->path.'?'.$strParam);
    }

    // ----------------------------------------- Show Data : ID
    public function show($id)
    {

    }

    // ----------------------------------------- View Update Page
    public function edit($id)
    {
        $url_to = $this->path.'/'.$id;
        $method = 'PUT';
        $txt_manage = "Update";
        Session::put('referUrl',URL::previous());

        $model = $this->model;
        $data = $model::find($id);

        $gallery = DB::table('product_gallery')->whereNull('deleted_at')->where('product_id',$id)->select('photo','product_gallery_id');
        $gallery = $gallery->get();

        return view($this->viewPath.'/update',compact('data','url_to','gallery','method','txt_manage'));
    }

    // ----------------------------------------- Record Update Data
    public function update(Request $request, $id )
    {
        $objFn = new MainFunction();
        $strParam = $request->strParam;

        $model = $this->model;
        $data = $model::find($id);
        $old_name = $data->photo;
        $objFn->db_update($data,$this->pkField,$request,$this->fieldList);

        if (Input::hasFile('photo')) { // test P.
            $photo = $request->file('photo');                    // get image from form
            $new_name = date('YmdHis').'-'.rand(10000, 99999);                             // set new name
            $path = 'uploads/product';           // set path

            unlink($path.'/'.$old_name);                   // delete old picture in storage
            unlink($path.'/100/'.$old_name);                   // delete old picture in storage
            unlink($path.'/340/'.$old_name);                   // delete old picture in storage
            unlink($path.'/420/'.$old_name);                   // delete old picture in storage
            unlink($path.'/570/'.$old_name);                   // delete old picture in storage

            $extension = $photo->getClientOriginalExtension();      // get extension
            $filename = $new_name . "." . $extension;               // set filename
            $destinationPath = $path;
            $objFn->img_full_resize($photo, $destinationPath, $filename);   // resize image
            $objFn->image_resize($photo, $destinationPath, 100, $filename);   // resize image (width: 100px)
            $objFn->image_resize($photo, $destinationPath, 340, $filename);   // resize image (width: 340px)
            $objFn->image_resize($photo, $destinationPath, 420, $filename);   // resize image (width: 420px)
            $objFn->image_resize($photo, $destinationPath, 570, $filename);   // resize image (width: 570px)
            $data = $model::find($id);
            $data->photo = $filename;
            $data->save();
        }

        return Redirect::to($this->path.'?'.$strParam);
    }

    // ----------------------------------------- Delete Data
    public function destroy($id)
    {
        Session::put('referUrl',URL::previous());
        $model = $this->model;

        $path = 'uploads/product/';           //// set path

        $data2 = $model::find($id);

        $file = $data2->photo;
        if($file != '') {
           unlink($path.$file);                    // delete image in public path
           unlink($path.'/100/'.$file);                   // delete resize image
           unlink($path.'/340/'.$file);                   // delete resize image
           unlink($path.'/420/'.$file);                   // delete resize image
           unlink($path.'/570/'.$file);                   // delete resize image
        }

        $model::find($id)->delete();

        return Redirect::to(Session::get('referUrl'));
    }
}
