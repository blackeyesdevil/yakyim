<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use App\Model\Zipcode;
use App\Model\Province;
use App\Model\District;
use App\Model\Amphur;

class AddressController extends Controller
{
    public function __construct(){

    }
    public function province(){
        $lang=session()->get('locale');
        $data = Province::select('province_id as id','province_name_'.$lang.' as name')
            ->orderBy('province_name_'.$lang,'asc')
            ->get();
//        return "Y";
        return $data;
    }
    public function amphur($province_id){
        $lang=session()->get('locale');
        $data = Amphur::select('amphur_id as id','amphur_name_'.$lang.' as name')
            ->where('province_id',$province_id)
            ->orderBy('amphur_name_'.$lang,'asc')
            ->get();
        return $data;
    }
    public function district($amphur_id){
        $lang=session()->get('locale');
        $data = District::select('district_id as id','district_name_'.$lang.' as name')
            ->where('amphur_id',$amphur_id)
            ->orderBy('district_name_'.$lang,'asc')
            ->get();
        return $data;
    }
    public function zipcode($district_id){
        $data = Zipcode::select('zipcode as id','zipcode as name')
            ->join('a_district','a_district.district_code','=','a_zipcode.district_code')
            ->where('district_id',$district_id)
            ->orderBy('zipcode','asc')
            ->first();
        return $data;
    }
}
