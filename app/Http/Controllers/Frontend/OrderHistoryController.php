<?php
namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use App\Model\User;
use App\Model\Orders;
use App\Model\OrdersDetail;
use App\Model\PaymentInform;
use App\Model\LogCodeUsed;
use App\Model\Product;

use Hash;
use Validator;
use Crypt;
use Carbon\Carbon;
use Mail;
use Input;
use SEOMeta;
use OpenGraph;

class OrderHistoryController extends Controller
{
    public function __construct(){
        $title = trans('seo.title');
        $description = trans('seo.description');
        $keyword = trans('seo.keyword');
        SEOMeta::setTitle('My Account - '.$title);
        SEOMeta::setDescription($description);
        SEOMeta::addKeyword($keyword);

        OpenGraph::setDescription($description);
        OpenGraph::setTitle($title);
        OpenGraph::setUrl(request()->url());
        OpenGraph::addImage(['url' => 'pathImg', 'size' => 300]);

        $this->user_id = session()->get('s_user_id');
        $this->user = User::find($this->user_id);
        $this->session_id = $session_id = session()->getId();
        $this->lang = session()->get('locale');
    }

    public function getIndex(){
        $objFn = new MainFunction();
        $user_id = $this->user_id;
        $user = $this->user;

        $data = Orders::where('user_id',$user_id)->orderBy('orders_id','desc')->paginate(10);

        foreach($data as $value){
            $value->payment_inform_id = null;
            $payment = PaymentInform::where('orders_id',$value->orders_id)->orderBy('payment_inform_id','desc')->first();
            if(!empty($payment)){
                $value->payment_inform_id = $payment->payment_inform_id;
            }
        }

        return view('frontend.account.order', compact('user', 'data','objFn'));
    }
    public function getDetail($orders_no){
        $objFn = new MainFunction();
        $objGal = new Product();
        $user_id = $this->user_id;
        $user = $this->user;
        $order = Orders::where('user_id',$user_id)->where('orders_no',$orders_no)->first();
        if(empty($order))
            return redirect()->to('account/order-history');

        $orders_id = $order->orders_id;
        $order_detail = OrdersDetail::where('orders_id',$orders_id)->get();
        $objGal->getFirstImage($order_detail);

        $log_code = LogCodeUsed::where('orders_id',$orders_id)->first();
        return view('frontend.account.order-detail', compact('user', 'order', 'order_detail','objFn','log_code'));
    }
    public function payment($orders_no){
        $encrypted = Crypt::encrypt($orders_no);
        return redirect()->to('paypal?token='.$encrypted);        
    }
}
