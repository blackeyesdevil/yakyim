<?php
namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use App\Model\NewPage;

class NewPageController extends Controller
{
    public function main($name,$id){
        $data = NewPage::find($id);

        return view('frontend.new-page',compact('data'));
    }
}
