<?php
namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use App\Model\Subscribe;

class SubscribeController extends Controller
{
    public function __construct()
    {

    }
    public function getIndex(){

    }
    public function postSend(Request $request){
        $email = $request->email;
        $data = Subscribe::where('email',$email)->first();
        if(empty($data)){
            Subscribe::create(['email' => $email]);
        }

        return ['status' => $email];
    }
}
