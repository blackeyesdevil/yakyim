<?php
namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;
use App\Model\Location;
use DB;
use Input;

class LocationController extends Controller
{
    public function __construct()
    {

    }
    public function getIndex(){
        $location = Location::get();
        return response()->json($location);
    }
}