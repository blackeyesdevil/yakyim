<?php
namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

class LocaleController extends Controller
{
    public function lang($lang){
        $available = ['th','en'];
        if(!in_array($lang, $available)) $lang = config()->get('app.locale');
        session()->put('locale',$lang);

        session()->forget('s_discount_id');
        session()->forget('s_discount_code');
        session()->forget('s_use_point');
        session()->forget('s_use_point_dollar');
        // return session()->get('locale');
        return redirect()->back();
    }
}
