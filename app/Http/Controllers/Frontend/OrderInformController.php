<?php
namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use App\Model\User;
use App\Model\Orders;
use App\Model\PaymentInform;

use Hash;
use Validator;
use Crypt;
use Carbon\Carbon;
use Mail;
use Input;
use SEOMeta;
use OpenGraph;


class OrderInformController extends Controller
{
    public function __construct(){
        $title = trans('seo.title');
        $description = trans('seo.description');
        $keyword = trans('seo.keyword');
        SEOMeta::setTitle('My Account - '.$title);
        SEOMeta::setDescription($description);
        SEOMeta::addKeyword($keyword);

        OpenGraph::setDescription($description);
        OpenGraph::setTitle($title);
        OpenGraph::setUrl(request()->url());
        OpenGraph::addImage(['url' => 'pathImg', 'size' => 300]);

        $this->user_id = session()->get('s_user_id');
        $this->user = User::find($this->user_id);
        $this->session_id = $session_id = session()->getId();
        $this->lang = session()->get('locale');
    }

    public function inform($orders_no){
        $user = $this->user;
        $user_id = $this->user_id;
        $order = Orders::where('user_id',$user_id)->where('orders_no',$orders_no)->first();
        if(empty($order))
            return redirect()->to('account/order-history');

        return view('frontend.account.inform',compact('user', 'order'));
    }
    public function check(Request $request){
        $objFn = new MainFunction();
        $user_id = $this->user_id;
        $orders_no = $request->orders_no;
        $order = Orders::where('user_id',$user_id)->where('orders_no',$orders_no)->first();
        if(empty($order))
            return redirect()->to('account/order-history');

        if (session()->get('locale') == 'en'){
            $grand_total = $order->dollar_total_price + $order->dollar_shipping_price - $order->dollar_discount_price - $order->dollar_use_point;
        }
        else{
            $grand_total = $order->total_price + $order->shipping_price - $order->discount_price - $order->use_point;
        }

        $total_price = $request->total_price;
        $total_price = str_replace(',', '', $total_price);

        $validator = Validator::make($request->all(),
            [
                "total_price" => 'required',
                "payment_date" => 'required',
                "img_name" => 'required',
                "bank" => 'required',
            ]
        );
        // Validate fields
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        if($grand_total != $total_price)
            return redirect()->back()->with('errorMsg', trans('message.inform_invalid_total_price'));

        $input_all = $request->except(['img_name']);
        $input_all['orders_id'] = $order->orders_id;

        $inform = PaymentInform::create($input_all);
        $inform_id = $inform->payment_inform_id;

        if (Input::hasFile('img_name')) {
            $photo = $request->file('img_name');
            $new_name = date('YmdHis').'-'.rand(10000, 99999);
            $path = public_path('uploads/inform');
            $extension = $photo->getClientOriginalExtension();
            $filename = $new_name . "." . $extension;
            $destinationPath = $path;
            $objFn->img_full_resize($photo, $destinationPath, $filename);

            PaymentInform::where('payment_inform_id',$inform_id)->update(['img_name' => $filename]);
        }

        $inform = PaymentInform::find($inform_id);
        Mail::send('emails.inform', ['inform' => $inform, 'orders_no' => $orders_no], function ($m) use($orders_no) {
            $m->from(session()->get('s_user_email'), session()->get('s_user_name'));
            $m->to(env('MAIL_CONTACT'), 'Webmaster Yakyim');
            $m->subject('แจ้งการชำระเงิน ใบสั่งซื้อเลขที่ '.$orders_no);
        });

        session()->flash('successMsg', trans('message.inform_success'));
        return redirect()->to('account/order-history');
    }


}
