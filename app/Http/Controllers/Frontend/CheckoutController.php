<?php
namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use App\Model\User;
use App\Model\Cart;
use App\Model\Product;
use App\Model\ProductOption;
use App\Model\ProductGallery;
use App\Model\Special;
use App\Model\AddressBook;

use App\Model\DiscountCode;
use App\Model\LogCodeUsed;
use App\Model\ShippingRate;
use App\Model\Province;
use App\Model\District;
use App\Model\Amphur;
use App\Model\Dollar;
use App\Model\PointTransaction;

use Illuminate\Support\Facades\Session;
use Validator;
use Carbon\Carbon;
use Input;
use SEOMeta;
use OpenGraph;

class CheckoutController extends Controller
{
    public function __construct(){
        $this->middleware('account');
        $this->user_id = session()->get('s_user_id');
        $this->session_id = $session_id = session()->getId();
        $this->lang = 'th';

        $title = trans('seo.title');
        $description = trans('seo.description');
        $keyword = trans('seo.keyword');
        SEOMeta::setTitle($title);
        SEOMeta::setDescription($description);
        SEOMeta::addKeyword($keyword);

        OpenGraph::setDescription($description);
        OpenGraph::setTitle($title);
        OpenGraph::setUrl(request()->url());
        OpenGraph::addImage(['url' => 'pathImg', 'size' => 300]);
    }
    public function getIndex(){
        // ทุกๆครั้งที่เค้าหน้านี้ให้เคลียร์
        session()->forget('s_discount_id');
        session()->forget('s_discount_code');
        session()->forget('s_use_point');
        session()->forget('s_use_point_dollar');

        $lang = $this->lang;
        $user_id = $this->user_id;
        $user = User::find($user_id);
        $obj_dollar = new Dollar();
        $session_id = $this->session_id;
        // Select Cart
        $objCart = new Cart();
        $result = $objCart->cart();

        // If there is no item in the cart, then redirect to Shopping Cart Page
        if (count($result['total_qty']) == 0)
            return redirect()->to('cart');

        $carts = $result['carts'];
        foreach ($carts as $cart){
            $cart->img_name = null;
            $photo = ProductGallery::where('product_id', $cart->product_id)->whereNotNull('photo')->first();
            if (!empty($photo))
                $cart->img_name = $photo->photo;
        }

        // Calculate Shipping Rate
        $objShipp = new ShippingRate();
        $shipping_price = $objShipp->shipping_price($result['total_price']);

        // Select Billing Address
        $billing = AddressBook::where('user_id',$user_id)->where('billing_shipping','1')->first();
        // Select Shipping Address
        $shipping = AddressBook::where('user_id',$user_id)->where('billing_shipping','2')->orderBy('set_default','desc')->get();


        // Check Birth Day Discount ว่าใช้ไปแล้วยัง
        $chk_used_code = LogCodeUsed::where('user_id',$user_id)
            ->whereIn('discount_code_id',[1,2,3,4])
            ->where('created_at','like','%'.date('Y').'%')
            ->orderBy('log_code_used_id','desc')->get()->count();

        // Check Point
        $user_point = PointTransaction::select('total')->where('user_id',$user_id)->orderBy('point_trans_id','desc')->first();


        return view('frontend.cart.checkout',compact('user', 'carts','billing','shipping','shipping_price','province','amphur','district','obj_dollar','chk_used_code','user_point'));
    }
}
