<?php
namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use App\Model\User;
use App\Model\Cart;
use App\Model\ProductOptionGallery;
use App\Model\DiscountCode;
use App\Model\LogCodeUsed;
use App\Model\ShippingRate;
use App\Model\Dollar;
use App\Model\PointTransaction;

use Validator;
use Carbon\Carbon;
use Input;

class DiscountController extends Controller
{
    public function __construct(){
        $this->user_id = session()->get('s_user_id');
        $this->session_id = $session_id = session()->getId();
        $this->lang = session()->get('locale');

    }
    public function postBirthday(Request $request){
        $obj_dollar = new Dollar();
        $objCart = new Cart();
        $objShipp = new ShippingRate();

        $this->clearsession();
        $lang = $this->lang;
        $year = date('Y');
        $today = date('m-d');

        $user_id = $this->user_id;
        $user = User::find($user_id);
        $user_birthday = substr($user->birth_date,5);
        $user_type = $user->user_type;

        $shipping_type = $request->shipping_method;
        $payment_type = $request->payment_method;


        // -------------------- Get Currency
        $dollar = Dollar::first();
        $currency = $dollar->dollar;
        // -------------------- Get Cart
        $result = $objCart->cart();
        $total_price = $result['total_price'];
        $cart_normal_price = $result['normal_price'];

        // Check Birth Day Discount ว่าใช้ไปแล้วยัง
        $chk_used_code = LogCodeUsed::where('user_id',$user_id)
            ->whereIn('discount_code_id',[1,2,3,4])
            ->where('created_at','like','%'.date('Y').'%')
            ->orderBy('log_code_used_id','desc')->get()->count();

        if($chk_used_code == 0 && $today == $user_birthday){
            if($user_type == 0){ // สมาชิกปกติ
                if($payment_type == 1){ // โอนเงิน, discount_code_id = 1
                    $discount_code_id = 1;
                    $data = DiscountCode::find($discount_code_id);
                    $min_price = $data->min_price;
                    if($total_price >= $min_price) {
                        $percent = $data->discount / 100;
                        $discount_code = $data['code_name_'.$lang];
                    }else{
                        $result = [ 'status'=>'0', 'text'=> trans('message.discount-min-price')];
                        return $result;
                    }
                }else if($payment_type == 2){ // Paypal, discount_code_id = 2
                    $discount_code_id = 2;
                    $data = DiscountCode::find($discount_code_id);
                    $min_price = $data->min_price;
                    if($total_price >= $min_price) {
                        $percent = $data->discount / 100;
                        $discount_code = $data['code_name_'.$lang];
                    }else{
                        $result = [ 'status'=>'0', 'text'=> trans('message.discount-min-price')];
                        return $result;
                    }
                }
            }else{ // สมาชิก VIP
                if($payment_type == 1){ // โอนเงิน, discount_code_id = 3
                    $discount_code_id = 3;
                    $data = DiscountCode::find($discount_code_id);
                    $min_price = $data->min_price;
                    if($total_price >= $min_price) {
                        $percent = $data->discount / 100;
                        $discount_code = $data['code_name_'.$lang];
                    }else{
                        $result = [ 'status'=>'0', 'text'=> trans('message.discount-min-price')];
                        return $result;
                    }
                }else if($payment_type == 2){ // Paypal, discount_code_id = 4
                    $discount_code_id = 4;
                    $data = DiscountCode::find($discount_code_id);
                    $min_price = $data->min_price;
                    if($total_price >= $min_price) {
                        $percent = $data->discount / 100;
                        $discount_code = $data['code_name_'.$lang];
                    }else{
                        $result = [ 'status'=>'0', 'text'=> trans('message.discount-min-price')];
                        return $result;
                    }
                }
            }

            // หาราคาส่วนลด (เฉพาะสินค้าราคาปกติ)
            $discount_price = $cart_normal_price * $percent;
            $sub_total = $total_price - $discount_price;
            // หาราคาจัดส่งใหม่
            $shipping_price = $objShipp->shipping_price($sub_total);
            if($user_type == '0' && $shipping_type == '2') $shipping_price = 0;

            // คำนวณราคารวมทั้งหมด
            $grand_total = $total_price - $discount_price + $shipping_price;
            $use_point = 0;
            if(session()->has('s_use_point')){
                $use_point = session()->get('s_use_point');
                if($use_point > $grand_total) $use_point = $grand_total;
                $grand_total = $grand_total - $use_point;
            }
            if($lang == 'en'){
                $discount_price = $obj_dollar->cal_dollar($discount_price,$currency);
                $shipping_price = $obj_dollar->cal_dollar($shipping_price,$currency);
                $grand_total = $obj_dollar->cal_dollar($grand_total,$currency);
            }
            $this->setsession($discount_code_id,$discount_code);
            $result = [ 'status'=>'1', 'text'=>'รหัสส่วนลดถูกต้อง', 'discount_price'=>$discount_price, 'shipping_price' => $shipping_price,'grand_total'=>$grand_total,'discount_code'=>$discount_code,'use_point' => $use_point];
            return $result;

        }else{
            $result = [ 'status'=>'0', 'text'=> trans('message.discount-min-price')];
            return $result;
        }
    }

    public function postCode(Request $request){
        $lang = $this->lang;
        $discount_code = $request->discount_code;
        $sel_payment_method = $request->payment_method;
        $shipping_method = $request->shipping_method;

        $this->clearsession(); // clear session discount

        $obj_dollar = new Dollar();
        // -------------------- Get Currency
        $dollar = Dollar::first();
        $currency = $dollar->dollar;

        $user_id = $this->user_id;
        session()->put('s_discount_code','');
        session()->put('s_discount_price','');

        $data = DiscountCode::where('code',$discount_code)->where('status','1')->first();
        // เช็คมีโค้ดมั๊ย
        if(empty($data)){
            $result = [ 'status'=>'0', 'text'=> trans('message.discount-not-found')];
            return $result;
        }
        $currentDate = Carbon::now();
        $codeId = $data->discount_code_id;
        $allot = $data->allot;
        $reserved = $data->reserved;
        $used = $data->used;
        $perUse = $data->per_use;
        $startDate = $data->start_date;
        $endDate = $data->end_date;
        $minPrice = $data->min_price;
        $maxPrice = $data->max_price;
        $type = $data->discount_type;
        $discount = $data->discount;
        $discountMax = $data->discount_max;
        $payment_method = $data->payment_method;
        $normalPrice = $data->normal_price;
        $flag = $data->flag;
        // เช็คโค้ดหมดอายุยัง
        if($currentDate >= $startDate && $currentDate <= $endDate){
            // เช็คโค้ดสำหรับสมาชิก VIP
            if($flag == 1 && session()->get('s_user_type') != 1){
                $result = [ 'status'=>'0', 'text'=> trans('message.discount-for-vip')];
                return $result;
            }
            // เช็คโค้ดสำหรับสมาชิกทั่วไป
            if($flag == 0 && session()->get('s_user_type') != 0){
                $result = [ 'status'=>'0', 'text'=>trans('message.discount-for-normal')];
                return $result;
            }
            // เช็คโค้ดหมดยัง
            if($allot == $reserved+$used){
                $result = [ 'status'=>'0', 'text'=> trans('message.discount-empty')];
                return $result;
            }
            // เช็ควิธีการชำระเงิน โอนเงิน
            if($payment_method == 1 && $sel_payment_method != 1){
                $result = [ 'status'=>'0', 'text'=>trans('message.discount-bank-transfer')];
                return $result;
            }
            // เช็ควิธีการชำระเงิน PayPal
            if($payment_method == 2 && $sel_payment_method != 2){
                $result = [ 'status'=>'0', 'text'=>trans('message.discount-paypal')];
                return $result;
            }

            // เช็คโค้ดว่าใช้ไปแล้วกี่ครั้ง / user
            $logData = LogCodeUsed::where('user_id',$user_id)->where('discount_code_id',$codeId)->where('discount_code',$discount_code)->get();
            $countUsed = count($logData);
            if($countUsed >= $perUse){
                $result = [ 'status'=>'0', 'text'=>trans('message.discount-used')];
                return $result;
            }
            // เช็คยอดการสั่งซื้อ
            $objCart = new Cart();
            $cartResult = $objCart->cart();
            $total_price = $cartResult['total_price'];
            $cart_normal_price = $cartResult['normal_price'];

            // เช็คสินค้าราคาปกติ
            if($normalPrice == 1 && $cart_normal_price <= 0){
                $result = [ 'status'=>'0', 'text'=>trans('message.discount-normal-price')];
                return $result;
            }

            if($total_price < $minPrice){
                $result = [ 'status'=>'0', 'text'=>trans('message.discount-min-price')];
                return $result;
            }
            if($total_price > $maxPrice){
                $result = [ 'status'=>'0', 'text'=>trans('message.discount-max-price')];
                return $result;
            }
            // Cal discount price
            if($type == 'fix'){
                $discount_price = $discount;
            }else{
                if($normalPrice == 1){
                    $discount_price = $cart_normal_price*$discount/100;
                }else{
                    $discount_price = $total_price*$discount/100;
                }
                if($discount_price > $discountMax ) $discount_price = $discountMax;
            }
            if($discount_price > $total_price) $discount_price = $total_price;
        }else{
            $result = [ 'status'=>'0', 'text'=>trans('message.discount-expire')];
            return $result;
        }

        $discount_price = round($discount_price,2);
        // คำนวณราคารวมที่หักส่วนลดแล้ว
        $sub_total = $total_price - $discount_price;
        // คำนวณค่าจัดส่งใหม่
        $objShipp = new ShippingRate();
        $shipping_price = $objShipp->shipping_price($sub_total);
        if(session()->get('s_user_type') == '0' && $shipping_method == '2') $shipping_price = 0;

        // คำนวณราคารวมทั้งหมด
        $grand_total = $total_price - $discount_price + $shipping_price;
        $use_point = 0;
        if(session()->has('s_use_point')){
            $use_point = session()->get('s_use_point');
            if($use_point > $grand_total) $use_point = $grand_total;
            $grand_total = $grand_total - $use_point;
        }
        if($lang == 'en'){
            $discount_price = $obj_dollar->cal_dollar($discount_price,$currency);
            $shipping_price = $obj_dollar->cal_dollar($shipping_price,$currency);
            $grand_total = $obj_dollar->cal_dollar($grand_total,$currency);
        }

        $this->setsession($codeId,$discount_code); // set session discount

        $result = [ 'status'=>'1', 'text'=>trans('message.discount-correct'), 'discount_price'=>$discount_price, 'shipping_price' => $shipping_price,'grand_total'=>$grand_total,'discount_code'=>$discount_code, 'use_point' => $use_point];
        return $result;
    }

    public function postFriendCode(Request $request){
        $lang = $this->lang;
        $this->clearsession();
        $discount_code = $request->discount_code;
        $shipping_method = $request->shipping_method;

        $obj_dollar = new Dollar();
        // -------------------- Get Currency
        $dollar = Dollar::first();
        $currency = $dollar->dollar;

        $user_id = $this->user_id;
        $data = User::where('user_no', $discount_code)->where('verified','1')->first();

        if(empty($data)){    // เช็คมี user_no ที่ระบุหรือไม่
            $result = [ 'status'=>'0', 'text'=>trans('message.discount-member-not-found')];
            return $result;
        }else if ($data->user_id == $user_id){    // เช็คกรณีที่ใช้โค้ดของตัวเอง
            $result = [ 'status'=>'0', 'text'=>trans('message.discount-me')];
            return $result;
        }
        else{
            // หาราคารวมของสินค้าราคาปกติ
            $objCart = new Cart();
            $cartResult = $objCart->cart();
            $total_price = $cartResult['total_price'];
            $cart_normal_price = $cartResult['normal_price'];

            if($total_price < 1500){
                $result = [ 'status'=>'0', 'text'=>trans('message.discount-min-price')];
                return $result;
            }
            // หาราคาส่วนลด (เฉพาะสินค้าราคาปกติ)
            $discount_price = $cart_normal_price * 0.1;
            $sub_total = $total_price - $discount_price;

            // หาราคาจัดส่งใหม่
            $objShipp = new ShippingRate();
            $shipping_price = $objShipp->shipping_price($sub_total);
            if(session()->get('s_user_type') == '0' && $shipping_method == '2') $shipping_price = 0;

            // คำนวณราคารวมทั้งหมด
            $grand_total = $total_price - $discount_price + $shipping_price;
            $use_point = 0;
            if(session()->has('s_use_point')){
                $use_point = session()->get('s_use_point');
                if($use_point > $grand_total) $use_point = $grand_total;
                $grand_total = $grand_total - $use_point;
            }
            if($lang == 'en'){
                $discount_price = $obj_dollar->cal_dollar($discount_price,$currency);
                $shipping_price = $obj_dollar->cal_dollar($shipping_price,$currency);
                $grand_total = $obj_dollar->cal_dollar($grand_total,$currency);
            }

            $this->setsession('0',$discount_code);

            $result = [ 'status'=>'1', 'text'=>trans('message.discount-correct'), 'discount_price'=>$discount_price, 'shipping_price' => $shipping_price,'grand_total'=>$grand_total,'discount_code'=>$discount_code,'use_point'=>$use_point];
            return $result;
        }
    }

    public function getUsePoint(){
        $obj_dollar = new Dollar();
        // -------------------- Get Currency
        $dollar = Dollar::first();
        $currency = $dollar->dollar;

        $lang = $this->lang;
        $user_id = $this->user_id;
        $user_point = PointTransaction::select('total')->where('user_id',$user_id)->orderBy('point_trans_id','desc')->first();
        if(!empty($user_point)){
            $total_point = $user_point->total;
            $dollar_total_point = $obj_dollar->cal_dollar($total_point,$currency);
            session()->put('s_use_point',$total_point);
            session()->put('s_use_point_dollar',$dollar_total_point);

            $grand_total = (float) str_replace(',','',Input::get('grand_total'));
            if($lang == 'en')
                $total_point = $dollar_total_point;
            if($total_point > $grand_total){
                $total_point = $grand_total;
            }

            $cal_grand_total = $grand_total - $total_point;
            $result = [ 'status'=>'1', 'text'=>trans('message.discount-can-use-point'), 'use_point' => $total_point,'grand_total'=>$cal_grand_total];
            return $result;
        }else{
            $this->getClearSessionPoint();
            $result = [ 'status'=>'0', 'text'=>trans('message.discount-can-not-use-point')];
            return $result;
        }

    }
    public function getUnUsePoint(){
        $total_price = (float) str_replace(',','',Input::get('total_price'));
        $discount_price = (float) str_replace(',','',Input::get('discount_price'));
        $shipping_price = (float) str_replace(',','',Input::get('shipping_price'));

        $grand_total = $total_price - $discount_price + $shipping_price;
        $this->getClearSessionPoint();
        $result = [ 'status'=>'1','grand_total'=>$grand_total, 'tt' => $total_price.' '.$discount_price.' '.$shipping_price];
        return $result;
    }

    public function getRemoveDiscount($shipping_method){
        $this->clearsession();
        $lang = $this->lang;
        $obj_dollar = new Dollar();
        // -------------------- Get Currency
        $dollar = Dollar::first();
        $currency = $dollar->dollar;

        // เช็คยอดการสั่งซื้อ
        $objCart = new Cart();
        $cartResult = $objCart->cart();
        $total_price = $cartResult['total_price'];

        // คำนวณค่าจัดส่งใหม่
        $objShipp = new ShippingRate();
        $shipping_price = $objShipp->shipping_price($total_price);
        if(session()->get('s_user_type') == '0' && $shipping_method == '2') $shipping_price = 0;

        // คำนวณราคารวมทั้งหมด
        $grand_total = $total_price + $shipping_price;
        $use_point = 0;
        if(session()->has('s_use_point')){
            $use_point = session()->get('s_use_point');
            if($use_point > $grand_total) $use_point = $grand_total;
            $grand_total = $grand_total - $use_point;
        }

        if($lang == 'en'){
            $shipping_price = $obj_dollar->cal_dollar($shipping_price,$currency);
            $grand_total = $obj_dollar->cal_dollar($grand_total,$currency);
        }

        $result = ['shipping_price' => $shipping_price,'grand_total'=>$grand_total,'use_point' => $use_point];
        return $result;
    }
    public function clearsession(){
        session()->forget('s_discount_id');
        session()->forget('s_discount_code');

    }
    public function setsession($codeId,$discount_code){
        session()->put('s_discount_id',$codeId);
        session()->put('s_discount_code',$discount_code);
    }
    public function getClearSessionPoint(){
        session()->forget('s_use_point');
        session()->forget('s_use_point_dollar');
    }
}
