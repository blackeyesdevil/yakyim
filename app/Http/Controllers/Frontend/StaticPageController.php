<?php
namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;
use App\Model\StaticPage;
use DB;
use Input;

class StaticPageController extends Controller
{
    public function __construct()
    {

    }
    public function getIndex($id){
        $static_page = StaticPage::find($id);
        return response()->json($static_page);
    }
}