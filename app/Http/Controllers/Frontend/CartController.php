<?php
namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use App\Model\User;
use App\Model\Cart;
use App\Model\Product;
use App\Model\Dollar;
use App\Model\ProductOption;
use App\Model\ProductGallery;

use Carbon\Carbon;
use Validator;
use SEOMeta;
use OpenGraph;

class CartController extends Controller
{
    public function __construct(){
        $this->user_id = session()->get('s_user_id');
        $this->session_id = $session_id = session()->getId();
        $this->lang = session()->get('locale');

        $title = trans('seo.title');
        $description = trans('seo.description');
        $keyword = trans('seo.keyword');
        SEOMeta::setTitle($title);
        SEOMeta::setDescription($description);
        SEOMeta::addKeyword($keyword);

        OpenGraph::setDescription($description);
        OpenGraph::setTitle($title);
        OpenGraph::setUrl(request()->url());
        OpenGraph::addImage(['url' => 'pathImg', 'size' => 300]);
    }
    public function getIndex(){
        $objCart = new Cart();
        $obj_dollar = new Dollar();
        $result = $objCart->cart();
        $carts = $result['carts'];
        $total_price = $result['total_price'];

        foreach ($carts as $cart){
            $cart->img_name = null;
            $photo = ProductGallery::where('product_id', $cart->product_id)->whereNotNull('photo')->first();
            if (!empty($photo))
                $cart->img_name = $photo->photo;
        }
//        return $carts;
        return view('frontend.cart.cart',compact('carts','total_price','obj_dollar'));
    }
    // ----- Add to cart from product list
    public function postAdd(Request $request){
        $objCart = new Cart();
        $option_id = $request->option_id;
        $qty = $request->qty;

        $user_id = $this->user_id;
        $session_id = $this->session_id;

        // Find product qty
        $product = ProductOption::find($option_id);
        $product_id = $product->product_id;
        $product_qty = $product->qty;

        // Check cart
        $cart = Cart::where('product_id',$product_id)->where('product_option_id',$option_id);
        if (!empty($user_id))
            $cart = $cart->where('user_id',$user_id);
        else
            $cart = $cart->where('session_id',$session_id);

        $cart = $cart->first();

        if($product_qty == 0){
            $result = [ 'status' => '0', 'text' => trans('message.cart-product-not-enough') ];
            return $result;
        }
        if($qty <= 0){
            $result = [ 'status' => '0', 'text' => trans('message.cart-choose-at-least-one') ];
            return $result;
        }
        if($qty > $product_qty){
            $result = [ 'status' => '0', 'text' => trans('message.cart-product-not-enough') ];
            return $result;
        }
        if(empty($cart)) {
            // ถ้าไม่มีของใน cart
            $new_cart = new Cart();
            if (!empty($user_id)){
                $new_cart->user_id = $user_id;
                $new_cart->session_id = '';
            }
            else{
                $new_cart->session_id = $session_id;
            }
            $new_cart->product_id = $product_id;
            $new_cart->product_option_id = $option_id;
            $new_cart->qty = $qty;
            $new_cart->save();

            $result_cart = $objCart->cart();
            $result = [ 'status' => '1', 'text' => trans('message.cart-add-success'), 'total_qty' => $result_cart['total_qty'] ];
            return $result;
        }else{
            // ถ้ามีของใน cart
            $cart_id = $cart->cart_id;
            $cart_qty = $cart->qty;
            $new_qty = $cart_qty + $qty;
            if($new_qty > $product_qty){
                $result = [ 'status' => '0', 'text' => trans('message.cart-have-max-item') ];
                return $result;
            }
            $update_cart = Cart::find($cart_id);
            $update_cart->qty = $new_qty;
            $update_cart->save();

            $result_cart = $objCart->cart();
            $result = [ 'status' => '1', 'text' => trans('message.cart-add-success'), 'total_qty' => $result_cart['total_qty'] ];
            return $result;
        }
    }

    // ----- Update cart
    public function postUpdate(Request $request){
        $cart_id = $request->cart_id;
        $qty = $request->qty;

        // Find cart
        $cart = Cart::find($cart_id);
        $option_id = $cart->product_option_id;
        $cart_qty = $cart->qty; // Current qty.
        // Find product qty
        $product = ProductOption::join('product','product.product_id','=','product_option.product_id')
            ->where('product_option_id',$option_id)
            ->first();
        $product_status = $product->status;
        $product_qty = $product->qty;

        // check product status
        if($product_status == 0){
            $result = [ 'status' => '0', 'text' => trans('message.cart-product-closed') ];
            return $result;
        }
        // check product qty
        if($product_qty == 0){
            $result = [ 'status' => '0', 'text' => trans('message.cart-out-of-stock') ];
            return $result;
        }
        // check qty
        if($qty <= 0){
            $result = [ 'status' => '0', 'text' => 'message.cart-choose-at-least-one' ];
            return $result;
        }
        // qty > stock
        if($qty > $product_qty){
            $result = [ 'status' => '0', 'text' => trans('message.cart-product-not-enough') ];
            return $result;
        }

        $cart->qty = $qty;
        $cart->save();
        $result = [ 'status' => '1', 'text' => trans('message.cart-update-amount-success') ];
        return $result;
    }

    // ----- Delete cart
    public function postDelete(Request $request){
        $cart_id = $request->cart_id;
        $cart = Cart::find($cart_id);
        $cart->forceDelete();
        $result = [ 'status' => '1', 'text' => trans('message.cart-remove-success') ];
        return $result;
    }

    // ---- Get qty for header
    public static function getQty(){
        $objCart = new Cart();
        $carts = $objCart->cart();
        $qty = 0;
        foreach ($carts as $cart){
            $qty += $cart->cart_qty;
        }
        return $qty;
    }
}
