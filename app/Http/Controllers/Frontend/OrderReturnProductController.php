<?php
namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use App\Model\User;
use App\Model\Orders;
use App\Model\OrdersDetail;
use App\Model\OrderTracking;
use App\Model\PaymentInform;
use App\Model\return_product;
use App\Model\return_product_detail;
use App\Model\Product;
use App\Model\ProductOption;
use App\Model\ProductGallery;

use Carbon\Carbon;

use Validator;
use Input;
use SEOMeta;
use OpenGraph;
use Mail;



class OrderReturnProductController extends Controller
{
    public function __construct(){
        $title = trans('seo.title');
        $description = trans('seo.description');
        $keyword = trans('seo.keyword');
        SEOMeta::setTitle('My Account - '.$title);
        SEOMeta::setDescription($description);
        SEOMeta::addKeyword($keyword);

        OpenGraph::setDescription($description);
        OpenGraph::setTitle($title);
        OpenGraph::setUrl(request()->url());
        OpenGraph::addImage(['url' => 'pathImg', 'size' => 300]);

        $this->user_id = session()->get('s_user_id');
        $this->user = User::find($this->user_id);
        $this->session_id = $session_id = session()->getId();
        $this->lang = session()->get('locale');
    }

    public function getIndex(){
      $objFn = new MainFunction();
      $user_id = $this->user_id;
      $user = $this->user;
      $data_return = [];

      // $data = Orders::where('user_id',$user_id)->where('status_id',6)->where('status_return_product',2)->orderBy('orders_id','desc')->paginate(10);
      $data = Orders::where('user_id', $user_id)->orderBy('orders_date','desc')->get();
      foreach ($data as $value) {
        $return_orders = return_product::where('orders_id', $value->orders_id)->get();
        // if(!empty($return_orders)){
        //   $value->return_order = $return_orders;
        // }
        foreach ($return_orders as $return_order) {
          if(!empty($return_order)){
            $return_order->orders_no = $value->orders_no;
           array_push($data_return, $return_order);
          }
        }
      }

        $data_returns = $data_return;

      // return $data_returns;
      // $data = return_product::where('orders_id')->orderBy('return_date')->get();
      //
      // foreach($data as $value){
      //     $order = Orders::where('orders_id', $value->orders_id)->whereNull('deleted_at')->first();
      //     // $value->payment_inform_id = null;
      //     // $value->status = null;
      //     // $payment = PaymentInform::where('orders_id',$value->orders_id)->orderBy('payment_inform_id','desc')->first();
      //     // $status = return_product::where('orders_id',$value->orders_id)->orderBy('return_date')->first();
      //     if(!empty($order)){
      //         $value->orders_no = $order->orders_no;
      //     }
      //     // if(!empty($status)){
      //     //     $value->status = $status->status;
      //     // }
      //
      // }
      return view('frontend.account.order-return', compact('data', 'data_returns','objFn'));
    }

    public function getDetail($orders_no){
        $objFn = new MainFunction();
        $user_id = $this->user_id;
        $user = $this->user;
        $order = Orders::where('user_id',$user_id)->where('orders_no',$orders_no)->first();
        if(empty($order))
            return redirect()->to('account/order-history');

        $orders_id = $order->orders_id;
        $order_detail = OrdersDetail::where('orders_id',$orders_id)->get();
        foreach ($order_detail as $key => $detail) {
          $product_option = ProductOption::where('product_option_id', $detail->product_option_id)->first();
          $order_detail[$key]['product_option'] = $product_option;
          $product_gallery = ProductGallery::where('product_id', $detail->product_id)->first();
          $order_detail[$key]['product_gallery'] = $product_gallery;
        }
        // return $order_detail;
        return view('frontend.account.order-return-detail', compact('user', 'order', 'order_detail','objFn'));
    }

    public function postReturn_Product(Request $request){

      // print_r($request->all());
      // echo json_encode($request->check);
      // print_r(json_encode($request->check));

      // $validator = Validator::make($request->all(),
      //   [
      //     'checkbox' => 'required',
      //   ]);
      //
      //   if($validator->fails()){
      //     return redirect()->back()->withErrors($validator)->withInput();
      //   }

        // return $request->all();
        $itemlist = $request->check_box;
        $order_shipping = $request->order_shipping;
        $order_no = $request->order_no;
        $order = $request->order;

        $item_number = $request->check_box;
        $item_qty = $request->qty;
        $item_reason = $request->reason;
        $item_items = $request->item;
        $arrayJson=[];

        if(!empty($item_number)){
          for($i = 0; $i < count($item_number); $i++){
            $array = array(
              'product' => json_decode($item_items[$i])->product_name_th,
              'number' => $item_qty[$item_number[$i]],
              'reason' => $item_reason[$item_number[$i]],
              'data' => $item_items[$item_number[$i]]
            );
            array_push($arrayJson,$array);
          }
          // return $arrayJson;
        }else if (empty($item_number)){
          return view('frontend.account.order-return');
        }


      $order = $request->order;
      $or =json_decode($order);
      $orders_id= $or->orders_id;
      $orders_no= $or->orders_no;
      $billing_address= $or->billing_address;
      $orders_date= $or->orders_date;

      $returnProduct = [
        'orders_id' => $orders_id,
        'return_date' => Carbon::now(),
        // 'detail' => $or->orders_no,
        'status' => 1,//status =1 กำลังดำเนินการคืนสินค้า ,status = 2 , คืยสินค้าเรียบร้อยแล้ว
        'order_return_detail' => '',
      ];
      $flight = return_product::create($returnProduct);
      $user = return_product::where('orders_id',$orders_id)->where('status', 1)->orderBy('created_at')->first();
      $order_return_id = $user->order_return_id;
      // if(count($user) == 1){
      //   foreach ($user as $key => $value) {
      //
      //       // echo $value->order_return_id;
      //   }
      // }
      // echo $order_return_id;
      // echo $aa;
      // $arrayJson =json_decode($arrayJson);
      foreach ($arrayJson as $key => $value) {

          $data = $value['data'];
          $data = json_decode($data);
          $product_id = $data->product_id;
          $orders_id = $data->orders_id;
          $orders_detail_id = $data->orders_detail_id;
          $product_id = $data->product_id;
          $number = $value['number'];
          $reason = $value['reason'];
          // echo $product_id.$number.$reason;
          $returnProductDeatail = [
            'order_return_id' => $order_return_id,
            'orders_id' => $orders_id,
            'orders_detail_id' => $orders_detail_id,
            'product_id' => $product_id,
            'reason' => $reason,
            'qty' => $number,
          ];
          $go = return_product_detail::create($returnProductDeatail);

      }
      $orders = Orders::where('orders_id',$orders_id)->update(['status_return_product' => 2]);
      // $orders = json_encode($or);
      // $orders = json_decode($orders);
      // send mail to admin
      Mail::send('frontend.account.return-mail',['orders' => $or, 'itemlist'=>$arrayJson],function($m){
          // $m->to('nut.nutvorn@gmail.com', 'Webmaster YakYim');
          $m->to(env('MAIL_CONTACT'), 'Webmaster Yakyim');
          $m->subject('แจ้งการคืนสินค้า');
      });
      $emyty_detail = '';
      $order = json_decode($order);
      $orders_id = $order->orders_id;
      $returnProduct = return_product::where('orders_id',$orders_id)->first();

      $order_return_id = $returnProduct->order_return_id;
      $return_detail = return_product_detail::where('order_return_id',$order_return_id)->get();
      $orders_id = $order->orders_id;
      $order_detail = OrdersDetail::where('orders_id',$orders_id)->get();
      // add เพิ่ม
      foreach ($order_detail as $key => $detail) {
        $product_option = ProductOption::where('product_option_id', $detail->product_option_id)->first();
        $order_detail[$key]['product_option'] = $product_option;
        $product_gallery = ProductGallery::where('product_id', $detail->product_id)->first();
        $order_detail[$key]['product_gallery'] = $product_gallery;
      }

      return view('frontend.account.order-return-refund-product-detail', compact('order','returnProduct', 'return_detail','order_detail','emyty_detail'));
      // return view('frontend.account.return-mail', compact('order','returnProduct', 'return_detail','order_detail','arrayJson','orders'));

    }

    public function getReturndetail($orders_no){
        $objFn = new MainFunction();
        $user_id = $this->user_id;
        $user = $this->user;

        $order = Orders::where('user_id',$user_id)->where('orders_no',$orders_no)->first();

        if(empty($order))
            return redirect()->to('account/order-history');

        $orders_id = $order->orders_id;
        $returnProduct = return_product::where('orders_id',$orders_id)->first();

        $order_return_id = $returnProduct->order_return_id;
        $return_detail = return_product_detail::where('order_return_id',$order_return_id)->get();
        $orders_id = $order->orders_id;
        $order_detail = OrdersDetail::where('orders_id',$orders_id)->get();
        foreach ($order_detail as $key => $detail) {
          $product_option = ProductOption::where('product_option_id', $detail->product_option_id)->first();
          $order_detail[$key]['product_option'] = $product_option;
          $product_gallery = ProductGallery::where('product_id', $detail->product_id)->first();
          $order_detail[$key]['product_gallery'] = $product_gallery;
        }
        // echo $returnProduct;
        // echo $return_detail;
        // echo $order_detail;
        $emyty_detail = '';
        return view('frontend.account.order-return-refund-product-detail', compact('order','returnProduct', 'return_detail','order_detail','emyty_detail'));
    }
    public function getReturndetailorderno(){
        $objFn = new MainFunction();
        $emyty_detail = 'This item empty';

        return view('frontend.account.order-return-refund-product-detail', compact('order','returnProduct', 'return_detail','order_detail','emyty_detail'));
    }
    public function accountNumber(Request $request){
        $this->model = 'App\Model\Orders'; // Model
        $this->obj_model = new $this->model; // Obj Model
        $this->obj_fn = new MainFunction(); // Obj Function

        echo json_encode($request->all());

        $filename = "";
        $obj_fn = $this->obj_fn;
        $obj_model = $this->obj_model;
        $validator = Validator::make($request->all(),
          [
            'img_path' => 'required',
            'account_number' => 'required'
          ]);

          if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();

          }
            if (Input::hasFile('img_path')) { // เพิ่มตรงนี้
                $data = $obj_model;
                $photo = $request->file('img_path');                    // get image from form
                $new_name = date('YmdHis').'-'.rand(10000, 99999);      // set new name
                $path = public_path('uploads/refund_product');              // set path
                $extension = $photo->getClientOriginalExtension();      // get extension
                $filename = $new_name . "." . $extension;               // set filename
                $destinationPath = $path;
                $obj_fn->img_full_resize($photo, $destinationPath, $filename);
                // ........................
                $order_id = $request->order_id;
                $accountNumber = $request->account_number;
                $bank = $request->bank;
                $insert = [
                  'image_name' => $filename,
                  'detail' => $bank.' : '.$accountNumber

                ];
                $orders = return_product::where('orders_id',$order_id)->update($insert);
                return redirect()->to('account/order-return');

            }


    }
    public function getPrint($order_return_id){

      // echo $order_return_id.'<br>'; //order_return_id
      $return_product = return_product::where('order_return_id', $order_return_id)->first();
      $orders_id = $return_product->orders_id;
      // echo $return_product.'<br>';
      // echo $orders_id.'<br>';
      $orders = Orders::where('orders_id', $orders_id)->first();
      // echo $orders.'<br>';
      return view('frontend.account.print-address', compact('orders','return-product'));


    }
}
