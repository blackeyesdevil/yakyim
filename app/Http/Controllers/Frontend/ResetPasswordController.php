<?php
namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use App\Model\User;

use Hash;
use Validator;
use Crypt;
use Carbon\Carbon;
use Mail;
use Input;

use SEOMeta;
use OpenGraph;


class ResetPasswordController extends Controller
{
    public function __construct()
    {
        $title = trans('seo.title');
        $description = trans('seo.description');
        $keyword = trans('seo.keyword');
        SEOMeta::setTitle('Reset Password - '.$title);
        SEOMeta::setDescription($description);
        SEOMeta::addKeyword($keyword);

        OpenGraph::setDescription($description);
        OpenGraph::setTitle($title);
        OpenGraph::setUrl(request()->url());
        OpenGraph::addImage(['url' => 'pathImg', 'size' => 300]);
    }
    public function getIndex(){
        $token = Input::get('token');
        $user_id = Crypt::decrypt($token);
        $user = User::find($user_id);
        return view('frontend.reset');
    }
    public function postCheck(Request $request){
        $token = $request->gen_token;
        $password = $request->password;
        $validator = Validator::make($request->all(),
            [
                "password" => 'required|between:6,20',
                "c_password" => 'required|same:password'
            ],
            [
                "password.required" => trans('account.password_req'),
                "c_password.required" => trans('account.c_password_req'),
                "c_password.same" => trans('account.password_match')
            ]
        );
        // Validate fields
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        // Check User
        $user_id = Crypt::decrypt($token);
        $hashed = Hash::make($password);
        User::where('user_id',$user_id)->update(['password'=>$hashed]);
        session()->flash('successMsg', trans('message.reset-password-success'));
        return redirect()->to('login');
    }
}
