<?php
namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use App\Model\User;
use App\Model\Subscribe;

use Hash;
use Validator;
use Crypt;
use Carbon\Carbon;
use Mail;
use Input;

use SEOMeta;
use OpenGraph;


class RegisterController extends Controller
{
    public function __construct(){
        $title = trans('seo.title');
        $description = trans('seo.description');
        $keyword = trans('seo.keyword');
        SEOMeta::setTitle('Register - '.$title);
        SEOMeta::setDescription($description);
        SEOMeta::addKeyword($keyword);

        OpenGraph::setDescription($description);
        OpenGraph::setTitle($title);
        OpenGraph::setUrl(request()->url());
        OpenGraph::addImage(['url' => 'pathImg', 'size' => 300]);
    }
    public function getIndex(){
        if(!empty(session()->get('s_user_id'))) return redirect()->to('account/profile');

        return view('frontend.register');
    }
    // ---------------- Register
    public function postCheck(Request $request){
        $obj_fn = new MainFunction();
        $firstname = $request->firstname;
        $lastname = $request->lastname;
        $email = $request->email;
        $pass = $request->password;
        $birth_date = $request->birth_date;
        $receive_newsletter = $request->receive_newsletter;

        $validator = Validator::make($request->all(),
            [
                "firstname" => 'required',
                "lastname" => 'required',
                "email" => 'required|email',
                "mobile" => 'required|numeric',
                "password" => 'required|between:6,20',
                "agree" => 'required',
                "birth_date" => 'required'
            ]
        );
        // Validate fields
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        // Check User
        $user = User::where('email',$email)->first();
        if(!empty($user)) {
            if($user->verified == 0){
                $error = trans('message.confirm-email-first');
            }else{
                $error = trans('message.same-email');
            }
        }else{
            $hashed = Hash::make($pass);
            $input_all = $request->all();
            $input_all['password'] = $hashed;
            $input_all['user_no'] = $obj_fn->gen_user_no('user','user_id','user_no','YAK');

            $user = User::create($input_all);

            $gen_token = Crypt::encrypt($user->user_id); // Gen token for verify email

            // Send mail verify
            Mail::send('emails.verify', ['firstname' => $firstname, 'lastname' => $lastname, 'gen_token' => $gen_token], function ($m) use ($email, $firstname, $lastname) {
                $m->to($email, $firstname . ' ' . $lastname);
                $m->subject('Thank you for register on website');
            });

            // Subscribe
            if ($receive_newsletter == 1) {
                $subscribe_data = Subscribe::where('email', $email)->first();
                if (empty($subscribe_data)) {
                    Subscribe::create(['email' => $email]);
                }
            }

            return redirect()->back()->with('successMsg', trans('message.register-success'));
        }
        return redirect()->back()->with('errorMsg',$error);
    }

    // ---------------- Verify Email
    public function getVerify(){
        $token = Input::get('token');
        $current_date = Carbon::now();
        $user_id = Crypt::decrypt($token);
        $get_user = User::find($user_id);
        if($get_user->verified == 0){
            User::where('user_id',$user_id)->update(['verified' => '1','verified_date' => $current_date]);
        }
        session()->flash('successMsg', trans('message.confirm-email-success'));
        return redirect()->to('login');
    }
    public function getCondition(){
        return view('frontend.register-condition');
    }
}
