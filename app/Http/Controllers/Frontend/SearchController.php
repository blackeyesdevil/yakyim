<?php
namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use App\Model\Subscribe;
use App\Model\Product;
use App\Model\Category;
use App\Model\Blog;

use Input;

class SearchController extends Controller
{
    public function getIndex(){
        $keyword = Input::get('q');

        // Get all products which are related to the keyword
        $products = Product::leftJoin('product_option', 'product.product_id', '=', 'product_option.product_id')
                            ->join('product_option_gallery', 'product_option_gallery.product_option_id', '=', 'product_option.product_option_id')
                            ->where(function($query) use ($keyword){
                                $query->where('product.product_name_en', 'like', '%' . $keyword . '%')
                                      ->orWhere('product.product_name_th', 'like', '%' . $keyword . '%');
                            })
                            ->where('product.status', '1')
                            ->whereNull('product.deleted_at')
                            ->whereNull('product_option.deleted_at')
                            ->whereNull('product_option_gallery.deleted_at')
                            ->groupBy('product.product_id')
                            ->paginate(8);

        // // Get all tags which are related to the keyword
        // $tags = Tag::where('tag_name_en', 'like', '%' . $keyword . '%')
        //           ->orWhere('tag_name_th', 'like', '%' . $keyword . '%')
        //           ->get();

        // Get all blogs which are related to the keyword
        $blogs = Blog::where('title_en', 'like', '%' . $keyword . '%')
                  ->orWhere('title_th', 'like', '%' . $keyword . '%')
                  ->get();

        return view('frontend.search-result', compact('keyword', 'products', 'categories', 'blogs'));
    }
}
