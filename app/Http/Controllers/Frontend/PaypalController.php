<?php
namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;
use App\Library\OrderFunction;

use App\Model\Orders;

use Config;
use Crypt;
use Input;

class PaypalController extends Controller
{
    public function __construct(){

    }
    public function getIndex(){
        $token = Input::get('token');
        $orders_no = Crypt::decrypt($token);
        $data = Orders::where('orders_no',$orders_no)->first();
        $grand_total = $data->total_price + $data->shipping_price - $data->discount_price - $data->use_point;

        return view('frontend.cart.paypal',compact('token','orders_no','grand_total'));
    }
    public function getStatus(){
        $orderFn = new OrderFunction();
        $tx = Input::get('tx');
        $status = Input::get('st');
        $amt = Input::get('amt');
        $cm = Input::get('cm');
        $orders_no = Input::get('item_number');

        $status_id = 3;
        $data = Orders::where('orders_no',$orders_no)->first();
        $orders_id = $data->orders_id;
        Orders::where('orders_id',$orders_id)->update(['status_id' => $status_id]);

        $orderFn->sendMailUser($orders_id,'การชำระเงินสำเร็จ'); // mail to user
        $orderFn->point($orders_id); // Get Point
        $orderFn->friendPoint($orders_id); // ถ้ามีบอกต่อเพื่อน ให้เอา point ให้เพื่อนด้วย
        $orderFn->memberVIP($orders_id); // Get Member VIP
        $orderFn->usedCode($orders_id); // Use Code
        $orderFn->orderTracking($orders_id,$status_id); // Order Tracking


        if($status == 'Completed'){
            // send mail to admin
            $orderFn->sendMailWh($orders_no);
        }else{
            // Pending
            // send mail to admin ให้ตรวจสอบการชำระเงินของ paypal
            $orderFn->sendMailWhPending($orders_no);
        }

        session()->flash('s_order_no',$orders_no);
        return redirect()->to('thankyou');
    }
    public function getCancel(){
        $token = Input::get('token');
        $orders_no = Crypt::decrypt($token);
        $orderFn = new OrderFunction();
        $status_id = 9;
        $data = Orders::where('orders_no',$orders_no)->first();
        $orders_id = $data->orders_id;
        Orders::where('orders_id',$orders_id)->update(['status_id' => $status_id]);

        $orderFn->sendMailUser($orders_id,'การชำระเงินล้มเหลว'); // mail to user
        $orderFn->returnStock($orders_id); // return stock
        $orderFn->returnCode($orders_id); // return code
        $orderFn->returnUsePoint($orders_id); // return code
        $orderFn->orderTracking($orders_id,$status_id); // Order Tracking


        session()->flash('s_order_no',$orders_no);
        return redirect()->to('paymentfail');
    }
}
