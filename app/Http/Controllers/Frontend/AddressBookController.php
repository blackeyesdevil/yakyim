<?php
namespace App\Http\Controllers\Frontend;

use App\Model\Product;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use App\Model\User;
use App\Model\Point;
use App\Model\AddressBook;
use App\Model\Wishlist;

use Hash;
use Validator;
use Crypt;
use Carbon\Carbon;
use Mail;
use SEOMeta;
use OpenGraph;

class AddressBookController extends Controller
{
    public function __construct(){
        $title = trans('seo.title');
        $description = trans('seo.description');
        $keyword = trans('seo.keyword');
        SEOMeta::setTitle('My Account - '.$title);
        SEOMeta::setDescription($description);
        SEOMeta::addKeyword($keyword);

        OpenGraph::setDescription($description);
        OpenGraph::setTitle($title);
        OpenGraph::setUrl(request()->url());
        OpenGraph::addImage(['url' => 'pathImg', 'size' => 300]);

        $this->user_id = session()->get('s_user_id');
        $this->user = User::find($this->user_id);
        $this->session_id = $session_id = session()->getId();
        $this->lang = session()->get('locale');
    }

    public function getIndex(){
        $user_id = $this->user_id;
        $user = $this->user;
        $billing = AddressBook::where('user_id',$user_id)->where('billing_shipping','1')->first();
        $shipping = AddressBook::where('user_id',$user_id)->where('billing_shipping','2')->get();

        return view('frontend.account.address-book',compact('user', 'billing','shipping'));

    }
    public function getAdd($billing_shipping){
        $user_id = $this->user_id;
        $user = $this->user;
        $mode = 'add';
        if($billing_shipping != 'billing' && $billing_shipping != 'shipping')
            return redirect()->to('account/address-book');
        // billing address มีได้แค่ที่อยู่เดียว
        if($billing_shipping == 'billing'){
            $check = AddressBook::where('user_id',$user_id)->where('billing_shipping','1')->first();
            if(!empty($check)) return redirect()->to('account/address-book/update/billing/'.$check->address_book_id);
        }
        $data = [];
        return view('frontend.account.address-book-update',compact('mode','data','user','billing_shipping'));

    }
    public function getUpdate($billing_shipping,$id){
        $user = $this->user;
        $mode = 'update';
        $data = AddressBook::find($id);
        return view('frontend.account.address-book-update',compact('mode','data','user','billing_shipping'));
    }
    public function getDelete($id){
        AddressBook::where('address_book_id',$id)->delete();
        return redirect()->to('account/address-book');
    }
    public function postCheck(Request $request){
        $mode = $request->mode;
        $user_id = $this->user_id;
        $input_all = $request->except(['_token','submit','mode']);
        $input_all['user_id'] = $user_id;
        if($request->billing_shipping == 'billing') $input_all['billing_shipping'] = 1;
        else $input_all['billing_shipping'] = 2;

        $validator = Validator::make($request->all(),
            [
                "firstname" => 'required',
                "lastname" => 'required',
                "tax_id" => 'numeric|digits_between:13,13',
                "address" => 'required',
                "province_id" => 'required',
                "amphur_id" => 'required',
                "district_id" => 'required',
                "zipcode" => 'required',
                "mobile" => 'required|numeric'
            ]
        );
        // Validate fields
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

//        return $input_all;
        if($mode == 'add') AddressBook::create($input_all);
        if($mode == 'update') {
            $address_book_id = $request->address_book_id;
            AddressBook::where('address_book_id', $address_book_id)->update($input_all);
        }
        return redirect()->to('account/address-book');
    }
}
