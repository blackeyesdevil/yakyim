<?php
namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use Validator;

use App\Model\User;
use SEOMeta;
use OpenGraph;


class ProfileController extends Controller
{
    public function __construct()
    {
        $title = trans('seo.title');
        $description = trans('seo.description');
        $keyword = trans('seo.keyword');
        SEOMeta::setTitle('My Account - '.$title);
        SEOMeta::setDescription($description);
        SEOMeta::addKeyword($keyword);

        OpenGraph::setDescription($description);
        OpenGraph::setTitle($title);
        OpenGraph::setUrl(request()->url());
        OpenGraph::addImage(['url' => 'pathImg', 'size' => 300]);
    }
    public function getIndex(){

        $data = User::find(session()->get('s_user_id'));
        return view('frontend.account.account',compact('data'));
    }
    public function postUpdate(Request $request){
        $validator = Validator::make($request->all(),
            [
                "firstname" => 'required',
                "lastname" => 'required',
                "mobile" => 'numeric',
            ]
        );

        // Validate fields
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $input = $request->except(['_token']);
        User::find(session()->get('s_user_id'))->update($input);


        return redirect()->back()->with('successMsg', trans('message.update-success'));
    }
}
