<?php
namespace App\Http\Controllers\Frontend;

use App\Model\Product;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

class FooterController extends Controller
{
    public function __construct(){
        $this->lang = session()->get('locale');
    }

    public function privacyPolicy(){
        return view('frontend.privacy-policy');
    }
    public function sitemap(){
        return view('frontend.sitemap');
    }
    public function howToOrder(){
        return view('frontend.how-to-order');
    }
    public function paymentMethod(){
        return view('frontend.payment-method');
    }
    public function delivery(){
        return view('frontend.delivery');
    }
    public function returnPolicy(){
        return view('frontend.return-policy');
    }
    public function trackingOrder(){
        return view('frontend.tracking-order');
    }
}
