<?php
namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use App\Model\User;
use App\Model\Orders;
use App\Model\OrdersDetail;
use App\Model\OrderTracking;

use Input;
use SEOMeta;
use OpenGraph;

class OrderTrackingController extends Controller
{
    public function __construct(){
        $title = trans('seo.title');
        $description = trans('seo.description');
        $keyword = trans('seo.keyword');
        SEOMeta::setTitle('My Account - '.$title);
        SEOMeta::setDescription($description);
        SEOMeta::addKeyword($keyword);

        OpenGraph::setDescription($description);
        OpenGraph::setTitle($title);
        OpenGraph::setUrl(request()->url());
        OpenGraph::addImage(['url' => 'pathImg', 'size' => 300]);

        $this->user_id = session()->get('s_user_id');
        $this->user = User::find($this->user_id);
        $this->session_id = $session_id = session()->getId();
        $this->lang = session()->get('locale');
    }

    public function getIndex(){
        $user = $this->user;

        return view('frontend.account.order-tracking', compact('user'));
    }

    public function tracking($orders_no = null){
        $order = Orders::where('orders_no',$orders_no)->where('user_id',$this->user_id)->first();
        if(empty($order))
            return redirect()->to('account/order-history');

        $orders_id = $order->orders_id;

        $data = OrderTracking::where('orders_id',$orders_id)->orderBy('orders_tracking_id','desc')->get();
        return view('frontend.account.order-tracking-detail', compact('order','data'));
    }
}
