<?php
namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use App\Model\BannerProduct;
use App\Model\Category;
use App\Model\Product;
use App\Model\ProductOption;
use App\Model\RelatedProduct;
use App\Model\ProductCategory;
use App\Model\Dollar;
use App\Model\SizeChart;
use App\Model\ProductGallery;
use App\Model\SizeChartDetail;
use App\Model\ProductStock;

use Input;
use SEOMeta;
use OpenGraph;
use DB;
class ProductController extends Controller
{
    public function __construct(){
        $description = trans('seo.description');
        $keyword = trans('seo.keyword');

        SEOMeta::setDescription($description);
        SEOMeta::addKeyword($keyword);

        OpenGraph::setDescription($description);
        OpenGraph::setUrl(request()->url());

        $this->lang = session()->get('locale');
    }

    public function getMainCat($category_id){
        $lang = $this->lang;
        $category = Category::find($category_id);
        $main_category_id = $category_id;
        $main_category_name = $category['category_name_'.$lang];
        $parent_category_id = $category->parent_category_id;
        $category_name_list = '<li class="active">'.$main_category_name.'</li>';

        while($parent_category_id != 0){
            $find_main_cate = Category::find($parent_category_id);

            $main_category_id = $find_main_cate->category_id;
            $parent_category_id = $find_main_cate->parent_category_id;
            $main_category_name = $find_main_cate['category_name_'.$lang];
            $main_category_name_en = $find_main_cate['category_name_en'];
            $category_name_list = '<li><a href="'.url()->to('product/'.str_slug($main_category_name_en).'/'.$main_category_id).'" title="'.$main_category_name.'">'.$main_category_name.'</a></li>'.$category_name_list;
        }

        return ['main_cate_id'=>$main_category_id,'main_cate_name'=>$main_category_name , 'cate_name_list'=>$category_name_list];
    }
    public function sumQty($products){
        foreach($products as $item){
            $sum_qty = ProductOption::select(DB::raw('sum(qty) as sum_qty') )
                ->where('product_id',$item->product_id)->first();
            $item->sum_qty = $sum_qty->sum_qty;
        }
    }
    public function sumQty2($product){
        $sum_qty = ProductOption::select(DB::raw('sum(qty) as sum_qty') )
            ->where('product_id',$product->product_id)->first();
        $product['sum_qty'] = $sum_qty->sum_qty;
    }
    public function productList($category,$category_id,$size = null,$size_value = null){
        $obj_dollar = new Dollar();
        $lang = $this->lang;
        $model_product = new Product();
        $category = Category::find($category_id);

        // ------ Find Main Category
        $main_category = $this->getMainCat($category_id);
        $main_category_id = $main_category['main_cate_id'];

        // ------ SEO
        $title = $category['category_name_'.$lang].' - '.trans('seo.title');
        SEOMeta::setTitle($title);
        OpenGraph::setTitle($title);
        OpenGraph::addImage(['url' => 'pathImg', 'size' => 300]);

        // ------ Product List
        $products = Product::join('product_category','product.product_id','=','product_category.product_id')
            ->where('product.status','1')
            ->where('product_category.category_id',$category_id)
            ->whereNull('product_category.deleted_at')
            ->groupBy('product.product_id')
            ->orderBy('product.product_id','desc');

        $get_products = $products->get();
        $count_product = count($get_products);
        // ------ Search Product Size
        $search_size = [];
        if( $count_product > 0){
            foreach($get_products as $item){
                $a_product_id[] = $item->product_id;
            }
            $search_size = ProductOption::select('option_name_'.$lang .' as option_name')
                ->join('product','product.product_id','=','product_option.product_id')
                ->whereIn('product_option.product_id',$a_product_id)
                ->whereNull('product.deleted_at')
                ->groupBy('product_option.option_name_'.$lang)
                ->orderBy('option_name_'.$lang,'asc')
                ->get();
        }

        $sizes = ProductOption::select('option_name_'.$lang .' as option_name')
            ->join('product','product.product_id','=','product_option.product_id')
            ->join('size_chart_detail','size_chart_detail.size_name','=','product_option.option_name_'.$lang.'')
            ->whereIn('product_option.product_id',$a_product_id)
            ->whereNull('product.deleted_at')
            ->where('size_chart_detail.size_chart_id',0)
            ->groupBy('product_option.option_name_'.$lang)
            ->orderBy('size_chart_detail.value','asc')
            ->get();

//        foreach($sizes_all as $value2) {
//            foreach($search_size as $value){
//                if($value2->option_name == $value->option_name){
//                    $sizes[] = $value2->option_name;
//                }
//            }
//        }
        // Size Chart

        // ------ Get Search Size
        if($size_value != null && count($search_size) > 0){
            $search_product = ProductOption::select('product_id','product_option_id','option_name_'.$lang)
//                ->where('option_name_'.$lang,'like','%'.$size_value.'%')
                ->where('option_name_'.$lang,$size_value)
                ->get();
            if(count($search_product) > 0){
                foreach($search_product as $search_item){
                    $s_product_id[] = $search_item->product_id;
                }

                $products = $products->whereIn('product.product_id',$s_product_id);
            }else{
                $products = $products->whereIn('product.product_id','0');
            }
        }

//        return $products->toSql();
        $products = $products->paginate(9);
        $model_product->getFirstImage($products);
        $this->sumQty($products);

        // ------ Dropdown Search Category
        $search_category = Category::select('category_id','category_name_th','category_name_en')
            ->where('parent_category_id',$main_category_id)->get();
        foreach($search_category as $item){
            $item->subcate = Category::select('category_id','category_name_th','category_name_en')
                ->where('parent_category_id',$item->category_id)
                ->orderBy('category_name_'.$lang,'asc')
                ->get();
        }

        return view('frontend.product.product-list',compact('category','products','obj_dollar','search_category','search_size','sizes','category_id'));
    }
    public function productDetail($product_id,$product_name){
        $obj_dollar = new Dollar();
        $lang = $this->lang;
        $model_product = new Product();
        $product = Product::find($product_id);
        $this->sumQty2($product);

        // SEO
        $title = $product['product_name_'.$lang];
        SEOMeta::setTitle($title);
        OpenGraph::setTitle($title);
        OpenGraph::addImage(['url' => 'pathImg', 'size' => 300]);

        // Find Product Category
        $category = ProductCategory::where('product_id',$product_id)->orderBy('product_category_id','desc')->first();
        $category_id = $category->category_id;

        // Find Main Category and list category
        $main_category = $this->getMainCat($category_id);
        $main_category_id = $main_category['main_cate_id'];
        $main_category_name = $main_category['main_cate_name'];
        $category_name_list = $main_category['cate_name_list'];

        // Product Size
        $product_option = ProductOption::where('product_id',$product_id)->orderBy('option_name_'.$lang,'asc')->get();

        $gallery = []; $chart = []; $arr_chart=[];
        // Product Gallery
        $gallery = ProductGallery::where('product_id',$product_id)->orderBy('sorting','asc')->get();

        // Size Chart
        $sizes = SizeChartDetail::where('size_chart_id', $product->size_chart_id)
                    ->groupBy('size_name')
                    ->orderBy('value', 'asc')
                    ->get();

        $chart = SizeChartDetail::where('size_chart_id',$product->size_chart_id)
            ->orderBy('part','asc')
            ->orderBy('value','asc')
            ->get();

        // Related Product
        $related_product = RelatedProduct::join('product','product.product_id','=','related_product.related_id')
            ->where('related_product.product_id',$product_id)
            ->where('product.status','1')
            ->groupBy('related_product.related_id')
            ->orderByRaw('RAND()')
            ->take(4)->get();
        $this->sumQty($related_product);

        $model_product->getFirstImage($related_product);
        return view('frontend.product.product-detail',compact('product','product_option','related_product','obj_dollar','main_category_name','category_name_list','gallery','sizes','chart'));
    }
}
