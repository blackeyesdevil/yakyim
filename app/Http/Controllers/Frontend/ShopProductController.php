<?php
namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;
use Carbon\Carbon;

use App\Model\Product;
use App\Model\ProductCategory;
use App\Model\Gallery;
use App\Model\RelatedProduct;
use App\Model\Review;
use App\Model\ProductOptionGroup;
use App\Model\ProductOption;
use App\Model\Option;
use App\Model\OptionGroup;
use App\Model\Category;

use DB;
use Input;

class ShopProductController extends Controller
{
    public function __construct()
    {

    }
    public function index(){
        $objProduct = new Product();
        $joinSpecial = $objProduct->JoinSpecial();

        $products = Product::select('product.product_id','product.product_name','product.retail_price','special.special_id','special.price')
            ->leftJoin(DB::raw($joinSpecial),'special.product_id','=','product.product_id')
            ->where('product.status','1')
            ->get();

        foreach ($products as $product){
            $product->url = str_slug($product->product_name, '-');
        }
        return view('frontend.shop-product',compact('products'));
    }

    public function show($id){
        $objProduct = new Product();
        $joinSpecial = $objProduct->JoinSpecial($id);

        $productId = $id;

        // Get Product Detail
        $product = Product::select('product.*','manufacturer.name as manufacturer_name','special.special_id','special.price')
            ->leftJoin(DB::raw($joinSpecial),'special.product_id','=','product.product_id')
            ->leftJoin('manufacturer','manufacturer.manufacturer_id','=','product.manufacturer_id')
            ->where('product.product_id',$id)
            ->where('product.status','1')
            ->first();

        // Get product option group id
        $productOptionGroup = ProductOptionGroup::where('product_id',$id)->where('require','1')->orderBy('product_option_group_id','desc')->take(1)->first();
        $product->product_option_group = array();

        if(count($productOptionGroup)){
            $option_group_id = $productOptionGroup->option_group_id;
            $product_option_group = $productOptionGroup;
            // Get option group name
            $groupName = OptionGroup::select('name as group_name')->where('option_group_id',$option_group_id)->first();
            // Get product option
            $productOption = ProductOption::select('product_option.product_option_id','option.name as option_name','product_option.price','product_option.qty')
                ->join('option','option.option_id','=','product_option.option_id')
                ->where('product_option.product_id',$id)
                ->where('option.option_group_id',$option_group_id)
                ->get();

            if(count($productOption) > 0){
                $product->product_option_group = $groupName;
                $product->product_option_group->option = $productOption;
            }
        }

        return view('frontend.shop-product-detail',compact('product'));
//        return response()->json($product);
//        return csrf_token();
    }

}
