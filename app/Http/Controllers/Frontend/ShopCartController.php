<?php
namespace App\Http\Controllers\Frontend;
use App\Model\Option;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;
use Carbon\Carbon;
use App\Model\Product;
use App\Model\ProductCategory;
use App\Model\ProductOptionGroup;
use App\Model\ProductOption;
use App\Model\Cart;
use App\Model\DiscountCode;
use App\Model\Discount;
use App\Model\LogCodeUsed;

use DB;
use Input;

class ShopCartController extends Controller
{
    public function __construct()
    {

    }
//    public function getIndex($user_id = null){
//        $current_time = Carbon::now();
//        $s_user_id = $user_id;
//
//        $objProduct = new Product();
//        $joinSpecial = $objProduct->JoinSpecial();
//
//        $carts = Cart::select('cart.cart_id','cart.user_id','cart.product_id','cart.product_option_id','cart.qty','product.product_id','product.product_name','product.retail_price','special.special_id','special.price','product.point','cart.product_option_id')
//            ->join('product','product.product_id','=','cart.product_id')
//            ->leftJoin(DB::raw($joinSpecial),'special.product_id','=','product.product_id')
//            ->where('user_id',$s_user_id)
//            ->orderBy('cart.cart_id','asc')
//            ->get();
//
//        foreach($carts as $key => $cart){
//            // Product Option
//            $product_option_id = $cart->product_option_id;
//            $productOption = ProductOption::select('option_group.name as group_name','option.name as option_name','product_option.price as option_price','product_option.point as option_point')->where('product_option_id',$product_option_id)
//                ->join('option','option.option_id','=','product_option.option_id')
//                ->leftJoin('option_group','option_group.option_group_id','=','option.option_group_id')
//                ->get();
//            $cart->product_option = $productOption;
//
//            // Discount Per Product
//            $product_id = $cart->product_id;
//            $cart_qty = $cart->qty;
//            $productDiscount = Discount::select('discount_id','minimum_qty','price as discount_price')->where('product_id',$product_id)
//                ->where('start_date','<=',$current_time)->where('end_date','>=',$current_time)
//                ->where('minimum_qty','<=',$cart_qty)
//                ->orderBy('price','desc')
//                ->take(1)
//                ->get();
//            $cart->product_discount = $productDiscount;
//        }
////        return view('frontend.shop-cart',compact('carts'));
//        return response()->json($carts);
//    }

    public function getIndex(){
        $s_user_id = session()->get('s_user_id');
        $carts = Cart::where('user_id', $s_user_id)->get();
        foreach ($carts as $cart){
            $product_id = $cart->product_id;
            $product_option_id = $cart->product_option_id;

            $cart->product = Product::find($product_id);
            if($product_option_id > 0){
                $cart->product_option = ProductOption::select('product_option.qty','option.option_id', 'option.name', 'option.name_th','option_group.name_th as option_name','option_group.name as option_name_en')
                    ->join('option', 'product_option.option_id', '=', 'option.option_id')
                    ->leftJoin('option_group','option_group.option_group_id', '=', 'option.option_group_id')
                    ->where('product_option_id', $product_option_id)->first();
            }else{
                $cart->product_option = array();
            }
        }

        return response()->json($carts);
    }

    // Add to Cart
    public function postAdd(Request $request){
//        $session_id = session()->getId(); // get session id
        $s_user_id = session()->get('s_user_id'); // get user id
//        $s_user_id = $request->user_id;

        $product_id = $request->product_id; // post product id
        $product_option_id = $request->product_option_id; // post product option id

        $amount = $request->amount;

        // Select qty of option
        $productOption = ProductOption::select('qty','price','point')->where('product_option_id',$product_option_id)->first();

        // Select minimum qty and qty
        $product = Product::select('minimum_qty', 'qty as product_qty')->where('product_id', $product_id)->first();

//        if ($amount >= $product->minimum_qty){
        if ($amount < $product->minimum_qty) {
            return array('status' => 0, 'msg' => trans('message.amount-greater-equal') . $product->minimum_qty);
        }
//        }
        if (!empty($productOption)) {
            if ($amount > $productOption->qty) {
                return array('status' => 0, 'msg' => trans('message.amount-greater-stock'));
            }
        }
        else {
            if ($amount > $product->product_qty) {
                return array('status' => 0, 'msg' => trans('message.amount-greater-stock'));
            }
        }

        // Find product in cart
        $cart = Cart::where('product_id', $product_id)
            ->where(function($query) use ($s_user_id){
                $query->orwhere('user_id',$s_user_id);
            });
        $cart = $cart->where('product_option_id',$product_option_id)->first();

        if(empty($cart)){
            // ถ้าไม่มี
            $objCart = new Cart();
            $objCart->user_id = $s_user_id;
            $objCart->product_id = $product_id;
            $objCart->product_option_id = $product_option_id;
            $objCart->qty = $amount;      // sure???
            $objCart->save();

            $result_cart = $objCart;
        }else{
            // ถ้ามี
            $cart_id = $cart->cart_id;
            $currentQty = $cart->qty;

            $findCart = Cart::find($cart_id);
            $findCart->qty = $currentQty+$amount;
            $findCart->save();

            $result_cart = $findCart;
        }

        $result_cart->option = DB::table('product_option')->join('option', 'product_option.option_id', '=', 'option.option_id')->select('product_option.qty','option.option_id', 'option.name', 'option.name_th')->where('product_option_id', $result_cart->product_option_id)->first();

        $result = array('status' => 1, 'msg' => trans('message.add-to-cart-success'), 'result' => $result_cart);

        return response()->json($result);
    }

    // เพิ่ม-ลด
    public function postAmount(Request $request){
        $cart_id = $request->cart_id;
        $amount = $request->cart_amount;

        if($amount > 0){
            $cart = Cart::find($cart_id);
            $product_id = $cart->product_id;
            $product_option_id = $cart->product_option_id;
            $result = array('status' => 1, 'msg' => 'Done');
            // Select qty of option
            $productOption = ProductOption::select('qty','price','point')->where('product_option_id',$product_option_id)->first();
            // Select minimum qty and qty
            $product = Product::select('minimum_qty', 'qty as product_qty')->where('product_id', $product_id)->first();

            if ($amount < $product->minimum_qty) {
                $result = array('status' => 0, 'msg' => trans('amount-greater-equal') . $product->minimum_qty);
            }
            if (!empty($productOption)) {
                if ($amount > $productOption->qty) {
                    $result = array('status' => 0, 'msg' => trans('message.amount-greater-stock'));
                }
            }
            else {
                if ($amount > $product->product_qty) {
                    $result = array('status' => 0, 'msg' => trans('message.amount-greater-stock'));
                }
            }
            if(!empty($result['status'] == 1)){
                $cart->qty = $amount;
                $cart->save();
            }
        }else{
            $result = array(
                'status' => '0',
                'msg' => trans('message.amount-at-least-one'),
            );
        }
        return response()->json($result);
    }
    // ลบ
    public function postDelete($cart_id){
        $data = Cart::find($cart_id);
        $data->forceDelete();

        $response = array(
            'status' => '1',
            'msg' => trans('message.remove-success'),
        );
        return response()->json($response);
    }

    public function getCode($code=null,$totalprice = 0){
        $s_user_id = session()->get('s_user_id'); // get user id
        $status = 0;
        $msg = "";
        $discount_price = 0;

        if($s_user_id == null){
            $msg = trans('message.sign-in-first');
        }else{
            if($code==null){
                $msg = trans('message.code-req');
            }else{
                $data = DiscountCode::where('code',$code)->where('status','1')->first();
                // เช็คมีโค้ดมั๊ย
                if(!empty($data)){
                    $currentDate = Carbon::now();
                    $codeId = $data->discount_code_id;
                    $allot = $data->allot;
                    $reserved = $data->reserved;
                    $used = $data->used;
                    $perUse = $data->per_use;
                    $minPrice = $data->min_price;
                    $maxPrice = $data->max_price;
                    $discountType = $data->discount_type;
                    $discount = $data->discount;
                    $discountMax = $data->discount_max;
                    $startDate = $data->start_date;
                    $endDate = $data->end_date;

                    // เช็คโค้ดหมดอายุยัง
                    if($currentDate >= $startDate && $currentDate <= $endDate){
                        // เช็คโค้ดหมดยัง
                        if( $allot >= $reserved+$used){
                            // เช็คโค้ดว่าใช้ไปแล้วกี่ครั้ง / member
                            $logData = LogCodeUsed::where('user_id',$s_user_id)->where('discount_code_id',$codeId)->get();
                            $countUsed = count($logData);
                            if($countUsed < $perUse){
                                // Check Price
                                if($minPrice <= $totalprice && $maxPrice >= $totalprice){
                                    if($discountType == '%'){ $discount_price = ($totalprice*$discount) / 100; }
                                    else{ $discount_price = $discount; }
                                    if($discount_price > $discountMax ){
                                       $discount_price = $discountMax;
                                    }
                                    $msg = trans('message.code-correct');
                                    $status = '1';
                                }else{
                                    $msg = trans('message.code-min-max-range1') . ' ' . $minPrice . trans('message.code-min-max-range2') . $maxPrice . trans('message.code-min-max-range3');
                                }
                            }else{ $msg = trans('message.code-unavailable'); }
                        }else{ $msg = trans('message.code-limit'); }
                    }else{ $msg = trans('message.code-expired'); }
                }else{ $msg = trans('message.code-not-found'); }
            }
        }

        $response = array(
            'status' => $status,
            'msg'   => $msg,
            'discount_price' => $discount_price
        );
        return response()->json($response);
    }

}
