<?php
namespace App\Http\Controllers\Frontend;

use App\Model\Product;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use App\Model\User;

use Hash;
use Validator;
use SEOMeta;
use OpenGraph;


class ChangePasswordController extends Controller
{
    public function __construct()
    {
        $title = trans('seo.title');
        $description = trans('seo.description');
        $keyword = trans('seo.keyword');
        SEOMeta::setTitle('My Account - '.$title);
        SEOMeta::setDescription($description);
        SEOMeta::addKeyword($keyword);

        OpenGraph::setDescription($description);
        OpenGraph::setTitle($title);
        OpenGraph::setUrl(request()->url());
        OpenGraph::addImage(['url' => 'pathImg', 'size' => 300]);
    }

    public function getIndex(){
        return view('frontend.account.change-password');
    }

    public function postCheck(Request $request){
        $user_id = session()->get('s_user_id');
        $old_password = $request->old_password;
        $password = $request->password;

        $validator = Validator::make($request->all(),
            [
                "old_password" => 'required',
                "password" => 'required|between:6,20',
                "c_password" => 'required|same:password'
            ],
            [
                "old_password.required" => trans('account.old_password_req'),
                "password.required" => trans('account.password_req'),
                "c_password.required" => trans('account.c_password_req'),
                "c_password.same" => trans('account.password_match')
            ]
        );
        // Validate fields
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $user = User::find($user_id);
        if(Hash::check($old_password,$user->password)){
            $hashed = Hash::make($password);
            User::where('user_id',$user_id)->update(['password' => $hashed]);
            return redirect()->back()->with('successMsg',trans('account.change-password-success'));
        }else{
            $error = trans('account.wrong-password');
        }
        return redirect()->back()->with('errorMsg',$error);
    }
}
