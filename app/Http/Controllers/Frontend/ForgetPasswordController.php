<?php
namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use App\Model\User;

use Hash;
use Validator;
use Crypt;
use Carbon\Carbon;
use Mail;
use SEOMeta;
use OpenGraph;

class ForgetPasswordController extends Controller
{
    public function __construct()
    {
        $title = trans('seo.title');
        $description = trans('seo.description');
        $keyword = trans('seo.keyword');
        SEOMeta::setTitle('Forget Password - '.$title);
        SEOMeta::setDescription($description);
        SEOMeta::addKeyword($keyword);

        OpenGraph::setDescription($description);
        OpenGraph::setTitle($title);
        OpenGraph::setUrl(request()->url());
        OpenGraph::addImage(['url' => 'pathImg', 'size' => 300]);
    }
    public function getIndex(){
        if(session()->has('s_user_id')){
            return redirect()->to('account/profile');
        }
        return view('frontend.forget');
    }
    public function postCheck(Request $request){
        $error = '';
        $email = $request->email;
        $validator = Validator::make($request->all(),
            [
                "email" => 'required|email',
            ]
        );
        // Validate fields
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        // Check User
        $user = User::where('email',$email)->first();

        if(!empty($user)) {
            $gen_token = Crypt::encrypt($user->user_id); // Gen token for verify email
            $firstname = $user->firstname;
            $lastname = $user->lastname;
            // Send mail for link reset password
            Mail::send('emails.forget-password', ['firstname' => $firstname,'lastname'=> $lastname,'gen_token'=>$gen_token], function ($m) use ($firstname,$lastname,$email) {
                $m->from(env('MAIL_CONTACT'), 'Webmaster Yakyim');
                $m->to($email, $firstname.' '.$lastname);
                $m->subject('Forget Password');
            });
            return redirect()->back()->with('successMsg', trans('message.check-email-to-set-password'));
        }else{
            $error = trans('message.email-not-found');
        }
        return redirect()->back()->with('errorMsg',$error);
    }
}
