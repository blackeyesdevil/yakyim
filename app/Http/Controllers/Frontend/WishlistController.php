<?php
namespace App\Http\Controllers\Frontend;
use App\Model\Option;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;
use Carbon\Carbon;
use App\Model\Product;
use App\Model\ProductCategory;
use App\Model\ProductOptionGroup;
use App\Model\ProductOption;
use App\Model\Wishlist;
use App\Model\Dollar;

use DB;
use Input;
use SEOMeta;
use OpenGraph;


class WishlistController extends Controller
{
    public function __construct()
    {
        $title = trans('seo.title');
        $description = trans('seo.description');
        $keyword = trans('seo.keyword');
        SEOMeta::setTitle('My Account - '.$title);
        SEOMeta::setDescription($description);
        SEOMeta::addKeyword($keyword);

        OpenGraph::setDescription($description);
        OpenGraph::setTitle($title);
        OpenGraph::setUrl(request()->url());
        OpenGraph::addImage(['url' => 'pathImg', 'size' => 300]);
    }

    public function getIndex(){
        $obj_prduct = new Product();
        $obj_dollar = new Dollar();
        $s_user_id = session()->get('s_user_id');
        $wishlists = Wishlist::join('product','product.product_id','=','wishlist.product_id')
            ->where('wishlist.user_id',$s_user_id)->get();
        $obj_prduct->getFirstImage($wishlists);

        return view('frontend.account.wishlist',compact('wishlists','obj_dollar'));
    }

    public function postAdd(Request $request){
        if(!session()->has('s_user_id')){

            return ['status' => '0', 'txt' => trans('message.wishlist-login-first')];
        }

        $s_user_id = session()->get('s_user_id');
        $product_id = $request->product_id;

        $find = Wishlist::where('user_id',$s_user_id)->where('product_id',$product_id)->first();
        if(count($find) == 0){
            $wishlist = new Wishlist();
            $wishlist->user_id = $s_user_id;
            $wishlist->product_id = $product_id;
            $wishlist->save();
        }
        $wishlist_qty = Wishlist::where('user_id',$s_user_id)->count();
        return ['status' => '1','txt' => trans('message.wishlist-added-success'),'wishlist_qty' => $wishlist_qty ];
    }

    public function getDelete($id){
        $wishlist_id = $id;
        $data = Wishlist::find($wishlist_id);
        $data->forceDelete();

        return redirect()->back();
    }


}
