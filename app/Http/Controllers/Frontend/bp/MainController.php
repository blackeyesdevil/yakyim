<?php
namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use App\Model\Banner;
use App\Model\Category;
use App\Model\Product;
use App\Model\SizeChart;
use App\Model\SizeChartDetail;
use Carbon\Carbon;

use SEOMeta;
use OpenGraph;

class MainController extends Controller
{

    public function __construct()
    {
        $title = trans('seo.title');
        $description = trans('seo.description');
        $keyword = trans('seo.keyword');
        SEOMeta::setTitle($title);
        SEOMeta::setDescription($description);
        SEOMeta::addKeyword($keyword);

        OpenGraph::setDescription($description);
        OpenGraph::setTitle($title);
        OpenGraph::setUrl(request()->url());
        OpenGraph::addImage(['url' => 'pathImg', 'size' => 300]);
    }

    public function getIndex(){
        $today = date('Y-m-d H:i:s');
        $banner_top = Banner::where('type','1')
            ->where('status','1')
            ->first();
        $banner_promo = Banner::where('type','2')
            ->where('status','1')
            ->first();
        $categories = Category::where('parent_category_id','0')->orderBy('sorting','asc')->take(4)->get();
        $new_products = Product::where('status','1')->where('new_product','1')->orderBy('new_sorting','desc')->take(3)->get();

        return view('frontend.index',compact('banner_top','banner_promo','categories','new_products'));
    }

    public function HowToOrder(){
    return view('frontend.howto.how-to-order');
    }
  public function HowToRegister(){
    return view('frontend.howto.how-to-register');
    }
  public function HowToPay(){
    return view('frontend.howto.how-to-pay');
    }
  public function HowToCheck(){
    return view('frontend.howto.how-to-check');
    }

  public function HowToSizingChart(){
      $charts = SizeChart::orderBy('size_chart_id', 'asc')->get();

      foreach ($charts as $chart){
          $chart->sizes = SizeChartDetail::where('size_chart_id', $chart->size_chart_id)
                      ->groupBy('size_name')
                      ->orderBy('size_name', 'asc')
                      ->get();

          $chart->values = SizeChartDetail::where('size_chart_id', $chart->size_chart_id)
              ->orderBy('part','asc')
              ->orderBy('size_name','asc')
              ->get();
      }

      return view('frontend.howto.how-to-sizing-chart', compact('charts'));
  }

    public function HowToChangeAddress(){
    return view('frontend.howto.how-to-change-address');
    }
    public function HowToShipping(){
    return view('frontend.howto.how-to-shipping');
    }
    public function HowToDiscount(){
    return view('frontend.howto.how-to-discount');
    }
    public function HowToChangeProduct(){
    return view('frontend.howto.how-to-change-product');
    }

}
