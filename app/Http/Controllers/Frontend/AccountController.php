<?php
namespace App\Http\Controllers\Frontend;

use App\Model\Product;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use App\Model\User;

use Hash;
use Validator;
use Crypt;
use Carbon\Carbon;
use Mail;


class AccountController extends Controller
{
    public function __construct(){
        $this->middleware('user');
        $this->user_id = session()->get('s_user_id');
        $this->user = User::find($this->user_id);
        $this->session_id = $session_id = session()->getId();
        $this->lang = session()->get('locale');
    }

    public function main(){
        $user_id = $this->user_id;
        $user = $this->user;

        return view('frontend.account.account');
    }
    public function check(Request $request){
        $user_id = $this->user_id;
        $input_all = $request->except(['_token','submit']);
        $validator = Validator::make($request->all(),
            [
                "firstname_th" => 'required',
                "lastname_th" => 'required',
                "mobile" => 'required|numeric',
                "email" => 'required|email',
            ]
        );
        // Validate fields
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        User::where('user_id',$user_id)->update($input_all);
        return redirect()->back()->with('successMsg', trans('account.update-success'));
    }
}
