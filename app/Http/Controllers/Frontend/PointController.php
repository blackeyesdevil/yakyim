<?php
namespace App\Http\Controllers\Frontend;

use App\Model\Product;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use App\Model\User;
use App\Model\Point;

use Hash;
use Validator;
use Crypt;
use Carbon\Carbon;
use Mail;


class PointController extends Controller
{
    public function __construct(){
        $this->middleware('user');
        $this->user_id = session()->get('s_user_id');
        $this->session_id = $session_id = session()->getId();
        $this->lang = session()->get('locale');
    }

    public function getIndex(){
        $user_id = $this->user_id;
        $points = Point::where('user_id',$user_id)->where('point','!=','0')->orderBy('point_trans_id','asc')->get();

        return view('frontend.account.point',compact('points'));
    }
}
