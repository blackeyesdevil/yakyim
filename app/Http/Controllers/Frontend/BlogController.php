<?php
namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Library\MainFunction;

use App\Http\Controllers\Controller;

use App\Model\Blog;
use App\Model\BlogTag;
use App\Model\BlogProduct;

use DB;

class BlogController extends Controller
{
    public function getIndex(){
        $blogs = Blog::paginate(6);

        $tags = BlogTag::groupBy('tag_id')->orderBy(DB::raw('rand()'))->take(8)->get();

        return view('frontend.blog.list', compact('blogs', 'tags'));
    }

    public function getDetail($blog_id){
        $blog = Blog::find($blog_id);
        $previous_blog = DB::select(DB::raw("select b1.blog_id, b1.title_th, b1.title_en from blog b1, blog b2
                                            where b1.blog_id = b2.blog_id - 1 and b2.blog_id = '" . $blog_id . "'
                                            limit 0, 1"));
        $next_blog = DB::select(DB::raw("select b1.blog_id, b1.title_th, b1.title_en from blog b1, blog b2
                                            where b1.blog_id = b2.blog_id + 1 and b2.blog_id = '" . $blog_id . "'
                                            limit 0, 1"));

        $tags = BlogTag::where('blog_id', $blog_id)->get();

        $products = BlogProduct::distinct('product.product_id')
                                ->join('product', 'blog_product.product_id', '=', 'product.product_id')
                                ->leftJoin('product_option', 'product.product_id', '=', 'product_option.product_id')
                                ->where('blog_id', $blog_id)->take(4)->get();

        return view('frontend.blog.detail', compact('blog', 'previous_blog', 'next_blog', 'tags', 'products'));
    }
}
