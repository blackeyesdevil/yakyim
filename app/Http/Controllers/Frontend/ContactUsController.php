<?php
namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;
use ReCaptcha\ReCaptcha;

use App\Model\ContactMsg;

use Mail;
use Validator;
use Input;

use SEOMeta;
use OpenGraph;

class ContactUsController extends Controller
{
    public function __construct(){
        $title = trans('seo.title');
        $description = trans('seo.description');
        $keyword = trans('seo.keyword');
        SEOMeta::setTitle('Contact Us - '.$title);
        SEOMeta::setDescription($description);
        SEOMeta::addKeyword($keyword);

        OpenGraph::setDescription($description);
        OpenGraph::setTitle($title);
        OpenGraph::setUrl(request()->url());
        OpenGraph::addImage(['url' => 'pathImg', 'size' => 300]);

    }
    public function captchaCheck($key)
    {
        $response = $key;
        $remoteip = $_SERVER['REMOTE_ADDR'];
        $secret   = env('RE_CAP_SECRET');

        $recaptcha = new ReCaptcha($secret);
        $resp = $recaptcha->verify($response, $remoteip);
        if ($resp->isSuccess()) {
            return true;
        } else {
            return false;
        }
    }
    public function getIndex(){
        return view('frontend.contactus');
    }

    public function postCheck(Request $request){
        $recaptcha = Input::get('g-recaptcha-response');
        $validator = Validator::make($request->all(),
            [
//                "subject_id" => 'required',
                "name" => 'required',
                "email" => 'required|email',
                "mobile" => 'required|numeric',
                "msg" => 'required',
                "g-recaptcha-response" => 'required',
            ],
            [
                'g-recaptcha-response.required' => trans('message.recaptcha-req')
            ]
        );
        // Validate fields
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        if($this->captchaCheck($recaptcha) == false) {
            return redirect()->back()->with('errorMsg','Wrong Captcha');
        }else{
            ContactMsg::create($request->all());
            $name = $request->name;
            $msg = $request->msg;
            $email = $request->email;
            $mobile = $request->mobile;

            // Send to admin
            Mail::send('emails.contact',['name' => $name, 'msg' => $msg, 'mobile' => $mobile],function($m) use ($email,$name){
                $m->from($email, $name);
                $m->to(env('MAIL_CONTACT'), 'Webmaster Yakyim');
                $m->subject('Contact from website');
            });

            return redirect()->back()->with('successMsg','Success!');
        }
    }
}
