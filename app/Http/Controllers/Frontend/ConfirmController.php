<?php
namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;
use App\Library\OrderFunction;

use App\Model\User;
use App\Model\Cart;
use App\Model\Product;
use App\Model\ProductOption;
use App\Model\AddressBook;
use App\Model\PaymentMethod;
use App\Model\Orders;
use App\Model\OrdersDetail;
use App\Model\ShippingRate;
use App\Model\LogCodeUsed;
use App\Model\DiscountCode;
use App\Model\Dollar;

use Carbon\Carbon;
use Illuminate\Support\Facades\View;
use Validator;
use Mail;
use SEOMeta;
use OpenGraph;
use Crypt;

class ConfirmController extends Controller
{
    public function __construct(){
        $this->middleware('user');
        $this->user_id = session()->get('s_user_id');
        $this->user_type = session()->get('s_user_type');
        $this->session_id = $session_id = session()->getId();
        $this->lang = session()->get('locale');

        $title = trans('seo.title');
        $description = trans('seo.description');
        $keyword = trans('seo.keyword');
        SEOMeta::setTitle($title);
        SEOMeta::setDescription($description);
        SEOMeta::addKeyword($keyword);

        OpenGraph::setDescription($description);
        OpenGraph::setTitle($title);
        OpenGraph::setUrl(request()->url());
        OpenGraph::addImage(['url' => 'pathImg', 'size' => 300]);

    }
    public function confirm(Request $request){
        $user_id = $this->user_id;
        $user_type = $this->user_type;
        $lang = $this->lang;
        $objFn = new MainFunction();
        $orderFn = new OrderFunction();
        $obj_dollar = new Dollar();
        // -------------------- Get Currency
        $dollar = Dollar::first();
        $currency = $dollar->dollar;

        // -------------------- Shipping Method
        $shipping_method_id = $request->shipping_method;
        // -------------------- Payment Method
        $payment_method_id = 0;
        if($shipping_method_id == 1 || ($shipping_method_id == 2 && $user_type == '1' ) ) {
            $payment_method_id = $request->payment_type;
        }
        // -------------------- Select cart
        $objCart = new Cart();
        $result_cart = $objCart->cart();
        $carts = $result_cart['carts'];
        $total_price = $result_cart['total_price'];
        $dollar_total_price = $obj_dollar->cal_dollar($total_price,$currency);
        $cart_normal_price = $result_cart['normal_price'];
        // -------------------- shipping price
        $objShipp = new ShippingRate();
        $shipping_price = $objShipp->shipping_price($total_price);
        if($user_type == '0' && $shipping_method_id == 2){ $shipping_price = 0; }
        $dollar_shipping_price = $obj_dollar->cal_dollar($shipping_price,$currency);


        // -------------------- Check Discount
        $discount_price = 0;
        $dollar_discount_price = 0;
        $discount_code_id = '';
        $discount_code = '';
//        return session()->get('s_discount_id').' '.session()->get('s_discount_code');
        if(session()->has('s_discount_id') && session()->has('s_discount_code')){
            $discount_code_id = session()->get('s_discount_id');
            $discount_code = session()->get('s_discount_code');
            if($discount_code_id == '0'){ // บอกต่อเพื่อน
                // หาราคาส่วนลด (เฉพาะสินค้าราคาปกติ)
                $discount_price = $cart_normal_price * 0.1;
                $dollar_discount_price = $obj_dollar->cal_dollar($discount_price,$currency);
                $sub_total = $total_price - $discount_price;
                // หาราคาจัดส่งใหม่
                $shipping_price = $objShipp->shipping_price($sub_total);
                if($user_type == '0' && $shipping_method_id == '2') { $shipping_price = 0; }
                $dollar_shipping_price = $obj_dollar->cal_dollar($shipping_price,$currency);

            }else if($discount_code_id == '1' || $discount_code_id == '2' || $discount_code_id == '3' || $discount_code_id == '4'){ // ส่วนลดวันเกิด
                $data = DiscountCode::find($discount_code_id);
                $min_price = $data->min_price;
                if($total_price >= $min_price) {
                    $percent = $data->discount / 100;
                    // หาราคาส่วนลด (เฉพาะสินค้าราคาปกติ)
                    $discount_price = $cart_normal_price * $percent;
                    $dollar_discount_price = $obj_dollar->cal_dollar($discount_price,$currency);
                    $sub_total = $total_price - $discount_price;
                    // หาราคาจัดส่งใหม่
                    $shipping_price = $objShipp->shipping_price($sub_total);
                    if($user_type == '0' && $shipping_method_id == '2') { $shipping_price = 0; }
                    $dollar_shipping_price = $obj_dollar->cal_dollar($shipping_price,$currency);
                }else{
                    return redirect()->to('checkout');
                }
            }else if($discount_code_id != '0' && $discount_code_id != '1' && $discount_code_id != '2' && $discount_code_id != '3' && $discount_code_id != '4'){ // รหัสส่วนลด
                $data = DiscountCode::find($discount_code_id);
                $allot = $data->allot;
                $reserved = $data->reserved;
                $used = $data->used;
                $type = $data->discount_type;
                $discount = $data->discount;
                $discountMax = $data->discount_max;
                $normalPrice = $data->normal_price;

                // เช็คโค้ดหมดยัง
                if($allot == $reserved+$used){
                    return redirect()->to('checkout');
                }
                // Cal discount price
                if($type == 'fix'){
                    $discount_price = $discount;
                }else{
                    if($normalPrice == 1){
                        $discount_price = $cart_normal_price*$discount/100;
                    }else{
                        $discount_price = $total_price*$discount/100;
                    }
                    if($discount_price > $discountMax ) $discount_price = $discountMax;
                }
                if($discount_price > $total_price) $discount_price = $total_price;
                $dollar_discount_price = $obj_dollar->cal_dollar($discount_price,$currency);
                $sub_total = $total_price - $discount_price;
                // หาราคาจัดส่งใหม่
                $shipping_price = $objShipp->shipping_price($sub_total);
                if($user_type == '0' && $shipping_method_id == '2') { $shipping_price = 0; }
                $dollar_shipping_price = $obj_dollar->cal_dollar($shipping_price,$currency);
            }
        }

        // -------------------- Use Point
        $use_point = 0;
        $dollar_use_point = 0;
        if(session()->has('s_use_point')){
            $xx = $total_price + $shipping_price - $discount_price;
            $use_point = session()->get('s_use_point');
            $dollar_use_point = session()->get('s_use_point_dollar');
            if($use_point > $xx){
                $use_point = $xx;
                $dollar_use_point = $obj_dollar->cal_dollar($use_point,$currency);
            }
        }
        // -------------------- Add or Update Shipping Address
        $shipping_type = $request->shipping_type;
        if($shipping_type == 'old'){
            $shipping_address_id = $request->old_address;
        }else{
            $input_ship = [
                'user_id' => $user_id,
                'firstname' => $request->shipping_firstname,
                'lastname' => $request->shipping_lastname,
                'address' => $request->shipping_address,
                'district_id' => $request->shipping_district,
                'amphur_id' => $request->shipping_amphur,
                'province_id' => $request->shipping_province,
                'zipcode' => $request->shipping_zipcode,
                'mobile' => $request->shipping_tel,
                'billing_shipping' => '2'
            ];
            $new_shipping = AddressBook::create($input_ship);
            $shipping_address_id = $new_shipping->address_book_id;
        }

        // -------------------- Same or Update Billing Address
        $billing_type = $request->billing_type;
        if($billing_type == 'same'){
            $billing_address_id = 0;
        }else{
            $check_bill = AddressBook::where('user_id',$user_id)->where('billing_shipping','1')->first();
            $input_bill = [
                'user_id' => $user_id,
                'tax_id' => $request->billing_tax,
                'firstname' => $request->billing_firstname,
                'lastname' => $request->billing_lastname,
                'address' => $request->billing_address,
                'district_id' => $request->billing_district,
                'amphur_id' => $request->billing_amphur,
                'province_id' => $request->billing_province,
                'zipcode' => $request->billing_zipcode,
                'mobile' => $request->billing_tel,
                'set_default' => '1',
                'billing_shipping' => '1'
            ];
            if(!empty($check_bill)){
                // -- มี billing address อยู่แล้ว ให้อัพเดท
                $billing_address_id = $check_bill->address_book_id;
                AddressBook::where('address_book_id',$billing_address_id)->update($input_bill);
            }else{
                // -- ไม่มี billing address ให้เพิ่ม
                $new_billing = AddressBook::create($input_bill);
                $billing_address_id = $new_billing->address_book_id;
            }
        }

        // -------------------- Check Stock and Status of Product Before Insert Order
        foreach($carts as $cart){
            if($cart->qty < $cart->cart_qty || $cart->status == '0'){
                return redirect()->to('cart');
            }
        }

        // -------------------- Insert into orders
        $order_no = $objFn->gen_order_no('orders','orders_id','orders_no');

        // Add Address
        $objAddress = new AddressBook();
        $shipping_address = $objAddress->pattern($shipping_address_id);
        if($billing_address_id == 0){
            $billing_address = $shipping_address;
        }else{
            $billing_address = $objAddress->pattern($billing_address_id);
        }
        // Order Status
        $orders_status = '2'; // Waiting Payment
        if($shipping_method_id == 2 && $user_type != '1')
            $orders_status = '1'; // หากเลือกการจัดส่งโดย Lalamove รอการอนุมัติจากผู้ดูแลระบบ

        // Insert order
        $insertOrder = [
            'orders_no' => $order_no,
            'orders_date' => Carbon::now(),
            'user_id' => $user_id,
            'billing_address_id' => $billing_address_id,
            'billing_address' => $billing_address,
            'shipping_address_id' => $shipping_address_id,
            'shipping_address' => $shipping_address,
            'shipping_method_id' => $shipping_method_id,
            'payment_method_id' => $payment_method_id,
            'total_price' => $total_price,
            'dollar_total_price' => $dollar_total_price,
            'shipping_price' => $shipping_price,
            'dollar_shipping_price' => $dollar_shipping_price,
            'discount_price' => $discount_price,
            'dollar_discount_price' => $dollar_discount_price,
            'use_point' => $use_point,
            'dollar_use_point' => $dollar_use_point,
            'status_id' => $orders_status
        ];
        $orders = Orders::create($insertOrder);
        $orders_id = $orders->orders_id;

        // -------------------- Insert into orders detail
        $text_stock = '';
        foreach($carts as $cart){
            $unit_price = $cart->retail_price;
            $flag = 0;
            if($cart->special_price > 0){
                $unit_price = $cart->special_price;
                $flag = 1;
            }
            $dollar_unit_price = $obj_dollar->cal_dollar($unit_price,$currency);

            $insertOrderDetail = [
                'orders_id' => $orders_id,
                'product_id' => $cart->product_id,
                'product_option_id' => $cart->product_option_id,
                'product_name_th' => $cart['product_name_th'].' '.$cart['option_name_th'],
                'product_name_en' => $cart['product_name_en'].' '.$cart['option_name_en'],
                'qty' => $cart->cart_qty,
                'unit_price' => $unit_price,
                'dollar_unit_price' => $dollar_unit_price,
                'flag' => $flag,
                'model' => $cart['model']
            ];
            $orders_detail[] = OrdersDetail::create($insertOrderDetail);
            // --------------- ตัด Stock จริง
            $cal_stock = $cart->qty - $cart->cart_qty;
            ProductOption::where('product_option_id',$cart->product_option_id)->update(['qty'=>$cal_stock]);
            // -------------- Check Stock
            if($cal_stock <= $cart->minimum_qty){
                $text_stock .= 'Product Name :: '.$cart['product_name_th'].' '.$cart['option_name_th'].'<br>';
            }
        }

        // --------------- Use point transaction
        if($use_point != 0){
            $orderFn->usePoint($orders_id,$use_point);
        }
        // --------------- Insert Discount Code
        if($discount_code_id != '' && $discount_code != ''){
            LogCodeUsed::create(['orders_id' => $orders_id, 'discount_code_id' => $discount_code_id, 'discount_code' => $discount_code, 'user_id' => $user_id]);
            if($discount_code_id != '0' && $discount_code_id == '1' && $discount_code_id == '2' && $discount_code_id == '3' && $discount_code_id == '4'){
                $logCode = DiscountCode::find($discount_code_id);
                $logCode->reserved = $logCode->reserved + 1;
                $logCode->save();
            }
        }

        // --------------- Send Mail to WH
        if(!empty($text_stock)){
            Mail::send('emails.stock',['text'=>$text_stock],function($m){
                $m->to(env('MAIL_CONTACT'), 'Webmaster YakYim');
                $m->subject('แจ้งเตือน stock สินค้า');
            });
        }
        // -------------- Send Mail to Admin
        Mail::send('emails.alert-order',[ "orders_id" => $orders_id],function($m){
            $m->to(env('MAIL_CONTACT'), 'Webmaster YakYim');
            $m->subject('แจ้งเตือนการสั่งซื้อสินค้า');
        });
        $orderFn->sendMailUser($orders_id,'ยืนยันการสั่งซื้อสินค้า' ); // send mail confirm order
        // --------------- Clear cart
        Cart::where('user_id',$user_id)->forceDelete();
        // --------------- Track Order
        $orderFn->orderTracking($orders_id,$orders_status);

        $grand_total = $total_price + $shipping_price - $discount_price;
        if(session()->has('s_use_point')){
            $use_point = session()->get('s_use_point');
            if($use_point > $grand_total) $use_point = $grand_total;
            $grand_total = $grand_total - $use_point;
        }
        if($grand_total <= 0){
            Orders::where('orders_id',$orders_id)->update(['status_id' => '3']);
            $orderFn->orderTracking($orders_id,'3');
            $orderFn->sendMailWh($order_no); // send mail to wh
            session()->flash('s_order_no',$order_no);
            return redirect()->to('/');
            // return redirect()->to('thankyou');
        }

        session()->forget('s_discount_id');
        session()->forget('s_discount_code');
        session()->forget('s_use_point');
        session()->forget('s_use_point_dollar');
        if($payment_method_id == '1' || $payment_method_id == '' ){
            session()->flash('s_order_no',$order_no);
            return redirect()->to('/');
            // return redirect()->to('thankyou');
        }else{
            $encrypted = Crypt::encrypt($order_no);
            return redirect()->to('paypal?token='.$encrypted);
        }
    }

    public function thankyou(){
        $this->chk_session_order();
        return view('frontend.cart.thankyou');
    }
    public function paymentfail(){
        $this->chk_session_order();
        return view('frontend.cart.paymentfail');
    }
    public function chk_session_order(){
        $order_no = session()->get('s_order_no');
        if(empty($order_no)){
            return redirect()->to('/');
        }
    }
}
