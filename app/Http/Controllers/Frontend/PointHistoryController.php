<?php
namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use App\Model\User;
use App\Model\PointTransaction;

use Hash;
use Validator;
use Crypt;
use Carbon\Carbon;
use Mail;
use Input;
use SEOMeta;
use OpenGraph;

class PointHistoryController extends Controller
{
    public function __construct(){
        $title = trans('seo.title');
        $description = trans('seo.description');
        $keyword = trans('seo.keyword');
        SEOMeta::setTitle('My Account - '.$title);
        SEOMeta::setDescription($description);
        SEOMeta::addKeyword($keyword);

        OpenGraph::setDescription($description);
        OpenGraph::setTitle($title);
        OpenGraph::setUrl(request()->url());
        OpenGraph::addImage(['url' => 'pathImg', 'size' => 300]);

        $this->user_id = session()->get('s_user_id');
        $this->user = User::find($this->user_id);
        $this->session_id = $session_id = session()->getId();
        $this->lang = session()->get('locale');
    }

    public function getIndex(){
        $objFn = new MainFunction();
        $user_id = $this->user_id;
        $user = $this->user;

        $data = PointTransaction::where('user_id',$user_id)->orderBy('point_trans_id','desc')->paginate(20);

        $last_point = PointTransaction::where('user_id', $user_id)
                                ->orderBy('point_trans_id', 'desc')
                                ->first();
        $current_point = (!empty($last_point))?$last_point->total:'0';

        return view('frontend.account.point-history', compact('user', 'data','objFn', 'current_point'));
    }
}
