<?php
namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\MainFunction;

use App\Model\User;
use App\Model\Cart;

use Hash;
use Validator;
use Crypt;
use Carbon\Carbon;
use Mail;

use SEOMeta;
use OpenGraph;


class LoginController extends Controller
{
    public function __construct()
    {
        $title = trans('seo.title');
        $description = trans('seo.description');
        $keyword = trans('seo.keyword');
        SEOMeta::setTitle('Signin - '.$title);
        SEOMeta::setDescription($description);
        SEOMeta::addKeyword($keyword);

        OpenGraph::setDescription($description);
        OpenGraph::setTitle($title);
        OpenGraph::setUrl(request()->url());
        OpenGraph::addImage(['url' => 'pathImg', 'size' => 300]);
    }

    public function getIndex(){
        if(session()->has('s_user_id')){
            return redirect()->to('account/profile');
        }

        return view('frontend.login');
    }
    public function postCheck(Request $request){
        $error = '';
        $email = $request->email;
        $pass = $request->password;

        $validator = Validator::make($request->all(),
            [
                "email" => 'required|email',
                "password" => 'required|between:6,20'
            ]
        );
        // Validate fields
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        // Check User
        $user = User::where('email',$email)->first();
        if(empty($user)) {
            $error = trans('message.email-not-found-please-register');
        }else{
            if($user->verified == 0){
                $error = trans('message.please-verify');
                $mail_encrypted = Crypt::encrypt($email);

                return redirect()->back()->with(['errorMsg' => $error, 'encrypted' => $mail_encrypted]);
            }else{
                if(!Hash::check($pass,$user->password)) {
                    $error = trans('message.wrong-password');

                }else{
                    session()->put('s_user_id', $user->user_id);
                    session()->put('s_user_name', $user->firstname.' '.$user->lastname);
                    session()->put('s_user_type', $user->user_type); // 1 = VIP
                    session()->put('s_user_email', $user->email);
                    session()->put('s_user_no', $user->user_no);

                    // Update Cart
                    $user_id = session()->get('s_user_id');
                    $session_id = session()->getId();
                    $chk_cart = Cart::where('session_id',$session_id)->orWhere('user_id',$user_id)->get();
                    if(count($chk_cart) > 0){
                        Cart::where('session_id',$session_id)->update(['user_id' => $user_id ]);
                        Cart::where('user_id',$user_id)->update(['session_id'=>'']);

                        $carts = Cart::selectRaw('session_id,user_id,product_id,product_option_id, sum(qty) as qty')->where('user_id',$user_id)->groupBy('product_id','product_option_id')->get();
                        Cart::where('user_id',$user_id)->forceDelete();
                        if(count($carts) > 0){
                            foreach($carts as $cart){
                                Cart::create(['user_id' => $cart->user_id, 'product_id' => $cart->product_id, 'product_option_id'=>$cart->product_option_id, 'qty' => $cart->qty ]);
                            }
                        }
                    }

                    $prev_page = session()->get('prev_page');
                    if (empty($prev_page)) {
                        return redirect()->to('account/profile');
                    } else {
                        session()->forget('prev_page');
                        return redirect()->to($prev_page);
                    }
                }
            }
        }
        session()->forget('prev_page');
        return redirect()->back()->with('errorMsg', $error);
    }
    public function getLogout(){
        session()->flush();
        return redirect()->to('login');
    }

    public function resendMail($token){
        $email = Crypt::decrypt($token);

//        return $email;
        $user = User::where('email',$email)->first();
        $firstname = $user->firstname;
        $lastname = $user->lastname;
        $gen_token = Crypt::encrypt($user->user_id); // Gen token for verify email
        // Send mail verify
        Mail::send('emails.verify', ['firstname' => $firstname, 'lastname' => $lastname, 'gen_token' => $gen_token], function ($m) use ($email, $firstname, $lastname) {
            $m->to($email, $firstname . ' ' . $lastname);
            $m->subject('Resend Verify Email ');
        });

        return redirect()->back()->with('successMsg', trans('message.register-success'));
    }

}
