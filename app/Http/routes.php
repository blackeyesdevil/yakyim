<?php
use App\Http\Middleware\Cors;
/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
*
*   FRONT END ROUTE
*
*/
Route::get('lang/{lang}',[
  'as'=>'lang',
  'uses'=>'Frontend\LocaleController@lang'
]);
Route::get('/',[
  'as'=>'home',
  'uses'=>'Frontend\MainController@getIndex'
]);
Route::get('aboutus',[
  'as'=>'aboutus',
  'uses'=>'Frontend\AboutUsController@getIndex'
]);
Route::controller('subscribe','Frontend\SubscribeController');
Route::controller('contactus','Frontend\ContactUsController');
Route::get('form-mail',function(){
    return view('emails.template');
});
Route::controller('login','Frontend\LoginController');
Route::get('logout','Frontend\LoginController@getLogout');
Route::controller('register','Frontend\RegisterController');
Route::controller('forget-password','Frontend\ForgetPasswordController');
Route::controller('reset-password','Frontend\ResetPasswordController');
Route::get('resend-email/{token}','Frontend\LoginController@resendMail');

// My Account
Route::post('wishlist/add','Frontend\WishlistController@postAdd');
Route::group(['middleware' => 'account','prefix'=>'account'],function() {
    Route::get('/',function(){ return redirect()->to('account/profile'); });
    Route::controller('profile','Frontend\ProfileController');
    Route::controller('change-password','Frontend\ChangePasswordController');
    Route::controller('order-return','Frontend\OrderReturnProductController');
    // Route::get('order-return-confirm','Frontend\OrderReturnProductController@postReturn_Product');
    Route::post('order-return-post/','Frontend\OrderReturnProductController@postReturn_Product');
    Route::post('order-return-success/','Frontend\OrderReturnProductController@success');
    Route::post('order-return-refund-product-detail/','Frontend\OrderReturnProductController@accountNumber');
    // Route::post('order-return/','Frontend\OrderReturnProductController@postReturn_Product');
    Route::controller('address-book','Frontend\AddressBookController');
    Route::controller('order-history','Frontend\OrderHistoryController');
    Route::get('order-inform/{orders_no}','Frontend\OrderInformController@inform');
    Route::post('order-inform/check','Frontend\OrderInformController@check');
    Route::get('order-tracking/{orders_no?}','Frontend\OrderTrackingController@tracking');
    Route::get('order-payment/{orders_no}','Frontend\OrderHistoryController@payment');
    Route::controller('point-history','Frontend\PointHistoryController');
    Route::controller('wishlist','Frontend\WishlistController');
});

// Product
Route::get('product/detail/{product_id}/{product_name}','Frontend\ProductController@productDetail');
Route::get('product/{category}/{category_id}/{size?}/{size_value?}','Frontend\ProductController@productList')->where(['size' => '(size)']);
Route::get('chart/{product_option_id}','Frontend\ProductController@getChart');

// Filter Address
Route::group(['prefix'=>'address'],function() {
    Route::get('province','AddressController@province');
    Route::get('amphur/{province_id}','AddressController@amphur');
    Route::get('district/{amphur_id}','AddressController@district');
    Route::get('zipcode/{district_id}','AddressController@zipcode');
});

// Shopping
Route::controller('cart','Frontend\CartController');
Route::controller('checkout','Frontend\CheckoutController');
Route::post('confirm','Frontend\ConfirmController@confirm');
Route::controller('discount','Frontend\DiscountController');
Route::get('thankyou','Frontend\ConfirmController@thankyou');
Route::get('paymentfail','Frontend\ConfirmController@paymentfail');


// Static Page
Route::get('how-to-order',[
    'as'=>'howto.how-to-order',
    'uses'=>'Frontend\MainController@HowToOrder'
]);
Route::get('how-to-register',[
    'as'=>'howto.how-to-register',
    'uses'=>'Frontend\MainController@HowToRegister'
]);
Route::get('how-to-pay',[
    'as'=>'howto.how-to-pay',
    'uses'=>'Frontend\MainController@HowToPay'
]);
Route::get('how-to-check',[
    'as'=>'howto.how-to-check',
    'uses'=>'Frontend\MainController@HowToCheck'
]);
Route::get('how-to-sizing-chart',[
    'as'=>'howto.how-to-sizing-chart',
    'uses'=>'Frontend\MainController@HowToSizingChart'
]);
Route::get('how-to-change-address',[
    'as'=>'howto.how-to-change-address',
    'uses'=>'Frontend\MainController@HowToChangeAddress'
]);
Route::get('how-to-shipping',[
    'as'=>'howto.how-to-shipping',
    'uses'=>'Frontend\MainController@HowToShipping'
]);
Route::get('how-to-discount',[
    'as'=>'howto.how-to-discount',
    'uses'=>'Frontend\MainController@HowToDiscount'
]);
Route::get('how-to-change-product',[
    'as'=>'howto.how-to-change-product',
    'uses'=>'Frontend\MainController@HowToChangeProduct'
]);
// Paypal
Route::controller('paypal','Frontend\PaypalController');

// ----------------------- อันไหนใช้ย้ายไปไว้ข้างบน เฉพาะหน้าบ้าน


Route::get('promotion/{name}/{id}','Frontend\NewPageController@main');

// ------ Link Footer
//Route::get('corporate-info','Frontend\FooterController@corporate');
Route::get('privacy-policy','Frontend\FooterController@privacyPolicy');
Route::get('sitemap','Frontend\FooterController@sitemap');
//Route::get('how-to-order','Frontend\FooterController@howToOrder');
Route::get('payment-method','Frontend\FooterController@paymentMethod');
Route::get('delivery','Frontend\FooterController@delivery');
Route::get('return-policy','Frontend\FooterController@returnPolicy');
Route::get('tracking-order','Frontend\FooterController@trackingOrder');
Route::get('order-tracking-detail',function(){
   return view('frontend.account.order-tracking-detail');
});



Route::group(array('middleware' => ['cors','web'], 'before' => '', 'prefix' => 'api'), function(){
    //API webservice route

});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});

Route::group(['prefix'=>'_admin'],function() {

    Route::get('/','Backend\DashboardController@index');
    Route::get('export-excel/{type}','Backend\DashboardController@exportSummary');

    Route::get('login','Backend\AuthController@getLogin');
    Route::post('login','Backend\AuthController@postLogin');
    Route::get('logout','Backend\AuthController@getLogout');

//    Route::controller('/product_stock','Backend\ProductStockController');
    Route::post('postRestock','Backend\ProductStockController@postRestock');

    Route::resource('banner_home', 'Backend\BannerHomeController');
    Route::resource('banner_promotion', 'Backend\BannerPromotionController');
    Route::resource('banner_product_pants', 'Backend\BannerProductPantsController');
    Route::resource('banner_product_shirts', 'Backend\BannerProductShirtsController');
    Route::resource('banner_product_sleepware', 'Backend\BannerProductSleepwareController');
    Route::resource('banner_product_accessories', 'Backend\BannerProductAccessoriesController');

    Route::resource('newsletter', 'Backend\NewsletterController');
    Route::resource('category', 'Backend\CategoryController');
    Route::resource('product_category', 'Backend\ProductCategoryController');
    Route::resource('product_attribute', 'Backend\ProductAttributeController');
    Route::resource('product_option_group', 'Backend\ProductOptionGroupController');
    Route::resource('product_option', 'Backend\ProductOptionController');
    Route::resource('product_gallery', 'Backend\ProductGalleryController');
    Route::resource('size_chart', 'Backend\SizeChartController');
    Route::resource('size_chart_detail', 'Backend\SizeChartDetailController');
    Route::resource('address_book', 'Backend\AddressBookController');
    Route::resource('discount', 'Backend\DiscountController');
    Route::resource('discount_code', 'Backend\DiscountCodeController');
    Route::resource('user', 'Backend\UserController');
    Route::resource('contact_msg', 'Backend\ContactMsgController');
    Route::resource('orders', 'Backend\OrdersController');
    Route::resource('orders_return', 'Backend\OrdersReturnController');
    Route::resource('orders_warehouse', 'Backend\OrdersWareHouseController');
    Route::resource('orders_detail', 'Backend\OrdersDetailController');
    Route::resource('about_us', 'Backend\AboutUsController');
    Route::resource('new_page', 'Backend\NewPageController');
    Route::resource('manufacturer', 'Backend\ManufacturerController');
    Route::resource('attribute', 'Backend\AttributeController');
    Route::resource('special', 'Backend\SpecialController');
    Route::resource('special_tag', 'Backend\SpecialTagController');
    Route::resource('special_category', 'Backend\SpecialCategoryController');
    Route::resource('review', 'Backend\ReviewController');

    Route::resource('dimension_class', 'Backend\DimensionClassController');
    Route::resource('weight_class', 'Backend\WeightClassController');
    Route::resource('career', 'Backend\CareerController');
    Route::resource('place_holder', 'Backend\PlaceHolderController');
    Route::resource('subscribe', 'Backend\SubscribeController');

    Route::resource('send_mail', 'Backend\SendMailController');

    Route::resource('sub_category', 'Backend\SubCategoryController');
    Route::resource('shipping_rate', 'Backend\ShippingRateController');
    Route::resource('point_condition', 'Backend\PointConditionController');
    Route::resource('member_condition', 'Backend\MemberConditionController');
    Route::resource('member_discount', 'Backend\MemberDiscountController');
    Route::resource('point_condition', 'Backend\PointConditionController');
    Route::resource('dollar', 'Backend\DollarController');
    Route::resource('birthday_discount', 'Backend\BirthdayDiscountController');

    Route::resource('product_new', 'Backend\ProductNewController');
    Route::resource('product_recommend', 'Backend\ProductRecommendController');
    Route::resource('product_stock', 'Backend\ProductStockController');

    Route::resource('product', 'Backend\ProductController');
    Route::resource('option_group', 'Backend\OptionGroupController');
    Route::resource('option', 'Backend\OptionController');
    Route::resource('location', 'Backend\LocationController');
    Route::resource('set_dollar', 'Backend\DollarController');

    Route::resource('banner_top', 'Backend\BannerTopController');
    Route::resource('banner_promotion', 'Backend\BannerPromotionController');
    Route::resource('warehouse', 'Backend\WareHouseController');
    Route::resource('product_related', 'Backend\ProductRelatedController');
    Route::resource('new_product', 'Backend\NewProductController');
    Route::resource('user/verified', 'Backend\UserController@postCheckVerified'); 

    Route::group(['prefix'=>'address'],function() {
        Route::get('province','AddressController@province');
        Route::get('amphur/{province_id}','AddressController@amphur');
        Route::get('district/{amphur_id}','AddressController@district');
        Route::get('zipcode/{district_id}','AddressController@zipcode');
    });

    Route::controller('/change','Backend\ChangeStatusController');

    Route::post('status-change','Backend\OrdersController@chk_status');
    Route::post('update-tracking','Backend\OrdersController@update_tracking');
    Route::get('orders/{order_id}/stock', 'Backend\OrdersController@stock');
    Route::post('orders/{order_id}/updateStock', 'Backend\OrdersController@updateStock');
    Route::get('orders/{order_id}/payment', 'Backend\OrdersController@payment');
    Route::post('orders/{order_id}/updatePayment', 'Backend\OrdersController@updatePayment');
    Route::post('tag_product','Backend\ProductController@postTagProduct');
    Route::post('tag_blog','Backend\BlogController@postTagBlog');
    Route::post('delete-product','Backend\BlogProductController@destroy');
    Route::post('copy_product','Backend\ProductController@postCopyProduct');

    Route::post('api-active','Backend\ApiController@changeActive');
    Route::post('zip-co','Backend\ApiController@District');
    Route::post('amphur','Backend\ApiController@Amphur');
    Route::post('province','Backend\ApiController@Province');

    Route::post('del-pro','Backend\ApiController@delProduct');
    Route::post('del-category','Backend\ApiController@delCategory');
    Route::post('del-product-related','Backend\ApiController@delRelated');
    Route::controller('/sequence','Backend\SequenceController');

    // Route::post('/test_send_mail', 'EmailController@send');

    Route::post('status-return-change','Backend\OrdersReturnController@change_status');

});
