<?php

namespace App\Http\Middleware;

use Closure;

class UserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $s_user_id = session()->get('s_user_id');
        if(empty($s_user_id)){
            $prev_page = request()->path();
            session()->put('prev_page',$prev_page);
            return redirect()->to('login');
        }
        return $next($request);
    }
}
