jQuery(document).ready(function(){
    DelProduct(jQuery('.delete-product'));

});

function DelProduct(element)
{
    element.on('click',function(event){

        var tb_name = $(this).data('tb_name');
        var value = $(this).data('value');
        var blog_id = $('input[name=blog_id]').val();
        var change_field = $(this).data('change_field');
        var _token = $('input[name=_token]').val();
        var FormData = {
            'tb_name' : tb_name,
            'blog_id' : blog_id,
            'value' : value,
            '_token' : _token
        };

        $.ajax({
            type : "POST",
            url : '../del-pro',
            data : FormData,
            dataType : 'json',
            success: function(data){

                if(data==0) {

                    $('a[id=' + change_field + '-' + value + ']').remove();

                }



            }
        });


/*        alert(tb_name)
        alert(value)
        alert(blog_id)*/



    });
}