jQuery(document).ready(function(){
    DelCategory(jQuery('.delete-category'));
    DelProduct(jQuery('.delete-product'));
});

function DelCategory(element)
{
    element.on('click',function(event){

        var tb_name = $(this).data('tb_name');
        var value = $(this).data('value');
        var product_id = $('input[name=product_id]').val();
        var change_field = $(this).data('change_field');
        var _token = $('input[name=_token]').val();
        var FormData = {
            'tb_name' : tb_name,
            'product_id' : product_id,
            'value' : value,
            '_token' : _token
        };



        $.ajax({
            type : "POST",
            url : '/_admin/del-category',
            data : FormData,
            success: function(data){



                if(data=="0") {

                    $('a[id=' + change_field + '-' + value + ']').remove();

                }



            }
        });


/*        alert(tb_name)
        alert(value)
        alert(blog_id)*/



    });
}

function DelProduct(element)
{


    element.on('click',function(event){

        var tb_name = $(this).data('tb_name');
        var value = $(this).data('value');
        var product_id = $('input[name=product_id]').val();
        var change_field = $(this).data('change_field');
        var _token = $('input[name=_token]').val();
        var FormData = {
            'tb_name' : tb_name,
            'product_id' : product_id,
            'value' : value,
            '_token' : _token
        };

        $.ajax({
            type : "POST",
            url : '/_admin/del-product-related',
            data : FormData,
            success: function(data){

                if(data=="0") {

                    $('a[id=' + change_field + '-' + value + ']').remove();

                }



            }
        });


        /*        alert(tb_name)
         alert(value)
         alert(blog_id)*/



    });
}