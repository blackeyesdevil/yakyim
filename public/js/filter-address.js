/**
 * Created by user on 4/28/16 AD.
 */
// load province
function filter_address(type,next_type,value,selected_val,section){
    if(value != '') {
        url = "";
        if(next_type != 'province' ) url = "/"+value;
        $.ajax({
            
            // url: "/yakyim/public/address/"+next_type+url,
            type: "GET",
            data: {},
            success: function (data) {
                if (next_type == "zipcode") {
                    $(".filter-" + next_type +"[data-section="+section+"]").val(data.name);
                } else {
                    option = "<option value=''>--Select--</option>";
                    //if(next_type == "zipcode") option = '';
                    $.each(data, function (key, item) {
                        selected = '';
                        if (selected_val == item['id']) selected = 'selected';
                        option += "<option value='" + item['id'] + "' " + selected + ">" + item['name'] + "</option>";
                    });
                    $(".filter-" + next_type+"[data-section="+section+"]").html(option)
                }
            }
        });
    }else{
        option = "<option value=''>--Select--</option>";
        if(type == "province"){
            $(".filter-amphur[data-section="+section+"],.filter-district[data-section="+section+"]").html(option);
            $(".filter-zipcode[data-section="+section+"]").val('');
        }
        if(type == "amphur"){
            $(".filter-district[data-section="+section+"]").html(option);
            $(".filter-zipcode[data-section="+section+"]").val('');
        }
        if(type == "district"){
            $(".filter-zipcode[data-section="+section+"]").val('');
        }
    }
}
$(document).ready(function() {
    $('.filter-province').on('change',function() {
        value = $(this).val();
        section = $(this).data('section');
        $(".filter-district[data-section="+section+"]").html("<option value=''>--Select--</option>");
        $(".filter-zipcode[data-section="+section+"]").val('');
        filter_address('province','amphur',value,null,section);
    });
    $('.filter-amphur').on('change',function() {
        value = $(this).val();
        section = $(this).data('section');
        $(".filter-zipcode[data-section="+section+"]").val('');
        filter_address('amphur','district',value,null,section);
    });
    $('.filter-district').on('change',function() {
        value = $(this).val();
        section = $(this).data('section');
        filter_address('district','zipcode',value,null,section);
    });
});