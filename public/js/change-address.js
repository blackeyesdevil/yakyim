jQuery(document).ready(function(){
    ProvinceActive(jQuery('.select-province'));
    AmphurActive(jQuery('.select-amphur'));


});




function ProvinceActive(element)
{

    element.on('change',function(event){

        var province_id = $('#province_id').val();
        var _token = $('input[name=_token]').val();

        var FormData1 = {
            '_token' : _token,
            'province_id' : province_id,
        };


        $.ajax({
            type : "POST",
            url : '/_admin/province-select',
            data : FormData1,

            success: function(data){


                $('select#amphur_id').html(data);



            }

        });


    });
}
function AmphurActive(element)
{
    element.on('change',function(event){

        var _token = $('input[name=_token]').val();
        var amphur_id = $('#amphur_id').val();


        var FormData2 = {
            '_token' : _token,
            'amphur_id' : amphur_id,
        };

        $.ajax({
            type : "POST",
            url : '/_admin/amphur-select',
            data : FormData2,
            success: function(data){
                $('select#district_id').html(data);



            }

        });


    });
}

