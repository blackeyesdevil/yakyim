$(function(){
  $('#register .header').on('click', function(){
    $('#register .form').toggle();

    icon = $(this).find('i.glyphicon');
    if (icon.hasClass('glyphicon-chevron-down'))
      icon.removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-right');
    else
      icon.removeClass('glyphicon-chevron-right').addClass('glyphicon-chevron-down');
  });
});
