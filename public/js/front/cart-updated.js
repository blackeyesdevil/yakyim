$(function(){
    var total_price;
    var token = $('meta[name="csrf-token"]').attr('content');
    var lang = $('meta[name="lang"]').attr('content');
    var sign;
    if(lang == 'th') sign = '฿';
    else sign = '$';

    $("input[type=number]").numble({ minValue: 1 });
    function numberFormat(price){
        price = parseFloat(price).toLocaleString(undefined, {minimumFractionDigits: 2});
        return price;
    }
    function totalPrice(){
        total_price = 0;
        $('.subtotal').each(function( index ) {
            price = parseFloat($( this ).text().substring(1).replace(',','').trim());
            total_price = total_price+price;

            //console.log('Total Price :: '+numberFormat(price));
        });
        //return addCommas(total_price);

        return numberFormat(total_price);
    }
    function totalQty(){
        total_qty = 0;
        $('.numble-value').each(function( index ) {
            price = parseInt($( this ).text().trim());
            total_qty = total_qty+price;
        });
        if(total_qty == 0){
            $('.header-qty').removeClass('alert-qty');
            total_qty = '';
        }
        $('.header-qty').text(total_qty);
    }
    function updateCart(obj){
        qty = obj.parent().find('.numble-value').text();
        old_qty = obj.parent().parent().parent().find('input[name=old_qty]').val();
        cart_id = obj.parent().parent().parent().find('input[name=cart_id]').val();

        price = obj.parent().parent().parent().parent().find('.eprice').text().substring(1).replace(',','').trim();
        sub_price = parseFloat(price)*qty;

        //console.log('Sub Price :: '+numberFormat(sub_price));
        $.ajax({
            url: '/yakyim/public/cart/update',
            headers: { 'X-CSRF-TOKEN': token },
            data: { 'cart_id':cart_id, 'qty':qty },
            type: 'POST',
            success: function (data) {
                status = data.status;
                text = data.text;
                if(status == '1'){
                    obj.parent().parent().parent().parent().find('.subtotal').text(sign+' '+numberFormat(sub_price));
                    $('.totalprice').text(sign+' '+totalPrice());
                    obj.parent().parent().parent().find('input[name=old_qty]').val(qty);
                }else{
                    obj.parent().find('.numble-value').text(old_qty);
                    obj.parent().parent().parent().find('input[name=qty]').val(old_qty);
                }
                totalQty();
                //console.log('Cart ID::'+cart_id+', Qty.:: '+qty+', Status::'+status+' '+', Text::'+text);
            }
        });
    }

    $('.numble-arrow').click(function(){ updateCart($(this)); });
    $('.numble-value').on('blur', function(){ updateCart($(this)); });

    $('.remove').click(function(){
        obj = $(this);
        cart_id = obj.parent().parent().parent().find('input[name=cart_id]').val();
        $.ajax({
            url: '/yakyim/public/cart/delete',
            headers: { 'X-CSRF-TOKEN': token },
            data: { 'cart_id':cart_id },
            type: 'POST',
            success: function (data) {
                status = data.status;
                text = data.text;
                if(status == '1'){
                    obj.parent().parent().parent().remove();
                    $('.totalprice').text(sign+' '+totalPrice());
                    if(totalPrice() == '0.00'){
                        $('tbody').html("<tr><td colspan=\"6\" class=\"text-center\">No Result</td></tr>");
                        $('tfoot').remove();
                        $('#last-section div:last-child').remove();
                    }
                }
                totalQty();
            }
        });
    });

});
