$(document).ready(function(){
    var timeouts = [];

    $('.qty').on('change', _.debounce(function(e){
        cart_id = $(this).data('id');
        old_qty = $(this).data('current');
        qty = $(this).val();

        if (qty <= 0 || qty.isInteger){
            qty = $(this).val(old_qty);
        }
        else {
            $('#content').css('pointer-events', 'none').css('opacity', '0.5');
            $('.qty').attr('disabled', 'disabled');

            $.ajax({
                url : '/cart/update/' + cart_id + '/' + qty,
                type : 'GET',
                data : {},
                success : function(data){
                    if(data.status == '0'){
                        $('.qty[data-id=' + cart_id + ']').val(old_qty);
                        $('.error-msg[data-id=' + cart_id + ']').html(data.text);

                        $('#content').css('pointer-events', 'inherit').css('opacity', '1');
                        $('.qty').removeAttr('disabled');
                    }else{
                        window.location.reload();
                    }
                }
            });
        }
    }, 1000));

    $('.del-btn').on('click',function(e){
        e.preventDefault();

        var r = confirm('Are your sure you want to delete this product?');

        if (r == true){
          cart_id = $(this).data('id');
          $('#content').css('pointer-events', 'none').css('opacity', '0.5');

          $.get('/cart/delete/' + cart_id, {}, function(){
              window.location.reload();
          });
        }
    });
});
