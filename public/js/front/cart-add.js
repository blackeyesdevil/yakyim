$(function(){
    $('.cart').on('click',function(e){
        e.preventDefault();
        token = $('meta[name="csrf-token"]').attr('content');
        option_id = $(".size.active a").data('id');
        qty = 1;

        $('.alert').removeClass('show').removeClass('alert-success').removeClass('alert-danger');
        if(option_id != null){
            $.ajax({
                url: '/yakyim/public/cart/add',
                headers: { 'X-CSRF-TOKEN': token },
                data: { 'option_id':option_id, 'qty':qty },
                type: 'POST',
                success: function (data) {
                    status = data.status;
                    text = data.text;
                    if(status == '1') {
                        alert_class = 'alert-success';
                        $('.header-qty').addClass('alert-qty');
                        $('.header-qty').text(data.total_qty);
                    } else {
                        alert_class = 'alert-danger';
                    }

                    $('.alert').removeClass('hide').addClass('show');
                    $('.alert').addClass(alert_class);
                    $('.alert').text(text);

                    $('.alert').delay(3000).fadeOut('slow',function(){
                        $('.alert').removeClass('show').removeClass('alert-success').removeClass('alert-danger');
                    });
                }
            });
        }else{

        }
    });
});
