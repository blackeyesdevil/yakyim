/**
 * Created by user on 7/27/16 AD.
 */
$(document).ready(function(){
    function alertResult(data){
        status = data.status;
        text = data.txt;

        //$('.alert').removeClass('alert-success').removeClass('alert-danger').html('');
        if(status == 1){
            $('.alert').html(text).addClass('alert-success').fadeIn('slow');
        }else{
            $('.alert').html(text).addClass('alert-danger').fadeIn('slow');
        }

    }

    $(".size:not('.disabled')").on('click',function(e){
        obj = $(this);
        option_id = obj.find('a').data('id');

        $('.size').removeClass('active');
        obj.addClass('active');

        $('.model span').removeClass('show').addClass('hide');
        $('.model span[data-id='+option_id+']').removeClass('hide').addClass('show');
    });

    $('.wishlist').on('click',function(e){
        var timer;
        clearTimeout(timer);
        token = $('meta[name="csrf-token"]').attr('content');
        obj = $(this);
        product_id = obj.data('id');

        $.ajax({
            url: '/yakyim/public/wishlist/add',
            headers: { 'X-CSRF-TOKEN': token },
            data: { 'product_id':product_id },
            type: 'POST',
            success: function (data) {
                alertResult(data);
                var timer = setTimeout(function(){
                    $('.alert').fadeOut('slow');
                },4000);
                $('.wishlist-qty').addClass('alert-qty');
                $('.wishlist-qty').text(data.wishlist_qty);
            }
        });
    });
});
