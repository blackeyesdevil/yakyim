$(function(){
  function bannerSize(obj){
    width = $( window ).width();
    value = obj.data('value');
    sm = obj.data('sm');
    md = obj.data('md');
    if(value == true){
      if(width >= 992)
        obj.css('background-image', 'url(' + md + ')');
      if(width >= 768 && width < 992)
        obj.css('background-image', 'url(' + sm + ')');
    }
  }
  bannerSize($('#banner'));
  $( window ).resize(function() {
    bannerSize($('#banner'));
  });

  $('.product-box').hover(function(){
    obj = $(this);
    objImgSmall = obj.find('.img-small');
    objImgBig = obj.find('.img-big');

    newSourceSmall = objImgSmall.data('alt-src');
    newSourceBig = objImgBig.data('alt-src');

    objImgSmall.data('alt-src', objImgSmall.attr('src'));
    objImgSmall.attr('src', newSourceSmall);
    objImgBig.data('alt-src', objImgBig.attr('src'));
    objImgBig.attr('src', newSourceBig);
    
  });
});
