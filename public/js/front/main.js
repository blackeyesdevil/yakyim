(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

$(function () {
  var lang = $('meta[name=lang]').attr('content');
  $(window).resize(function () {
    var window_width = $(window).width();
    console.log(window_width);
    if (window_width > 991) {
      $(".nav-menu ul").css({ 'display': 'block' });
    } else {
      $(".nav-menu ul").css({ 'display': 'none' });
    }
  });
  $(".toggle-menu").on('click', function (e) {
    e.preventDefault();
    $(".nav-menu ul").slideToggle("slow");
  });
  $("#newsletter-form").submit(function (e) {
    e.preventDefault();

    var token = $('meta[name="csrf-token"]').attr('content');
    var obj = $("input[name='email-subscribe']");
    var email = obj.val();
    var result = true;
    var addClass = 'text-danger';
    var textAlert = lang == 'en' ? 'Your e-mail address is incorrect.' : 'กรุณากรอกอีเมลให้ถูกต้องค่ะ';

    var atpos = email.indexOf("@");
    var dotpos = email.lastIndexOf(".");
    if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length) {
      result = false;
    }

    if (email != '' && result == true) {
      $.ajax({
        url: '/subscribe/send',
        headers: { 'X-CSRF-TOKEN': token },
        data: { 'email': email },
        type: 'POST',
        success: function success(data) {
          textAlert = lang == 'en' ? 'Subscribed Successfully' : 'ขอบคุณที่สมัครรับข่าวสารกับเรา';

          addClass = 'text-success';
          $('.alert-subscribe').removeClass('hide').removeClass('text-danger').addClass(addClass).html(textAlert).addClass('show');
          obj.val('');
        }
      });
    } else {
      $('.alert-subscribe').removeClass('hide').removeClass('text-success').addClass(addClass).html(textAlert).addClass('show');
    }
  });
});

},{}]},{},[1]);

//# sourceMappingURL=main.js.map
