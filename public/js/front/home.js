/**
 * Created by user on 7/8/16 AD.
 */
$(function(){
    function bannerSize(obj){
        width = $( window ).width();
        value = obj.data('value');
        xs = obj.data('xs');
        sm = obj.data('sm');
        md = obj.data('md');
        mo = obj.data('mo');

        if(value == true){
            if(width >= 992){
                // obj.css('background-image', 'url(' + md + ')');
                $('#headertop').attr("src", md);
            }
            if(width >= 768 && width < 992){
              // obj.css('width','500px');
                // obj.css('background-image', 'url(' + sm + ')');
                $('#headertop').attr("src", sm);
            }
            if(width < 768 ){
                // obj.css('background-image', 'url(' + xs + ')');
                $('#headertop').attr("src", xs);

            }
            if(width <= 500 ){
                // obj.css('background-image', 'url(' + mo + ')');
                $('#headertop').attr("src", mo);

            }

        }
    }

    $('.bxSlider').bxSlider({
        'auto': true,
        'controls': true
    });
    // bannerSize($('header'));
    bannerSize($('.headertop'));
    $( window ).resize(function() {
        // bannerSize($('header'));
        bannerSize($('.headertop'));
    });
});
