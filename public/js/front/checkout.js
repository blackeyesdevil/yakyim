var path = '/yakyim/public';
$(document).ready(function(){

  //console.log(parseFloat('2,000.23'.replace(',','')));
  // -------------- Number Format
  function numberFormat(price){
    price = parseFloat(price).toLocaleString(undefined, {minimumFractionDigits: 2});
    return price;
  }
  // -------------- Next Step
  function nextStep(obj){
    current = $('#' + obj.data('current-step'));
    next = $('#' + obj.data('next-step'));

    //console.log(obj.data('current-step')+' '+obj.data('next-step'));
    current.removeClass('active');
    current.find('.content').slideUp('slow', function(){
      next.addClass('active');
      next.find('.content').slideDown('slow', function(){
        $('html,body').animate({ scrollTop: next.offset().top - 100 }, 'slow');
      });
    });
  }
  // -------------- Clear All Code
  function clearAll(){
    $('#order-confirmation .content').css('pointer-events', 'none').css('opacity', '0.5');
    shipping_method = $('input[name=shipping_method]:checked').val();

    $('.discount-div .birth-error').text(''); // error birth day
    $('#discount_code').val(''); // input value
    $('#discount-form').hide(); // ซ่อน form code
    $('.discount-div .error-msg').text('').hide(); // error code
    $('.discount-div .current-code .code').text(''); // clear show code
    $('.discount-div .current-code').hide(); // hidden current code

    url = path+'/discount/remove-discount/'+shipping_method;
    $.get(url, {  }, function(data){
      $('#discount-price').text('');
      $('.discount-price').addClass('hide');
      if (data.shipping_price > 0) {
        $('#shipping-price').text(numberFormat(data.shipping_price));
        $('.shipping-price').removeClass('hide');
      }else{
        $('#shipping-price').text('');
        $('.shipping-price').addClass('hide')
      }
      if(data.use_point > 0){
        $('#point').text(numberFormat(data.use_point));
      }
      $('.grand-total').text(numberFormat(data.grand_total));

      $('#order-confirmation .content').css('pointer-events', 'inherit').css('opacity', '1');

      console.log(data);
    });
  }
  // -------------- Clear Point
  function clearPoint(){
    url = path+'/discount/clear-session-point';
    $.get(url, {  }, function(data){
      $('input[name=select_point]:checked').prop("checked", false);
      $(".point").hide();
      $("#point").text('');
    });
  }
  // -------------- Select Address Choice
  $('.choice > label').on('click', function(){
    $(this).parent().parent().find('.active').removeClass('active');
    $(this).parent().addClass('active');
    $('.alert-danger').removeClass('show').addClass('hide');
  });
  // -------------- Prev Btn
  $('.prev-btn').on('click', function(e){
    e.preventDefault();
    current = $('#' + $(this).data('current-step'));
    prev = $('#' + $(this).data('prev-step'));
    current.removeClass('active');
    current.find('.content').slideUp('slow', function(){
      prev.addClass('active');
      prev.find('.content').slideDown('slow', function(){
        $('html,body').animate({ scrollTop: prev.offset().top - 100 }, 'slow');
      });
    });
    if($(this).data('prev-step') == 'payment-method'){
      clearAll();
      $('input[name=select_discount]:checked').prop("checked", false);
    }
  });
  // -------------- Shipping Tab
  $('#shipping-continue-btn').on('click', function(e){
    obj = $(this);
    if ($('input[name=shipping_type]:checked').val() == 'new'){
      firstname = $('#shipping_firstname').val();
      lastname = $('#shipping_lastname').val();
      address = $('#shipping_address').val();
      zipcode = $('#shipping_zipcode').val();
      province = $('#shipping_province').val();
      amphur = $('#shipping_amphur').val();
      district = $('#shipping_district').val();
      tel = $('#shipping_tel').val();

      if (firstname == '' || lastname == '' || address == '' || zipcode == '' || province == '' || amphur == '' || district == '' || tel == ''){
        $('#shipping-information .alert-danger').removeClass('hide').addClass('show');
        $('html,body').animate({ scrollTop: $('#shipping-information').offset().top - 100 }, 'slow');
      }
      else{
        $('#shipping-information .alert-danger').removeClass('show').addClass('hide');
        nextStep(obj);
      }
    }
    else{
      nextStep(obj);
    }
  });
  // -------------- Billing Tab
  $('#billing-continue-btn').on('click', function(e){
    obj = $(this);
    if ($('input[name=billing_type]:checked').val() == 'old'){
      firstname = $('#billing_firstname').val();
      lastname = $('#billing_lastname').val();
      address = $('#billing_address').val();
      zipcode = $('#billing_zipcode').val();
      province = $('#billing_province').val();
      amphur = $('#billing_amphur').val();
      district = $('#billing_district').val();
      tel = $('#billing_tel').val();
      tax_id = $('#billing_tax').val();

      if (firstname == '' || lastname == '' || address == '' || zipcode == '' || province == '' || amphur == '' || district == '' || tel == '' || tax_id == ''){
        $('#billing-information .alert-danger').removeClass('hide').addClass('show');
        $('html,body').animate({ scrollTop: $('#billing-information').offset().top - 100 }, 'slow');
      }else{
        $('#billing-information .alert-danger').removeClass('show').addClass('hide');
        nextStep(obj);
      }
    }else{
      nextStep(obj);
    }
  });
  // -------------- Shipping Method and Payment Method
  $('#payment-continue-btn').on('click',function(){
    obj = $(this);
    shipping_method = $('input[name=shipping_method]:checked').val();
    if(shipping_method == '2' && user_type != 1){
      nextStep(obj);
    }else{
      if($('input[name=payment_type]').is(':checked')){
        $('#payment-method .alert-danger').removeClass('show').addClass('hide');
        $('.birth_day').removeClass('hide').addClass('show');
        nextStep(obj);
      }else{
        $('#payment-method .alert-danger').removeClass('hide').addClass('show');
        $('html,body').animate({ scrollTop: $('#payment-method').offset().top - 100 }, 'slow');
      }
    }
  });
  // -------------- Shipping Method on Select Lalamove
  $('input[name=shipping_method]').on('change',function(){
    shipping_method = $('input[name=shipping_method]:checked').val();
    $('.birth_day').removeClass('show').addClass('hide');
    clearAll();
    if(shipping_method == '2' && user_type != 1){
      $('#payment-list').removeClass('show').addClass('hide');
      $('#shipping-list .text-danger').removeClass('hide').addClass('show');
    }else{
      $('#payment-list').removeClass('hide').addClass('show');
      $('#shipping-list .text-danger').removeClass('show').addClass('hide');
    }
  });
  // -------------- Current Code
  function currentCode(data){
    if (data.discount_price > 0){
      $('#discount-price').text(numberFormat(data.discount_price));
      $('.discount-price').removeClass('hide');
    }
    if (data.shipping_price > 0) {
      $('#shipping-price').text(numberFormat(data.shipping_price));
      $('.shipping-price').removeClass('hide');
    }
    $('.grand-total').text(numberFormat(data.grand_total));
    $('.discount-div .current-code .code').text(data.discount_code);
    $('.discount-div .current-code').show();
  }

  // -------------- Select Discount
  $('.select-discount').on('change',function(){
    clearAll();
    token = $('meta[name="csrf-token"]').attr('content');
    select_discount = $('input[name=select_discount]:checked').val();

    if(select_discount == '1'){ // birth day
      shipping_method = $('input[name=shipping_method]:checked').val();
      payment_method = $('input[name=payment_type]:checked').val();
      url = path+'/discount/birthday';
      $.post(url, { _token:token, payment_method:payment_method,shipping_method:shipping_method }, function(data){
        if (data.status == '1'){
          currentCode(data);
        }else{
          $('.discount-div .birth-error').text(data.text);
        }
        console.log(data);
      });
    }else{
      $('#discount-form').show();
    }
  });

  // -------------- Input Discount Code
  $('#discount-form').on('submit',function(e){
    e.preventDefault();
    token = $('meta[name="csrf-token"]').attr('content');
    lang = $('meta[name="lang"]').attr('content');
    url = $(this).attr('action');
    discount_code = $('#discount_code').val();
    select_discount = $('input[name=select_discount]:checked').val();
    payment_method = $('input[name=payment_type]:checked').val();
    shipping_method = $('input[name=shipping_method]:checked').val();

    if(select_discount == 2)
      url = url + '/code';
    else if(select_discount == 3)
      url = url + '/friend-code';

    if(discount_code != ''){
      $('#order-confirmation .content').css('opacity', '0.5').css('pointer-events', 'none');
      $.post(url, { _token: token, discount_code: discount_code, payment_method: payment_method,shipping_method:shipping_method }, function(data){
        if (data.status == '1'){
          currentCode(data);
          $('.discount-div .error-msg').text('').hide();
          $('#discount_code').val('');
          $('#discount-form').hide();
        }
        else{
          $('.discount-div .error-msg').text(data.text).show();
        }
        $('#point').text(numberFormat(data.use_point));
        $('#order-confirmation .content').css('opacity', '1').css('pointer-events', 'inherit');

        console.log(data);
      });
    }else{
      if(lang == 'th') err_txt = 'กรุณากรอกรหัส';
      else err_txt = 'Please enter the code';
      $('.discount-div .error-msg').text(err_txt).show();
    }
  });

  // -------------- Delete Code Button
  $('.discount-div .btn-danger').on('click', function(e){
    e.preventDefault();
    clearAll();
    $('input[name=select_discount]:checked').prop("checked", false);
  });

  // -------------- Point BTN
  $('.select-point').on('change', function(e){
    $('#order-confirmation .content').css('opacity', '0.5').css('pointer-events', 'none');
    grand_total = $('.grand-total').text().trim();
    total_price = $('#total-price').text().trim();

    if($(this).is(':checked')){
      $.get(path+'/discount/use-point', { grand_total:grand_total }, function(data){
        if (data.status == '1'){
          $(".point").show();
          $("#point").text(numberFormat(data.use_point));
          $(".grand-total").text(numberFormat(data.grand_total));
        }else{
          $(".point").hide();
          $("#point").text('');
        }
        console.log(data);
        $('#order-confirmation .content').css('opacity', '1').css('pointer-events', 'inherit');
      });
    }else{
      shipping_price = $('#shipping-price').text().trim();
      discount_price = $('#discount-price').text().trim();
      // clear point
      url = path+'/discount/un-use-point';
      $.get(url, { total_price:total_price, discount_price:discount_price, shipping_price:shipping_price }, function(data){
        $(".grand-total").text(numberFormat(data.grand_total));
        $(".point").hide();
        $("#point").text('');
        $('#order-confirmation .content').css('opacity', '1').css('pointer-events', 'inherit');
        console.log(data);
      });
    }

  });

  // -------------- Check Out BYN
  $('#finish-checkout').on('click',function(){
    $('#order-confirmation .content').css('opacity', '0.5').css('pointer-events', 'none');
    $('#confirm-form').submit();
  });
});
